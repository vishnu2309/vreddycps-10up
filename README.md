# CPS WordPress theme and plugins

This repository contains the WordPress theme and plugins for the [CPS](https://cps.northeastern.edu) website. Its content is intended to be deployed to wp-content folder in the root folder of the domain.

## Dependencies and assets build script

The project uses [composer](https://getcomposer.org/) and [npm](https://www.npmjs.com/) to manage its dependencies, stylesheet and javascript file concatenation and to run linting tests. Stylesheets use the [PostCSS](https://postcss.org/) preprocessor. [Webpack](https://webpack.js.org/) is used to bundle assets.

### Install project dependencies

`composer install`

### In the cps theme folder, install dependencies
`cd themes && composer install && npm install`

### Run theme build task

`npm run build`

A watch task is available for local development

`npm run watch`


## Folder structure

- **/**    
Next to various configuration files, the root folder contains WordPress template files for rendering single posts, archive pages and the default page template, the footer etcetera. Template files may use partials found in the `/partials` folder.

- **/assets**    
Contains the source JavaScript files, stylesheets and images, separated into subfolders for use in the admin, the front end or shared between the two. The files are preprocessed and concatenated when running `npm run build` or `npm run watch`. The concatenated files are placed in the `/dist` folder.

- **/config**     
[Webpack](https://webpack.js.org/) configuration files for the assets build tasks.

- **/dist** (not part of the repository)    
This folder is created by the build task and will contain the scripts, styles and images used in the theme.

- **/includes**    
Contains all theme functions and classes. All functionality is loaded via the `functions.php` file in the theme root folder. Custom post types, taxonomies and meta boxes are defined in the `includes/content` folder.

- **/languages**    
Theme translation files. All strings are being output using the WordPress translation functions. The theme is currently not translated.

- **/page-templates**    
Holds custom page templates.

- **/partials**    
Contains items used by various templates like items rendered in lists, widgets and templates used to output the Gutenberg blocks.

## Gutenberg Blocks

WordPress block development guide: [Block Editor Handbook](https://developer.wordpress.org/block-editor/)

The custom blocks used by the theme are created, initialized and rendered in three locations. Note that all of the theme's blocks are [dynamic blocks](https://developer.wordpress.org/block-editor/tutorials/block-tutorial/creating-dynamic-blocks/).

- **Server-side block registration in PHP**   
All blocks are registered in the `includes/blocks` folder. Each block is defined via a class which contains a `render()` function that renders the block ouput in the theme. Blocks might be limited to a specific post type via its 'post_type' attribute.

- **Templates for rendering blocks in the theme**   
All blocks are dynamic and as such their ouput is rendered server-side using PHP templates. Templates for all blocks are located in the `/partials` folder in the root of the theme and are prefixed with `block-....`

- **Block registration for the admin**   
The scripts and styles that define blocks in the admin editor are located in the `assets/js/admin/` folder.
    - **/blocks**    
    Block definitions.
    - **/components**    
    Shared components that may be used in one or more blocks.
    - **/filters**   
    [Filters](https://developer.wordpress.org/block-editor/developers/filters/block-filters/) that modify behaviour of existing blocks.
    - **/styles**    
    Registers and unregisters various styles for blocks.    


## Installing plugins

The [WordPress Packagist](https://wpackagist.org/) repository is used to add plugins as dependencies. New plugins should be added to this repository via the composer.json file.


## Coding Standards

The theme follows the [WordPress Coding Standards](https://codex.wordpress.org/WordPress_Coding_Standards), with an additional [10up coding standards](https://github.com/10up/phpcs-composer) configuration added via the composer file. A pre-commit hook is in place that checks and enforces adherence to coding standards.


