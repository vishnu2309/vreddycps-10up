<?php
/**
 * Plugin Name: MU Plugin Loader
 * Description: Must-use plugin loader for the CPS site. Loads the 10up Experience, Disable Comments, Gutenberg and Yoast SEO plugins.
 * Version:     1.0
 * Author:      10up
 * Author URI:  https://10up.com
 * License:     GPLv2 or later
 * Domain Path: /languages/
 */

// When updating this array, also update the list of plugins in the plugin description.
$plugins = array(
	'/10up-experience/10up-experience.php',
	'/breadcrumb-trail/breadcrumb-trail.php',
	'/disable-comments-mu/disable-comments-mu.php',
	'/gutenberg/gutenberg.php',
	'/nu-fim/plugin.php',
	'/nu-loader/nu-loader.php',
	'/nu-pim/plugin.php',
	'/wordpress-fieldmanager/fieldmanager.php',
	'/wordpress-seo-premium/wp-seo-premium.php',
	'/cps-migration/cps-migration.php',
	'/wpforms/wpforms.php',
	'/wpforms-post-submissions/wpforms-post-submissions.php',
	'/wpcom-legacy-redirector/wpcom-legacy-redirector.php',
);

foreach ( $plugins as $plugin ) {
	if ( file_exists( WPMU_PLUGIN_DIR . $plugin ) ) {
		require WPMU_PLUGIN_DIR . $plugin;
	}
}
