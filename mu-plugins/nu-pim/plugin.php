<?php
/**
 * Plugin Name: NU PIM
 * Plugin URI:  https://northeastern.edu/
 * Description: Registers a WP CLI command to import data from the PIM API.
 * Version:     1.0.0
 * Author:      10up
 * Author URI:  https://10up.com
 * Text Domain: nu-pim
 * Domain Path: /languages
 *
 * @package NU_PIM
 */

namespace NU_PIM;

// Useful global constants.
if ( ! defined( 'NU_PIM_VERSION' ) ) {
	define( 'NU_PIM_VERSION', '1.0.0' );
}

if ( ! defined( 'NU_PIM_URL' ) ) {
	define( 'NU_PIM_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'NU_PIM_PATH' ) ) {
	define( 'NU_PIM_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'NU_PIM_INC' ) ) {
	define( 'NU_PIM_INC', NU_PIM_PATH . 'includes/' );
}

// Include helper functions.
require_once NU_PIM_INC . 'helpers.php';
require_once NU_PIM_INC . 'scheduler.php';

// Include taxonomies.
require_once NU_PIM_INC . 'taxonomies/area-of-study.php';
require_once NU_PIM_INC . 'taxonomies/location.php';
require_once NU_PIM_INC . 'taxonomies/program-level.php';

// Run the setup functions.
Taxonomy\AreaOfStudy\setup();
Taxonomy\Location\setup();
Taxonomy\ProgramLevel\setup();
Scheduler\setup();

// Include debug bar functions.
require_once NU_PIM_INC . 'debug-bar/core.php';

// Include files.
require_once NU_PIM_INC . 'wp-cli/core.php';

// Bootstrap.
CLI\setup();
