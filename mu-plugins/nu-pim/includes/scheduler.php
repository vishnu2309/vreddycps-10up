<?php
/**
 * Cron Event Scheduler functions
 *
 * @package NU_PIM
 */

namespace NU_PIM\Scheduler;

/**
 * Setup scheduled events.
 *
 * @since 1.0.0
 */
function setup() {

	add_action( 'wp_loaded', __NAMESPACE__ . '\setup_scheduler_actions' );
	add_action( 'nu_pim_process_import_cron', __NAMESPACE__ . '\process_pim_import' );
}

/**
 * Cron job scheduler/
 *
 * @return void
 */
function setup_scheduler_actions() {

	$daily_local_time_00 = get_date_from_gmt( '00:00:00' );

	if ( is_admin() && ! defined( 'DOING_AJAX' ) && ! wp_next_scheduled( 'nu_pim_process_import_cron' ) ) {
		wp_schedule_event( strtotime( $daily_local_time_00 ), 'daily', 'nu_pim_process_import_cron' );
	}
}

/**
 * Process pim import cron event.
 */
function process_pim_import() {

	require_once NU_PIM_INC . 'class-import.php';

	/*
	 * Default college id is 121 (college-id for CPS for Program API),
	 * so we don't have to pass it explicitly.
	 */
	new \NU_PIM\Import();
}
