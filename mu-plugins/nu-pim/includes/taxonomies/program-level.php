<?php
/**
 * "Program Level" custom taxonomy.
 *
 * The "Program Level" taxonomy is used to categorize programs.
 *
 * @package NU_PIM
 */

namespace NU_PIM\Taxonomy\ProgramLevel;

use \NU_PIM\Helpers as Helpers;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from program level meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Program Levels', 'nu-pim' ),
		'singular_name'              => esc_html__( 'Program Level', 'nu-pim' ),
		'search_items'               => esc_html__( 'Search Program Levels', 'nu-pim' ),
		'popular_items'              => esc_html__( 'Program Levels', 'nu-pim' ),
		'all_items'                  => esc_html__( 'All Program Levels', 'nu-pim' ),
		'parent_item'                => esc_html__( 'Parent Program Level', 'nu-pim' ),
		'parent_item_colon'          => esc_html__( 'Parent Program Level:', 'nu-pim' ),
		'edit_item'                  => esc_html__( 'Edit Program Level', 'nu-pim' ),
		'view_item'                  => esc_html__( 'View Program Level', 'nu-pim' ),
		'update_item'                => esc_html__( 'Update Program Level', 'nu-pim' ),
		'add_new_item'               => esc_html__( 'Add New Program Level', 'nu-pim' ),
		'new_item_name'              => esc_html__( 'New Program Level', 'nu-pim' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'nu-pim' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove program levels', 'nu-pim' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used program levels', 'nu-pim' ),
		'not_found'                  => esc_html__( 'No program levels found', 'nu-pim' ),
		'no_terms'                   => esc_html__( 'No program levels', 'nu-pim' ),
		'items_list_navigation'      => esc_html__( 'Program Levels', 'nu-pim' ),
		'items_list'                 => esc_html__( 'Program Levels', 'nu-pim' ),
		'most_used'                  => esc_html__( 'Most Used', 'nu-pim' ),
		'back_to_items'              => esc_html__( 'Back to program levels', 'nu-pim' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'The program level is used to categorize programs.', 'nu-pim' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => true,
		'rest_base'          => 'program-level',
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => true,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'program-level',
	);

	/*
	 * Assigning 'thisisnotused' to disable managing terms via the admin,
	 * but keeping it accessible via JSON requests for the post editor.
	 */
	if ( is_admin() ) {

		$args['capabilities'] = array(
			'manage_terms' => 'thisisnotused',
			'edit_terms'   => 'thisisnotused',
			'delete_terms' => 'thisisnotused',
			'assign_terms' => 'thisisnotused',
		);
	}

	/**
	 * Fires before registering the program level taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @var array $args Array of arguments for registering the taxonomy/
	 */
	$args = apply_filters( 'nu_pim_program_level_args', $args );

	register_taxonomy(
		'nu_pim_degree_type',
		Helpers\get_program_post_type(),
		$args
	);
}

/**
 * Hide parent select from program level meta box.
 *
 * The nu_pim_program_level is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();
	$post_type      = Helpers\get_program_post_type();

	if ( 'post' === $current_screen->base && $post_type === $current_screen->post_type ) {
		echo '<style>#newnu_pim_program_level_parent{display:none;}</style>';
	}
}
