<?php
/**
 * "Location" custom taxonomy.
 *
 * The "Location" taxonomy is used to categorize programs.
 *
 * @package NU_PIM
 */

namespace NU_PIM\Taxonomy\Location;

use \NU_PIM\Helpers as Helpers;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from location meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Locations', 'nu-pim' ),
		'singular_name'              => esc_html__( 'Location', 'nu-pim' ),
		'search_items'               => esc_html__( 'Search Locations', 'nu-pim' ),
		'popular_items'              => esc_html__( 'Locations', 'nu-pim' ),
		'all_items'                  => esc_html__( 'All Locations', 'nu-pim' ),
		'parent_item'                => esc_html__( 'Parent Location', 'nu-pim' ),
		'parent_item_colon'          => esc_html__( 'Parent Location:', 'nu-pim' ),
		'edit_item'                  => esc_html__( 'Edit Location', 'nu-pim' ),
		'view_item'                  => esc_html__( 'View Location', 'nu-pim' ),
		'update_item'                => esc_html__( 'Update Location', 'nu-pim' ),
		'add_new_item'               => esc_html__( 'Add New Location', 'nu-pim' ),
		'new_item_name'              => esc_html__( 'New Location', 'nu-pim' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'nu-pim' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove locations', 'nu-pim' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used locations', 'nu-pim' ),
		'not_found'                  => esc_html__( 'No locations found', 'nu-pim' ),
		'no_terms'                   => esc_html__( 'No locations', 'nu-pim' ),
		'items_list_navigation'      => esc_html__( 'Locations', 'nu-pim' ),
		'items_list'                 => esc_html__( 'Locations', 'nu-pim' ),
		'most_used'                  => esc_html__( 'Most Used', 'nu-pim' ),
		'back_to_items'              => esc_html__( 'Back to locations', 'nu-pim' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'The location is used to categorize programs.', 'nu-pim' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => true,
		'rest_base'          => 'location',
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => true,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'location',
	);

	/*
	 * Assigning 'thisisnotused' to disable managing terms via the admin,
	 * but keeping it accessible via JSON requests for the post editor.
	 */
	if ( is_admin() ) {

		$args['capabilities'] = array(
			'manage_terms' => 'thisisnotused',
			'edit_terms'   => 'thisisnotused',
			'delete_terms' => 'thisisnotused',
			'assign_terms' => 'thisisnotused',
		);
	}

	/**
	 * Fires before registering the location taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @var array $args Array of arguments for registering the taxonomy/
	 */
	$args = apply_filters( 'nu_pim_location_args', $args );

	register_taxonomy(
		'nu_pim_location',
		Helpers\get_program_post_type(),
		$args
	);
}

/**
 * Hide parent select from location meta box.
 *
 * The nu_pim_location is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();
	$post_type      = Helpers\get_program_post_type();

	if ( 'post' === $current_screen->base && $post_type === $current_screen->post_type ) {
		echo '<style>#newnu_pim_location_parent{display:none;}</style>';
	}
}
