<?php
/**
 * Adds a panel for the Debug Bar plugin menu.
 *
 * @see https://wordpress.org/plugins/debug-bar/
 *
 * @package NU_PIM
 */

namespace NU_PIM\DebugBar;

// Register debug bar panel.
add_filter( 'debug_bar_panels', __NAMESPACE__ . '\add_debug_bar_panel' );

/**
 * Register debug bar panel.
 *
 * @param array $panels The list of registered panels.
 * @return array
 */
function add_debug_bar_panel( $panels ) {

	require_once NU_PIM_INC . 'debug-bar/class-panel.php';

	$panels[] = new \NU_PIM\DebugBar\Panel();

	return $panels;
}
