<?php
/**
 * Define Debug Bar panel.
 *
 * @package NU_PIM
 */

namespace NU_PIM\DebugBar;

class Panel extends \Debug_Bar_Panel {

	/**
	 * Faculty post type.
	 */
	public $program_post_type;

	/**
	 * Post ID.
	 */
	public $program_post_id;

	/**
	 * Panel menu title.
	 */
	public $title;

	/**
	 * Initialize debug bar panel.
	 */
	public function init() {

		$this->program_post_type = \NU_PIM\Helpers\get_program_post_type();

		if ( empty( $this->program_post_type ) ) {
			return;
		}

		$this->title( 'PIM Data' );
	}

	/**
	 * Show Debug Bar menu item.
	 */
	public function prerender() {

		$this->set_visible( true );
	}

	/**
	 * Render panel content.
	 */
	public function render() {

		$program_post_id = $this->get_post_id();

		if ( ! $program_post_id ) {

			echo 'No PIM API data found for this page.';
			return;
		}

		$nuid = get_post_meta( $program_post_id, 'nu_pim_id', true );

		if ( empty( $nuid ) ) {

			echo 'Data only available for programs imported via the PIM API.';
			return;
		}

		ob_start();

		include NU_PIM_INC . 'debug-bar/table.php';

		$html = ob_get_contents();

		ob_end_clean();

		echo $html;
	}

	/**
	 * Get post ID if we are on a faculty post type edit page or front end page.
	 *
	 * @return int|false Post ID or false.
	 */
	protected function get_post_id() {

		$post_id = false;

		if ( is_admin() ) {

			$current_screen = get_current_screen();

			if ( $this->program_post_type === $current_screen->id && 'post' === $current_screen->base ) {

				if ( isset( $_GET['post'] ) ) {
					$post_id = absint( $_GET['post'] );
				}
			}

		} else {

			if ( is_singular( $this->program_post_type ) ) {
				$post_id = get_the_ID();
			}
		}

		return $post_id;
	}
}
