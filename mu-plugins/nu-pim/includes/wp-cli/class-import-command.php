<?php // phpcs:ignore WordPress.Files.FileName.InvalidClassFileName
/**
 * Import programs command
 *
 * @package NU_PIM
 */

namespace NU_PIM\CLI;

/**
 * Command for importing programs.
 *
 * @package NU_PIM\CLI
 */
class ImportCommand extends \NU_PIM\CLI\AbstractCommand {

	/**
	 * Import program data from the PIM API.
	 *
	 * ## OPTIONS
	 *
	 * [--college]
	 * : College ID. See: https://pim.northeastern.edu/api-docs
	 *
	 * [--pid]
	 * : Program ID. Set this flag to import a single program.
	 *
	 * [--dry-run]
	 * : Run an import but do not save changes to the database.
	 *
	 * [--force]
	 * : Update existing posts even if the 'update' field matches.
	 *
	 * [--setup]
	 * : Delete all existing posts before importing. Intended to be used during development only.
	 *
	 * [--yes]
	 * : Answer yes to the confirmation messages.
	 *
	 * [--debug]
	 * : Optional. Show debug messages in the console.
	 *
	 * ## EXAMPLES
	 *
	 *     $ wp pim import --college=121
	 *     $ wp pim import --pid=1234
	 *
	 * @param  array $args       Positional command arguments.
	 * @param  array $assoc_args Associative arguments.
	 *
	 * @return void
	 */
	public function __invoke( array $args = array(), array $assoc_args = array() ) {

		$config = \WP_CLI::get_config();

		$college = \WP_CLI\Utils\get_flag_value( $assoc_args, 'college', 121 ); // CPS = 121
		$pid     = \WP_CLI\Utils\get_flag_value( $assoc_args, 'pid', 0 );
		$force   = \WP_CLI\Utils\get_flag_value( $assoc_args, 'force', false );
		$dry_run = \WP_CLI\Utils\get_flag_value( $assoc_args, 'dry-run', false );
		$quiet   = isset( $config['quiet'] ) && $config['quiet'] ? true : false;
		$setup   = \WP_CLI\Utils\get_flag_value( $assoc_args, 'setup', false );
		$yes     = \WP_CLI\Utils\get_flag_value( $assoc_args, 'yes', false );

		$this->start_bulk_operation();

		require_once NU_PIM_INC . 'class-import.php';

		$args = [
			'college' => absint( $college ),
			'pid'     => absint( $pid ),
			'force'   => $force,
			'dry_run' => $dry_run,
			'quiet'   => $quiet,
			'setup'   => $setup,
			'yes'     => $yes,
			'is_cli'  => true,
		];

		new \NU_PIM\Import( $args );

		$this->end_bulk_operation();
	}
}

\WP_CLI::add_command( 'pim import', __NAMESPACE__ . '\ImportCommand' );
