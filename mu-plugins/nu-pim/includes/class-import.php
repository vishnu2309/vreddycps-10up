<?php
/**
 * Contains the programs import class
 *
 * @package NU_PIM
 */

namespace NU_PIM;

use function NU_PIM\Helpers\get_program_post_type;
use function NU_PIM\Helpers\get_whitelisted_fields;
use function NU_PIM\Helpers\log;
use function NU_PIM\Helpers\stop_the_insanity;

/**
 * Class containing the programs import functionality.
 *
 * @package NU_PIM
 */
class Import {

	/**
	 * College ID.
	 *
	 * To import programs from a particular college.
	 *
	 * @var int
	 */
	private $college;

	/**
	 * Program ID. If this is set, only single program will be imported.
	 *
	 * @var int
	 */
	private $pid;

	/**
	 * Flag for updating existing posts even if the 'update' field has not changed.
	 *
	 * @var bool
	 */
	private $force;

	/**
	 * Are we performing a dry run.
	 *
	 * @var bool
	 */
	private $dry_run;

	/**
	 * To hide the logging.
	 *
	 * @var bool
	 */
	private $quiet;

	/**
	 * To delete all posts and re-import form scratch.
	 *
	 * @var bool
	 */
	private $setup;

	/**
	 * To bypass the confirmation.
	 *
	 * @var bool
	 */
	private $yes;

	/**
	 * The post type used for programs.
	 *
	 * @var string
	 */
	private $post_type;

	/**
	 * If we are running a CLI command, the command instance.
	 *
	 * @var bool
	 */
	private $is_cli;

	/**
	 * Holds the WP CLI progress bar.
	 *
	 * @var \WP_CLI\Progress\Bar
	 */
	private $progress_bar = null;

	/**
	 * Prefix for meta keys.
	 *
	 * @var array
	 */
	private $prefix = 'nu_pim_';

	/**
	 * Parent term id for graduate terms.
	 *
	 * @var int
	 */
	private $graduate_term_id;

	/**
	 * Parent term id for undergraduate terms.
	 *
	 * @var int
	 */
	private $undergraduate_term_id;

	/**
	 * Import constructor.
	 *
	 * @param array $args Import arguments.
	 */
	public function __construct( $args = [] ) {

		$default_college_id = 121; // CPS college-id for Programs API.

		$default_args = [
			'college' => $default_college_id,
			'pid'     => 0,
			'force'   => false,
			'dry_run' => false,
			'quiet'   => false,
			'setup'   => false,
			'yes'     => false,
			'is_cli'  => false,
		];

		$args = wp_parse_args( $args, $default_args );

		/*
		 * Note: checking for defined( '\WP_CLI' ) && \WP_CLI; returns true if the class is used
		 *       outside of the WP CLI command context, but ran via a cron job.
		 */
		$this->is_cli = isset( $args['is_cli'] ) && $args['is_cli'] && defined( '\WP_CLI' ) && \WP_CLI ? true : false;

		$this->post_type = get_program_post_type();

		if ( ! post_type_exists( $this->post_type ) ) {

			$message = sprintf( 'The %1$s post-type is not registered.', $this->post_type );

			log( $message, $this->is_cli, true );

			return;
		}

		$this->college = isset( $args['college'] ) ? filter_var( $args['college'], FILTER_VALIDATE_INT ) : $default_college_id;
		$this->pid     = isset( $args['pid'] ) ? filter_var( $args['pid'], FILTER_VALIDATE_INT ) : 0;
		$this->force   = isset( $args['force'] ) ? filter_var( $args['force'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->dry_run = isset( $args['dry_run'] ) ? filter_var( $args['dry_run'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->quiet   = isset( $args['quiet'] ) ? filter_var( $args['quiet'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->setup   = isset( $args['setup'] ) ? filter_var( $args['setup'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->yes     = isset( $args['yes'] ) ? filter_var( $args['yes'], FILTER_VALIDATE_BOOLEAN ) : false;

		if ( $this->is_cli ) {

			if ( $this->setup && $this->quiet ) {
				\WP_CLI::error( 'Cannot run import script in quiet mode with "setup" set to true' );
			}

			if ( ! $this->yes ) {

				$message = 'Running import script in live mode';

				if ( $this->setup ) {
					$message = $message . ' with "setup" flag. All existing programs will be removed before running the import script';
				}

				\WP_CLI::confirm( $message . '. Are you sure?' );
			}
		}

		$this->graduate_term_id      = $this->get_parent_level_term_id( 'graduate' );
		$this->undergraduate_term_id = $this->get_parent_level_term_id( 'undergraduate' );

		$result = $this->import();

		if ( is_wp_error( $result ) ) {
			log( $result->get_error_message(), $this->is_cli, true );
		} else {
			log( 'Program import complete.', $this->is_cli );
		}
	}

	/**
	 * Get top level "Graduate" or "Undergraduate" term.
	 * Creates the top level term if it does not already exist.
	 *
	 * @param string $slug Valid options are 'graduate' or 'undergraduate'.
	 *
	 * @return int|\WP_Error Term ID or WP_Error object.
	 */
	protected function get_parent_level_term_id( $slug ) {

		if ( ! in_array( $slug, [ 'graduate', 'undergraduate' ], true ) ) {
			return new \WP_Error( 'pim_import_incorrect_parent_slug', "Valid options are 'graduate' or 'undergraduate'" );
		}

		$args = array(
			'parent'     => 0,
			'hide_empty' => false,
			'slug'       => $slug,
		);

		$terms = get_terms( 'nu_pim_degree_type', $args );

		if ( empty( $terms ) ) {

			$term = wp_insert_term( ucwords( $slug ), 'nu_pim_degree_type' );

			if ( is_wp_error( $term ) ) {
				return new \WP_Error( 'pim_import_cannot_insert_parent', $term->get_error_message() );
			} else {
				return $term->term_id;
			}
		} else {
			return $terms[0]->term_id;
		}
	}

	/**
	 * Import program data.
	 *
	 * @return true|\WP_Error Returns true on successful import, else a \WP_Error object.
	 */
	public function import() {

		/*
		 * Important: The setup flag is only intended for development, program on the live site will contain
		 *            additional data added manually by site editors.
		 */
		if ( $this->setup && ! $this->pid ) {
			$this->delete_all_posts();
		}

		log( 'Fetching data from the API...', $this->is_cli );

		if ( $this->pid ) {

			$response = $this->get( 0, $this->college, $this->pid );
			$response = $this->validate_response( $response );

		} else {

			$page             = 0;
			$total_pages      = 0;
			$response         = array();
			$response['data'] = array();

			do {

				$per_page_response = $this->get( $page, $this->college );
				$per_page_response = $this->validate_response( $per_page_response );

				if ( is_wp_error( $per_page_response ) ) {

					$response = $per_page_response;

					break;
				}

				if ( $total_pages <= 0 && ! empty( $per_page_response['data']['pages']['page_end'] ) ) {
					$total_pages = absint( $per_page_response['data']['pages']['page_end'] );
				}

				$response['data'] = array_merge( $response['data'], $per_page_response['data']['entities'] );

				$page++;

				sleep( 3 );

				if ( $this->is_cli ) {
					stop_the_insanity();
				}
			} while ( $page <= $total_pages );
		}

		if ( is_wp_error( $response ) ) {
			return $response;
		} else {

			log( 'Remote data fetched successfully.', $this->is_cli );

			$errors       = array();
			$import_count = count( $response['data'] );

			// Set pid as a key to each item.
			$pids          = array_column( $response['data'], 'id' );
			$response_data = array_combine( $pids, $response['data'] );

			/*
			 * Delete posts if we don't get some posts in the API response,
			 * and those posts are still present in the system.
			 */
			if ( ! $this->setup && ! $this->pid ) {

				$local_posts = $this->get_local_posts();

				$this->delete_posts_by_id( array_diff_key( $local_posts, $response_data ) );

				$response_data = array_diff_key( $response_data, $local_posts );
			}

			$message = sprintf( 'Importing %1$d posts:', $import_count );

			log( $message );

			if ( $this->is_cli && ! $this->quiet ) {
				$this->progress_bar = \WP_CLI\Utils\make_progress_bar( $message, $import_count );
			}

			$iteration = 0;

			foreach ( $response_data as $program_data ) {

				if ( ! $this->dry_run ) {

					$post_id = $this->upsert_post( $program_data, $this->force );

					if ( is_wp_error( $post_id ) ) {
						$errors[] = $post_id->get_error_message();
					}
				}

				if ( $this->is_cli && ! $this->quiet ) {
					$this->progress_bar->tick();
				}

				if ( 0 === $iteration % 100 ) {

					sleep( 3 );

					if ( $this->is_cli ) {
						stop_the_insanity();
					}
				}
			}

			if ( $this->is_cli && ! $this->quiet ) {
				$this->progress_bar->finish();
			}

			if ( ! empty( $errors ) ) {

				foreach ( $errors as $error ) {
					log( $error, $this->is_cli );
				}
			}
		}

		return true;
	}

	/**
	 * Delete all existing posts from the database.
	 *
	 * This function is intended to be used for development, not for use on the live site.
	 *
	 * Important: Only posts with that have a nu_pim_pid meta field will be deleted.
	 *            It is expected that any posts added manually via other means than
	 *            importing from the API do not have this field set.
	 */
	private function delete_all_posts() {

		global $wpdb;

		$t1 = $wpdb->posts;
		$t2 = $wpdb->postmeta;

		$querystr = $wpdb->prepare(
			"SELECT COUNT( * ) FROM {$t1} INNER JOIN {$t2} ON ( {$t1}.ID = {$t2}.post_id ) WHERE 1=1 AND ( {$t2}.meta_key = %s ) AND $t1.post_type = %s", // phpcs:ignore WordPress.DB.PreparedSQL.InterpolatedNotPrepared
			"{$this->prefix}id",
			$this->post_type
		);

		$count = $wpdb->get_var( $querystr ); // phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared

		if ( empty( $count ) ) {

			log( 'Setup mode: No existing posts found.', $this->is_cli );

			return;
		}

		$querystr = $querystr . " AND post_status IN ('future','publish','private','draft')";

		$count_message = $wpdb->get_var( $querystr ); // phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
		$message       = $this->dry_run ? 'Dry run: ' : 'Setup mode: ';
		$message      .= empty( $count_message ) ? 'Removing existing posts:' : 'Removing ' . $count_message . ' existing posts:';

		log( $message );

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar = \WP_CLI\Utils\make_progress_bar( $message, $count );
		}

		$post_stati   = get_post_stati( array( 'exclude_from_search' => false ) );
		$post_stati[] = 'auto-draft';

		$args = array(
			'post_type'              => $this->post_type,
			'post_status'            => $post_stati,
			'posts_per_page'         => 100,
			'fields'                 => 'ids',
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'meta_query'             => array(
				array(
					'key'     => "{$this->prefix}id",
					'compare' => 'EXISTS',
				),
			),
		);

		$query = new \WP_Query( $args );

		while ( $query->have_posts() ) {

			$post_ids = $query->posts;

			foreach ( $post_ids as $post_id ) {

				if ( ! $this->dry_run ) {

					// phpcs:disable

					// Todo: Should remove attached thumbnail.
					// $post_thumbnail_id = get_post_thumbnail_id();

					// if ( ! empty( $post_thumbnail_id ) ) {
					// wp_delete_post( $post_thumbnail_id, true );
					// }

					// phpcs:enable

					wp_delete_post( $post_id, true );
				}

				if ( $this->is_cli && ! $this->quiet ) {
					$this->progress_bar->tick();
				}
			}

			sleep( 3 );

			if ( $this->is_cli ) {
				stop_the_insanity();
			}

			$query = new \WP_Query( $args );
		}

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar->finish();
		}
	}

	/**
	 * Fetch data from the programs API.
	 *
	 * When the $program_id parameter is set, a single program will be retrieved,
	 * else a paginated list of results.
	 *
	 * "data": {
	 *     "pages": {
	 *         "page_size": Total number of records for the current page,
	 *         "page_start": Starting page number,
	 *         "page_end": Ending page number,
	 *         "record_count": Total number of records overall
	 *     },
	 *     "entities": List of Program data
	 * },
	 * "error": "There was a server error, see the Drupal logs"
	 *
	 * @param int $page       Page parameter to page through results.
	 * @param int $college_id College ID, Default is 121 (CPS).
	 * @param int $program_id Program ID.
	 *
	 * @return \WP_Error|array The response body or WP_Error on failure.
	 */
	public function get( $page = 0, $college_id = 121, $program_id = null ) {

		$endpoint = 'https://pim.northeastern.edu/api/v2/';

		if ( empty( $program_id ) ) {

			$endpoint .= 'programs/';

			if ( absint( $page ) >= 0 ) {
				$endpoint .= absint( $page ) . '/';
			}

			if ( ! empty( $college_id ) ) {
				$endpoint .= absint( $college_id );
			}
		} else {
			$endpoint .= 'program/' . absint( $program_id );
		}

		$args = array(
			'timeout'   => 60,
			'sslverify' => false, // Disable checking if the SSL certificate is valid.
		);

		$response = wp_remote_get( $endpoint, $args );

		if ( ! is_wp_error( $response ) ) {

			if ( 200 !== (int) wp_remote_retrieve_response_code( $response ) ) {

				$response = new \WP_Error(
					'nu_pim_request_error',
					wp_remote_retrieve_body( $response )
				);
			} else {

				$response = json_decode( wp_remote_retrieve_body( $response ), true );

				if ( ! $response ) {

					$response = new \WP_Error(
						'nu_pim_request_error',
						'Could not parse API response data.'
					);
				}
			}
		}

		return $response;
	}

	/**
	 * Check response and validate if it has required data.
	 *
	 * @param array $response API Response.
	 *
	 * @return \WP_Error|array
	 */
	public function validate_response( $response ) {

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		if ( ! array_key_exists( 'data', $response ) || ! is_array( $response['data'] ) || empty( $response['data'] ) ) {

			return new \WP_Error(
				'nu_pim_import_data_empty',
				'No program data found in API response'
			);
		}

		return $response;
	}

	/**
	 * Get pid for all posts stored in the database.
	 *
	 * @return array Array where key is the pid and value is the post ID.
	 */
	protected function get_local_posts() {

		global $wpdb;

		$posts = [];

		$query     = "SELECT post_id, meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = %s";
		$results   = $wpdb->get_results( $wpdb->prepare( $query, "{$this->prefix}id" ) ); // phpcs:ignore
		$increment = 1;

		if ( ! empty( $results ) ) {

			foreach ( $results as $result ) {

				if ( empty( $result->meta_value ) ) {
					$result->meta_value = 'temp' . $increment;
				}

				$posts[ $result->meta_value ] = $result->post_id; // phpcs:ignore

				$increment++;
			}
		}

		return $posts;
	}

	/**
	 * Bulk delete posts by a list of IDs.
	 *
	 * @param array $post_ids List of post IDs.
	 */
	protected function delete_posts_by_id( $post_ids ) {

		if ( empty( $post_ids ) || ! is_array( $post_ids ) ) {
			return;
		}

		$count     = count( $post_ids );
		$iteration = 0;

		$message = sprintf( 'Deleting %1$d existing programs before import:', $count );

		log( $message );

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar = \WP_CLI\Utils\make_progress_bar( $message, $count );
		}

		foreach ( $post_ids as $post_id ) {

			if ( ! $this->dry_run ) {

				// phpcs:disable

				// TODO: Should remove attached thumbnail.
				// $post_thumbnail_id = get_post_thumbnail_id();

				// if ( ! empty( $post_thumbnail_id ) ) {
				// wp_delete_post( $post_thumbnail_id, true );
				// }

				// phpcs:enable

				wp_delete_post( $post_id, true );

				if ( 0 === $iteration % 20 ) {

					sleep( 3 );

					if ( $this->is_cli ) {
						stop_the_insanity();
					}
				}

				$iteration++;
			}

			if ( $this->is_cli && ! $this->quiet ) {
				$this->progress_bar->tick();
			}
		}

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar->finish();
		}
	}

	/**
	 * Insert or update a post.
	 *
	 * @param array $data  Data for a single Program as returned by the API.
	 * @param bool  $force Update existing post if the content of the 'updated' field
	 *                     is identical to the stored value. Default false.
	 *
	 * @return int|\WP_Error Post ID on success, else a WP_Error object.
	 */
	public function upsert_post( $data, $force = false ) {

		if ( empty( $data['id'] ) ) {

			return new \WP_Error(
				'nu_pim_field_missing',
				'The "id" field must be defined when updating a post.'
			);
		}

		$hash = md5( serialize( $data ) ); // phpcs:ignore WordPress.PHP.DiscouragedPHPFunctions.serialize_serialize

		$postdata = array(
			'post_title'     => '',
			'post_type'      => $this->post_type,
			'post_status'    => 'publish',
			'comment_status' => 'closed',
			'ping_status'    => 'closed',
			'tax_input'      => array(),
			'meta_input'     => array(
				"{$this->prefix}hash" => $hash,
			),
		);

		$post = Helpers\get_post_by_pid( $data['id'] );

		if ( is_a( $post, '\WP_Post' ) ) {

			$postdata['ID'] = $post->ID;

			/*
			 *  If we're not forcing an update and the hash key
			 *  matched the new hash key do nothing.
			 */
			if ( ! $force ) {

				$stored_hash = get_post_meta( $post->ID, "{$this->prefix}hash", true );

				if ( $stored_hash === $hash ) {
					return $post->ID;
				}
			}
		}

		if ( ! array_key_exists( 'title', $data ) || empty( $data['title'] ) ) {

			return new \WP_Error(
				'nu_pim_field_missing',
				'The "title" field cannot be empty when inserting/updating a post.'
			);
		}

		$postdata['post_title'] = $this->get_value( 'title', $data['title'] );

		$whitelisted_fields = get_whitelisted_fields();
		$term_fields        = [];

		foreach ( $data as $name => $value ) {

			if ( ! array_key_exists( $name, $whitelisted_fields ) ) {
				continue;
			}

			if ( 'meta' === $whitelisted_fields[ $name ]['context'] ) {

				/*
				 * Note: This approach only allows saving the value to a single meta key.
				 *       A future update might allow saving the content of a specific field
				 *       to multiple keys.
				 */
				$meta_value = $this->get_value( $name, $value );

				if ( ! is_wp_error( $meta_value ) && ! empty( $meta_value ) ) {

					$meta_key = $this->get_formatted_key( $name );

					$postdata['meta_input'][ $meta_key ] = $meta_value;
				}
			}

			if ( 'term' === $whitelisted_fields[ $name ]['context'] ) {

				if ( empty( $whitelisted_fields[ $name ]['taxonomy'] ) ) {
					$taxonomy = $this->get_formatted_key( $name );
				} else {
					$taxonomy = $whitelisted_fields[ $name ]['taxonomy'];
				}

				/*
				 * Terms in the 'field_degree_type' and 'field_undergrad_degree_type' are
				 * assigned to the same taxonomy later on.
				 */
				if ( empty( $term_fields[ $taxonomy ] ) ) {
					$term_fields[ $taxonomy ] = $this->get_value( $name, $value );
				} else {
					$term_fields[ $taxonomy ] = array_merge( $term_fields[ $taxonomy ], $this->get_value( $name, $value ) );
				}
			}
		}

		$post_id = wp_insert_post( $postdata );

		if ( is_wp_error( $post_id ) ) {
			return $post_id;
		} else {

			foreach ( $term_fields as $taxonomy => $terms ) {

				if ( empty( $terms ) ) {
					continue;
				}

				// The array of term IDs will be used to assign terms to the post.
				$term_ids = [];

				foreach ( $terms as $term ) {

					if ( ! is_array( $term ) || empty( $term['name'] ) ) {
						continue;
					}

					$term_meta = [];
					$term_name = $term['name'];

					if ( ! empty( $term['tid'] ) ) {
						$term_meta[ "{$this->prefix}tid" ] = $term['tid'];
					}

					$args = array(
						'name'       => $term_name,
						'hide_empty' => false,
					);

					// A little abracadabra to add undergrad terms to the proper parent term.
					switch ( $taxonomy ) {
						case 'nu_pim_degree_type':
							$args['parent'] = $this->graduate_term_id;
							break;
						case 'nu_pim_undergrad_degree_type':
							$taxonomy       = 'nu_pim_degree_type';
							$args['parent'] = $this->undergraduate_term_id;
							break;

					}

					$existing_terms = get_terms( $taxonomy, $args );

					// If the term does not exist we'll insert one.
					if ( empty( $existing_terms ) ) {

						// Create new term.
						$inserted_term = wp_insert_term( $term_name, $taxonomy, $args );

						// If an error occurs move to the next term.
						if ( is_wp_error( $inserted_term ) ) {

							$errors[] = $inserted_term->get_error_message();

							continue;
						}

						if ( empty( $inserted_term['term_id'] ) ) {

							$errors[] = "An unknown error occurred while inserting the '{$term_name}' term in the {$taxonomy} taxonomy.";

							continue;
						}

						$term_ids[] = $inserted_term['term_id'];

						if ( ! empty( $term_meta ) ) {

							foreach ( $term_meta as $meta_key => $meta_value ) {
								update_term_meta( $inserted_term['term_id'], $meta_key, $meta_value );
							}
						}
					} else {
						$term_ids[] = $existing_terms[0]->term_id;
					}
				}

				if ( ! empty( $term_ids ) ) {

					$term_ids = array_map( 'intval', $term_ids );

					$response = wp_set_object_terms( $post_id, $term_ids, $taxonomy, true );

					if ( is_wp_error( $response ) ) {
						$errors[] = $response->get_error_message();
					}
				}
			}

			// phpcs:disable

			// @todo If we want to import images into WordPress, we should do that here.
			// if ( ! empty( $image_url ) ) {

			// require_once( ABSPATH . 'wp-admin/includes/media.php' );
			// require_once( ABSPATH . 'wp-admin/includes/file.php' );
			// require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// $image_url = trim( $image_url );

			// Todo: check if image already exists before creating a new one.
			// $attachment_id = media_sideload_image( $image_url, $post_id, '', 'id' );

			// if ( is_wp_error( $attachment_id ) ) {
			// Do something.
			// } else {
			// update_post_meta( $attachment_id, $this->prefix . 'original_url', $image_url );
			// update_post_meta( $post_id, '_thumbnail_id', $attachment_id );
			// }
			// }

			// phpcs:enable
		}

		if ( ! empty( $errors ) ) {

			return new \WP_Error(
				'nu_pim_assign_term_error',
				join( ', ', $errors )
			);
		}

		return $post_id;
	}

	/**
	 * Get formatted value for a field.
	 *
	 * - If a callback exists for the fields in the whitelisted fields list, the
	 *   value will be passed to the callback.
	 * - If 'term' is the field type, the get_terms() method is applied.
	 * - Else, the value will be returned directly.
	 *
	 * @param string $field_name  Field key.
	 * @param mixed  $field_value Field data.
	 *
	 * @return mixed Formatted field value.
	 */
	private function get_value( $field_name, $field_value ) {

		$whitelisted_fields = get_whitelisted_fields();

		if ( ! array_key_exists( $field_name, $whitelisted_fields ) ) {

			return new \WP_Error(
				'nu_pim_not_whitelisted',
				"The '{$field_name}' field is not part of the whitelisted fields."
			);
		}

		// Do we have a callback function for getting this field's value?
		if ( isset( $whitelisted_fields[ $field_name ]['callback'] ) ) {

			if ( is_callable( [ $this, $whitelisted_fields[ $field_name ]['callback'] ] ) ) {
				return $this->{$whitelisted_fields[ $field_name ]['callback']}( $field_value );
			}

			return new \WP_Error(
				'nu_pim_not_callable',
				"The '{$whitelisted_fields[ $field_name ]['callback']}' function is not callable."
			);
		}

		if ( 'term' === $whitelisted_fields[ $field_name ]['context'] ) {
			return $this->get_formatted_terms( $field_value );
		}

		return $field_value;
	}

	/**
	 * Get formatted list of term data from a taxonomy field.
	 *
	 * @param array $data Field value.
	 *
	 * @return array Array containing 'name', 'description' and 'tid' fields for each term.
	 */
	private function get_formatted_terms( $data ) {

		$terms = array();

		if ( isset( $data['entities'] ) && is_array( $data['entities'] ) ) {

			foreach ( $data['entities'] as $entity ) {

				$term_array = array();

				if ( empty( $entity['name'] ) ) {
					continue;
				}

				$term_name = $this->get_value_key( $entity['name'] );

				if ( empty( $term_name ) ) {
					continue;
				}

				$term_array['name'] = $term_name;

				if ( ! empty( $entity['tid'] ) ) {
					$term_array['tid'] = $entity['tid'];
				}

				if ( ! empty( $entity['description'] ) ) {
					$term_array['description'] = $this->get_value_key( $entity['description'] );
				}

				$terms[] = $term_array;
			}
		}

		return $terms;
	}

	/**
	 * Get the "value" field from the data if it is present.
	 *
	 * @param array|string $data Field value.
	 *
	 * @return false|mixed Formatted field value, false if the 'value' field is not present.
	 */
	private function get_value_key( $data ) {

		if ( is_array( $data ) && isset( $data[0]['value'] ) ) {
			return $data[0]['value'];
		}

		if ( isset( $data['value'] ) ) {

			// If only one element found in the array, add it directly.
			if ( is_array( $data['value'] ) && 1 === count( $data['value'] ) ) {
				return $data['value'][0];
			}

			return $data['value'];
		}

		return false;
	}

	/**
	 * Generate meta key or taxonomy name from field key.
	 * Removes the "field_" prefix and adds $this->prefix.
	 *
	 * @param string $name Field name.
	 *
	 * @return string
	 */
	private function get_formatted_key( $name ) {

		if ( 'field_' === substr( $name, 0, 6 ) ) {
			$name = substr( $name, 6 );
		}

		return $this->prefix . $name;
	}

	/**
	 * Get the top level "entities" field from the data if it is present.
	 *
	 * @param array|string $data Field value.
	 *
	 * @return array List of entities.
	 */
	protected function get_entities_key( $data ) {

		if ( isset( $data['entities'] ) && is_array( $data['entities'] ) ) {
			return $data['entities'];
		}

		return array();
	}

	/**
	 * Get the program IDs from the get_program_family_ids field.
	 *
	 * For simplicity: in stead of saving all program data in this field,
	 * let's only store the related program IDs.
	 *
	 * Note: Data in this field is more complex than what we are representing here.
	 *       A program family can consist of multiple entities, each with its own
	 *       college and associated data.
	 *
	 * @param array $data Field value.
	 *
	 * @return array List of program ids (note: these are not WordPress post IDs).
	 */
	protected function get_program_family_ids( $data ) {

		$result = array();

		if ( ! isset( $data['entities'] ) || ! is_array( $data['entities'] ) ) {
			return $result;
		}

		foreach ( $data['entities'] as $entity ) {

			if ( ! isset( $entity['program'] ) || ! isset( $entity['program']['entities'] ) || ! is_array( $entity['program']['entities'] ) ) {
				return;
			}

			foreach ( $entity['program']['entities'] as $program ) {

				if ( ! empty( $program['id'] ) ) {
					$result[] = $program['id'];
				}
			}
		}

		return $result;
	}

	/**
	 * Get the ids for related programs from the 'field_get_related_program' and 'field_get_related_program_2' fields.
	 *
	 * @param array $data Field value.
	 *
	 * @return array List of program ids.
	 */
	protected function get_related_program_ids( $data ) {

		$result = array();

		if ( ! isset( $data['entities'] ) || ! is_array( $data['entities'] ) ) {
			return $result;
		}

		foreach ( $data['entities'] as $entity ) {

			if ( ! empty( $entity['id'] ) ) {
				$result[] = $entity['id'];
			}
		}

		return $result;
	}
}
