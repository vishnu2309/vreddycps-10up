<?php
/**
 * Helper functions
 *
 * @package NU_PIM
 */

namespace NU_PIM\Helpers;

/**
 * Get the name of the post type used for program.
 *
 * @return string
 * @since 1.0.0
 */
function get_program_post_type() {

	return apply_filters( 'nu_pim_post_type', 'program' );
}

/**
 * Get post by the program id custom field value.
 *
 * @since 1.0.0
 * @param int $id Program ID field value.
 * @return false|\WP_Post WP_Post object or false if the post cannot be found.
 */
function get_post_by_pid( $id ) {

	$args = array(
		'post_type'  => get_program_post_type(),
		'meta_key'   => 'nu_pim_id', // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
		'meta_value' => $id, // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_value
	);

	$query = new \WP_Query( $args );

	if ( ! $query->have_posts() ) {
		return false;
	}

	return $query->posts[0];
}

/**
 * Get a list of whitelisted fields.
 *
 * Note: Do not add or remove fields from this function. To blacklist fields,
 *       add a filter to 'nu_pim_whitelisted_fields'.
 *
 * @return array List of fields.
 * @since 1.0.0
 */
function get_whitelisted_fields() {

	// phpcs:disable Squiz.PHP.CommentedOutCode.Found

	$fields = array(

		/*
		 * Example:
		 * "17026"
		 */
		'id'                                  => array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'ID', 'nu-pim' ),
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "Bachelor of Science in Advanced Manufacturing Systems — Boston"
		 *     }
		 * ]
		 */
		'title'                               => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Title', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>Revolutionize your career with the&nbsp;Bachelor of Science in Advanced Manufacturing
		 *                  Systems (BS-AVMS) degree, co-developed with General Electric.&nbsp;This innovative
		 *                  bachelor’s degree completion program is offered in partnership with and on-ground at
		 *                  Springfield Technical Community College (STCC) in Springfield, MA.</p>\r\n",
		 *         "summary": "",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'body'                                => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Body', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>The Bachelor of Science in Finance and Accounting Management is&nbsp;accredited by the
		 *                  Association to Advance Collegiate Schools of Business
		 *                  (<a href=\"http://www.aacsb.edu/accreditation/\" target=\"_blank\">AACSB</a>).</p>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_accredidation'                 => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Accredidation', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "Advanced Manufacturing Systems"
		 *     }
		 * ]
		 */
		'field_formatted_title'               => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Formatted Title', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "target_id": "1841",
		 *         "alt": "Bachelor of Science in Advanced Manufacturing Systems — Boston",
		 *         "title": "",
		 *         "width": "100",
		 *         "height": "150",
		 *         "uri": "https://pim.northeastern.edu/sites/default/files/2017-11/AdvancedManufacturingSystemsThumbnail.jpg"
		 *     }
		 * ]
		 */
		'field_program_thumbnail'             => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Program Thumbnail', 'nu-pim' ),
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<ul>\r\n\t<li>Program co-developed with General Electric</li>\r\n\t<li>First-of-its kind
		 *                  program represents a new model of education where real world experience earns university
		 *                  credit</li>\r\n\t<li>An estimated 3.4 million manufacturing jobs to be created in the next
		 *                  decade, according to The Manufacturing Institute and Deloitte</li>\r\n\t<li>Curriculum
		 *                  organized around real workplace challenges and projects to provide hands-on learning
		 *                  experiences</li>\r\n\t<li>Program culminates with a comprehensive capstone project</li>\r\n
		 *                  </ul>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_unique_features'               => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Unique Features', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<ul>\r\n\t<li>Optional Supporting documents:\r\n\t<ul>\r\n\t\t<li>Statement of purpose
		 *                  </li>\r\n\t\t<li>Professional resumé</li>\r\n\t\t<li>Letter of recommendation</li>\r\n\t
		 *                  </ul>\r\n\t</li>\r\n</ul>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_key_requirements'              => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Key Requirements', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "a2j1a000000QFCIAA4"
		 *     }
		 * ]
		 */
		'field_sv_id'                         => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'SV ID', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "0"
		 *     }
		 * ]
		 */
		'field_visa_eligible'                 => array(
			'type'     => 'int',
			'context'  => 'meta',
			'label'    => esc_html__( 'Visa Eligible', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "120"
		 *     }
		 * ]
		 */
		'field_total_credit_hours'            => array(
			'type'     => 'int',
			'context'  => 'meta',
			'label'    => esc_html__( 'Total Credit Hours', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "http://www.aacsb.edu/accreditation/"
		 *     }
		 * ]
		 */
		'field_accreditation_link'            => array(
			'type'     => 'url',
			'context'  => 'meta',
			'label'    => esc_html__( 'Accredidation Link', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "type": "entity",
		 *     "entities": [
		 *         {
		 *             "id": "17884",
		 *             "title": [
		 *                 {
		 *                     "value": "Bach Boston Campus Programs Promo"
		 *                 }
		 *             ],
		 *             "body": [
		 *                 {
		 *                     "value": "<p>There's still time to join us for Fall term.
		 *                              <a href=\"https://app.applyyourself.com/AYApplicantLogin/fl_ApplicantLogin.asp?id=NEU-CPS#_ga=2.34613537.1806484157.1583960777-124964839.1565271416\">Apply now</a>
		 *                              and begin classes on September 8th!</p>\r\n",
		 *                     "summary": "",
		 *                     "format": "basic_html"
		 *                 }
		 *             ],
		 *             "field_display_title": [
		 *                 {
		 *                     "value": "Thinking about finishing your bachelor's degree?"
		 *                 }
		 *             ],
		 *             "field_link": [],
		 *             "field_marketo_form_url": [],
		 *             "field_show_title": [
		 *                 {
		 *                     "value": "1"
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ]
		 * }
		 */
		'field_sidebar'                       => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Sidebar', 'nu-pim' ),
			'callback' => 'get_entities_key',
		),

		/*
		 * Example:
		 * {
		 *     "type": "eck",
		 *     "entities": [
		 *         {
		 *             "field_degree_iped_code": [
		 *                 {
		 *                     "value": "5"
		 *                 }
		 *             ],
		 *             "field_level_code": [
		 *                 {
		 *                     "value": "UC"
		 *                 }
		 *             ]
		 *         }
		 *     ]
		 * }
		 */
		'field_banner_id_er'                  => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Banner ID Er', 'nu-pim' ),
			'callback' => 'get_entities_key',
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "id": "18119",
		 *             "title": [
		 *                 {
		 *                     "value": "Master of Arts in Strategic Intelligence & Analysis "
		 *                 }
		 *             ],
		 *             "field_catalog_year": [
		 *                 {
		 *                     "value": "2020"
		 *                 }
		 *             ],
		 *             "field_level_1_group": {
		 *                 "type": "eck",
		 *                 "entities": [
		 *                     {
		 *                         "title": [
		 *                             {
		 *                                 "value": "General Requirements "
		 *                             }
		 *                         ],
		 *                         "field_group_type": [
		 *                             {
		 *                                 "value": "core"
		 *                             }
		 *                         ],
		 *                         "field_group_level_2": {
		 *                             "type": "eck",
		 *                             "entities": [
		 *                                 {
		 *                                     "title": [
		 *                                         {
		 *                                             "value": "Master of Arts in Strategic Intelligence and Analysis
		 *                                                      General Requirements"
		 *                                         }
		 *                                     ],
		 *                                     "field_requirements": [
		 *                                         {
		 *                                             "value": "<h3>Required Courses&nbsp;</h3>\n\n<p>[course_table id=0]
		 *                                                      </p>\n\n<h3>Capstone</h3>\n\n<p>Complete one of the
		 *                                                      following:&nbsp;</p>\n\n<p>[course_table id=1]&nbsp;</p>
		 *                                                      \n\n<p>Please Note: Students who are interested in taking
		 *                                                      <a href=\"http://catalog.northeastern.edu/search/?search=SIA+7990\">SIA 7990</a>
		 *                                                      need to first take and pass
		 *                                                      <a href=\"http://catalog.northeastern.edu/search/?search=GST+6109\">GST 6109</a>
		 *                                                      Basic Field Research.</p>\n\n<h3>Electives</h3>\n\n<p>
		 *                                                      Complete 6-8 quarter hours from the following:&nbsp;</p>
		 *                                                      \n\n<p>[course_table id=2]&nbsp;</p>\n",
		 *                                             "format": "basic_html"
		 *                                         }
		 *                                     ],
		 *                                     "field_course_table": {
		 *                                         "type": "eck",
		 *                                         "entities": [
		 *                                             {
		 *                                                 "field_courses": {
		 *                                                     "type": "eck",
		 *                                                     "entities": [
		 *                                                         {
		 *                                                             "id": "6808",
		 *                                                             "title": [
		 *                                                                 {
		 *                                                                     "value": "SIA 6000 - Psychology of Intelligence Analysis"
		 *                                                                 }
		 *                                                             ],
		 *                                                             "field_course_code": [
		 *                                                                 {
		 *                                                                     "value": "SIA 6000"
		 *                                                                 }
		 *                                                             ],
		 *                                                             "field_credits": [
		 *                                                                 {
		 *                                                                     "value": "4.00"
		 *                                                                 }
		 *                                                             ],
		 *                                                             "field_credit_low": [
		 *                                                                 {
		 *                                                                     "value": "4.00"
		 *                                                                 }
		 *                                                             ],
		 *                                                             "field_credit_high": [],
		 *                                                             "field_credit_connector": [],
		 *                                                             "field_description": [
		 *                                                                 {
		 *                                                                     "value": "Offers an interdisciplinary viewpoint and approach to both
		 *                                                                              security and intelligence analysis through the use of case
		 *                                                                              studies as well as current research in psychology. Focuses on
		 *                                                                              four sections: our mental machinery, involving cognition perception
		 *                                                                              and memory; tools for thinking, which encompasses strategies for
		 *                                                                              analytical judgment, the need for more information, keeping an open
		 *                                                                              mind, structuring analytical problems, and analysis of competing
		 *                                                                              hypotheses; cognitive biases, including biases in evaluation of
		 *                                                                              evidence, perception of cause and effect, estimating probabilities,
		 *                                                                              and evaluation of intelligence reporting; and improving intelligence
		 *                                                                              analysis for homeland security and military applications.",
		 *                                                                     "format": null
		 *                                                                 }
		 *                                                             ]
		 *                                                         },
		 *                                                         ...,
		 *                                                         ...
		 *                                                     ]
		 *                                                 }
		 *                                             },
		 *                                             ...,
		 *                                             ...
		 *                                         ]
		 *                                     }
		 *                                 }
		 *                             ]
		 *                         }
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ]
		 *             }
		 *         }
		 *     ]
		 * }
		 */
		'field_curriculum'                    => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Curriculum', 'nu-pim' ),
			'callback' => 'get_entities_key', // Temporary. probably requires its own callback.
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "69",
		 *             "name": [
		 *                 {
		 *                     "value": "Online"
		 *                 }
		 *             ],
		 *             "description": [
		 *                 {
		 *                     "value": "",
		 *                     "format": null
		 *                 }
		 *             ]
		 *         }
		 *     ],
		 *     "nids": [
		 *         "69"
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_location'                      => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_pim_location',
			'label'    => esc_html__( 'Location', 'nu-pim' ),
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "3",
		 *             "name": [
		 *                 {
		 *                     "value": "Full-Time"
		 *                 }
		 *             ],
		 *             "description": [
		 *                 {
		 *                     "value": "",
		 *                     "format": null
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ],
		 *     "nids": [
		 *         "3",
		 *         ...,
		 *         ...
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_commitment'                    => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Commitment', 'nu-pim' ),
			'callback' => 'get_entities_key', // @todo Temporary. Should probably be saved as taxonomy terms.
		),

		/*
		 * Example:
		 * {
		 *     "entities": [],
		 *     "nids": [],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_align_program'                 => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Align Program', 'nu-pim' ),
			'callback' => 'get_entities_key', // @todo Temporary. Should probably be saved as taxonomy terms.
		),

		/*
		 * Example:
		 * {
		 *     "entities": [],
		 *     "nids": [],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_pan'                           => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'PAN', 'nu-pim' ),
			'callback' => 'get_entities_key', // @todo Temporary. Should probably be saved as taxonomy terms.
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "38",
		 *             "name": [
		 *                 {
		 *                     "value": "Master's"
		 *                 }
		 *             ],
		 *             "description": [
		 *                 {
		 *                     "value": "",
		 *                     "format": null
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ],
		 *     "nids": [
		 *         "38",
		 *         ...,
		 *         ...
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_degree_type'                   => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_pim_degree_type',
			'label'    => esc_html__( 'Degree Type', 'nu-pim' ),
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "74",
		 *             "name": [
		 *                 {
		 *                     "value": "Bachelor's"
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ],
		 *     "nids": [
		 *         "74",
		 *         ...,
		 *         ...
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_undergrad_degree_type'         => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_pim_undergrad_degree_type', // Note: this will be translated to nu_pim_degree_type.
			'label'    => esc_html__( 'Undergrad Degree Type', 'nu-pim' ),
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "35",
		 *             "name": [
		 *                 {
		 *                     "value": "Science & Mathematics"
		 *                 }
		 *             ],
		 *             "description": [
		 *                 {
		 *                     "value": "",
		 *                     "format": null
		 *                 }
		 *             ],
		 *             "field_slug": [
		 *                 {
		 *                     "value": "science-math"
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ],
		 *     "nids": [
		 *         "35",
		 *         ...,
		 *         ...
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_area_of_study'                 => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_pim_area_of_study',
			'label'    => esc_html__( 'Area of Study', 'nu-pim' ),
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "40",
		 *             "name": [
		 *                 {
		 *                     "value": "Classroom"
		 *                 }
		 *             ],
		 *             "description": [
		 *                 {
		 *                     "value": "",
		 *                     "format": null
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ],
		 *     "nids": [
		 *         "40",
		 *         ...,
		 *         ...
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_study_options'                 => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Study Options', 'nu-pim' ),
			'callback' => 'get_entities_key', // @todo Temporary. Should probably be saved as taxonomy terms.
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "tid": "77",
		 *             "name": [
		 *                 {
		 *                     "value": "Fall"
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ],
		 *     "nids": [
		 *         "77",
		 *         ...,
		 *         ...
		 *     ],
		 *     "type": "taxonomy"
		 * }
		 */
		'field_entry_terms'                   => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Entry Terms', 'nu-pim' ),
			'callback' => 'get_entities_key', // @todo Temporary. Should probably be saved as taxonomy terms.
		),

		/*
		 * Example:
		 * {
		 *     "type": "entity",
		 *     "entities": [
		 *         {
		 *             "title": [
		 *                 {
		 *                     "value": "Analytics"
		 *                 }
		 *             ],
		 *             "field_college": {
		 *                 "type": "entity",
		 *                     "entities": [
		 *                         {
		 *                             "id": "121",
		 *                             "title": [
		 *                                 {
		 *                                     "value": "College of Professional Studies"
		 *                                 }
		 *                             ],
		 *                             "field_experiential_learning": [
		 *                                 {
		 *                                     "value": "<p>Northeastern's signature experience-powered learning model has been at the heart of
		 *                                              the university for more than a century. It combines world-class academics with professional
		 *                                              practice, allowing you to acquire relevant, real-world skills you can immediately put into
		 *                                              action in your current workplace. This makes a Northeastern education a dynamic, transformative
		 *                                              experience, giving you countless opportunities to grow as a professional and person.</p>\r\n\r\n
		 *                                              <p>&nbsp;</p>\r\n",
		 *                                     "format": "basic_html"
		 *                                 }
		 *                             ],
		 *                             "field_requirements": [
		 *                                 {
		 *                                     "value": "<ul>\r\n\t<li>Online application</li>\r\n\t<li>Statement of purpose (500–1000 words): Identify your
		 *                                              educational goals and expectations of the program. Please be aware that Northeastern University's
		 *                                              academic policy on plagiarism applies to your statement of purpose.</li>\r\n\t<li>Professional
		 *                                              resumé</li>\r\n\t<li>Unofficial undergraduate transcripts; official transcripts required at the time
		 *                                              of admission</li>\r\n\t<li>Two letters of recommendation from individuals who have either academic or
		 *                                              professional knowledge of your capabilities such as a faculty member, colleague, or mentor, preferably
		 *                                              one from your current employer</li>\r\n\t<li>English language proficiency proof. Students for whom
		 *                                              English is not their primary language must submit one of the following:\r\n\t<ul>\r\n\t\t<li>Official
		 *                                              associate or bachelor's degree transcript from an accredited college or university in the U.S., stating
		 *                                              degree conferral and date</li>\r\n\t\t<li>TOEFL, IELTS, or NU Global Exam scores</li>\r\n\t</ul>\r\n\t
		 *                                              </li>\r\n</ul>\r\n",
		 *                                     "format": "basic_html"
		 *                                 }
		 *                             ],
		 *                             "field_college_link": [
		 *                                 {
		 *                                     "value": "https://cps.northeastern.edu/"
		 *                                 }
		 *                             ]
		 *                         },
		 *                         ...,
		 *                         ...
		 *                     ]
		 *                 },
		 *                 "program": {
		 *                     "entities": [
		 *                         {
		 *                             "id": "14859",
		 *                             "title": [
		 *                                 {
		 *                                     "value": "Master of Professional Studies in Analytics — Boston"
		 *                                 }
		 *                             ]
		 *                         },
		 *                         ...,
		 *                         ...
		 *                     ]
		 *                 }
		 *             }
		 *         }
		 *     ]
		 * }
		 */
		'field_program_family'                => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Program Family', 'nu-pim' ),
			'callback' => 'get_program_family_ids',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "target_id": "2320",
		 *         "alt": "",
		 *         "title": "",
		 *         "width": "462",
		 *         "height": "276",
		 *         "uri": "https://pim.northeastern.edu/sites/default/files/2020-05/Analytics-Masters-Anywhere-Hero.jpg"
		 *     }
		 * ]
		 */
		'field_hero_image'                    => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Hero Image', 'nu-pim' ),
		),

		/*
		 * Example:
		 * []
		 */
		'field_hero_video_webm'               => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Hero Video Webm', 'nu-pim' ),
		),

		/*
		 * Example:
		 * []
		 */
		'field_overview'                      => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Overview', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>The revenue of the global professional sports industry has grown to $145 billion and also projects
		 *                  an increase in jobs by up to 13 percent by 2020 (PwC, 2015).</p>\r\n\r\n<p>Through the Graduate Certificate
		 *                  in Professional Sports Administration curriculum, students will be given the opportunity to acquire professional
		 *                  leadership skills and knowledge in a variety of topical areas including sports management, marketing, sponsorship,
		 *                  event management, risk management, and finance. Upon completion, all credits earned in this certificate can also
		 *                  be applied directly into the
		 *                  <a href=\"https://www.northeastern.edu/graduate/program/master-of-sports-leadership-online-268/\">Master of Sports Leadership</a>
		 *                  program.</p>\r\n\r\n<hr />\r\n<h3>More Details</h3>\r\n\r\n<div class=\"collapse wrapper\">\r\n<h4>Unique Features</h4>\r\n\r\n<ul>
		 *                  \r\n\t<li>Courses in this certificate can be applied to the
		 *                  <a href=\"https://www.northeastern.edu/graduate/program/master-of-sports-leadership-online-268/\">Master of Sports Leadership</a>
		 *                  &nbsp;program</li>\r\n\t<li>Students will be given the opportunity to acquire professional leadership skills and knowledge in a
		 *                  variety of topical areas including sports management, marketing, sponsorship, event management, risk management, and finance</li>
		 *                  \r\n</ul>\r\n</div>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_program_description_long'      => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Program Description Long', 'nu-pim' ),
			'callback' => 'get_value_key', // @todo This is an assumption.
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<ul>\r\n\t<li>Online application</li>\r\n\t<li>Professional resumé: Current resumé that displays job responsibilities, relevant
		 *                  experience, and education history</li>\r\n\t<li>Unofficial undergraduate transcripts; official transcripts required at the time
		 *                  of admission</li>\r\n\t<li>Proof of English language proficiency: ONLY for students for whom English is not their primary language
		 *                  </li>\r\n</ul>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_requirements'                  => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Requirements', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "1"
		 *     }
		 * ]
		 */
		'field_requirements_append'           => array(
			'type'     => 'int',
			'context'  => 'meta',
			'label'    => esc_html__( 'Requirements Append', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "https://pages.northeastern.edu/Grad-WEBS-Prog-Page-iFrames-1A-1B_P-MPS-ANLY-Simple-POI-1A.html"
		 *     }
		 * ]
		 */
		'field_marketo_form_one'              => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Marketo Form One', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "https://pages.northeastern.edu/Grad-WEBS-Prog-Page-iFrames-1A-1B_P-MPS-ANLY-Grad-POI-Hidden-1B.html"
		 *     }
		 * ]
		 */
		'field_marketo_form_two'              => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Marketo Form Two', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "https://pages.northeastern.edu/Grad-WEBS-Prog-Page-iFrames-2_P-MPS-ANLY-No-POI-Form-2.html"
		 *     }
		 * ]
		 */
		'field_marketo_form_three'            => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Marketo Form Three', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "https://pages.northeastern.edu/GRAD-WEBS-Prog-Page-iFrames-4_P-MPS-ANLY-Find-Out-More-POI-Hidden.html"
		 *     }
		 * ]
		 */
		'field_marketo_form_four'             => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Marketo Form Four', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>Northeastern's signature experiential learning model combines academics with professional practice to help students acquire
		 *                  relevant,&nbsp;real-world skills they can apply to their desired industry. Each program offers its own unique experiential
		 *                  learning opportunities, but they might include:</p>\r\n\r\n<ul>\r\n\t<li><strong>Experiential Learning at Work&nbsp;</strong>—
		 *                  Working professionals enrolled in our program collaborate with their employer to develop a project that addresses a key business
		 *                  need their company has in an area they want familiarity with.</li>\r\n\t<li><strong>Experiential Network (XN)&nbsp;</strong>—
		 *                  Students work virtually with a sponsoring organization on a short-term project over a six-week period.</li>\r\n\t<li><strong>
		 *                  Full-Time Co-op Opportunities&nbsp;</strong>— Students on co-op work in a paid position in their field of study for three to
		 *                  six months.<strong>&nbsp;</strong></li>\r\n</ul>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_program_experience'            => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Program Experience', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "entities": [
		 *         {
		 *             "field_link": [],
		 *             "field_image": [],
		 *             "field_quote": [
		 *                 {
		 *                     "value": "As I reflect back on this journey through knowing who I am in order to communicate more competently with others
		 *                              from different cultural background, it makes me very proud to be a student of cross cultural communication.
		 *                              Examining the importance of realizing my identity, my own cultural root and the cultural history, politics and
		 *                              linguistics of the higher education field, and awareness of how conflicts can occur in intercultural relationship,
		 *                              will be very beneficial going forward. For that reason, this course of study has enhanced my life."
		 *                 }
		 *             ],
		 *             "field_source": [
		 *                 {
		 *                     "value": "Yue Huang, reflection of CMN 609"
		 *                 }
		 *             ]
		 *         },
		 *         ...,
		 *         ...
		 *     ]
		 * }
		 */
		'field_testimonials'                  => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Testimonials', 'nu-pim' ),
			'callback' => 'get_entities_key',
		),

		/*
		 * Example:
		 * []
		 */
		'field_faculty_overview'              => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Faculty Overview', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>Below is a look at where our Digital Media and&nbsp;Design alumni work, the positions they hold, and the skills they bring
		 *                  to their organization.</p>\r\n\r\n<div class=\"element-container striped-list\" data-eq-pts=\"small: 300, medium: 500, large:
		 *                  760\">\r\n<p>&nbsp;</p>\r\n\r\n<ul class=\"striped-table columns\">\r\n\t<li>\r\n\t<h4>Where They Work</h4>\r\n\r\n\t<ul>\r\n\t
		 *                  \t<li>Harvard</li>\r\n\t\t<li>MIT</li>\r\n\t\t<li>ViralGains</li>\r\n\t\t<li>Google</li>\r\n\t\t<li>Cognizant</li>\r\n\t</ul>\r
		 *                  \n\t</li>\r\n\t<li>\r\n\t<h4>What They Do</h4>\r\n\r\n\t<ul>\r\n\t\t<li>Art and Design</li>\r\n\t\t<li>Engineering</li>\r\n\t\t
		 *                  <li>Information Technology</li>\r\n\t\t<li>Marketing</li>\r\n\t\t<li>Media and Communication</li>\r\n\t</ul>\r\n\t</li>\r\n\t<li>
		 *                  \r\n\t<h4>What They're Skilled At</h4>\r\n\r\n\t<ul>\r\n\t\t<li>Adobe Creative Suite</li>\r\n\t\t<li>Social Media</li>\r\n\t\t
		 *                  <li>CSS</li>\r\n\t\t<li>Digital Media</li>\r\n\t\t<li>Marketing</li>\r\n\t</ul>\r\n\t</li>\r\n</ul>\r\n</div>\r\n\r\n<p>&nbsp;
		 *                  </p>\r\n\r\n<p>Learn more about Northeastern Alumni on&nbsp;
		 *                  <a href=\"https://www.linkedin.com/school/northeastern-university/people/\">Linkedin</a>.</p>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_students_overview'             => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Students Overview', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "2-3 Years"
		 *     }
		 * ]
		 */
		'field_duration'                      => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Duration', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * []
		 */
		'field_accreditation_abbreviation'    => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Accreditation Abbreviation', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "type": "entity",
		 *     "entities": [
		 *         {
		 *             "id": "14943",
		 *             "title": [
		 *                 {
		 *                     "value": "Bachelor of Science in Biotechnology — Boston"
		 *                 }
		 *             ],
		 *             "field_program_thumbnail": [
		 *                 {
		 *                     "target_id": "1727",
		 *                     "alt": "Bachelor of Science in Biotechnology — Boston",
		 *                     "title": "",
		 *                     "width": "100",
		 *                     "height": "150",
		 *                     "uri": "https://pim.northeastern.edu/sites/default/files/2017-09/BSBiotechnologyThumbnail.jpg"
		 *                 }
		 *             ],
		 *             "field_degree_type": {
		 *                 "entities": [],
		 *                 "nids": [],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_location": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "1",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Boston"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "1",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_commitment": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "3",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Full-Time"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "3",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_duration": [],
		 *             "field_study_options": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "40",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Classroom"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "40",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_align_program": {
		 *                 "entities": [],
		 *                 "nids": [],
		 *                 "type": "taxonomy"
		 *             }
		 *         },
		 *         ...,
		 *         ...
		 *     ]
		 * }
		 */
		'field_related_program'               => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Related Program', 'nu-pim' ),
			'callback' => 'get_related_program_ids',
		),

		/*
		 * Example:
		 * {
		 *     "type": "entity",
		 *     "entities": [
		 *         {
		 *             "id": "14859",
		 *             "title": [
		 *                 {
		 *                     "value": "Master of Professional Studies in Analytics — Boston"
		 *                 }
		 *             ],
		 *             "field_formatted_title": [
		 *                 {
		 *                     "value": "Analytics"
		 *                 }
		 *             ],
		 *             "field_program_thumbnail": [
		 *                 {
		 *                     "target_id": "1727",
		 *                     "alt": "Bachelor of Science in Biotechnology — Boston",
		 *                     "title": "",
		 *                     "width": "100",
		 *                     "height": "150",
		 *                     "uri": "https://pim.northeastern.edu/sites/default/files/2017-09/BSBiotechnologyThumbnail.jpg"
		 *                 }
		 *             ],
		 *             "field_degree_type": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "38",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Master's"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "38",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_location": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "1",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Boston"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "1",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_commitment": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "3",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Full-Time"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "3",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_duration": [
		 *                 {
		 *                     "value": "2-3 Years"
		 *                 }
		 *             ],
		 *             "field_study_options": {
		 *                 "entities": [
		 *                     {
		 *                         "tid": "40",
		 *                         "name": [
		 *                             {
		 *                                 "value": "Classroom"
		 *                             }
		 *                         ]
		 *                     },
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "nids": [
		 *                     "40",
		 *                     ...,
		 *                     ...
		 *                 ],
		 *                 "type": "taxonomy"
		 *             },
		 *             "field_align_program": {
		 *                 "entities": [],
		 *                 "nids": [],
		 *                 "type": "taxonomy"
		 *             }
		 *         },
		 *         ...,
		 *         ...
		 *     ]
		 * }
		 */
		'field_related_program_2'             => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Related Program 2', 'nu-pim' ),
			'callback' => 'get_related_program_ids',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "{\"tuitionCalcOption\":\"tuitionPerCreditHours\",\"fields\":{\"costPerCreditHour\":\"903\",
		 *                  \"creditHours\":\"45\"},\"tuitionCalcValue\":40635,\"tuitionCalcLabel\":\"Tuition\"}"
		 *     }
		 * ]
		 */
		'field_finance_tuition_calc'          => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Tuition Calc', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "0"
		 *     }
		 * ]
		 */
		'field_hide_tuition_costs'            => array(
			'type'     => 'int',
			'context'  => 'meta',
			'label'    => esc_html__( 'Hide Tuition Costs', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * []
		 */
		'field_finance_program_fees'          => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Program Fees', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "10,183"
		 *     }
		 * ]
		 */
		'field_finance_aid'                   => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Aid', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "45"
		 *     }
		 * ]
		 */
		'field_finance_percent_aid'           => array(
			'type'     => 'int',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Percent Aid', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "We offer a variety of resources, including scholarships and assistantships."
		 *     }
		 * ]
		 */
		'field_finance_information'           => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Information', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "https://studentfinance.northeastern.edu/#_ga=2.59552109.19549433.1506949601-148400310.1505504629"
		 *     }
		 * ]
		 */
		'field_finance_link'                  => array(
			'type'     => 'url',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Link', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "global"
		 *     }
		 * ]
		 */
		'field_finance_link_one_source'       => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Link One Source', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "uri": "https://studentfinance.northeastern.edu/#_ga=2.59552109.19549433.1506949601-148400310.1505504629",
		 *         "title": "Finance Your Education",
		 *         "options": [],
		 *         "value": ""
		 *     }
		 * ]
		 */
		'field_finance_link_one_link'         => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Finance Link One Link', 'nu-pim' ),
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "We offer a variety of resources, including scholarships and assistantships."
		 *     }
		 * ]
		 */
		'field_finance_link_one_text'         => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Link One Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "global"
		 *     }
		 * ]
		 */
		'field_finance_link_two_source'       => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Link Two Source', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "uri": "https://studentfinance.northeastern.edu/#_ga=2.59552109.19549433.1506949601-148400310.1505504629",
		 *         "title": "Finance Your Education",
		 *         "options": [],
		 *         "value": ""
		 *     }
		 * ]
		 */
		'field_finance_link_two_link'         => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Finance Link Two Link', 'nu-pim' ),
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "Learn more about the application process and requirements."
		 *     }
		 * ]
		 */
		'field_finance_link_two_text'         => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Link Two Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "college"
		 *     }
		 * ]
		 */
		'field_admissions_deadline_source'    => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Deadline Source', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>Our admissions process operates on a rolling basis, however we do recommend following the application guidelines
		 *                  below to ensure you can begin during your desired start term.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>
		 *                  &nbsp;</p>\r\n\r\n<p class=\"MsoNormal\"><o:p></o:p></p>\r\n\r\n<p>&nbsp;</p>\r\n",
		 *         "format": "basic_html"
		 *     }
		 * ]
		 */
		'field_admissions_intro_text'         => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Intro Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "type": "eck",
		 *     "entities": [
		 *         {
		 *             "title": [
		 *                 {
		 *                     "value": "Fall 2018"
		 *                 }
		 *             ],
		 *             "field_table_header_col1": [
		 *                 {
		 *                     "value": "Start Date:"
		 *                 }
		 *             ],
		 *             "field_table_header_col2": [
		 *                 {
		 *                     "value": "Recommended Admissions Deadline:"
		 *                 }
		 *              ],
		 *              "field_admissions_table_rows": {
		 *                  "type": "eck",
		 *                  "entities": [
		 *                      {
		 *                          "field_table_row_col1": [
		 *                              {
		 *                                  "value": "September 17, 2018"
		 *                              }
		 *                          ],
		 *                          "field_table_row_col2": [
		 *                              {
		 *                                  "value": "September 7, 2018"
		 *                              }
		 *                          ]
		 *                      },
		 *                      ...,
		 *                      ...
		 *                  ]
		 *              }
		 *          },
		 *          ...,
		 *          ...
		 *      ]
		 * }
		 */
		'field_admissions_tables'             => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Tables', 'nu-pim' ),
			'callback' => 'get_entities_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "college"
		 *     }
		 * ]
		 */
		'field_admissions_link_one_source'    => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Link One Source', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * []
		 */
		'field_admissions_link_one_link'      => array(
			'type'     => 'url',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Link One Link', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * []
		 */
		'field_admissions_link_one_text'      => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Link One Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "college"
		 *     }
		 * ]
		 */
		'field_admissions_link_two_source'    => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Link Two Source', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * []
		 */
		'field_admissions_link_two_link'      => array(
			'type'    => 'url',
			'context' => 'meta',
			'label'   => esc_html__( 'Admissions Link Two Link', 'nu-pim' ),
		),

		/*
		 * Example:
		 * []
		 */
		'field_admissions_link_two_text'      => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Link Two Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "Learn more about the application process and requirements."
		 *     }
		 * ]
		 */
		'field_apply_text'                    => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Apply Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "https://app.applyyourself.com/AYApplicantLogin/fl_ApplicantLogin.asp?id=NEU-CPS&_ga=1.258375305.265676930.1387287772"
		 *     }
		 * ]
		 */
		'field_apply_link'                    => array(
			'type'     => 'url',
			'context'  => 'meta',
			'label'    => esc_html__( 'Apply Link', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "college"
		 *     }
		 * ]
		 */
		'field_apply_link_source'             => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Apply Link Source', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * [
		 *     {
		 *         "value": "<p>A graduate degree or&nbsp;certificate from Northeastern—a top-40 university—can accelerate your career through rigorous
		 *                  academic coursework and hands-on professional experience in the area of your interest. Apply now—and take your career to the
		 *                  next level.</p>\r\n"
		 *     }
		 * ]
		 */
		'field_admissions_section_intro_text' => array(
			'type'     => 'string',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Section Intro Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": {
		 *         "intro_text": "<p>Our admissions process operates on a rolling basis, however we do recommend following the application guidelines below
		 *                       to ensure you can begin during your desired start term.</p>\r\n",
		 *         "tables": [
		 *             {
		 *                 "title": "Fall 2019",
		 *                 "header": [
		 *                     "Start Date",
		 *                     "Recommended Admissions Deadline"
		 *                 ],
		 *                 "rows": [
		 *                     [
		 *                         "September 16, 2019",
		 *                         "September 6, 2019"
		 *                     ],
		 *                     [
		 *                         "October 28, 2019",
		 *                         "October 18, 2019"
		 *                     ]
		 *                 ]
		 *             },
		 *             ...,
		 *             ...
		 *         ]
		 *     }
		 * }
		 */
		'field_admissions_deadlines'          => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Deadlines', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": {
		 *         "one": {
		 *             "url": "https://www.northeastern.edu/graduate/admissions-information/how-to-apply/applying-as-an-international-student/",
		 *             "title": "Are You an International Student?",
		 *             "text": "Find out what additional documents are required to apply."
		 *         },
		 *         "two": {
		 *             "url": "https://www.northeastern.edu/graduate/why-northeastern/about-our-academics/global-engagement/",
		 *             "title": "Global Engagement",
		 *             "text": "Learn how our teaching and research benefit from a worldwide network of students, faculty, and industry partners."
		 *         }
		 *     }
		 * }
		 */
		'field_admissions_links'              => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Admissions Links', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": {
		 *         "one": {
		 *             "url": "https://studentfinance.northeastern.edu/#_ga=2.59552109.19549433.1506949601-148400310.1505504629",
		 *             "title": "Finance Your Education",
		 *             "text": "We offer a variety of resources, including scholarships and assistantships."
		 *         },
		 *         "two": {
		 *             "url": "https://www.northeastern.edu/graduate/admissions-information/how-to-apply/the-process/",
		 *             "title": "How to Apply",
		 *             "text": "Learn more about the application process and requirements."
		 *         }
		 *     }
		 * }
		 */
		'field_financial_links'               => array(
			'type'     => 'array',
			'context'  => 'meta',
			'label'    => esc_html__( 'Financial Links', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "https://app.applyyourself.com/AYApplicantLogin/fl_ApplicantLogin.asp?id=NEU-CPS&_ga=2.15527222.1824387636.1524836665-81214367.1520869011"
		 * }
		 */
		'field_apply_link_transform'          => array(
			'type'     => 'url',
			'context'  => 'meta',
			'label'    => esc_html__( 'Apply Link Transform', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "<ul>\r\n\t<li>Online application</li>\r\n\t<li>Statement of purpose (500–1000 words): Identify your educational goals and
		 *              expectations of the program. Please be aware that Northeastern University's academic policy on plagiarism applies to your
		 *              statement of purpose.</li>\r\n\t<li>Professional resumé</li>\r\n\t<li>Unofficial undergraduate transcripts; official transcripts
		 *              required at the time of admission</li>\r\n\t<li>Two letters of recommendation from individuals who have either academic or
		 *              professional knowledge of your capabilities such as a faculty member, colleague, or mentor, preferably one from your current
		 *              employer</li>\r\n\t<li>English language proficiency proof. Students for whom English is not their primary language must submit
		 *              one of the following:\r\n\t<ul>\r\n\t\t<li>Official associate or bachelor's degree transcript from an accredited college or
		 *              university in the U.S., stating degree conferral and date</li>\r\n\t\t<li>TOEFL, IELTS, or NU Global Exam scores</li>\r\n\t</ul>
		 *              \r\n\t</li>\r\n</ul>\r\n"
		 * }
		 */
		'field_requirements_transform'        => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Requirements Transform', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "<ul>\r\n\t<li>Statement of purpose&nbsp;</li>\r\n\t<li>Professional resume</li>\r\n\t<li>Two letters of recommendation</li>\r\n
		 *              </ul>\r\n"
		 * }
		 */
		'field_key_requirements_transform'    => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Key Requirements Transform', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "<p>Northeastern's signature experience-powered learning model has been at the heart of the university for more than a century.
		 *              It combines world-class academics with professional practice, allowing you to acquire relevant, real-world skills you can
		 *              immediately put into action in your current workplace. This makes a Northeastern education a dynamic, transformative experience,
		 *              giving you countless opportunities to grow as a professional and person.</p>\r\n\r\n<p>&nbsp;</p>\r\n"
		 * }
		 */
		'field_experiential_transform'        => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Experiential Transform', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": [
		 *         {
		 *             "value": "<span class='bullet-currency'>$</span><span class='bullet-value'>40.6</span><span class='bullet-units'>K</span>",
		 *             "label": "Tuition",
		 *             "modal": "<h4 class=\"title\">Estimated Total Tuition</h4>\r\n\r\n<p>This is based on <em>tuition rates for Academic Year 2019&nbsp;-
		 *                      20&nbsp;</em>and does not include any fees or other expenses. Some courses and labs have tuition rates that may increase or
		 *                      decrease total tuition. Tuition and fees are subject to revision by the president and Board of Trustees at any time.</p>\r\n"
		 *         }
		 *     ]
		 * }
		 */
		'field_finance_blocks'                => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Finance Blocks', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "<p>Our faculty represents a wide cross-section of professional practices and fields ranging from finance to education to biomedical
		 *              science to management to the U.S. military. They serve as mentors and advisors and collaborate alongside students to solve the most
		 *              pressing global challenges facing established and emerging markets.</p>\r\n"
		 * }
		 */
		'field_faculty_section_intro_text'    => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Faculty Section Intro Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "<p>By enrolling in Northeastern, you gain access to a network of more than 255,000 alumni and 3,350+ employer partners, including
		 *              Fortune 500 companies, government agencies, and global nongovernmental organizations. Our current students and faculty across
		 *              strategically located regional locations further foster a lifelong, global community of learning and mentoring.</p>\r\n"
		 * }
		 */
		'field_student_section_intro_text'    => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Student Section Intro Text', 'nu-pim' ),
			'callback' => 'get_value_key',
		),

		/*
		 * Example:
		 * {
		 *     "value": "<p>Please note: this curriculum is for marketing purposes only, and is subject to change. Official curriculum can be found within
		 *              the Course Catalog.</p>\r\n"
		 * }
		 */
		'field_curriculum_disclaimer'         => array(
			'type'     => 'html',
			'context'  => 'meta',
			'label'    => esc_html__( 'Curriculum Disclaimer', 'nu-pim' ),
			'callback' => 'get_value_key',
		),
	);

	// phpcs:enable Squiz.PHP.CommentedOutCode.Found

	/**
	 * Filter whitelisted fields.
	 *
	 * Note: the 'id' field cannot be removed.
	 *
	 * @var $fields Array of fields and their attributes.
	 */
	$filtered_fields = apply_filters( 'nu_pim_whitelisted_fields', $fields );

	// Prevent removing the 'id' key from the fields array.
	if ( ! array_key_exists( 'id', $filtered_fields ) ) {

		$filtered_fields['id'] = $fields['id'];
	}

	return $filtered_fields;
}

/**
 * Log a message to a log file and the wp cli interface.
 *
 * @param string $message Message string.
 * @param bool   $cli     Output message to cli as well.
 * @param bool   $error   Render an error in the cli.
 */
function log( $message, $cli = false, $error = false ) {

	if ( empty( $message ) ) {
		return;
	}

	$timestamp = '[' . current_time( 'mysql' ) . '] ';
	$message   = trim( (string) $message );

	// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_error_log
	// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_var_export
	if ( ! defined( 'ABSPATH' ) ) {

		$log_file = ini_get( 'error_log' );

		if ( ! empty( $log_file ) && is_writable( $log_file ) ) {
			error_log( var_export( $timestamp . $message, true ) . PHP_EOL, 3, $log_file );
		}
	} else {

		$log_file = ABSPATH . '/pim-import.log';

		if ( is_writable( $log_file ) ) {
			error_log( $timestamp . $message . PHP_EOL, 3, $log_file );
		}
	}

	if ( $cli && defined( '\WP_CLI' ) && \WP_CLI ) {
		if ( $error ) {
			\WP_CLI::error( $message );
		} else {
			\WP_CLI::log( $message );
		}
	}
	// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_error_log
	// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_var_export
}

/**
 *  Clear all of the caches for memory management
 */
function stop_the_insanity() {

	global $wpdb, $wp_object_cache;

	$wpdb->queries = array();

	if ( ! is_object( $wp_object_cache ) ) {
		return;
	}

	$wp_object_cache->group_ops      = array();
	$wp_object_cache->stats          = array();
	$wp_object_cache->memcache_debug = array();
	$wp_object_cache->cache          = array();

	if ( is_callable( $wp_object_cache, '__remoteset' ) ) {
		$wp_object_cache->__remoteset(); // important.
	}
}
