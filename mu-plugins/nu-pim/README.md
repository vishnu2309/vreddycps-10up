# NU PIM WordPress Plugin

The NU PIM plugin adds a WP CLI command that imports Programs into WordPress via the PIM API.

This plugin supports the following endpoints:

- Fetching a paginated list of programs for a specific college:
`/api/v2/programs/{page}/{college}`
- Fetching data for a specific program
`/api/v2/program/{nid}`

See: https://pim.northeastern.edu/api-docs

## Use

- Import CPS program data:
`wp pim import --college=121 --allow-root`
