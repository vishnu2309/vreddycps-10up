# CPS Migration

This is a migration plugin for https://cps.northeastern.edu/ from Drupal to WP.

## Users

Below is the list of Drupal DB tables:
- users_field_data
- user__roles

Below is the list of WordPress DB tables will be affected once migration script runs:
- wp_users
- wp_usermeta

### # Field Mapping

```text
wp_users.user_login      = users_field_data.name
wp_users.user_email      = users_field_data.mail
wp_users.user_registered = users_field_data.created
wp_usermeta              = {
    wp_capabilities = user__roles.roles_target_id
}
```

### # Script Command
```php
wp migrate users

    [--dry-run=<true/false>]
        Run the entire migration and show report, but don’t save changes to the database.

    [--output=<output>]
        Specify the file path to write the log to.
```

## News
Below is the list of Drupal DB tables:
- node_field_data
- taxonomy_term_field_data
- node__field_blog_type_ref
- url_alias
- node__body

Below is the list of WordPress DB tables will be affected once migration script runs:
- wp_posts
- wp_postmeta
- wp_termmeta
- wp_terms
- wp_term_relationships
- wp_term_taxonomy

### # Field Mapping

```text
wp_posts.post_title        = node_field_data.title
wp_posts.post_author       = node_field_data.uid (main table users_field_data)
wp_posts.post_date         = node_field_data.created
wp_posts.post_date_gmt     = node_field_data.created
wp_posts.post_content      = node__body.body_value
wp_posts.post_excerpt      = node__body.body_summary
wp_posts.post_status       = node_field_data.status (1 = publish, 0 = draft)
wp_posts.post_name         = url_alias.alias
wp_posts.post_modified     = node_field_data.changed
wp_posts.post_modified_gmt = node_field_data.changed
wp_posts.post_type         = node_field_data.type (News = News, Story = News with Story as category, Event = Event)
wp_postmeta                = {
    _thumbnail_id              = node__field_image.field_image_target_id (main tables - file_managed, media_field_data),
    _legacy_cps_nid            = node_field_data.nid,
    _legacy_cps_author         = node_field_data.uid,
    _legacy_cps_author         = node_field_data.uid,
    _legacy_cps_blog_authors   = taxonomy_term_field_data.name, (Drupal's "blog_authors" taxonomy)
    _legacy_cps_title_override = node__field_display_title_override.field_display_title_override_value,
}
Terms                      = {
    category => {
        taxonomy_term_field_data.name
    }
}
```

### # Script Command
```php
wp migrate news

    [--dry-run=<true/false>]
        Run the entire migration and show report, but don’t save changes to the database.

    [--force=<true/false>]
        Delete all existing posts before importing. Intended to be used during development only.

    [--yes=<true/false>]
        Answer yes to the confirmation messages.
```

## Attachments
**The attachments will be migrated dynamically with the news migration.**

Below is the list of Drupal DB tables:
- file_managed
- file__field_image_title_text
- file__field_image_alt_text

Below is the list of WordPress DB tables will be affected once migration script runs:
- wp_posts
- wp_postmeta

### # Field Mapping

```text
wp_posts.post_title        = file__field_image_title_text.field_image_title_text_value || file_managed.filename
wp_posts.post_excerpt      = drupal-entity.data-caption attribute
wp_postmeta                = {
    _legacy_cps_original_url = file_managed.uri,
    _wp_attachment_image_alt = drupal-entity.alt attribute || file__field_image_alt_text.field_image_alt_text_value,
}
```
