<?php
/**
 * Custom CLI commands bootstrap file.
 *
 * @package CPSMigration
 */

namespace CPSMigration\CLI;

/**
 * Define custom CLI scripts if WP_CLI is defined.
 *
 * @since 1.0.0
 */
function setup() {

	if ( defined( 'WP_CLI' ) && WP_CLI ) {

		require CPS_MIGRATION_INC . 'wp-cli/abstract-command.php';
		require CPS_MIGRATION_INC . 'wp-cli/class-migrate-users.php';
		require CPS_MIGRATION_INC . 'wp-cli/class-migrate-news.php';
	}
}
