<?php
/**
 * Command for the users migration.
 *
 * @package CPSMigration
 */

namespace CPSMigration\CLI;

/**
 * Command for the migrating users.
 *
 * @package CPSMigration\CLI
 */
class MigrateUsers extends \CPSMigration\CLI\AbstractCommand {

	/**
	 * Migrate users from Drupal DB.
	 *
	 * ## OPTIONS
	 *
	 * [--dry-run]
	 * : Run the entire migration and show report, but don’t save changes to the database.
	 *
	 * [--output=<output>]
	 * : Specify the file path to write the log to.
	 *
	 * ## EXAMPLES
	 *
	 *     $ wp migrate users
	 *     $ wp migrate users --dry-run
	 *
	 * @param  array $args       Positional command arguments.
	 * @param  array $assoc_args Associative arguments.
	 *
	 * @return void
	 */
	public function __invoke( array $args = [], array $assoc_args = [] ) {

		// Starting time of the script.
		$start_time = time();

		global $wpdb;

		$this->dry_run          = \WP_CLI\Utils\get_flag_value( $assoc_args, 'dry-run', false );
		$this->output_file_path = ! empty( $assoc_args['output'] ) ? $assoc_args['output'] : trailingslashit( ABSPATH ) . 'CPS-USER-MIGRATION.' . time() . '.csv';

		\WP_CLI::line();

		// Process users.
		$users_field_data = $wpdb->get_results(
			'
			SELECT * FROM users_field_data
			',
			ARRAY_A
		);

		$remote_data_count = count( $users_field_data );

		$this->progress_bar = \WP_CLI\Utils\make_progress_bar(
			'Migrating ' . $remote_data_count . ' users:',
			$remote_data_count
		);

		$failed  = 0;
		$success = 0;

		$failed_data = [];

		foreach ( $users_field_data as $user ) {

			// Some of the users doesn't have name & email, skipping those.
			if ( empty( $user['name'] ) || empty( $user['mail'] ) ) {

				$failed++;

				$failed_data[] = [
					'ID'     => $user['uid'],
					'Name'   => $user['name'],
					'Status' => 'Skipped',
					'Reason' => 'Name or Email do not exist for this user!',
				];

				$this->progress_bar->tick();

				continue;
			}

			// Some of the users are not active anymore, skipping those.
			if ( 0 === (int) $user['status'] ) {

				$failed++;

				$failed_data[] = [
					'ID'     => $user['uid'],
					'Name'   => $user['name'],
					'Status' => 'Skipped',
					'Reason' => 'The status of the user is 0, means user is not needed anymore!',
				];

				$this->progress_bar->tick();

				continue;
			}

			if ( ! $this->dry_run ) {

				// Convert timestamp into date time in GMT/America/New_York timezones.
				$registered_date_time = $this->get_exact_date( $user['created'] );

				// Get user role from Drupal DB.
				$drupal_user_role = $wpdb->get_var(
					$wpdb->prepare(
						'
						SELECT roles_target_id FROM user__roles
						WHERE entity_id = %d
						',
						$user['uid']
					)
				);

				$user_data = [
					'user_login'      => $user['name'],
					'user_email'      => $user['mail'],
					'user_pass'       => wp_generate_password(),
					'role'            => $this->get_wp_user_role( $drupal_user_role ),
					'user_registered' => $registered_date_time['site'],
				];

				$user_id = wp_insert_user( $user_data );

				if ( is_wp_error( $user_id ) ) {

					$failed++;

					$failed_data[] = [
						'ID'     => $user['uid'],
						'Name'   => $user['name'],
						'Status' => 'Failed',
						'Reason' => "The user is failed to create - {$user_id->get_error_message()}!",
					];

					$this->progress_bar->tick();

					continue;
				}

				$this->process_meta(
					'user',
					$user_id,
					$user,
					[ 'name', 'mail', 'created' ],
					[ 'uid' ]
				);
			}

			$success++;

			$this->progress_bar->tick();
		}

		$this->progress_bar->finish();

		$this->stop_the_insanity();

		$message_dry_run = 'are ';
		if ( $this->dry_run ) {
			$message_dry_run = 'will be ';
		}

		\WP_CLI::line();
		\WP_CLI::line( '==========================' );
		\WP_CLI::success( "Total {$success} user(s) {$message_dry_run}migrated successfully!" );

		\WP_CLI::line( '==========================' );
		\WP_CLI::warning( "Total {$failed} user(s) {$message_dry_run}failed to migrate!" );

		if ( ! empty( $failed_data ) ) {

			\WP_CLI::line();
			\WP_CLI::line( '==========================' );

			if ( $this->dry_run ) {

				\WP_CLI\Utils\format_items(
					'table',
					$failed_data,
					[
						'ID',
						'Name',
						'Status',
						'Reason',
					]
				);
			} else {

				$this->start_log(
					[
						'ID',
						'Name',
						'Status',
						'Reason',
					]
				);

				foreach ( $failed_data as $row ) {
					$this->log_row( $row );
				}

				$this->close_log();

				\WP_CLI::line( "Output CSV available at {$this->output_file_path}" );
			}

			\WP_CLI::line( '==========================' );
		}

		\WP_CLI::line();
		\WP_CLI::success( sprintf( 'Total time taken by this script: %s', human_time_diff( $start_time, time() ) ) );
		\WP_CLI::line();
	}

	/**
	 * Get WP role based on Drupal role.
	 *
	 * @param string $drupal_role User role in Drupal CMS.
	 *
	 * @return string
	 */
	protected function get_wp_user_role( $drupal_role ) {

		$role = 'subscriber';

		$role_mapping = [
			'administrator'     => 'administrator',
			'content_moderator' => 'editor',
		];

		if ( ! empty( $role_mapping[ $drupal_role ] ) ) {
			$role = $role_mapping[ $drupal_role ];
		}

		return $role;
	}
}

\WP_CLI::add_command( 'migrate users', __NAMESPACE__ . '\MigrateUsers' );
