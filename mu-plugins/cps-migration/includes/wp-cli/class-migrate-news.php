<?php
/**
 * Command for the news migration.
 *
 * @package CPSMigration
 */

namespace CPSMigration\CLI;

/**
 * Command for the migrating news.
 *
 * @package CPSMigration\CLI
 */
class MigrateNews extends \CPSMigration\CLI\AbstractCommand {

	/**
	 * News ID. If this is set, only single news item will be imported.
	 *
	 * @var int
	 */
	private $nid;

	/**
	 * To delete all posts and re-import form scratch.
	 *
	 * @var bool
	 */
	private $setup = false;

	/**
	 * Flag for updating existing posts even if the 'update' field has not changed.
	 *
	 * @var bool
	 */
	private $force = false;

	/**
	 * To bypass the confirmation.
	 *
	 * @var bool
	 */
	public $yes = false;

	/**
	 * Allowed extensions array in WP.
	 *
	 * @var array
	 */
	protected $extensions = [];

	/**
	 * File URL prefix to check if file is viewable.
	 *
	 * @var string
	 */
	protected $file_url_prefix = 'https://cps.northeastern.edu/sites/default/files';

	/**
	 * Migrate news from Drupal DB.
	 *
	 * ## OPTIONS
	 *
	 * [--nid]
	 * : News ID. Set this flag to import a single news item.
	 *
	 * [--dry-run]
	 * : Run the entire migration and show report, but don’t save changes to the database.
	 *
	 * [--setup]
	 * : Delete all existing posts before importing. Intended to be used during development only.
	 *
	 * [--force]
	 * : Update existing posts even if the 'update' field matches.
	 *
	 * [--yes]
	 * : Answer yes to the confirmation messages.
	 *
	 * ## EXAMPLES
	 *
	 *     $ wp migrate news
	 *     $ wp migrate news --dry-run
	 *
	 * @param  array $args       Positional command arguments.
	 * @param  array $assoc_args Associative arguments.
	 *
	 * @return void
	 */
	public function __invoke( array $args = [], array $assoc_args = [] ) {

		// Starting time of the script.
		$start_time = time();

		global $wpdb;

		$this->nid              = \WP_CLI\Utils\get_flag_value( $assoc_args, 'nid', 0 );
		$this->dry_run          = \WP_CLI\Utils\get_flag_value( $assoc_args, 'dry-run', false );
		$this->setup            = \WP_CLI\Utils\get_flag_value( $assoc_args, 'setup', false );
		$this->force            = \WP_CLI\Utils\get_flag_value( $assoc_args, 'force', false );
		$this->yes              = \WP_CLI\Utils\get_flag_value( $assoc_args, 'yes', false );
		$this->output_file_path = ! empty( $assoc_args['output'] ) ? $assoc_args['output'] : trailingslashit( ABSPATH ) . 'CPS-NEWS-MIGRATION.' . time() . '.csv';

		if ( ! $this->yes ) {

			$message = 'Running migration script in live mode';

			if ( $this->setup ) {
				$message = $message . ' with "setup" flag. All existing posts will be removed before running the migration script';
			}

			\WP_CLI::confirm( $message . '. Are you sure?' );
		}

		/*
		 * Important: The setup flag is only intended for development, news/events on the live site might contain
		 *            additional data added manually by site editors.
		 */
		if ( $this->setup ) {
			$this->delete_all_posts();
		}

		$nid_clause = '';

		if ( ! empty( $this->nid ) ) {
			$nid_clause = "AND node_field_data.nid = {$this->nid}";
		}

		// Process news.
		$news = $wpdb->get_results(
			$wpdb->prepare(
				"
				SELECT node_field_data.*, taxonomy_term_field_data.name FROM node_field_data
				LEFT JOIN node__field_blog_type_ref ON node_field_data.nid = node__field_blog_type_ref.entity_id
				LEFT JOIN taxonomy_term_field_data ON node__field_blog_type_ref.field_blog_type_ref_target_id = taxonomy_term_field_data.tid
				WHERE node_field_data.type = %s {$nid_clause} ORDER BY nid ASC
				",
				'news'
			),
			ARRAY_A
		);

		$this->allowed_extensions();
		$this->process_news( $news );

		\WP_CLI::line();
		\WP_CLI::line();
		\WP_CLI::success( sprintf( 'Total time taken by this script: %s', human_time_diff( $start_time, time() ) ) );
		\WP_CLI::line();
		\WP_CLI::line();
	}

	/**
	 * Process all news items.
	 *
	 * @param array $items  All items (posts)
	 *
	 * @throws \Exception
	 */
	protected function process_news( $items ) {

		$remote_data_count = count( $items );

		$this->progress_bar = \WP_CLI\Utils\make_progress_bar(
			'Migrating ' . $remote_data_count . ' news articles:',
			$remote_data_count
		);

		$failed  = 0;
		$success = 0;

		$failed_data = [];

		$index = 0;

		foreach ( $items as $post ) {

			$errors      = [];
			$attachments = [];

			if ( ! $this->dry_run ) {

				$term_fields = [];
				$new_post    = [
					'post_title'     => $post['title'],
					'post_type'      => 'post',
					'post_content'   => '',
					'post_status'    => 'publish',
					'comment_status' => 'closed',
					'ping_status'    => 'closed',
					'meta_input'     => array(),
				];

				$new_post['meta_input'][ "{$this->prefix}nid" ] = $post['nid'];

				// If status is "0", set the post status to Draft.
				if ( 0 === (int) $post['status'] ) {
					$new_post['post_status'] = 'draft';
				}

				$existing_post = $this->get_post_by_nid( $post['nid'] );

				if ( is_a( $existing_post, '\WP_Post' ) ) {

					$new_post['ID'] = $existing_post->ID;

					/*
					 * This is required, because if we don't pass the post_status, then the wp_insert_post method
					 * considers default post_status as "draft" and it'll apply it to the post, so our published
					 * post will become draft and when next time the import script runs, it'll import whole new post,
					 * making the duplication.
					 */
					$new_post['post_status'] = $existing_post->post_status;

					/*
					 *  If we're not forcing an update and the existing changed field
					 *  matched the new changed field do nothing.
					 */
					if ( ! $this->force ) {

						$failed++;

						$failed_data[] = [
							'ID'     => $post['nid'],
							'Title'  => $post['title'],
							'Status' => 'Post already exist',
							'Reason' => "This news item was already created! Try '--force' to update it forcefully.",
						];

						$this->progress_bar->tick();

						continue;
					}
				}

				// Convert timestamp into date time in GMT/America/New_York timezones.
				$created_date_time  = $this->get_exact_date( $post['created'] );
				$modified_date_time = $this->get_exact_date( $post['changed'] );

				// Set the post date.
				$new_post['post_date_gmt']     = $created_date_time['gmt'];
				$new_post['post_date']         = $created_date_time['site'];
				$new_post['post_modified_gmt'] = $modified_date_time['gmt'];
				$new_post['post_modified']     = $modified_date_time['site'];

				// Set the post author.
				$new_post['post_author']                           = $this->get_post_author( $post['uid'] );
				$new_post['meta_input'][ "{$this->prefix}author" ] = $post['uid'];

				// Set post slug.
				$new_post['post_name'] = $this->get_post_slug( $post['nid'], $post['title'], $new_post['post_status'] );

				// Grab the post content and excerpt.
				$content_data = $this->get_post_content( $post['nid'] );
				$_content     = '';

				if ( ! empty( $content_data['body_value'] ) ) {
					$_content .= $content_data['body_value'];
				}

				$block_content = $this->get_block_content( $post['nid'] );

				if ( ! empty( $block_content ) ) {
					$_content .= $block_content;
				}

				if ( ! empty( $_content ) ) {

					$post_content_data = $this->handle_attachment( $post['nid'], $_content );

					$new_post['post_content'] = $post_content_data['content'];
					$attachments              = $post_content_data['attachments'];
				}

				if ( ! empty( $content_data['body_summary'] ) ) {
					$new_post['post_excerpt'] = $content_data['body_summary'];
				}

				if ( ! empty( $post['name'] ) ) {

					if ( 'Story' === $post['name'] ) {
						$term_fields['category'] = trim( $post['name'] );
					} elseif ( 'Event' === $post['name'] ) {
						$new_post['post_type'] = 'cps-event';
					}
				}

				/*
				 * TODO: Check for alternative if this can be transformed into a Taxonomy.
				 *
				 * This is a Drupal Taxonomy - "blog_authors".
				 *
				 * Now this can be faculties/guest-authors fro co-author plus plugin (not sure),
				 * thought all the blog_authors are not present in faculties.
				 */
				$blog_authors = $this->get_blog_authors( $post['nid'] );

				if ( ! empty( $blog_authors ) ) {

					$blog_authors = wp_list_pluck( $blog_authors, 'name' );

					$new_post['meta_input'][ "{$this->prefix}blog_authors" ] = implode( ', ', $blog_authors );
				}

				// Get title override value.
				$title_override = $this->get_title_override( $post['nid'] );

				if ( ! empty( $title_override ) ) {
					$new_post['meta_input'][ "{$this->prefix}title_override" ] = $title_override;
				}

				$post_id = wp_insert_post( $new_post );

				if ( is_wp_error( $post_id ) ) {

					$failed++;

					$failed_data[] = [
						'ID'     => $post['nid'],
						'Title'  => $post['title'],
						'Status' => 'Failed',
						'Reason' => "The news is failed to create - {$post_id->get_error_message()}!",
					];

					$this->progress_bar->tick();

					continue;
				}

				$this->process_meta(
					'post',
					$post_id,
					$post,
					[ 'title', 'created', 'changed', 'nid', 'uid' ],
					[ 'name' ]
				);

				if ( ! empty( $term_fields ) ) {

					foreach ( $term_fields as $taxonomy => $term ) {

						$_term = get_term_by( 'name', $term, $taxonomy );

						// Check if term exist, if not, we'll create new at runtime.
						if ( empty( $_term ) ) {

							// Create new term.
							$inserted_term = wp_insert_term( $term, $taxonomy );

							// If fail to create new term, move to next one.
							if ( is_wp_error( $inserted_term ) ) {

								$errors[] = "Failed to create {$term} in {$taxonomy}: " . $inserted_term->get_error_message();

								continue;
							}

							if ( empty( $inserted_term['term_id'] ) ) {

								$errors[] = "Failed to create {$term} in {$taxonomy}: " . 'Something went wrong';

								continue;
							}

							$term_id = $inserted_term['term_id'];
						} else {
							$term_id = $_term->term_id;
						}

						$term_id = absint( $term_id );

						wp_set_object_terms( $post_id, $term_id, $taxonomy );
					}
				}

				if ( ! empty( $attachments ) ) {

					$thumbnail_id = 0;
					$post_content = $new_post['post_content'];

					foreach ( $attachments as $index => $attachment_data ) {

						$attachment_id = $this->get_attachment_id( $post_id, $attachment_data['url'] );

						if ( is_wp_error( $attachment_id ) ) {

							$errors[] = "Failed to create {$attachment_data['url']} attachment: " . $attachment_id->get_error_message();

							continue;

						} else {

							$_attachment_args = array(
								'ID'         => $attachment_id,
								'post_title' => $attachment_data['title'],
								'meta_input' => array(
									"{$this->prefix}original_url" => trim( $attachment_data['url'] ),
								),
							);

							if ( ! empty( $attachment_data['caption'] ) ) {
								$_attachment_args['post_excerpt'] = $attachment_data['caption'];
							}

							if ( ! empty( $attachment_data['alt_text'] ) ) {
								$_attachment_args['meta_input']['_wp_attachment_image_alt'] = $attachment_data['alt_text'];
							}

							$update_attachment_id = wp_update_post( $_attachment_args, true );

							if ( is_wp_error( $update_attachment_id ) ) {
								$errors[] = "Failed to update attachment data for {$attachment_data['url']} attachment: " . $update_attachment_id->get_error_message();
							}

							if ( 'content' === $attachment_data['type'] ) {

								$attachment  = '<figure>';
								$attachment .= wp_get_attachment_image( $attachment_id, 'full' );

								$caption = wp_get_attachment_caption( $attachment_id );

								if ( ! empty( $caption ) ) {

									$attachment .= '<figcaption>';
									$attachment .= $caption;
									$attachment .= '</figcaption>';
								}

								$attachment .= '</figure>';

								$post_content = str_replace( "%attachment-{$index}%", $attachment, $post_content );

							} elseif ( empty( $thumbnail_id ) ) {
								$thumbnail_id = $attachment_id;
							}
						}
					}

					if ( $post_content !== $new_post['post_content'] ) {

						$args = [
							'ID'           => $post_id,
							'post_content' => $post_content,
						];

						if ( ! empty( $thumbnail_id ) ) {
							$args['meta_input']['_thumbnail_id'] = $thumbnail_id;
						}

						$update_post_id = wp_update_post( $args, true );

						if ( is_wp_error( $update_post_id ) ) {
							$errors[] = 'Failed to update post content after attachment import: ' . $update_post_id->get_error_message();
						}
					}
				}
			}

			if ( ! empty( $errors ) ) {

				$failed_data[] = [
					'ID'     => $post['nid'],
					'Title'  => $post['title'],
					'Status' => 'Post created, but...',
					'Reason' => join( ', ', $errors ),
				];
			}

			$success++;

			$this->progress_bar->tick();

			if ( 0 === ( $index % 20 ) ) {

				sleep( 3 );
				$this->stop_the_insanity();
			}

			$index++;
		}

		$this->progress_bar->finish();

		$this->stop_the_insanity();

		$message_dry_run = 'are ';
		if ( $this->dry_run ) {
			$message_dry_run = 'will be ';
		}

		\WP_CLI::line();
		\WP_CLI::line( '==========================' );
		\WP_CLI::success( "Total {$success} news {$message_dry_run}migrated successfully!" );

		\WP_CLI::line( '==========================' );
		\WP_CLI::warning( "Total {$failed} news {$message_dry_run}failed to migrate!" );

		if ( ! empty( $failed_data ) ) {

			\WP_CLI::line();
			\WP_CLI::line( '==========================' );

			if ( $this->dry_run ) {

				\WP_CLI\Utils\format_items(
					'table',
					$failed_data,
					[
						'ID',
						'Name',
						'Status',
						'Reason',
					]
				);
			} else {

				$this->start_log(
					[
						'ID',
						'Name',
						'Status',
						'Reason',
					]
				);

				foreach ( $failed_data as $row ) {
					$this->log_row( $row );
				}

				$this->close_log();

				\WP_CLI::line( "Output CSV available at {$this->output_file_path}" );
			}

			\WP_CLI::line( '==========================' );
		}
	}

	/**
	 * Get WP user id from Drupal user id which is already migrated.
	 *
	 * @param int $drupal_user_id Drupal User ID.
	 *
	 * @return int
	 */
	protected function get_post_author( $drupal_user_id ) {

		global $wpdb;

		$user_id = 1;

		if ( ! empty( $drupal_user_id ) ) {

			$wp_user_id = $wpdb->get_var(
				$wpdb->prepare(
					'
				SELECT user_id FROM wp_usermeta
				WHERE meta_key = %s AND meta_value = %d
				',
					"{$this->prefix}uid",
					$drupal_user_id
				)
			);

			$user_id = empty( $wp_user_id ) ? $user_id : $wp_user_id;
		}

		return (int) $user_id;
	}

	/**
	 * Get post slug.
	 *
	 * @param int    $nid    News ID.
	 * @param string $title  News title.
	 * @param string $status News status (post status).
	 *
	 * @return string|null
	 */
	protected function get_post_slug( $nid, $title, $status ) {

		global $wpdb;

		$post_slug = '';

		if ( ! empty( $nid ) ) {

			$post_slug = $wpdb->get_var(
				$wpdb->prepare(
					'
				SELECT alias FROM url_alias
				WHERE source = %s
				',
					"/node/{$nid}"
				)
			);

			// All of the alias contains "/blog/news/" as prefix, removing it.
			if ( '/blog/news/' === substr( $post_slug, 0, 11 ) ) {
				$post_slug = substr( $post_slug, 11 );
			}
		}

		/*
		 * Create a valid post name. Drafts and pending posts are allowed to have
		 * an empty post name.
		 */
		if ( empty( $post_slug ) && ! in_array( $status, array( 'draft', 'pending', 'auto-draft' ), true ) ) {
			$post_slug = sanitize_title( $title );
		}

		return $post_slug;
	}

	/**
	 * Get the content of the news using an ID.
	 *
	 * @param int $nid News ID.
	 *
	 * @return array|object|void|null
	 */
	protected function get_post_content( $nid ) {

		global $wpdb;

		$content_data = [];

		if ( ! empty( $nid ) ) {

			$content_data = $wpdb->get_row(
				$wpdb->prepare(
					"
					SELECT body_value,body_summary FROM node__body
					WHERE entity_id = %d AND bundle = 'news'
					",
					$nid
				),
				ARRAY_A
			);
		}

		return $content_data;
	}

	/**
	 * Get post by the news item id custom field value.
	 *
	 * @since 1.0.0
	 * @param int $id News ID field value.
	 * @return false|\WP_Post WP_Post object or false if the post cannot be found.
	 */
	protected function get_post_by_nid( $id ) {

		$args = array(
			'post_type'  => [ 'post', 'cps-event' ],
			'meta_key'   => $this->prefix . 'nid', // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
			'meta_value' => $id, // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_value
		);

		$query = new \WP_Query( $args );

		if ( ! $query->have_posts() ) {
			return false;
		}

		return $query->posts[0];
	}

	/**
	 * Delete all existing news/events from the database.
	 *
	 * This function is intended to be used for development, not for use on the live site.
	 *
	 * Important: Only posts with that have a _legacy_cps_nid meta field will be deleted.
	 *            It is expected that any posts added manually via other means than
	 *            migrating from the DB do not have this field set.
	 *
	 * @since 1.0.0
	 */
	protected function delete_all_posts() {

		global $wpdb;

		$t1 = $wpdb->posts;
		$t2 = $wpdb->postmeta;

		$querystr = $wpdb->prepare(
			"SELECT COUNT( * ) FROM {$t1} INNER JOIN {$t2} ON ( {$t1}.ID = {$t2}.post_id ) WHERE 1=1 AND ( {$t2}.meta_key = %s ) AND ($t1.post_type = 'post' OR $t1.post_type = 'cps-event')", // phpcs:ignore
			$this->prefix . 'nid'
		);

		$count = $wpdb->get_var( $querystr ); // phpcs:ignore

		if ( empty( $count ) ) {
			\WP_CLI::line( 'Setup mode: No existing posts found.' );
			return;
		}

		$querystr = $querystr . " AND post_status IN ('future','publish','private','draft')";

		$count_message = $wpdb->get_var( $querystr ); // phpcs:ignore
		$message       = $this->dry_run ? 'Dry run: ' : 'Setup mode: ';
		$message      .= empty( $count_message ) ? 'Removing existing posts:' : 'Removing ' . $count_message . ' posts:';

		$this->progress_bar = \WP_CLI\Utils\make_progress_bar(
			$message,
			$count
		);

		$post_stati   = get_post_stati( array( 'exclude_from_search' => false ) );
		$post_stati[] = 'auto-draft';

		$args = array(
			'post_type'              => array( 'post', 'cps-event' ),
			'post_status'            => $post_stati,
			'posts_per_page'         => 100,
			'fields'                 => 'ids',
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'meta_query'             => array(
				array(
					'key'     => $this->prefix . 'nid',
					'compare' => 'EXISTS',
				),
			),
		);

		$query = new \WP_Query( $args );

		while ( $query->have_posts() ) {

			$post_ids = $query->posts;

			foreach ( $post_ids as $post_id ) {

				if ( ! $this->dry_run ) {
					wp_delete_post( $post_id, true );
				}

				$this->progress_bar->tick();
			}

			sleep( 3 );
			$this->stop_the_insanity();

			$query = new \WP_Query( $args );
		}

		$this->progress_bar->finish();
	}

	/**
	 * Replace Drupal attachment with WP attachments in post content.
	 *
	 * @param int    $nid          News ID.
	 * @param string $post_content Post content.
	 *
	 * @return array
	 */
	protected function handle_attachment( $nid, $post_content ) {

		$all_attachments = [];

		preg_match_all( '/<drupal-entity.*?data-entity-uuid="(.*?)".*?><\/drupal-entity>/sm', $post_content, $matches );

		$uuids    = [];
		$entities = [];

		if ( ! empty( $matches[0] ) && ! empty( $matches[1] ) ) {

			foreach ( $matches[1] as $uuid ) {
				$uuids[] = $this->get_file_data( $uuid, 'uuid' );
			}

			$entities = $matches[0];
		}

		preg_match_all( '/<img.*?src=\"(.*?)\".*?>/sm', $post_content, $matches );

		if ( ! empty( $matches[0] ) && ! empty( $matches[1] ) ) {

			foreach ( $matches[1] as $src ) {

				$filename = basename( $src );
				$uuids[]  = $this->get_file_data( $filename, 'filename' );
			}

			$entities = array_merge( $entities, $matches[0] );
		}

		$all_attachments['content'] = [
			'uuids'    => $uuids,
			'entities' => $entities,
		];

		$thumbnail_file_data = $this->get_thumbnail_file_data( $nid );

		$all_attachments['thumbnail'] = [
			'uuids' => [ $this->get_file_data( $thumbnail_file_data['uuid'], 'uuid' ) ],
			'mids'  => [ $thumbnail_file_data['mid'] ],
		];

		$attachments = [];
		$index       = 0;

		foreach ( $all_attachments as $type => $attachment_data ) {

			if ( empty( $attachment_data['uuids'] ) ) {
				continue;
			}

			foreach ( $attachment_data['uuids'] as $i => $file_data ) {

				if ( empty( $file_data ) || empty( $file_data['uri'] ) ) {
					continue;
				}

				$ext = pathinfo( $file_data['uri'], PATHINFO_EXTENSION );

				if ( ! $this->is_extension_allowed_as_attachment( $ext ) ) {
					continue;
				}

				// Get attachment url.
				$attachment_url = $this->get_attachment_url( $file_data );

				if ( empty( $attachment_url ) ) {
					continue;
				}

				$attachments[ $index ]['url'] = $attachment_url;

				// Get file title.
				$title = $this->get_file_title( $file_data['fid'] );

				$attachments[ $index ]['title'] = empty( $title ) ? pathinfo( $file_data['filename'], PATHINFO_FILENAME ) : $title;

				if ( 'content' === $type ) {

					// Get caption text if present.
					preg_match( '/.*?data-caption="(.*?)"/sm', $attachment_data['entities'][ $i ], $caption );

					$caption_text = empty( $caption[1] ) ? '' : $caption[1];

					if ( empty( $caption_text ) ) {
						$caption_text = $this->get_file_caption( $file_data['fid'] );
					}

					// Get alt text if present.
					preg_match( '/.*?alt="(.*?)"/sm', $attachment_data['entities'][ $i ], $alt );

					$alt_text = empty( $alt[1] ) ? '' : $alt[1];

					if ( empty( $alt_text ) ) {
						$alt_text = $this->get_file_alt_text( $file_data['fid'] );
					}

					/*
					 * Temporary replace the attachment with "%attachment-{$index}%", later after
					 * the attachment successfully uploaded in WP, replace its markup.
					 */
					$post_content = str_replace( $attachment_data['entities'][ $i ], "%attachment-{$index}%", $post_content );

				} else {

					$caption_text = $this->get_media_caption( $attachment_data['mids'][ $i ] );
					$alt_text     = $this->get_media_alt_text( $attachment_data['mids'][ $i ] );
				}

				if ( ! empty( $caption_text ) ) {
					$attachments[ $index ]['caption'] = $caption_text;
				}

				if ( ! empty( $alt_text ) ) {
					$attachments[ $index ]['alt_text'] = $alt_text;
				}

				$attachments[ $index ]['type'] = $type;

				$index++;
			}
		}

		return [
			'attachments' => $attachments,
			'content'     => $post_content,
		];
	}

	/**
	 * Get file data using uuid.
	 *
	 * @param string $value Value of the file field.
	 * @param string $field Field name (DB column).
	 *
	 * @return array|object|void|null
	 */
	protected function get_file_data( $value, $field ) {

		global $wpdb;

		return $wpdb->get_row(
			$wpdb->prepare(
				"
				SELECT * FROM file_managed
				WHERE {$field} = %s
				",
				$value
			),
			ARRAY_A
		);
	}

	/**
	 * Allowed extensions for media upload.
	 */
	protected function allowed_extensions() {

		$allowed_extensions = array_keys( get_allowed_mime_types() );

		foreach ( $allowed_extensions as $extension ) {
			$this->extensions = array_merge( $this->extensions, explode( '|', $extension ) );
		}
	}

	/**
	 * Check for extension whether it is allowed as an attachment in Media Library.
	 *
	 * @param string $ext File extension.
	 *
	 * @return bool
	 */
	protected function is_extension_allowed_as_attachment( $ext ) {

		if ( in_array( strtolower( $ext ), $this->extensions, true ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Get attachment URL.
	 *
	 * @param array $file_data File data.
	 *
	 * @return string
	 */
	protected function get_attachment_url( $file_data ) {

		$file_url = $this->get_exact_file_url( $file_data['uri'] );

		// Checking if attachment exists on remote URL, if not then bail out..
		if ( ! $this->check_url_exists( $file_url ) ) {
			return '';
		}

		return $file_url;
	}

	/**
	 * Check if attachment exists.
	 *
	 * @param string $url URL to check.
	 *
	 * @return bool
	 */
	public function check_url_exists( $url ) {

		$headers = @get_headers( $url ); // phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged

		return stripos( $headers[0], '404' ) ? false : true;
	}

	/**
	 * Get exact file url.
	 *
	 * @param string $file_url File URL (in Drupal format).
	 *
	 * @return string
	 */
	protected function get_exact_file_url( $file_url ) {

		if ( 'public:/' === substr( $file_url, 0, 8 ) ) {
			$file_url = substr( $file_url, 8 );
		}

		return $this->file_url_prefix . $file_url;
	}
	/**
	 * Check if the attachment is already exist.
	 * If yes, return it, else create it.
	 *
	 * @param int    $post_id    Post ID.
	 * @param string $attachment_url Image URL.
	 *
	 * @return int|string|\WP_Error
	 */
	protected function get_attachment_id( $post_id, $attachment_url ) {

		global $wpdb;

		$attachment_id = $wpdb->get_var(
			$wpdb->prepare(
				"
				SELECT post_id FROM {$wpdb->prefix}postmeta
				WHERE meta_key = %s AND meta_value = %s
				",
				"{$this->prefix}original_url",
				$attachment_url
			)
		);

		if ( ! empty( $attachment_id ) ) {
			return $attachment_id;
		}

		require_once ABSPATH . 'wp-admin/includes/media.php';
		require_once ABSPATH . 'wp-admin/includes/file.php';
		require_once ABSPATH . 'wp-admin/includes/image.php';

		if ( preg_match( '/[^\?]+\.(jpe?g|jpe|JPG|gif|png)\b/i', $attachment_url, $matches ) ) {
			return media_sideload_image( $attachment_url, $post_id, '', 'id' );
		} else {

			$file_array         = array();
			$file_array['name'] = wp_basename( $attachment_url );

			// Download file to temp location.
			$file_array['tmp_name'] = download_url( $attachment_url );

			// If error storing temporarily, return the error.
			if ( is_wp_error( $file_array['tmp_name'] ) ) {
				return $file_array['tmp_name'];
			}

			// Do the validation and storage stuff.
			return media_handle_sideload( $file_array, $post_id );
		}
	}

	/**
	 * Get file title.
	 *
	 * @param int $file_id File ID.
	 *
	 * @return string|null
	 */
	protected function get_file_title( $file_id ) {

		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_image_title_text_value FROM file__field_image_title_text
				WHERE entity_id = %d
				',
				$file_id
			)
		);
	}

	/**
	 * Get file caption value.
	 *
	 * @param int $file_id File ID.
	 *
	 * @return string|null
	 */
	protected function get_file_caption( $file_id ) {

		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_caption_value FROM file__field_caption
				WHERE entity_id = %d
				',
				$file_id
			)
		);
	}

	/**
	 * Get media caption value.
	 *
	 * @param int $media_id Media ID.
	 *
	 * @return string|null
	 */
	protected function get_media_caption( $media_id ) {

		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_caption_value FROM media__field_caption
				WHERE entity_id = %d
				',
				$media_id
			)
		);
	}

	/**
	 * Get file alt text value.
	 *
	 * @param int $file_id File ID.
	 *
	 * @return string|null
	 */
	protected function get_file_alt_text( $file_id ) {

		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_image_alt_text_value FROM file__field_image_alt_text
				WHERE entity_id = %d
				',
				$file_id
			)
		);
	}

	/**
	 * Get media alt text value.
	 *
	 * @param int $media_id Media ID.
	 *
	 * @return string|null
	 */
	protected function get_media_alt_text( $media_id ) {

		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_image_alt FROM media__field_image
				WHERE entity_id = %d
				',
				$media_id
			)
		);
	}

	/**
	 * Get blog authors of the post.
	 *
	 * @param int $nid News ID.
	 *
	 * @return array|object|null
	 */
	protected function get_blog_authors( $nid ) {

		global $wpdb;

		return $wpdb->get_results(
			$wpdb->prepare(
				"
				SELECT taxonomy_term_field_data.name FROM taxonomy_term_field_data
				LEFT JOIN node__field_authors_ref ON taxonomy_term_field_data.tid = node__field_authors_ref.field_authors_ref_target_id
				WHERE taxonomy_term_field_data.vid = 'blog_authors' AND node__field_authors_ref.entity_id = %d AND node__field_authors_ref.bundle = 'news'
				",
				$nid
			),
			ARRAY_A
		);
	}

	/**
	 * Get title override value.
	 *
	 * @param int $nid News ID.
	 *
	 * @return string|null
	 */
	protected function get_title_override( $nid ) {

		global $wpdb;

		return $wpdb->get_var(
			$wpdb->prepare(
				"
				SELECT field_display_title_override_value FROM node__field_display_title_override
				WHERE entity_id = %d AND bundle = 'news'
				",
				$nid
			)
		);
	}

	/**
	 * Get media (featured image) data of the news.
	 *
	 * @param int $nid News ID.
	 *
	 * @return array|null
	 */
	protected function get_thumbnail_file_data( $nid ) {

		global $wpdb;

		return $wpdb->get_row(
			$wpdb->prepare(
				"
				SELECT file_managed.uuid, media_field_data.mid FROM file_managed
				LEFT JOIN media_field_data ON file_managed.fid = media_field_data.thumbnail__target_id
				LEFT JOIN node__field_image ON media_field_data.mid = node__field_image.field_image_target_id
				WHERE node__field_image.entity_id = %d AND node__field_image.bundle = 'news'
				",
				$nid
			),
			ARRAY_A
		);
	}

	/**
	 * Append paragraph blocks in content.
	 *
	 * @param int $nid News ID.
	 *
	 * @return string
	 */
	protected function get_block_content( $nid ) {

		global $wpdb;

		$block_content = '';

		$block_data = $wpdb->get_results(
			$wpdb->prepare(
				"
				SELECT node__field_paragraphs.field_paragraphs_target_id, paragraphs_item_field_data.type FROM paragraphs_item_field_data
				LEFT JOIN node__field_paragraphs ON paragraphs_item_field_data.id = node__field_paragraphs.field_paragraphs_target_id
				WHERE node__field_paragraphs.entity_id = %d AND node__field_paragraphs.bundle = 'news'
				ORDER BY node__field_paragraphs.delta ASC
				",
				$nid
			),
			ARRAY_A
		);

		if ( empty( $block_data ) ) {
			return $block_content;
		}

		foreach ( $block_data as $block ) {

			switch ( trim( $block['type'] ) ) {

				case 'text_interrupt':
				case 'content_columns':
					$block_content .= $this->get_columns_block( $block );

					break;

				case 'content_aside':
					$block_content .= $this->get_content_aside_block( $block );

					break;

				case 'text_block':
					$block_content .= $this->get_text_block( $block );

					break;
			}
		}

		return $block_content;
	}

	/**
	 * Generate HTML markup for text_interrupt paragraph type.
	 *
	 * @param array $block_data Paragraph block data, like id, type.
	 *
	 * @return string
	 */
	protected function get_columns_block( $block_data ) {

		global $wpdb;

		$block_content = '';

		$columns = $wpdb->get_results(
			$wpdb->prepare(
				'
				SELECT titled_text_field_data.title, titled_text__field_body.field_body_value FROM titled_text_field_data
				LEFT JOIN paragraph__field_column_reference ON titled_text_field_data.id = paragraph__field_column_reference.field_column_reference_target_id
				LEFT JOIN titled_text__field_body ON titled_text_field_data.id = titled_text__field_body.entity_id
				WHERE paragraph__field_column_reference.bundle = %s AND paragraph__field_column_reference.entity_id = %d
				ORDER BY paragraph__field_column_reference.delta ASC
				',
				$block_data['type'],
				$block_data['field_paragraphs_target_id']
			),
			ARRAY_A
		);

		if ( empty( $columns ) ) {
			return $block_content;
		}

		$col_count = count( $columns );

		if ( 'text_interrupt' === $block_data['type'] ) {

			$block_content .= '<div class="callout-group color-inversion">';
			$block_content .= '<div class="inner">';

		} else {
			$block_content .= '<div class="callout-group">';
		}

		$block_content .= '<div class="row small-up-1 medium-up-' . min( 2, $col_count ) . ' large-up-' . min( 3, $col_count ) . '">';

		foreach ( $columns as $column ) {

			$block_content .= '<div class="column">';

			if ( 'text_interrupt' === $block_data['type'] ) {
				$block_content .= sprintf( '<h3 class="small">%s</h3>', $column['title'] );
			} else {
				$block_content .= sprintf( '<p>%s</p>', $column['title'] );
			}

			$block_content .= $column['field_body_value'];
			$block_content .= '</div>';
		}

		$block_content .= '</div>';

		if ( 'text_interrupt' === $block_data['type'] ) {
			$block_content .= '</div>';
		}

		$block_content .= '</div>';

		return $block_content;
	}

	/**
	 * Generate HTML markup for content_aside paragraph type.
	 *
	 * @param array $block_data Paragraph block data, like id, type.
	 *
	 * @return string
	 */
	protected function get_content_aside_block( $block_data ) {

		global $wpdb;

		$block_content = '';

		$aside = $wpdb->get_row(
			$wpdb->prepare(
				'
				SELECT paragraph__field_title.field_title_value, paragraph__field_body.field_body_value, paragraph__field_aside.field_aside_value FROM paragraph__field_title
				LEFT JOIN paragraph__field_body ON paragraph__field_title.entity_id = paragraph__field_body.entity_id
				LEFT JOIN paragraph__field_aside ON paragraph__field_title.entity_id = paragraph__field_aside.entity_id
				WHERE paragraph__field_title.bundle = %s AND paragraph__field_title.entity_id = %d
				',
				$block_data['type'],
				$block_data['field_paragraphs_target_id']
			),
			ARRAY_A
		);

		if ( empty( $aside ) ) {
			return $block_content;
		}

		$block_content .= '<div class="row">';
		$block_content .= '<div class="small-12 medium-7 columns">';

		if ( ! empty( $aside['field_title_value'] ) ) {
			$block_content .= sprintf( '<h2>%s</h2>', $aside['field_title_value'] );
		}

		if ( ! empty( $aside['field_body_value'] ) ) {
			$block_content .= $aside['field_body_value'];
		}

		$block_content .= '</div>';
		$block_content .= '<div class="small-12 medium-5 columns">';

		if ( ! empty( $aside['field_aside_value'] ) ) {
			$block_content .= $aside['field_aside_value'];
		}

		$block_content .= '</div>';
		$block_content .= '</div>';

		return $block_content;
	}

	/**
	 * Generate HTML markup for text_block paragraph type.
	 *
	 * @param array $block_data Paragraph block data, like id, type.
	 *
	 * @return string
	 */
	protected function get_text_block( $block_data ) {

		global $wpdb;

		$block_content = '';

		$title = $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_title_value FROM paragraph__field_title
				WHERE bundle = %s AND entity_id = %d
				',
				$block_data['type'],
				$block_data['field_paragraphs_target_id']
			)
		);

		if ( ! empty( $title ) ) {
			$block_content .= '<h2 id="' . str_replace( ' ', '-', strtolower( $title ) ) . '">' . $title . '</h2>';
		}

		$text = $wpdb->get_var(
			$wpdb->prepare(
				'
				SELECT field_body_value FROM paragraph__field_body
				WHERE bundle = %s AND entity_id = %d
				',
				$block_data['type'],
				$block_data['field_paragraphs_target_id']
			)
		);

		if ( ! empty( $text ) ) {
			$block_content .= $text;
		}

		return $block_content;
	}
}

\WP_CLI::add_command( 'migrate news', __NAMESPACE__ . '\MigrateNews' );
