<?php
/**
 * An abstract command for WP CLI scripts to implement.
 *
 * @package CPSMigration
 */

namespace CPSMigration\CLI;

/**
 * Abstract class for WP CLI scripts.
 *
 * @package CPSMigration\CLI
 */
abstract class AbstractCommand extends \WP_CLI_Command {

	/**
	 * Arguments for the command.
	 *
	 * @var array
	 */
	public $args = [];

	/**
	 * Associative arguments for the command.
	 *
	 * @var array
	 */
	public $assoc_args = [];

	/**
	 * Are we performing a dry run.
	 *
	 * @var bool
	 */
	protected $dry_run;

	/**
	 * Holds the WP CLI progress bar.
	 *
	 * @var \WP_CLI\Progress\Bar
	 */
	public $progress_bar = null;

	/**
	 * Authors.
	 *
	 * @var array
	 */
	public $authors = [];

	/**
	 * Prefix to store old data in meta key.
	 *
	 * @var array
	 */
	public $prefix = '_legacy_cps_';

	/**
	 * The file path to write command results to.
	 *
	 * @var string
	 */
	public $output_file_path = '';

	/**
	 * The file pointer resource to write command results to.
	 *
	 * @var resource|null
	 */
	public $output_file_handle = null;

	/**
	 * Open a file handle for logging.
	 *
	 * @param array $column_headers Array of column headers for the CSV log.
	 *
	 * @return void
	 */
	public function start_log( $column_headers = array() ) {

		if ( empty( $this->output_file_path ) ) {
			return;
		}

		$this->output_file_handle = fopen( $this->output_file_path, 'w' ); // phpcs:ignore WordPress.WP.AlternativeFunctions.file_system_read_fopen

		if ( ! empty( $column_headers ) && is_Array( $column_headers ) ) {
			fputcsv( $this->output_file_handle, $column_headers );
		}
	}

	/**
	 * Close the current log file handle.
	 *
	 * @return void
	 */
	public function close_log() {

		if ( is_resource( $this->output_file_handle ) ) {
			fclose( $this->output_file_handle ); // phpcs:ignore WordPress.WP.AlternativeFunctions.file_system_read_fclose
		}
	}

	/**
	 * Log the import status for a location.
	 *
	 * @param array $row_data The row date for the CSV log.
	 *
	 * @return void
	 */
	public function log_row( $row_data ) {

		if ( true !== is_resource( $this->output_file_handle ) ) {
			return;
		}

		fputcsv( $this->output_file_handle, $row_data );
	}

	/**
	 *  Clear all of the caches for memory management
	 */
	protected function stop_the_insanity() {

		global $wpdb, $wp_object_cache;

		$wpdb->queries = [];

		if ( ! is_object( $wp_object_cache ) ) {
			return;
		}

		$wp_object_cache->group_ops      = [];
		$wp_object_cache->stats          = [];
		$wp_object_cache->memcache_debug = [];
		$wp_object_cache->cache          = [];

		if ( is_callable( $wp_object_cache, '__remoteset' ) ) {
			$wp_object_cache->__remoteset(); // important.
		}
	}

	/**
	 * Timestamp to date-time conversion in GMT & America/New_York timezone conversion.
	 *
	 * @param int    $timestamp Timestamp.
	 * @param string $format    Date format in which date-time needed to be converted, default Y-m-d H:i:s.
	 *
	 * @return array
	 *
	 * @throws \Exception Emits Exception in case of an error
	 */
	protected function get_exact_date( $timestamp, $format = 'Y-m-d H:i:s' ) {

		$exact_date = [];

		// Convert timestamp to date.
		$date = date( 'Y-m-d H:i:s', $timestamp ); // phpcs:ignore WordPress.DateTime.RestrictedFunctions.date_date

		// Site timezone.
		$site_timezone = new \DateTimeZone( 'America/New_York' );

		// GMT timezone.
		$gmt_timezone = new \DateTimeZone( 'GMT' );

		// Create date using GMT timezone.
		$date_time = new \DateTime( $date, $gmt_timezone );

		$exact_date['gmt'] = $date_time->format( $format );

		// Get offset of the site timezone compared to GMT timezone.
		$offset = $site_timezone->getOffset( $date_time );

		// Add site timezone offset.
		$interval = \DateInterval::createFromDateString( (string) $offset . 'seconds' );

		$date_time->add( $interval );

		$exact_date['site'] = $date_time->format( $format );

		return $exact_date;
	}

	/**
	 * Create item meta based on the type.
	 *
	 * @param string $type            Whether it is a post meta or user meta.
	 * @param int    $created_item_id Newly created item ID.
	 * @param array  $item            Item array.
	 * @param array  $ignore_meta     Meta which are already used and not be needed
	 * @param array  $individual_meta Meta which will be inserted individually, which will help later.
	 */
	protected function process_meta( $type, $created_item_id, $item, $ignore_meta, $individual_meta ) {

		foreach ( $item as $key => $value ) {

			if ( in_array( $key, $ignore_meta, true ) ) {
				unset( $item[ $key ] );
			}

			if ( in_array( $key, $individual_meta, true ) ) {

				$_meta_key = "{$this->prefix}{$key}";

				if ( 'user' === $type ) {
					update_user_meta( $created_item_id, $_meta_key, $value );
				} elseif ( 'post' === $type ) {
					update_post_meta( $created_item_id, $_meta_key, $value );
				}

				unset( $item[ $key ] );
			}
		}

		if ( ! empty( $item ) ) {

			$_meta_key = "{$this->prefix}data";

			if ( 'user' === $type ) {
				update_user_meta( $created_item_id, $_meta_key, $item );
			} elseif ( 'post' === $type ) {
				update_post_meta( $created_item_id, $_meta_key, $item );
			}
		}
	}
}
