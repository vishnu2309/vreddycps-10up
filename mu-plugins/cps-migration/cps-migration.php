<?php
/**
 * Plugin Name: CPS Migration
 * Plugin URI: https://cps.northeastern.edu/
 * Description: Migration plugin for CPS.
 * Version:     1.0.0
 * Author:      10up
 * Author URI:  https://10up.com
 * Text Domain: cps-migration
 * Domain Path: /languages
 *
 * @package CPSMigration
 */

namespace CPSMigration;

// Useful global constants.
if ( ! defined( 'CPS_MIGRATION_VERSION' ) ) {
	define( 'CPS_MIGRATION_VERSION', '1.1.0' );
}

if ( ! defined( 'CPS_MIGRATION_URL' ) ) {
	define( 'CPS_MIGRATION_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'CPS_MIGRATION_PATH' ) ) {
	define( 'CPS_MIGRATION_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'CPS_MIGRATION_INC' ) ) {
	define( 'CPS_MIGRATION_INC', CPS_MIGRATION_PATH . 'includes/' );
}

// Include files.
require_once CPS_MIGRATION_INC . 'wp-cli/core.php';

// Bootstrap.
\CPSMigration\CLI\setup();
