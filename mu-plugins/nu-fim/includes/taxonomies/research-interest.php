<?php
/**
 * "Research Interest" custom taxonomy.
 *
 * The "Research Interest" taxonomy is used to categorize faculty members.
 *
 * @package NU_FIM
 */

namespace NU_FIM\Taxonomy\ResearchInterest;

use \NU_FIM\Helpers as Helpers;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from research interest meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Research Interests', 'nu-fim' ),
		'singular_name'              => esc_html__( 'Research Interest', 'nu-fim' ),
		'search_items'               => esc_html__( 'Search Research Interests', 'nu-fim' ),
		'popular_items'              => esc_html__( 'Research Interests', 'nu-fim' ),
		'all_items'                  => esc_html__( 'All Research Interests', 'nu-fim' ),
		'parent_item'                => esc_html__( 'Parent Research Interest', 'nu-fim' ),
		'parent_item_colon'          => esc_html__( 'Parent Research Interest:', 'nu-fim' ),
		'edit_item'                  => esc_html__( 'Edit Research Interest', 'nu-fim' ),
		'view_item'                  => esc_html__( 'View Research Interest', 'nu-fim' ),
		'update_item'                => esc_html__( 'Update Research Interest', 'nu-fim' ),
		'add_new_item'               => esc_html__( 'Add New Research Interest', 'nu-fim' ),
		'new_item_name'              => esc_html__( 'New Research Interest', 'nu-fim' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'nu-fim' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove research interests', 'nu-fim' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used research interests', 'nu-fim' ),
		'not_found'                  => esc_html__( 'No research interests found', 'nu-fim' ),
		'no_terms'                   => esc_html__( 'No research interests', 'nu-fim' ),
		'items_list_navigation'      => esc_html__( 'Research Interests', 'nu-fim' ),
		'items_list'                 => esc_html__( 'Research Interests', 'nu-fim' ),
		'most_used'                  => esc_html__( 'Most Used', 'nu-fim' ),
		'back_to_items'              => esc_html__( 'Back to research interests', 'nu-fim' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'Research interests are used to categorize faculty members.', 'nu-fim' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_ui'            => false,
		'show_in_menu'       => false,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => false,
		'rest_base'          => 'research-interest',
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => false,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'research-interest',
		// Assigning 'thisisnotused' to disable managing terms.
		'capabilities'       => array(
			'manage_terms' => 'thisisnotused',
			'edit_terms'   => 'thisisnotused',
			'delete_terms' => 'thisisnotused',
			'assign_terms' => 'thisisnotused',
		),
	);

	/**
	 * Fires before registering the research interst taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @var array $args Array of arguments for registering the taxonomy/
	 */
	$args = apply_filters( 'nu_fim_research_interest_args', $args );

	register_taxonomy(
		'nu_fim_research_interest',
		Helpers\get_faculty_post_type(),
		$args
	);
}

/**
 * Hide parent select from research interest meta box.
 *
 * The nu_fim_research_interest is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();
	$post_type      = Helpers\get_faculty_post_type();

	if ( 'post' === $current_screen->base && $post_type === $current_screen->post_type ) {
		echo '<style>#newnu_fim_research_interest_parent{display:none;}</style>';
	}
}
