<?php
/**
 * "Secondary College" custom taxonomy.
 *
 * The "Secondary College" taxonomy is used to categorize faculty members.
 *
 * @package NU_FIM
 */

namespace NU_FIM\Taxonomy\SecondaryCollege;

use \NU_FIM\Helpers as Helpers;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from secondary college meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Secondary Colleges', 'nu-fim' ),
		'singular_name'              => esc_html__( 'Secondary College', 'nu-fim' ),
		'search_items'               => esc_html__( 'Search Secondary Colleges', 'nu-fim' ),
		'popular_items'              => esc_html__( 'Secondary Colleges', 'nu-fim' ),
		'all_items'                  => esc_html__( 'All Secondary Colleges', 'nu-fim' ),
		'parent_item'                => esc_html__( 'Parent Secondary College', 'nu-fim' ),
		'parent_item_colon'          => esc_html__( 'Parent Secondary College:', 'nu-fim' ),
		'edit_item'                  => esc_html__( 'Edit Secondary College', 'nu-fim' ),
		'view_item'                  => esc_html__( 'View Secondary College', 'nu-fim' ),
		'update_item'                => esc_html__( 'Update Secondary College', 'nu-fim' ),
		'add_new_item'               => esc_html__( 'Add New Secondary College', 'nu-fim' ),
		'new_item_name'              => esc_html__( 'New Secondary College', 'nu-fim' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'nu-fim' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove secondary colleges', 'nu-fim' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used secondary colleges', 'nu-fim' ),
		'not_found'                  => esc_html__( 'No secondary colleges found', 'nu-fim' ),
		'no_terms'                   => esc_html__( 'No secondary colleges', 'nu-fim' ),
		'items_list_navigation'      => esc_html__( 'Secondary Colleges', 'nu-fim' ),
		'items_list'                 => esc_html__( 'Secondary Colleges', 'nu-fim' ),
		'most_used'                  => esc_html__( 'Most Used', 'nu-fim' ),
		'back_to_items'              => esc_html__( 'Back to secondary colleges', 'nu-fim' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'Secondary colleges are used to categorize faculty members.', 'nu-fim' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_ui'            => false,
		'show_in_menu'       => false,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => false,
		'rest_base'          => 'secondary-college',
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => false,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'secondary-college',
		// Assigning 'thisisnotused' to disable managing terms.
		'capabilities'       => array(
			'manage_terms' => 'thisisnotused',
			'edit_terms'   => 'thisisnotused',
			'delete_terms' => 'thisisnotused',
			'assign_terms' => 'thisisnotused',
		),
	);

	/**
	 * Fires before registering the secondary colleges taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @var array $args Array of arguments for registering the taxonomy/
	 */
	$args = apply_filters( 'nu_fim_secondary_college_args', $args );

	register_taxonomy(
		'nu_fim_secondary_college',
		Helpers\get_faculty_post_type(),
		$args
	);
}

/**
 * Hide parent select from secondary college meta box.
 *
 * The nu_fim_secondary_college is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();
	$post_type      = Helpers\get_faculty_post_type();

	if ( 'post' === $current_screen->base && $post_type === $current_screen->post_type ) {
		echo '<style>#newnu_fim_secondary_college_parent{display:none;}</style>';
	}
}
