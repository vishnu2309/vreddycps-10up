<?php
/**
 * "Areas of Expertise" custom taxonomy.
 *
 * The "Areas of Expertise" taxonomy is used to categorize faculty members
 * via the FIM API.
 *
 * @package NU_FIM
 */

namespace NU_FIM\Taxonomy\AreasExpertise;

use \NU_FIM\Helpers as Helpers;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from areas of expertise meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Areas of Expertise', 'nu-fim' ),
		'singular_name'              => esc_html__( 'Area of Expertise', 'nu-fim' ),
		'search_items'               => esc_html__( 'Search Areas of Expertise', 'nu-fim' ),
		'popular_items'              => esc_html__( 'Popular Areas of Expertise', 'nu-fim' ),
		'all_items'                  => esc_html__( 'All Areas of Expertise', 'nu-fim' ),
		'parent_item'                => esc_html__( 'Parent Area of Expertise', 'nu-fim' ),
		'parent_item_colon'          => esc_html__( 'Parent Area of Expertise:', 'nu-fim' ),
		'edit_item'                  => esc_html__( 'Edit Area of Expertise', 'nu-fim' ),
		'view_item'                  => esc_html__( 'View Area of Expertise', 'nu-fim' ),
		'update_item'                => esc_html__( 'Update Area of Expertise', 'nu-fim' ),
		'add_new_item'               => esc_html__( 'Add New Area of Expertise', 'nu-fim' ),
		'new_item_name'              => esc_html__( 'New Area of Expertise', 'nu-fim' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'nu-fim' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove areas of expertise', 'nu-fim' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used areas of expertise', 'nu-fim' ),
		'not_found'                  => esc_html__( 'No areas of expertise found', 'nu-fim' ),
		'no_terms'                   => esc_html__( 'No areas of expertise', 'nu-fim' ),
		'items_list_navigation'      => esc_html__( 'Areas of Expertise', 'nu-fim' ),
		'items_list'                 => esc_html__( 'Areas of Expertise', 'nu-fim' ),
		'most_used'                  => esc_html__( 'Most Used', 'nu-fim' ),
		'back_to_items'              => esc_html__( 'Back to areas of expertise', 'nu-fim' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'The area of expertise is used to categorize faculty members via the FIM API.', 'nu-fim' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => true,
		'rest_base'          => 'areas-expertise',
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => false,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'areas-expertise',
		// Assigning 'thisisnotused' to disable managing terms.
		'capabilities'       => array(
			'manage_terms' => 'thisisnotused',
			'edit_terms'   => 'thisisnotused',
			'delete_terms' => 'thisisnotused',
			'assign_terms' => 'thisisnotused',
		),
	);

	/**
	 * Fires before registering the area of expertise taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @var array $args Array of arguments for registering the taxonomy/
	 */
	$args = apply_filters( 'nu_fim_areas_expertise_args', $args );

	register_taxonomy(
		'nu_fim_areas_expertise',
		Helpers\get_faculty_post_type(),
		$args
	);
}

/**
 * Hide parent select from area of expertise meta box.
 *
 * The nu_fim_areas_expertise is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();
	$post_type      = Helpers\get_faculty_post_type();

	if ( 'post' === $current_screen->base && $post_type === $current_screen->post_type ) {
		echo '<style>#newnu_fim_areas_expertise_parent{display:none;}</style>';
	}
}
