<?php
/**
 * "Secondary Department" custom taxonomy.
 *
 * The "Secondary Department" taxonomy is used to categorize faculty members.
 *
 * @package NU_FIM
 */

namespace NU_FIM\Taxonomy\SecondaryDepartment;

use \NU_FIM\Helpers as Helpers;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from secondary department meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Secondary Departments', 'nu-fim' ),
		'singular_name'              => esc_html__( 'Secondary Department', 'nu-fim' ),
		'search_items'               => esc_html__( 'Search Secondary Departments', 'nu-fim' ),
		'popular_items'              => esc_html__( 'Secondary Departments', 'nu-fim' ),
		'all_items'                  => esc_html__( 'All Secondary Departments', 'nu-fim' ),
		'parent_item'                => esc_html__( 'Parent Secondary Department', 'nu-fim' ),
		'parent_item_colon'          => esc_html__( 'Parent Secondary Department:', 'nu-fim' ),
		'edit_item'                  => esc_html__( 'Edit Secondary Department', 'nu-fim' ),
		'view_item'                  => esc_html__( 'View Secondary Department', 'nu-fim' ),
		'update_item'                => esc_html__( 'Update Secondary Department', 'nu-fim' ),
		'add_new_item'               => esc_html__( 'Add New Secondary Department', 'nu-fim' ),
		'new_item_name'              => esc_html__( 'New Secondary Department', 'nu-fim' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'nu-fim' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove secondary departments', 'nu-fim' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used secondary departments', 'nu-fim' ),
		'not_found'                  => esc_html__( 'No secondary departments found', 'nu-fim' ),
		'no_terms'                   => esc_html__( 'No secondary departments', 'nu-fim' ),
		'items_list_navigation'      => esc_html__( 'Secondary Departments', 'nu-fim' ),
		'items_list'                 => esc_html__( 'Secondary Departments', 'nu-fim' ),
		'most_used'                  => esc_html__( 'Most Used', 'nu-fim' ),
		'back_to_items'              => esc_html__( 'Back to secondary departments', 'nu-fim' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'Secondary departments are used to categorize faculty members.', 'nu-fim' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => false,
		'show_ui'            => false,
		'show_in_menu'       => false,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => true,
		'rest_base'          => 'secondary-department',
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => false,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'secondary-department',
		// Assigning 'thisisnotused' to disable managing terms.
		'capabilities'       => array(
			'manage_terms' => 'thisisnotused',
			'edit_terms'   => 'thisisnotused',
			'delete_terms' => 'thisisnotused',
			'assign_terms' => 'thisisnotused',
		),
	);

	/**
	 * Fires before registering the secondary department taxonomy.
	 *
	 * @since 1.0.0
	 *
	 * @var array $args Array of arguments for registering the taxonomy/
	 */
	$args = apply_filters( 'nu_fim_secondary_department', $args );

	register_taxonomy(
		'nu_fim_secondary_department',
		Helpers\get_faculty_post_type(),
		$args
	);
}

/**
 * Hide parent select from secondary department meta box.
 *
 * The nu_fim_secondary_department is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();
	$post_type      = Helpers\get_faculty_post_type();

	if ( 'post' === $current_screen->base && $post_type === $current_screen->post_type ) {
		echo '<style>#newnu_fim_secondary_department_parent{display:none;}</style>';
	}
}
