<?php
/**
 * Helper functions
 *
 * @package NU_FIM
 */

namespace NU_FIM\Helpers;

/**
 * Get the name of the post type used for faculty.
 *
 * @since 1.0.0
 * @return string
 */
function get_faculty_post_type() {

	return apply_filters( 'nu_fim_post_type', 'faculty' );
}

/**
 * Get a list of whitelisted fields.
 *
 * Note: Do not add or remove fields from this function. To blacklist fields,
 *       add a filter to 'nu_fim_whitelisted_fields'.
 *
 * @return array List of fields.
 * @since 1.0.0
 */
function get_whitelisted_fields() {

	// phpcs:disable Squiz.PHP.CommentedOutCode.Found

	$fields = array(

		/*
		 * "FACULTY_NUID"
		 *
		 * Example:
		 * 000026501
		 */
		'nuid'                             => array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'ID', 'nu-fim' ),
		),

		/*
		 * UNIX_TIMESTAMP, // When the record was first created.
		 *
		 * Example:
		 * 1496931811
		 */
		'created'                          => array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'Created', 'nu-fim' ),
		),

		/*
		 * UNIX_TIMESTAMP, // When the record was last updated.
		 *
		 * Example:
		 * 1586446835
		 */
		'changed'                          => array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'Updated', 'nu-fim' ),
		),

		/*
		 * "FACULTY_ACADEMIC_TITLE"
		 *
		 * Example:
		 * "Associate Teaching Professor"
		 */
		'academic_title'                   => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Academic Title', 'nu-fim' ),
		),

		/*
		 * ZERO_OR_ONE, // Whether this faculty is still considered "active".
		 *
		 * Example:
		 * 1
		 */
		'field_faculty_active'             => array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'Active', 'nu-fim' ),
		),

		/*
		 * "FACULTY_EMAIL"
		 *
		 * Example:
		 * "d.oppenheimer@northeastern.edu"
		 */
		'field_faculty_email'              => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Email', 'nu-fim' ),
		),

		/*
		 * "FACULTY_FNAME"
		 *
		 * Example:
		 * "Darin"
		 */
		'field_faculty_fname'              => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'First Name', 'nu-fim' ),
		),

		/*
		 * "FACULTY_LNAME"
		 *
		 * Example:
		 * "Oppenheimer"
		 */
		'field_faculty_lname'              => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Last Name', 'nu-fim' ),
		),

		/*
		 * "FACULTY_BIO"
		 *
		 * Example:
		 * "<p><span>Dr. Darin S. Oppenheimer is an Executive Director of the Drug Device Center of Excellence
		 * focusing on Medical Devices and Combination products at Merck, based in Upper Gwynedd, PA. </span>
		 * </p>\r\n\r\n<p><span>&nbsp;</span></p>\r\n\r\n<p><span>Darin is involved in many facets of the Product
		 * Development Lifecycle including regulatory submissions, due dili­gence, and active participation on
		 * industry trade organizations and standards committees. His prior background as a Research and Development
		 * Scientist focused on pharmaceuticals and medical device diagnostic applications for biomarker and drug
		 * discovery. </span></p>\r\n\r\n<p><span>&nbsp;</span></p>\r\n\r\n<p><span>Darin’s undergraduate degree is
		 * in Molecular Biology from the University of Tampa. He also holds two Masters Degrees from Johns Hopkins
		 * University in Biotechnology and Regulatory Science as well as a graduate Certificate in Biotechnology
		 * Enterprise. Recently Darin has completed his Doctorate degree in Regu­latory Science from the University
		 * of Southern California.</span></p>\r\n"
		 */
		'field_faculty_bio'                => array(
			'type'    => 'html',
			'context' => 'meta',
			'label'   => esc_html__( 'Bio', 'nu-fim' ),
		),

		/*
		 * "FACULTY_SHORT_BIO"
		 *
		 * Example:
		 * "An award-winning Clinical Social and School Adjustment Counselor in an urban school district who brings
		 * over 25 years of experience in the field to her classrooms."
		 */
		'field_faculty_short_bio'          => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Bio (Short)', 'nu-fim' ),
		),

		/*
		 * "FACULTY_IS_CORE"
		 *
		 * Example:
		 * "0"
		 */
		'field_faculty_is_core'            => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Is Core', 'nu-fim' ),
		),

		/*
		 * "FACULTY_EDU_DEGREE"
		 *
		 * Example:
		 * "Master of Science from Boston University (2003)"
		 */
		'field_faculty_edu_degree'         => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Degree', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_PORTRAIT_CROPPED_PHOTO_URL",
		 *     "original_url":"FACULTY_PORTRAIT_ORIGINAL_PHOTO_URL",
		 *     "title":"FACULTY_PORTRAIT_PHOTO_TITLE",
		 *     "size":"548030",
		 *     "mimetype":"image\/jpeg"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "https://fim.northeastern.edu/sites/default/files/styles/faculty_portrait/public/portrait-photos/David_Leussler_018.JPG?itok=1aJXntkS",
		 *     "original_url": "https://fim.northeastern.edu/sites/default/files/portrait-photos/David_Leussler_018.JPG",
		 *     "title": "David_Leussler_018.JPG",
		 *     "size": "6614674",
		 *     "mimetype": "image/jpeg"
		 * }
		 */
		'field_faculty_portrait_photo'     => array(
			'type'    => 'array',
			'context' => 'attachment',
			'label'   => esc_html__( 'Portrait', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_LANDSCAPE_CROPPED_PHOTO_URL",
		 *     "original_url":"FACULTY_LANDSCAPE_ORIGINAL_PHOTO_URL",
		 *     "title":"FACULTY_LANDSCAPE_PHOTO_TITLE",
		 *     "size":"346841",
		 *     "mimetype":"image\/jpeg"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "https://fim.northeastern.edu/sites/default/files/styles/faculty_landscape/public/landscape-photos/dagmar%20research.jpg?itok=1M3EWwJ5",
		 *     "original_url": "https://fim.northeastern.edu/sites/default/files/landscape-photos/dagmar%20research.jpg",
		 *     "title": "dagmar research.jpg",
		 *     "size": "9409752",
		 *     "mimetype": "image/jpeg"
		 * }
		 */
		'field_faculty_landscape_photo'    => array(
			'type'    => 'array',
			'context' => 'attachment',
			'label'   => esc_html__( 'Portrait (Landscape)', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_THUMBNAIL_CROPPED_PHOTO_URL",
		 *     "original_url":"FACULTY_THUMBNAIL_ORIGINAL_PHOTO_URL",
		 *     "title":"FACULTY_THUMBNAIL_PHOTO_TITLE",
		 *     "size":"346841",
		 *     "mimetype":"image\/jpeg"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "https://fim.northeastern.edu/sites/default/files/styles/faculty_thumbnail/public/thumbnail-photos/Leidig_Michael_125x150px.jpg?itok=LoIhzBpF",
		 *     "original_url": "https://fim.northeastern.edu/sites/default/files/thumbnail-photos/Leidig_Michael_125x150px.jpg",
		 *     "title": "Leidig_Michael_125x150px.jpg",
		 *     "size": "11952",
		 *     "mimetype": "image/jpeg"
		 * }
		 */
		'field_faculty_thumbnail_photo'    => array(
			'type'    => 'array',
			'context' => 'attachment',
			'label'   => esc_html__( 'Portrait (Thumbnail)', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_LINKEDIN_URL",
		 *     "title":"LinkedIn"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "https://www.linkedin.com/in/lydia-young-3b33357/",
		 *     "title": "LinkedIn"
		 * }
		 */
		'field_faculty_linkedin'           => array(
			'type'    => 'url',
			'context' => 'meta',
			'label'   => esc_html__( 'LinkedIn', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_TWITTER_URL",
		 *     "title":"Twitter"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "https://twitter.com/Peter_Lucash",
		 *     "title": "Twitter"
		 * }
		 */
		'field_faculty_twitter'            => array(
			'type'    => 'url',
			'context' => 'meta',
			'label'   => esc_html__( 'Twitter', 'nu-fim' ),
		),

		/*
		 * "FACULTY_ACTIVITIES_TEXT"
		 *
		 * Example:
		 * "<p>Regulatory Affairs Professional Society Fellow (FRAPS)&nbsp;</p>\r\n\r\n<p>Vice Chair Boston Chapter,
		 * Regulatory Affairs Professional Society&nbsp;</p>\r\n\r\n<p>Senior Member IEEE</p>\r\n\r\n<p>Senior
		 * Member ASQ&nbsp;</p>\r\n"
		 */
		'field_faculty_prof_activities'    => array(
			'type'    => 'html',
			'context' => 'meta',
			'label'   => esc_html__( 'Professional Activities', 'nu-fim' ),
		),

		/*
		 * "FACULTY_PROFESSIONAL_EXPERIENCE_TEXT"
		 *
		 * Example:
		 * "<p><br />\r\n Global Regulatory Affairs Strategy<br />\r\n 100+ 510(k) Submissions<br />\r\n
		 * Adjunct Professor Regulatory Affairs and Bioengineering<br />\r\n Intellectual Property Coordination
		 * <br />\r\n Patented Automated System<br />\r\n Liaison to Contract Manufacturer<br />\r\n Manager of
		 * R&amp;D Laboratory<br />\r\n Quality System auditor<br />\r\n Clinical Evaluation of New Products</p>
		 * \r\n"
		 */
		'field_faculty_prof_exp'           => array(
			'type'    => 'html',
			'context' => 'meta',
			'label'   => esc_html__( 'Professional Experience', 'nu-fim' ),
		),

		/*
		 * [ // array of secondary departments ]
		 *
		 * Example:
		 * [
		 *     "Electrical and Computer Engineering",
		 *     ...
		 * ]
		 */
		'field_faculty_secondary_dept'     => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_fim_secondary_department',
			'label'    => esc_html__( 'Secondary Department(s)', 'nu-fim' ),
		),

		/*
		 * [ // array of secondary colleges ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "name": "College of Engineering",
		 *         "id": "16"
		 *     },
		 *     ...
		 * ]
		 */
		'field_faculty_secondary_colleges' => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_fim_secondary_college',
			'label'    => esc_html__( 'Secondary College(s)', 'nu-fim' ),
		),

		/*
		 * [ // array of areas of expertise ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "name": "Motor Control",
		 *         "id": "79"
		 *     },
		 *     {
		 *         "name": "Neuroscience",
		 *         "id": "80"
		 *     },
		 *     ...
		 * ]
		 */
		'field_faculty_areas_expertise'    => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_fim_areas_expertise',
			'label'    => esc_html__( 'Areas of Expertise', 'nu-fim' ),
		),

		/*
		 * [ // array of research interests ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "name": "Motor Control",
		 *         "id": "79"
		 *     },
		 *     {
		 *         "name": "Neuroscience",
		 *         "id": "80"
		 *     },
		 *     ...
		 * ]
		 */
		'field_faculty_research_interests' => array(
			'type'     => 'array',
			'context'  => 'term',
			'taxonomy' => 'nu_fim_research_interest',
			'label'    => esc_html__( 'Research Interests', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_RESUME_URL",
		 *     "title":"FACULTY_RESUME_TITLE",
		 *     "size":"14701",
		 *     "mimetype":"application\/pdf"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "https://fim.northeastern.edu/sites/default/files/faculty-resumes/Sternad.CV_.November.2017.docx",
		 *     "title": "Sternad.CV_.November.2017.docx",
		 *     "size": "170705",
		 *     "mimetype": "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
		 * }
		 */
		'field_faculty_research_resume'    => array(
			'type'    => 'link_array',
			'context' => 'meta',
			'label'   => esc_html__( 'Resume', 'nu-fim' ),
		),

		/*
		 * [ // array of training certificates ]
		 *
		 * Example:
		 * [
		 *     "RD, LDN, ACE-certified Personal Trainer & Health Coach",
		 *     ...
		 * ]
		 */
		'field_faculty_training_certs'     => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Training Certifications', 'nu-fim' ),
		),

		/*
		 * [ // array of research article url/titles
		 *     {
		 *         "url":"ARTICLE_URL",
		 *         "title":"ARTICLE_TITLE"
		 *     }
		 * ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "url": "http://scholar.google.com/citations?user=P22kNPQAAAAJ&hl=en",
		 *         "title": "Google Scholar "
		 *     },
		 *     ...
		 * ]
		 */
		'field_faculty_research_articles'  => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Research Articles', 'nu-fim' ),
		),

		/*
		 * {
		 *     "url":"FACULTY_RESEARCH_LAB_SITE_URL",
		 *     "title":"FACULTY_RESEARCH_LAB_SITE_TITLE"
		 * }
		 *
		 * Example:
		 * {
		 *     "url": "http://www.florenciagabriele.com",
		 *     "title": "http://www.florenciagabriele.com"
		 * }
		 */
		'field_faculty_research_lab_site'  => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Lab Site', 'nu-fim' ),
		),

		/*
		 * {
		 *     "name":"Full Time",
		 *     "id":"1"
		 * }
		 *
		 * Example:
		 * {
		 *     "name": "Part Time",
		 *     "id": "2"
		 * }
		 */
		'field_faculty_employment_type'    => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Employment Type', 'nu-fim' ),
		),

		/*
		 * [ // array of colleges (same as field in field_faculty_positions) ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "name": "CPS",
		 *         "id": "4"
		 *     },
		 *     ...
		 * ]
		 */
		'field_faculty_colleges'           => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Colleges', 'nu-fim' ),
		),

		/*
		 * [
		 *     {
		 *         "field_faculty_department":"FACULTY_DEPARTMENT",
		 *         "field_faculty_academic_title":"FACULTY_ACADEMIC_TITLE",
		 *         "field_faculty_colleges":{
		 *             "name":"FACULTY_COLLEGE01_NAME",
		 *             "id":"FACULTY_COLLEGE01_ID"
		 *         },
		 *         "field_faculty_division":{
		 *             "name":"FACULTY_DIVISION_NAME",
		 *             "id":"FACULTY_DIVISION_ID"
		 *         }
		 *     }
		 * ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "field_faculty_department": "College of Professional Studies",
		 *         "field_faculty_academic_title": "Lecturer",
		 *         "field_faculty_colleges": {
		 *             "name": "CPS",
		 *             "id": "4"
		 *         },
		 *         "field_faculty_division": {
		 *             "name": "DIV27",
		 *             "id": "5"
		 *         }
		 *     }
		 * ]
		 */
		'field_faculty_positions'          => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Positions', 'nu-fim' ),
		),

		/*
		 * [
		 *     {
		 *         "field_faculty_building":"FACULTY_BUILDING",
		 *         "field_faculty_maildrop":"FACULTY_MAILDROP",
		 *         "field_faculty_city":"FACULTY_CITY",
		 *         "field_faculty_state":"FACULTY_STATE",
		 *         "field_faculty_zip":"FACULTY_ZIP"
		 *     }
		 * ]
		 *
		 * Example:
		 * [
		 *     {
		 *         "field_faculty_building": "Nightingale Hall",
		 *         "field_faculty_maildrop": "125 NI",
		 *         "field_faculty_city": "Boston",
		 *         "field_faculty_state": "MA",
		 *         "field_faculty_zip": "02115"
		 *     }
		 * ]
		 */
		'field_faculty_office_address'     => array(
			'type'    => 'array',
			'context' => 'meta',
			'label'   => esc_html__( 'Office Address', 'nu-fim' ),
		),

		/*
		 * "FACULTY_PHONE"
		 *
		 * Example:
		 * "617.373.2926"
		 */
		'field_faculty_phone'              => array(
			'type'    => 'string',
			'context' => 'meta',
			'label'   => esc_html__( 'Phone Number', 'nu-fim' ),
		),
	);

	// phpcs:enable Squiz.PHP.CommentedOutCode.Found

	/**
	 * Filter whitelisted fields.
	 *
	 * Note: the 'nuid' & 'changed' field cannot be removed.
	 *
	 * @var array $fields An array of fields and their attributes.
	 */
	$fields = apply_filters( 'nu_fim_whitelisted_fields', $fields );

	// Prevent removing the 'nuid' key from the fields array.
	if ( ! array_key_exists( 'nuid', $fields ) ) {
		$fields['nuid'] = array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'ID', 'nu-fim' ),
		);
	}

	// Prevent removing the 'changed' key from the fields array.
	if ( ! array_key_exists( 'changed', $fields ) ) {
		$fields['changed'] = array(
			'type'    => 'int',
			'context' => 'meta',
			'label'   => esc_html__( 'Updated', 'nu-fim' ),
		);
	}

	return $fields;
}

/**
 * Log a message to a log file and the wp cli interface.
 *
 * @param string $message Message string.
 * @param bool   $cli     Output message to cli as well.
 * @param bool   $error   Render an error in the cli.
 */
function log( $message, $cli = false, $error = false ) {

	if ( empty( $message ) ) {
		return;
	}

	$timestamp = '[' . current_time( 'mysql' ) . '] ';
	$message   = trim( (string) $message );

	// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_error_log
	// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_var_export
	if ( ! defined( 'ABSPATH' ) ) {

		$log_file = ini_get( 'error_log' );

		if ( ! empty( $log_file ) && is_writable( $log_file ) ) {
			error_log( var_export( $timestamp . $message, true ) . PHP_EOL, 3, $log_file );
		}
	} else {

		$log_file = ABSPATH . '/fim-import.log';

		if ( is_writable( $log_file ) ) {
			error_log( $timestamp . $message . PHP_EOL, 3, $log_file );
		}
	}

	if ( $cli && defined( '\WP_CLI' ) && \WP_CLI ) {
		if ( $error ) {
			\WP_CLI::error( $message );
		} else {
			\WP_CLI::log( $message );
		}
	}
	// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_error_log
	// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_var_export
}

/**
 *  Clear all of the caches for memory management
 */
function stop_the_insanity() {

	global $wpdb, $wp_object_cache;

	$wpdb->queries = array();

	if ( ! is_object( $wp_object_cache ) ) {
		return;
	}

	$wp_object_cache->group_ops      = array();
	$wp_object_cache->stats          = array();
	$wp_object_cache->memcache_debug = array();
	$wp_object_cache->cache          = array();

	if ( is_callable( $wp_object_cache, '__remoteset' ) ) {
		$wp_object_cache->__remoteset(); // important.
	}
}
