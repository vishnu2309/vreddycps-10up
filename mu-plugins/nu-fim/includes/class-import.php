<?php
/**
 * Contains the faculty members import class
 *
 * @package NU_FIM
 */

namespace NU_FIM;

use function NU_FIM\Helpers\get_faculty_post_type;
use function NU_FIM\Helpers\get_whitelisted_fields;
use function NU_FIM\Helpers\log;
use function NU_FIM\Helpers\stop_the_insanity;

/**
 * Class containing the faculty members import functionality.
 *
 * @package NU_FIM
 */
class Import {

	/**
	 * College ID. See: https://fim.northeastern.edu/api-docs/college-ids
	 *
	 * To import faculty members from a particular college.
	 *
	 * @var int
	 */
	private $college;

	/**
	 * Faculty Member ID. If this is set, only single faculty member will be imported.
	 *
	 * @var int
	 */
	private $nuid;

	/**
	 * Flag for updating existing posts even if the 'update' field has not changed.
	 *
	 * @var bool
	 */
	private $force;

	/**
	 * Are we performing a dry run.
	 *
	 * @var bool
	 */
	private $dry_run;

	/**
	 * To hide the logging.
	 *
	 * @var bool
	 */
	private $quiet;

	/**
	 * To delete all posts and re-import form scratch.
	 *
	 * @var bool
	 */
	private $setup;

	/**
	 * To bypass the confirmation.
	 *
	 * @var bool
	 */
	private $yes;

	/**
	 * The post type used for faculty members.
	 *
	 * @var string
	 */
	private $post_type;

	/**
	 * If we are running a CLI command, the command instance.
	 *
	 * @var bool
	 */
	private $is_cli;

	/**
	 * Holds the WP CLI progress bar.
	 *
	 * @var \WP_CLI\Progress\Bar
	 */
	private $progress_bar = null;

	/**
	 * Prefix for meta keys.
	 *
	 * @var array
	 */
	private $prefix = 'nu_fim_';

	/**
	 * Import constructor.
	 *
	 * @param array $args Import arguments.
	 */
	public function __construct( $args = [] ) {

		$default_college_id = 4; // CPS college-id for Faculty API.

		$default_args = [
			'college' => $default_college_id,
			'nuid'    => 0,
			'force'   => false,
			'dry_run' => false,
			'quiet'   => false,
			'setup'   => false,
			'yes'     => false,
			'is_cli'  => false,
		];

		$args = wp_parse_args( $args, $default_args );

		$this->college = isset( $args['college'] ) ? filter_var( $args['college'], FILTER_VALIDATE_INT ) : $default_college_id;
		$this->nuid    = isset( $args['nuid'] ) ? filter_var( $args['nuid'], FILTER_SANITIZE_STRING ) : '';
		$this->force   = isset( $args['force'] ) ? filter_var( $args['force'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->dry_run = isset( $args['dry_run'] ) ? filter_var( $args['dry_run'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->quiet   = isset( $args['quiet'] ) ? filter_var( $args['quiet'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->setup   = isset( $args['setup'] ) ? filter_var( $args['setup'], FILTER_VALIDATE_BOOLEAN ) : false;
		$this->yes     = isset( $args['yes'] ) ? filter_var( $args['yes'], FILTER_VALIDATE_BOOLEAN ) : false;

		/*
		 * Note: checking for defined( '\WP_CLI' ) && \WP_CLI; returns true if the class is used
		 *       outside of the WP CLI command context, but ran via a cron job.
		 */
		$this->is_cli = isset( $args['is_cli'] ) && $args['is_cli'] && defined( '\WP_CLI' ) && \WP_CLI ? true : false;

		if ( ! defined( 'NU_FIM_API_KEY' ) || empty( NU_FIM_API_KEY ) ) {

			log( 'The NU_FIM_API_KEY constant must be defined.', $this->is_cli, true );

			return;
		}

		if ( ! defined( 'NU_FIM_APP_NAME' ) || empty( NU_FIM_APP_NAME ) ) {

			log( 'The NU_FIM_APP_NAME constant must be defined.', $this->is_cli, true );

			return;
		}

		$this->post_type = get_faculty_post_type();

		if ( ! post_type_exists( $this->post_type ) ) {

			$message = sprintf( 'The %1$s post-type is not registered.', $this->post_type );

			log( $message, $this->is_cli, true );

			return;
		}

		if ( $this->is_cli ) {

			if ( $this->setup && $this->quiet ) {
				\WP_CLI::error( 'Cannot run import script in quiet mode with "setup" set to true' );
			}

			if ( ! $this->yes ) {

				$message = 'Running import script in live mode';

				if ( $this->setup ) {
					$message = $message . ' with "setup" flag. All existing posts will be removed before running the import script';
				}

				\WP_CLI::confirm( $message . '. Are you sure?' );
			}
		}

		$result = $this->import();

		if ( is_wp_error( $result ) ) {
			log( $result->get_error_message(), $this->is_cli, true );
		} else {
			log( 'Faculty import complete.', $this->is_cli );
		}
	}

	/**
	 * Import faculty data.
	 *
	 * @return true|\WP_Error Returns true on successful import, else a \WP_Error object.
	 */
	public function import() {

		/*
		 * Important: The setup flag is only intended for development, faculty on the live site will contain
		 *            additional data added manually by site editors.
		 */
		if ( $this->setup && ! $this->nuid ) {
			$this->delete_all_posts();
		}

		log( 'Fetching data from the API...', $this->is_cli );

		if ( $this->nuid ) {

			$response = $this->get( false, false, $this->nuid );
			$response = $this->validate_response( $response );

		} else {

			$offset           = 0;
			$response         = array();
			$response['data'] = array();

			do {

				$per_page_response = $this->get( $offset, $this->college );
				$per_page_response = $this->validate_response( $per_page_response );

				if ( is_wp_error( $per_page_response ) ) {

					$response = $per_page_response;

					break;
				}

				if ( empty( $response['total'] ) ) {
					$response['total'] = absint( $per_page_response['total'] );
				}

				$response['data'] = array_merge( $response['data'], $per_page_response['data'] );

				$offset += absint( $per_page_response['count'] );

				sleep( 3 );

				if ( $this->is_cli ) {
					stop_the_insanity();
				}
			} while ( $offset < absint( $per_page_response['total'] ) );
		}

		if ( is_wp_error( $response ) ) {
			return $response;
		} else {

			log( 'Remote data fetched successfully.', $this->is_cli );

			$errors       = array();
			$import_count = count( $response['data'] );

			// Set nuid as a key to each item.
			$nuids         = array_column( $response['data'], 'nuid' );
			$response_data = array_combine( $nuids, $response['data'] );

			/*
			 * Delete posts if we don't get some posts in the API response,
			 * and those posts are still present in the system.
			 */
			if ( ! $this->setup && ! $this->nuid ) {

				$local_posts = $this->get_local_posts();

				$this->delete_posts_by_id( array_diff_key( $local_posts, $response_data ) );

				$response_data = array_diff_key( $response_data, $local_posts );
			}

			$message = sprintf( 'Importing %1$d posts:', $import_count );

			log( $message );

			if ( $this->is_cli && ! $this->quiet ) {
				$this->progress_bar = \WP_CLI\Utils\make_progress_bar( $message, $import_count );
			}

			$iteration = 0;

			foreach ( $response_data as $faculty_data ) {

				if ( ! $this->dry_run ) {

					$post_id = $this->upsert_post( $faculty_data, $this->force );

					if ( is_wp_error( $post_id ) ) {
						$errors[] = $post_id->get_error_message();
					}
				}

				if ( $this->is_cli && ! $this->quiet ) {
					$this->progress_bar->tick();
				}

				if ( 0 === $iteration % 100 ) {

					sleep( 3 );

					if ( $this->is_cli ) {
						stop_the_insanity();
					}
				}
			}

			if ( $this->is_cli && ! $this->quiet ) {
				$this->progress_bar->finish();
			}

			if ( ! empty( $errors ) ) {

				foreach ( $errors as $error ) {
					log( $error, $this->is_cli );
				}
			}
		}

		return true;
	}

	/**
	 * Delete all existing posts from the database.
	 *
	 * This function is intended to be used for development, not for use on the live site.
	 *
	 * Important: Only posts with that have a nu_fim_nuid meta field will be deleted.
	 *            It is expected that any posts added manually via other means than
	 *            importing from the API do not have this field set.
	 */
	private function delete_all_posts() {

		global $wpdb;

		$t1 = $wpdb->posts;
		$t2 = $wpdb->postmeta;

		$querystr = $wpdb->prepare(
			"SELECT COUNT( * ) FROM {$t1} INNER JOIN {$t2} ON ( {$t1}.ID = {$t2}.post_id ) WHERE 1=1 AND ( {$t2}.meta_key = %s ) AND $t1.post_type = %s", // phpcs:ignore WordPress.DB.PreparedSQL.InterpolatedNotPrepared
			"{$this->prefix}nuid",
			$this->post_type
		);

		$count = $wpdb->get_var( $querystr ); // phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared

		if ( empty( $count ) ) {

			log( 'Setup mode: No existing posts found.', $this->is_cli );

			return;
		}

		$querystr = $querystr . " AND post_status IN ('future','publish','private','draft')";

		$count_message = $wpdb->get_var( $querystr ); // phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared
		$message       = $this->dry_run ? 'Dry run: ' : 'Setup mode: ';
		$message      .= empty( $count_message ) ? 'Removing existing posts:' : 'Removing ' . $count_message . ' existing posts:';

		log( $message );

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar = \WP_CLI\Utils\make_progress_bar( $message, $count );
		}

		$post_stati   = get_post_stati( array( 'exclude_from_search' => false ) );
		$post_stati[] = 'auto-draft';

		$args = array(
			'post_type'              => $this->post_type,
			'post_status'            => $post_stati,
			'posts_per_page'         => 100,
			'fields'                 => 'ids',
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
			'meta_query'             => array(
				array(
					'key'     => "{$this->prefix}nuid",
					'compare' => 'EXISTS',
				),
			),
		);

		$query = new \WP_Query( $args );

		while ( $query->have_posts() ) {

			$post_ids = $query->posts;

			foreach ( $post_ids as $post_id ) {

				if ( ! $this->dry_run ) {

					$post_thumbnail_id = get_post_thumbnail_id();

					if ( ! empty( $post_thumbnail_id ) ) {
						wp_delete_post( $post_thumbnail_id, true );
					}

					wp_delete_post( $post_id, true );
				}

				if ( $this->is_cli && ! $this->quiet ) {
					$this->progress_bar->tick();
				}
			}

			sleep( 3 );

			if ( $this->is_cli ) {
				stop_the_insanity();
			}

			$query = new \WP_Query( $args );
		}

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar->finish();
		}
	}

	/**
	 * Fetch data from the API endpoint.
	 *
	 * When the $nuid parameter is set, a single faculty profile will be retrieved,
	 * else a paginated list of results.
	 *
	 * total:  Total number of records for the current selection.
	 * count:  Number of records in the current response
	 * offset: Current offset
	 * data:   List of Faculty data
	 *
	 * @param int $offset     Offset parameter to page through results.
	 * @param int $college_id College ID.
	 * @param int $nuid       Faculty member ID.
	 *
	 * @return \WP_Error|array The response body or WP_Error on failure.
	 */
	private function get( $offset = null, $college_id = null, $nuid = null ) {

		$endpoint = 'https://fim.northeastern.edu/api/v1/faculty/';

		if ( empty( $nuid ) ) {

			$params = array(
				'include_unpublished' => 'true',
			);

			if ( ! empty( $college_id ) ) {
				$params['college'] = absint( $college_id );
			}

			if ( ! empty( $offset ) ) {
				$params['offset'] = absint( $offset );
			}

			if ( ! empty( $params ) ) {

				$endpoint = add_query_arg(
					$params,
					$endpoint
				);
			}
		} else {
			$endpoint = $endpoint . sanitize_text_field( $nuid );
		}

		$args = array(
			'timeout' => 10,
			'headers' => array(
				'Authorization' => 'ApiKey ' . NU_FIM_API_KEY,
				'X-APP-NAME'    => NU_FIM_APP_NAME,
			),
		);

		$response = wp_remote_get( $endpoint, $args );

		if ( ! is_wp_error( $response ) ) {

			if ( 200 !== (int) wp_remote_retrieve_response_code( $response ) ) {

				$response = new \WP_Error(
					'nu_fim_request_error',
					wp_remote_retrieve_body( $response )
				);
			} else {

				$response = json_decode( wp_remote_retrieve_body( $response ), true );

				if ( ! $response ) {

					$response = new \WP_Error(
						'nu_fim_request_error',
						'Could not parse API response data.'
					);
				}
			}
		}

		return $response;
	}

	/**
	 * Check response and validate if it has required data.
	 *
	 * @param array $response API Response.
	 *
	 * @return \WP_Error|array
	 */
	private function validate_response( $response ) {

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		if ( ! array_key_exists( 'data', $response ) || ! is_array( $response['data'] ) || empty( $response['data'] ) ) {

			return new \WP_Error(
				'nu_fim_import_data_empty',
				'No faculty data found in API response'
			);
		}

		return $response;
	}

	/**
	 * Get nuid for all posts stored in the database.
	 *
	 * @return array Array where key is the nuid and value is the post ID.
	 */
	private function get_local_posts() {

		global $wpdb;

		$posts = [];

		$query     = "SELECT post_id, meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = %s";
		$results   = $wpdb->get_results( $wpdb->prepare( $query, "{$this->prefix}nuid" ) ); // phpcs:ignore
		$increment = 1;

		if ( ! empty( $results ) ) {

			foreach ( $results as $result ) {

				if ( empty( $result->meta_value ) ) {
					$result->meta_value = 'temp' . $increment;
				}

				$posts[ $result->meta_value ] = $result->post_id;

				$increment++;
			}
		}

		return $posts;
	}

	/**
	 * Bulk delete posts by ID.
	 *
	 * @param array $post_ids List of post IDs.
	 */
	private function delete_posts_by_id( $post_ids ) {

		if ( empty( $post_ids ) || ! is_array( $post_ids ) ) {
			return;
		}

		$count     = count( $post_ids );
		$iteration = 0;

		$message = sprintf( 'Deleting %1$d existing faculty members before import:', $count );

		log( $message );

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar = \WP_CLI\Utils\make_progress_bar( $message, $count );
		}

		foreach ( $post_ids as $post_id ) {

			if ( ! $this->dry_run ) {

				$post_thumbnail_id = get_post_thumbnail_id();

				if ( ! empty( $post_thumbnail_id ) ) {
					wp_delete_post( $post_thumbnail_id, true );
				}

				wp_delete_post( $post_id, true );

				if ( 0 === $iteration % 20 ) {

					sleep( 3 );

					if ( $this->is_cli ) {
						stop_the_insanity();
					}
				}

				$iteration++;
			}

			if ( $this->is_cli && ! $this->quiet ) {
				$this->progress_bar->tick();
			}
		}

		if ( $this->is_cli && ! $this->quiet ) {
			$this->progress_bar->finish();
		}
	}

	/**
	 * Insert or update a post.
	 *
	 * @param array $data  Data for a single Faculty member as returned by the API.
	 * @param bool  $force Update existing post if the content of the 'updated' field
	 *                     is identical to the stored value. Default false.
	 *
	 * @return int|\WP_Error Post ID on success, else a WP_Error object.
	 */
	private function upsert_post( $data, $force = false ) {

		if ( empty( $data['nuid'] ) ) {

			return new \WP_Error(
				'nu_fim_field_missing',
				'The "nuid" field must be defined when updating a post.'
			);
		}

		if ( empty( $data['changed'] ) ) {

			return new \WP_Error(
				'nu_fim_field_missing',
				'The "changed" field must be defined when updating a post.'
			);
		}

		$insert_or_update = 'update';

		$postdata = array(
			'post_title'     => '',
			'post_type'      => $this->post_type,
			'comment_status' => 'closed',
			'ping_status'    => 'closed',
			'meta_input'     => array(),
			'tax_input'      => array(),
		);

		$post = $this->get_post_by_nuid( $data['nuid'] );

		if ( is_a( $post, '\WP_Post' ) ) {

			$postdata['ID'] = $post->ID;

			/*
			 * This is required, because if we don't pass the post_status, then the wp_insert_post method
			 * considers default post_status as "draft" and it'll apply it to the post, so our published
			 * post will become draft and when next time the import script runs, it'll import whole new post,
			 * making the duplication.
			 */
			$post_status = $post->post_status;

			/*
			 *  If we're not forcing an update and the existing changed field
			 *  matched the new changed field do nothing.
			 */
			if ( ! $force ) {

				$changed = get_post_meta( $post->ID, "{$this->prefix}changed", true );

				if ( (int) $changed === (int) $data['changed'] ) {
					return $post->ID;
				}
			}
		} else {

			$insert_or_update = 'insert';
			$post_status      = 'publish';
		}

		// If it's not in active status, mark it as a draft.
		if ( array_key_exists( 'field_faculty_active', $data ) && (int) $data['field_faculty_active'] < 1 ) {
			$post_status = 'draft';
		}

		$postdata['post_status'] = $post_status;

		$post_title_array = array();

		if ( array_key_exists( 'field_faculty_fname', $data ) && ! empty( $data['field_faculty_fname'] ) ) {
			$post_title_array[] = $data['field_faculty_fname'];
		}

		if ( array_key_exists( 'field_faculty_lname', $data ) && ! empty( $data['field_faculty_lname'] ) ) {
			$post_title_array[] = $data['field_faculty_lname'];
		}

		if ( empty( $post_title_array ) ) {

			return new \WP_Error(
				'nu_fim_field_missing',
				'The "field_faculty_fname" and "field_faculty_lname" fields cannot be empty when updating a post.'
			);
		}

		$postdata['post_title'] = join( ' ', $post_title_array );

		$whitelisted_fields = get_whitelisted_fields();
		$image_data         = [];
		$term_fields        = [];

		$postdata['meta_input'][ "{$this->prefix}changed" ] = $data['changed'];
		$postdata['meta_input'][ "{$this->prefix}nuid" ]    = $data['nuid'];

		foreach ( $data as $name => $value ) {

			if ( ! array_key_exists( $name, $whitelisted_fields ) ) {
				continue;
			}

			// Skip the fields which are already considered above.
			if ( in_array( $name, [ 'changed', 'nuid' ], true ) ) {
				continue;
			}

			if ( 'meta' === $whitelisted_fields[ $name ]['context'] ) {

				$meta_key = $this->get_formatted_key( $name );

				$postdata['meta_input'][ $meta_key ] = $data[ $name ];
			}

			if ( 'term' === $whitelisted_fields[ $name ]['context'] ) {

				if ( empty( $whitelisted_fields[ $name ]['taxonomy'] ) ) {
					$taxonomy = $this->get_formatted_key( $name );
				} else {
					$taxonomy = $whitelisted_fields[ $name ]['taxonomy'];
				}

				$term_fields[ $taxonomy ] = $value;
			}

			if ( empty( $image_data['url'] )
				&& in_array( $name, [ 'field_faculty_portrait_photo', 'field_faculty_landscape_photo', 'field_faculty_thumbnail_photo' ], true )
				&& ! empty( $data[ $name ]['original_url'] )
			) {

				$image_data['url']   = $data[ $name ]['original_url'];
				$image_data['title'] = empty( $data[ $name ]['title'] ) ? '' : $data[ $name ]['title'];
			}
		}

		$post_id = wp_insert_post( $postdata );

		if ( is_wp_error( $post_id ) ) {
			return $post_id;
		} else {

			$errors = [];

			foreach ( $term_fields as $taxonomy => $terms ) {

				if ( empty( $terms ) ) {
					continue;
				}

				$term_ids = [];

				foreach ( $terms as $term ) {

					$term_meta = [];

					if ( is_array( $term ) ) {
						$term_name                         = $term['name'];
						$term_meta[ "{$this->prefix}tid" ] = $term['id'];
					} else {
						$term_name = $term;
					}

					// Explode the value, because there could be ";" separated terms.
					$term_names = array_map( 'trim', explode( ';', $term_name ) );

					foreach ( $term_names as $term_name ) {

						$_term = get_term_by( 'name', $term_name, $taxonomy );

						// Check if term exists, if not, we'll create it.
						if ( empty( $_term ) ) {

							// Create new term.
							$inserted_term = wp_insert_term( $term_name, $taxonomy );

							// If we fail to create new term, move to next one.
							if ( is_wp_error( $inserted_term ) ) {

								$errors[] = $inserted_term->get_error_message();

								continue;
							}

							if ( empty( $inserted_term['term_id'] ) ) {

								$errors[] = "An unknown error occurred while inserting the '{$term_name}' term in the {$taxonomy} taxonomy.";

								continue;
							}

							$term_ids[] = $inserted_term['term_id'];

							if ( ! empty( $term_meta ) ) {
								foreach ( $term_meta as $meta_key => $meta_value ) {
									update_term_meta( $inserted_term['term_id'], $meta_key, $meta_value );
								}
							}
						} else {
							$term_ids[] = $_term->term_id;
						}
					}
				}

				if ( ! empty( $term_ids ) ) {

					$term_ids = array_map( 'intval', $term_ids );

					$response = wp_set_object_terms( $post_id, $term_ids, $taxonomy, true );

					if ( is_wp_error( $response ) ) {
						$errors[] = $response->get_error_message();
					}
				}
			}

			// Set featured image.
			if ( ! empty( $image_data['url'] ) ) {

				$attachment_id = $this->get_attachment_id( $post_id, $image_data );

				if ( is_wp_error( $attachment_id ) ) {
					$errors[] = $attachment_id->get_error_message();
				} else {
					update_post_meta( $attachment_id, "{$this->prefix}original_url", trim( $image_data['url'] ) );
					update_post_meta( $post_id, '_thumbnail_id', $attachment_id );
				}
			}
		}

		if ( ! empty( $errors ) ) {

			return new \WP_Error(
				'nu_fim_assign_term_error',
				join( ', ', $errors )
			);
		}

		/**
		 * Runs after inserting or updating a post.
		 *
		 * @param int   $post_id Post ID.
		 * @param array $data    Data from the FIM API used to insert or update the post.
		 */
		do_action( "nu_fim_after_{$insert_or_update}_post", $post_id, $data );

		return $post_id;
	}

	/**
	 * Get post by the nuid custom field value.
	 *
	 * @param int $nuid NUID field value.
	 *
	 * @return false|\WP_Post WP_Post object or false if the post cannot be found.
	 */
	private function get_post_by_nuid( $nuid ) {

		$args = array(
			'post_type'  => $this->post_type,
			'meta_key'   => $this->prefix . 'nuid', // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
			'meta_value' => $nuid, // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_value
		);

		$query = new \WP_Query( $args );

		if ( ! $query->have_posts() ) {
			return false;
		}

		return $query->posts[0];
	}

	/**
	 * Generate meta key or taxonomy name from field key.
	 * Removes the "field_" prefix and adds $this->prefix.
	 *
	 * @param string $name Field name.
	 *
	 * @return string
	 */
	private function get_formatted_key( $name ) {

		if ( 'field_' === substr( $name, 0, 6 ) ) {
			$name = substr( $name, 6 );
		}

		return $this->prefix . $name;
	}

	/**
	 * Check if the attachment already exists.
	 * If yes, return it, else create it.
	 *
	 * @param int   $post_id    Post ID.
	 * @param array $image_data Image data, contains url & title.
	 *
	 * @return int|string|\WP_Error
	 */
	private function get_attachment_id( $post_id, $image_data ) {

		global $wpdb;

		$image_url = trim( $image_data['url'] );
		$title     = trim( $image_data['title'] );

		$query         = "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = %s AND meta_value = %s";
		$attachment_id = $wpdb->get_var( $wpdb->prepare( $query, "{$this->prefix}original_url", $image_url ) ); // phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared

		if ( ! empty( $attachment_id ) ) {
			return $attachment_id;
		}

		require_once ABSPATH . 'wp-admin/includes/media.php';
		require_once ABSPATH . 'wp-admin/includes/file.php';
		require_once ABSPATH . 'wp-admin/includes/image.php';

		return media_sideload_image( $image_url, $post_id, $title, 'id' );
	}
}
