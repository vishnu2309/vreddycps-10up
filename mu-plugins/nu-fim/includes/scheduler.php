<?php
/**
 * Cron Event Scheduler functions
 *
 * @package NU_FIM
 */

namespace NU_FIM\Scheduler;

/**
 * Setup scheduled events.
 *
 * @since 1.0.0
 */
function setup() {

	add_action( 'wp_loaded', __NAMESPACE__ . '\setup_scheduler_actions' );
	add_action( 'nu_fim_process_import_cron', __NAMESPACE__ . '\process_fim_import' );
}

/**
 * Cron job scheduler/
 *
 * @return void
 */
function setup_scheduler_actions() {

	$daily_local_time_00 = get_date_from_gmt( '00:00:00' );

	if ( is_admin() && ! defined( 'DOING_AJAX' ) && ! wp_next_scheduled( 'nu_fim_process_import_cron' ) ) {
		wp_schedule_event( strtotime( $daily_local_time_00 ), 'daily', 'nu_fim_process_import_cron' );
	}
}

/**
 * Process fim import cron event.
 */
function process_fim_import() {

	require_once NU_FIM_INC . 'class-import.php';

	/*
	 * Default college id is 4 (college-id for CPS for Faculty API),
	 * so we don't have to pass it explicitly.
	 */
	new \NU_FIM\Import();
}

