<?php
/**
 * Table to visualize fields stored via the FIM API plugin.
 *
 * @package CPS
 *
 * @global $faculty_post_id
 */

$fields             = get_post_meta( $faculty_post_id );
$whitelisted_fields = \NU_FIM\Helpers\get_whitelisted_fields();
?>
<table>
	<tbody>
		<?php if ( ! empty( get_post_thumbnail_id( $faculty_post_id ) ) ) : ?>
			<tr>
				<th>Profile Image</th>
				<td>
					<?php the_post_thumbnail( 'medium' ); ?>
				</td>
			<tr>
		<?php endif; ?>
		<tr>
			<th>
				<strong>Areas of Expertise</strong><br>
				<span style="font-weight: normal;">field_faculty_areas_expertise, <em>Note: displayed as comma-separated list but the API returns an array of items.</em></span>
			</th>
			<td>
				<?php
				$areas_expertise_terms = wp_get_post_terms( $faculty_post_id, 'nu_fim_areas_expertise' );
				$areas_expertise_terms = wp_list_pluck( $areas_expertise_terms, 'name' );
				echo join( ', ', $areas_expertise_terms ); // phpcs:ignore
				?>
			</td>
		</tr>


		<tr>
			<th>
				<strong>Research Interest</strong><br>
				<span style="font-weight: normal;">field_faculty_research_interests, <em>Note: displayed as comma-separated list but the API returns an array of items.</em></span>
			</th>
			<td>
				<?php
				$research_interest_terms = wp_get_post_terms( $faculty_post_id, 'nu_fim_research_interest' );
				$research_interest_terms = wp_list_pluck( $research_interest_terms, 'name' );
				echo join( ', ', $research_interest_terms ); // phpcs:ignore
				?>
			</td>
		</tr>
		<tr>
			<th>
				<strong>Secondary College</strong><br>
				<span style="font-weight: normal;">field_faculty_secondary_colleges, <em>Note: displayed as comma-separated list but the API returns an array of items.</em></span>
			</th>
			<td>
				<?php
				$secondary_college_terms = wp_get_post_terms( $faculty_post_id, 'nu_fim_secondary_college' );
				$secondary_college_terms = wp_list_pluck( $secondary_college_terms, 'name' );
				echo join( ', ', $secondary_college_terms ); // phpcs:ignore
				?>
			</td>
		</tr>
		<tr>
			<th>
				<strong>Secondary Department</strong><br>
				<span style="font-weight: normal;">field_faculty_secondary_dept, <em>Note: displayed as comma-separated list but the API returns an array of items.</em></span>
			</th>
			<td>
				<?php
				$secondary_department_terms = wp_get_post_terms( $faculty_post_id, 'nu_fim_secondary_department' );
				$secondary_department_terms = wp_list_pluck( $secondary_department_terms, 'name' );
				echo join( ', ', $secondary_department_terms ); // phpcs:ignore
				?>
			</td>
		</tr>

		<?php foreach ( $whitelisted_fields as $key => $value ) : ?>

			<?php
			// We're showing terms at the start of the table.
			if ( ! empty( $value['context'] ) && 'term' === $value['context'] ) {
				continue;
			}

			if ( array_key_exists( 'nu_fim_' . $key, $fields ) ) {
				$meta_key = 'nu_fim_' . $key;
			} else {
				$meta_key = 'nu_fim_' . str_replace( 'field_', '', $key );
			}
			$meta_value = get_post_meta( $faculty_post_id, $meta_key, true );
			?>
			<tr>
				<th>
					<strong><?php echo esc_html( $value['label'] ); ?></strong>
					<br>
					<span style="font-weight: normal;"><?php echo esc_html( $key ); ?></span>
				</th>
				<td>
					<?php if ( is_array( $meta_value ) ) : ?>
						<?php if ( 'nu_fim_faculty_research_articles' === $meta_key && ! empty( $meta_value ) ) : ?>
							<ul>
							<?php
							foreach ( $meta_value as $link_arr ) {
								printf(
									'<li><a href="%s">%s</a>',
									esc_url( $link_arr['url'] ),
									wp_kses_post( $link_arr['title'] )
								);
							}
							?>
							</ul>
						<?php else : ?>
							<pre><?php echo var_export( $meta_value, true );  // phpcs:ignore ?></pre>
						<?php endif ?>
					<?php else : ?>
						<?php echo $meta_value;  // phpcs:ignore ?>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
