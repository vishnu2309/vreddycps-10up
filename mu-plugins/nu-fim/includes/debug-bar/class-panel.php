<?php
/**
 * Define Debug Bar panel.
 *
 * @package NU_FIM
 */

namespace NU_FIM\DebugBar;

class Panel extends \Debug_Bar_Panel {

	/**
	 * Faculty post type.
	 */
	public $faculty_post_type;

	/**
	 * Post ID.
	 */
	public $faculty_post_id;

	/**
	 * Panel menu title.
	 */
	public $title;

	/**
	 * Initialize debug bar panel.
	 */
	public function init() {

		$this->faculty_post_type = \NU_FIM\Helpers\get_faculty_post_type();

		if ( empty( $this->faculty_post_type ) ) {
			return;
		}

		$this->title( 'FIM Data' );
	}

	/**
	 * Show Debug Bar menu item.
	 */
	public function prerender() {

		$this->set_visible( true );
	}

	/**
	 * Render panel content.
	 */
	public function render() {

		$faculty_post_id = $this->get_post_id();

		if ( ! $faculty_post_id ) {

			echo 'No faculty data found for this page.';
			return;
		}

		$nuid = get_post_meta( $faculty_post_id, 'nu_fim_nuid', true );

		if ( empty( $nuid ) ) {

			echo 'Data only available for faculty imported via the FIM API.';
			return;
		}

		ob_start();

		include NU_FIM_INC . 'debug-bar/table.php';

		$html = ob_get_contents();

		ob_end_clean();

		echo $html;
	}

	/**
	 * Get post ID if we are on a faculty post type edit page or front end page.
	 *
	 * @return int|false Post ID or false.
	 */
	protected function get_post_id() {

		$post_id = false;

		if ( is_admin() ) {

			$current_screen = get_current_screen();

			if ( $this->faculty_post_type === $current_screen->id && 'post' === $current_screen->base ) {

				if ( isset( $_GET['post'] ) ) {
					$post_id = absint( $_GET['post'] );
				}
			}

		} else {

			if ( is_singular( $this->faculty_post_type ) ) {
				$post_id = get_the_ID();
			}
		}

		return $post_id;
	}
}
