<?php // phpcs:ignore WordPress.Files.FileName.InvalidClassFileName
/**
 * An abstract command for WP CLI scripts to implement
 *
 * @package NU_FIM
 */

namespace NU_FIM\CLI;

/**
 * Abstract class for WP CLI scripts.
 *
 * @package NU_FIM\CLI
 */
abstract class AbstractCommand extends \WP_CLI_Command {

	/**
	 * Arguments for the command.
	 *
	 * @var array
	 */
	public $args = array();

	/**
	 * Associative arguments for the command.
	 *
	 * @var array
	 */
	public $assoc_args = array();

	/**
	 * Disable term counting so that terms are not all recounted after every term operation.
	 *
	 * @access protected
	 * @return void
	 */
	protected function start_bulk_operation() {

		// Disable term count updates for speed.
		wp_defer_term_counting( true );

		if ( ! defined( 'WP_IMPORTING' ) ) {
			define( 'WP_IMPORTING', true );
		}
	}

	/**
	 * Re-enable Term counting and trigger a term counting operation to update all term counts.
	 *
	 * @access protected
	 * @return void
	 */
	protected function end_bulk_operation() {

		// Enable term count updates again.
		wp_defer_term_counting( false );
	}
}
