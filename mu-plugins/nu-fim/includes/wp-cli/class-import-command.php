<?php // phpcs:ignore WordPress.Files.FileName.InvalidClassFileName
/**
 * Import faculty command
 *
 * @package NU_FIM
 */

namespace NU_FIM\CLI;

/**
 * Command for importing faculty members.
 *
 * @package NU_FIM\CLI
 */
class ImportCommand extends \NU_FIM\CLI\AbstractCommand {

	/**
	 * Import faculty from the FIM API.
	 *
	 * ## OPTIONS
	 *
	 * [--college]
	 * : College ID. See: https://fim.northeastern.edu/api-docs/college-ids
	 *
	 * [--nuid]
	 * : Faculty Member ID. Set this flag to import a single faculty member.
	 *
	 * [--dry-run]
	 * : Run an import but do not save changes to the database.
	 *
	 * [--force]
	 * : Update existing posts even if the 'update' field has not changed.
	 *
	 * [--setup]
	 * : Delete all existing posts before importing. Intended to be used during development only.
	 *
	 * [--yes]
	 * : Answer yes to the confirmation messages.
	 *
	 * [--debug]
	 * : Optional. Show debug messages in the console.
	 *
	 * ## EXAMPLES
	 *
	 *     $ wp fim import --college=4
	 *     $ wp fim import --nuid=1234
	 *
	 * @param  array $args       Positional command arguments.
	 * @param  array $assoc_args Associative arguments.
	 *
	 * @return void
	 */
	public function __invoke( array $args = array(), array $assoc_args = array() ) {

		$config = \WP_CLI::get_config();

		$college = \WP_CLI\Utils\get_flag_value( $assoc_args, 'college', 4 ); // 4 is college-id for CPS for Faculty API.
		$force   = \WP_CLI\Utils\get_flag_value( $assoc_args, 'force', false );
		$dry_run = \WP_CLI\Utils\get_flag_value( $assoc_args, 'dry-run', false );
		$quiet   = isset( $config['quiet'] ) && $config['quiet'] ? true : false;
		$setup   = \WP_CLI\Utils\get_flag_value( $assoc_args, 'setup', false );
		$yes     = \WP_CLI\Utils\get_flag_value( $assoc_args, 'yes', false );

		/*
		 * We cannot use get_flag_value for nuid, because it removes prepended 0s from the nuid,
		 * for example, if nuid is 000026501, then it returns 26501.
		 *
		 * So instead we're using the old method of key checking.
		 */
		$nuid = empty( $assoc_args['nuid'] ) ? '' : $assoc_args['nuid'];

		$this->start_bulk_operation();

		require_once NU_FIM_INC . 'class-import.php';

		$args = [
			'college' => $college,
			'nuid'    => $nuid,
			'force'   => $force,
			'dry_run' => $dry_run,
			'quiet'   => $quiet,
			'setup'   => $setup,
			'yes'     => $yes,
			'is_cli'  => true,
		];

		new \NU_FIM\Import( $args );

		$this->end_bulk_operation();
	}
}

\WP_CLI::add_command( 'fim import', __NAMESPACE__ . '\ImportCommand' );
