<?php
/**
 * Custom CLI commands bootstrap file
 *
 * @package NU_FIM
 */

namespace NU_FIM\CLI;

/**
 * Define custom CLI scripts if WP_CLI is defined.
 *
 * @since 1.0.0
 */
function setup() {

	if ( defined( 'WP_CLI' ) && WP_CLI ) {

		require NU_FIM_INC . 'wp-cli/abstract-command.php';
		require NU_FIM_INC . 'wp-cli/class-import-command.php';
	}
}
