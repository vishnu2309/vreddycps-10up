# NU FIM WordPress Plugin

The NU FIM plugin adds a WP CLI command that imports faculty into WordPress via the FIM API.

## Credentials

Add the `NU_FIM_API_KEY` and `NU_FIM_APP_NAME` constants to the wp-config.php file.

```php
define( 'NU_FIM_API_KEY', '<YOUR_API_KEY>' );
define( 'NU_FIM_APP_NAME', '<YOUR_APP_NAME>' );
```

## Use

- Import faculty data for the College of Professional Studies:
`wp fim import --college=4  --allow-root`
