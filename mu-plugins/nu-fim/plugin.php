<?php
/**
 * Plugin Name: NU FIM
 * Plugin URI:  https://northeastern.edu/
 * Description: Registers a WP CLI command to import data from the FIM API.
 * Version:     1.0.0
 * Author:      10up
 * Author URI:  https://10up.com
 * Text Domain: nu-fim
 * Domain Path: /languages
 *
 * @package NU_FIM
 */

namespace NU_FIM;

// Useful global constants.
use function NU_FIM\CLI\setup;

if ( ! defined( 'NU_FIM_VERSION' ) ) {
	define( 'NU_FIM_VERSION', '1.0.0' );
}

if ( ! defined( 'NU_FIM_URL' ) ) {
	define( 'NU_FIM_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'NU_FIM_PATH' ) ) {
	define( 'NU_FIM_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'NU_FIM_INC' ) ) {
	define( 'NU_FIM_INC', NU_FIM_PATH . 'includes/' );
}

// Include helper functions.
require_once NU_FIM_INC . 'helpers.php';
require_once NU_FIM_INC . 'scheduler.php';

// Include taxonomies.
require_once NU_FIM_INC . 'taxonomies/areas-expertise.php';
require_once NU_FIM_INC . 'taxonomies/secondary-department.php';
require_once NU_FIM_INC . 'taxonomies/secondary-college.php';
require_once NU_FIM_INC . 'taxonomies/research-interest.php';

// Run the setup functions.
Taxonomy\AreasExpertise\setup();
Taxonomy\SecondaryDepartment\setup();
Taxonomy\SecondaryCollege\setup();
Taxonomy\ResearchInterest\setup();
Scheduler\setup();

// Include files.
require_once NU_FIM_INC . 'wp-cli/core.php';

// Include debug bar functions.
require_once NU_FIM_INC . 'debug-bar/core.php';

// Bootstrap.
CLI\setup();
