<?php
/**
 * Renders the Footer Contact block
 *
 * Note: This block is intended to be used in the footer.php template, not in the post content.
 *
 * @package CPS
 *
 * @global $attributes
 */

$columns       = [ $attributes['columnOneCopy'], $attributes['columnTwoCopy'], $attributes['columnThreeCopy'] ];
$columns       = array_filter( $columns );
$block_classes = [
	'block-footer-contact',
	( isset( $attributes['colorScheme'] ) && ! empty( $attributes['colorScheme'] ) ) ? 'has-' . $attributes['colorScheme'] . '-background-color' : '',
];
?>

<div class="<?php echo esc_attr( implode( ' ', $block_classes ) ); ?>">
	<div class="block-footer-contact__inner">

		<?php if ( ! empty( $attributes['headline'] ) ) : ?>
			<h2 class="block-footer-contact__headline is-style-h1"><?php echo esc_html( $attributes['headline'] ); ?></h2>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['copy'] ) ) : ?>
			<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore ?>
		<?php endif; ?>

		<?php if ( ! empty( $columns ) ) : ?>
			<div>
				<span><?php echo wp_kses_post( join( '</span> <span class="block-footer-contact__divider">|</span> <span>', $columns ) ); ?></span>
			</div>
		<?php endif; ?>
		<p class="block-footer-contact__top"><a href="#top"><?php echo esc_html__( 'Back to Top', 'cps' ); ?></a></p>
	</div>
</div>
