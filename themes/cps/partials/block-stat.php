<?php
/**
 * Renders the Stat block
 *
 * @package CPS\Blocks
 *
 * @global $attributes
 */

$block_classes = [
	'block-stat',
	isset( $attributes['className'] ) ? $attributes['className'] : '',
];

$color_scheme = isset( $attributes['colorScheme'] ) ? ' has-' . $attributes['colorScheme'] . '-color' : '';
?>

<div class="<?php echo esc_attr( implode( ' ', $block_classes ) ); ?>">

	<?php if ( empty( $attributes['statType'] ) || 'text' === $attributes['statType'] ) : ?>

		<?php if ( ! empty( $attributes['statText'] ) ) : ?>
			<div
				class="block-stat__number<?php echo esc_attr( $color_scheme ); ?>"
				<?php if ( ! empty( $attributes['statTextSize'] ) ) : ?>
				style="font-size: <?php echo esc_attr( $attributes['statTextSize'] ); ?>px"
				<?php endif; ?>
			>
				<?php echo wp_kses_post( $attributes['statText'] ); ?>
			</div>
		<?php endif; ?>

	<?php else : ?>
		<?php if ( ! empty( $attributes['statImage'] ) && is_array( $attributes['statImage'] ) ) : ?>
			<?php echo wp_get_attachment_image( $attributes['statImage']['id'], 'full' ); ?>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h3><?php echo esc_html( $attributes['headline'] ); ?></h3>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore ?>
	<?php endif; ?>
</div>
