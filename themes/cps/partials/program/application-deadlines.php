<?php
/**
 * Renders the application deadlines section
 *
 * @package CPS
 */

$post_meta = get_post_meta( get_the_ID(), 'nu_pim_admissions_deadlines', true );

if ( empty( $post_meta ) || ! is_array( $post_meta ) ) {
	return;
}

if ( ! isset( $post_meta['tables'] ) || empty( $post_meta['tables'] ) || ! is_array( $post_meta['tables'] ) ) {
	return;
}
?>
<div class="wp-block-group" id="application-deadlines" data-nav-title="<?php esc_attr_e( 'Application Deadlines', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">
		<h2><?php esc_html_e( 'Application Deadlines', 'cps' ); ?></h2>
		<?php if ( isset( $post_meta['intro_text'] ) && ! empty( $post_meta['intro_text'] ) ) : ?>
			<?php echo wp_kses_post( $post_meta['intro_text'] ); ?>
		<?php endif; ?>
		<?php foreach ( $post_meta['tables'] as $table ) : ?>
			<table class="application-deadlines-table">
				<thead>
					<tr>
						<th class="is-style-h3"><?php echo esc_html( $table['title'] ); ?></th>
						<th class="is-style-h3"><?php esc_html_e( 'Deadline', 'cps' ); ?></th>
					</tr>
				</thead>
				<tbody class="is-style-small">
					<?php
					foreach ( $table['rows'] as $row ) {
						printf(
							'<tr><td>%s</td><td>%s</td></tr>',
							esc_html( $row[1] ),
							esc_html( $row[0] )
						);
					}
					?>
				</tbody>
			</table>
		<?php endforeach; ?>
	</div>
</div>
