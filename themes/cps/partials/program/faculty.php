<?php
/**
 * Renders the faculty section on the program detail page
 *
 * @package CPS
 */

$program_id = get_the_ID();
$items      = get_post_meta( $program_id, 'cps_program_faculty_items', true );
$title      = get_post_meta( $program_id, 'cps_program_faculty_title', true );
$nav_label  = get_post_meta( $program_id, 'cps_program_faculty_nav_label', true );
$copy       = get_post_meta( $program_id, 'cps_program_faculty_copy', true );
$quote      = get_post_meta( $program_id, 'cps_program_faculty_quote', true );
$stat       = get_post_meta( $program_id, 'cps_program_faculty_stat', true );
$stat_title = get_post_meta( $program_id, 'cps_program_faculty_stat_title', true );
$stat_copy  = get_post_meta( $program_id, 'cps_program_faculty_stat_copy', true );

$has_faculty = false;

if ( ! empty( $items ) && is_array( $items ) ) {
	$has_faculty = true;
}

// No need to show the block if we have no content for it.
if ( empty( $title ) && empty( $copy ) && empty( $quote ) && empty( $stat ) && empty( $has_faculty ) ) {
	return;
}
?>

<?php
printf(
	'<div class="wp-block-group has-background has-dark-gray-background-color has-white-color"%s%s>',
	empty( $nav_label ) ? '' : ' data-nav-title="' . esc_attr( $nav_label ) . '"', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	empty( $nav_label ) ? '' : ' id="' . sanitize_title( $nav_label ) . '"' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
);
?>
	<div class="wp-block-group__inner-container">
		<h2><?php echo esc_html( $title ); ?></h2>
		<?php if ( ! empty( $copy ) ) : ?>
			<?php echo wpautop( wp_kses_post( $copy ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		<?php endif; ?>

		<?php if ( ! empty( $quote ) || ! empty( $stat ) ) : ?>
			<div class="wp-block-columns">
				<?php if ( ! empty( $quote ) ) : ?>
					<div class="wp-block-column" style="flex-basis:66.66%">
						<blockquote class="wp-block-quote is-style-no-border">
							<?php echo wpautop( wp_kses_post( $quote ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						</blockquote>
					</div>
				<?php endif; ?>
				<?php if ( ! empty( $stat ) ) : ?>
					<div class="wp-block-column" style="flex-basis:33.33%">
						<div class="block-stat">
							<div class="block-stat__number has--color"><?php echo esc_html( $stat ); ?></div>
							<?php if ( ! empty( $stat_title ) ) : ?>
								<h3><?php echo esc_html( $stat_title ); ?></h3>
							<?php endif; ?>
							<?php if ( ! empty( $stat_copy ) ) : ?>
								<?php echo wpautop( wp_kses_post( $stat_copy ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( $has_faculty ) : ?>
			<div class="block-faculty-grid">
				<div class="faculty-grid">
					<div class="faculty-grid__slider">
						<?php
						foreach ( $items as $item ) {

							$faculty = get_post( $item['post_id'] );

							if ( ! is_a( $faculty, 'WP_Post' ) || 'cps-person' !== $faculty->post_type ) {
								continue;
							}

							$post_data = [
								'imageID'   => get_post_thumbnail_id( $faculty->ID ),
								'titleText' => get_the_title( $faculty ),
								'ctaLink'   => \CPS\Person\Helpers\get_faculty_link( $faculty ),
							];

							require locate_template( 'partials/block-faculty-grid-column.php' );
						}
						?>
					</div>
				</div>


				<div class="wp-block-button aligncenter">
					<a class="wp-block-button__link has-white-background-color has-background" href="https://google.com">
						<?php esc_html_e( 'See all Faculty', 'cps' ); ?>
					</a>
				</div>

			</div>
		<?php endif; ?>
	</div>
</div>
