<?php
/**
 * Renders the experiential / coop opportunities section on the program detail page.
 *
 * @package CPS
 */

?>
<div class="wp-block-group" id="coop-opportunities" data-nav-title="Co-op Opportunities">
	<div class="wp-block-group__inner-container">
		<h2>Experiential / Co-op opportunities</h2>
		<p>Northeastern's signature experience-powered learning model has been at the heart of the university for more than a century. It combines world-class academics with professional practice, allowing you to acquire relevant, real-world skills you can immediately put into action in your current workplace.</p>

		<div class="block-image-text block-image-text--alignright">
			<div class="block-image-text__image">
				<img src="http://placehold.it/876x436" alt="">
			</div>
			<div class="block-image-text__content">
				<p>This makes a Northeastern education a dynamic, transformative experience, giving you countless opportunities to grow as a professional and person.</p>
				<p><a href="#">Learn About Getting Real World Experience</a></p>
			</div>
		</div>

	</div>
</div>
