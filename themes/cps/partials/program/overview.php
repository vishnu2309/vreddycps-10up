<?php
/**
 * Renders the program overview section on the program detail page.
 *
 * @package CPS
 */

$program_icon_columns  = \CPS\Program\Helpers\get_program_icon_columns();
$accordion_items       = [];
$program_overview      = \CPS\Program\Helpers\get_program_long_description();
$program_accredidation = get_post_meta( get_the_ID(), 'nu_pim_accredidation', true );

if ( ! empty( $program_overview ) ) {

	$accordion_items[] = array(
		'title'   => __( 'Overview', 'cps' ),
		'content' => $program_overview,
	);
}

if ( ! empty( $program_accredidation ) ) {

	$accordion_items[] = array(
		'title'   => __( 'Accreditation', 'cps' ),
		'content' => $program_accredidation,
	);
}
?>
<div class="wp-block-group" id="program-overview" data-nav-title="<?php esc_attr_e( 'Overview', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">

		<?php if ( ! empty( $program_icon_columns ) ) : ?>
			<div class="block-icon-columns">
				<div class="grid grid--icons grid--<?php echo absint( count( $program_icon_columns ) ); ?>">
					<?php foreach ( $program_icon_columns as $program_icon_column ) : ?>

						<div class="grid-item grid-item--icon">
							<div class="grid-item__icon">
								<?php cps_svg_icon( $program_icon_column['icon'], true ); ?>
							</div>
							<div class="grid-item__content">
								<h3 class="mb-zero"><?php echo esc_html( $program_icon_column['title'] ); ?></h3>
								<p class="mt-zero is-style-small"><?php echo esc_html( $program_icon_column['value'] ); ?></p>
							</div>
						</div>

					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ( ! empty( $accordion_items ) ) : ?>
			<div class="block-accordion">
				<div class="block-accordion__items js-block-accordion">
					<?php foreach ( $accordion_items as $accordion_item ) : ?>
						<button class="accordion-header is-style-h3" type="button" aria-label="<?php esc_attr_e( 'Expand/Close', 'cps' ); ?> <?php echo esc_attr( $accordion_item['title'] ); ?>">
							<?php echo esc_html( $accordion_item['title'] ); ?>
						</button>
						<div class="accordion-content">
							<h2 class="accordion-label screen-reader-text"><?php echo esc_html( $accordion_item['title'] ); ?></h2>
							<?php
							/*
							 * Note: not escaping the content. It may contain embeds and all kinds of other HTML elements so we're going
							 *       to have to trust what comes from the API.
							 */
							echo $accordion_item['content']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>

	</div>
</div>
