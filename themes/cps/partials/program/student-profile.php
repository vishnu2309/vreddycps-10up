<?php
/**
 * Renders the student body profile section
 *
 * @package CPS
 */

$students_overview = get_post_meta( get_the_ID(), 'nu_pim_students_overview', true );

if ( empty( $students_overview ) ) {
	return;
}

?>
<div class="wp-block-group" id="student-profile" data-nav-title="<?php esc_attr_e( 'Review our student profile', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">
		<h2><?php esc_html_e( 'Student Body Profile', 'cps' ); ?></h2>
		<?php echo wp_kses_post( $students_overview ); ?>
	</div>
</div>
