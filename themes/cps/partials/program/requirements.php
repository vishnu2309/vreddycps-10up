<?php
/**
 * Renders the program requirements/course plan section on the program detail page.
 *
 * @package CPS
 */

$requirements = \CPS\Program\Helpers\get_requirements();

if ( empty( $requirements ) ) {
	return;
}

$accordion_items = array(
	array(
		'title'   => __( 'General Requirements', 'cps' ),
		'content' => $requirements,
	),
);

$program_id = get_the_ID();

$title     = get_post_meta( $program_id, 'cps_program_course_plan_title', true );
$nav_label = get_post_meta( $program_id, 'cps_program_course_plan_nav_label', true );
$copy      = get_post_meta( $program_id, 'cps_program_course_plan_copy', true );

if ( empty( $title ) ) {
	$title = __( 'Get Set with a Custom Course Plan', 'cps' );
}

if ( empty( $nav_label ) ) {
	$nav_label = __( 'Create a Course Plan', 'cps' );
}
?>

<?php
printf(
	'<div class="wp-block-group"%s%s>',
	empty( $nav_label ) ? '' : ' data-nav-title="' . esc_attr( $nav_label ) . '"', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	empty( $nav_label ) ? '' : ' id="' . sanitize_title( $nav_label ) . '"' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
);
?>
	<div class="wp-block-group__inner-container">
		<h2><?php echo esc_html( $title ); ?></h2>
		<?php if ( ! empty( $copy ) ) : ?>
			<?php echo wpautop( wp_kses_post( $copy ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		<?php endif; ?>
		<div class="block-accordion">
			<div class="block-accordion__items js-block-accordion">
				<?php foreach ( $accordion_items as $accordion_item ) : ?>
					<button class="accordion-header is-style-h3" type="button" aria-label="<?php esc_attr_e( 'Expand/Close', 'cps' ); ?> <?php echo esc_attr( $accordion_item['title'] ); ?>">
						<?php echo esc_html( $accordion_item['title'] ); ?>
					</button>
					<div class="accordion-content">
						<h2 class="accordion-label screen-reader-text"><?php echo esc_html( $accordion_item['title'] ); ?></h2>
						<?php
						/*
						 * Note: not escaping the content. It may contain embeds and all kinds of other HTML elements so we're going
						 *       to have to trust what comes from the API.
						 */
						echo $accordion_item['content']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
						?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
