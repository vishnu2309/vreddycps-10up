<?php
/**
 * Renders the quote section on the program detail page.
 *
 * @package CPS
 */

$attributes = get_post_meta( get_the_ID(), 'cps_program_quote', true );

if ( empty( $attributes ) || empty( $attributes['quote'] ) ) {
	return;
}

?>

<div class="wp-block-group">
	<div class="wp-block-group__inner-container">
		<?php require locate_template( 'partials/block-quote.php' ); ?>
	</div>
</div>
