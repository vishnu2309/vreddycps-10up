<?php
/**
 * Renders the related programs section on the program detail page.
 *
 * @package CPS
 */

$program_id       = get_the_ID();
$related_programs = CPS\Program\Helpers\get_related_programs( $program_id );

if ( empty( $related_programs ) ) {
	return;
}

?>
<div class="wp-block-group" id="find-related-degrees" data-nav-title="<?php esc_attr_e( 'Related Degrees', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">
		<div class="block-program-grid">
			<h2><?php esc_html_e( 'Related Degrees', 'cps' ); ?></h2>
			<div class="grid grid--3 grid--gutter-lg grid--align-left">
				<?php
				foreach ( $related_programs as $related_program ) {
					$program = cps_get_single_program( $related_program );
					include locate_template( 'partials/block-program-grid-item.php' );
				}
				?>
			</div>
		</div>
	</div>
</div>
