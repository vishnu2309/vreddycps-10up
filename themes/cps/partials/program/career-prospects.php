<?php
/**
 * Renders the career prospects section on the program detail page.
 *
 * @package CPS
 */

$program_id = get_the_ID();

$title     = get_post_meta( $program_id, 'cps_program_career_prospects_title', true );
$nav_label = get_post_meta( $program_id, 'cps_program_career_prospects_nav_label', true );
$copy      = get_post_meta( $program_id, 'cps_program_career_prospects_copy', true );
$items     = get_post_meta( $program_id, 'cps_program_career_prospects_items', true );

if ( empty( $title ) && empty( $copy ) && empty( $items ) ) {
	return;
}
?>

<?php
printf(
	'<div class="wp-block-group has-background has-dark-gray-background-color has-white-color"%s%s>',
	empty( $nav_label ) ? '' : ' data-nav-title="' . esc_attr( $nav_label ) . '"', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	empty( $nav_label ) ? '' : ' id="' . sanitize_title( $nav_label ) . '"' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
);
?>
	<div class="wp-block-group__inner-container">
		<?php if ( ! empty( $title ) ) : ?>
			<h2><?php echo esc_html( $title ); ?></h2>
		<?php endif; ?>
		<?php if ( ! empty( $copy ) ) : ?>
			<?php echo wpautop( wp_kses_post( $copy ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
		<?php endif; ?>
		<?php if ( ! empty( $items ) ) : ?>
			<div class="grid grid--<?php echo absint( count( $items ) ); ?> grid--align-left">
				<?php
				foreach ( $items as $item ) {

					$stat  = empty( $item['number'] ) ? '' : $item['number'];
					$title = empty( $item['title'] ) ? '' : $item['title'];
					$copy  = empty( $item['copy'] ) ? '' : $item['copy'];

					printf(
						'<div class="grid-item"><div class="block-stat"><div class="block-stat__number has--color">%s</div><h3>%s</h3>%s</div></div>',
						esc_html( $stat ),
						esc_html( $title ),
						wpautop( wp_kses_post( $copy ) )  // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					);
				}
				?>
			</div>
		<?php endif; ?>
	</div>
</div>
