<?php
/**
 * Renders the hero section on the program detail page.
 *
 * @package CPS
 */

$program_id      = get_the_ID();
$description     = \CPS\Program\Helpers\get_program_short_description();
$image_url       = '';
$overlay_opacity = 0;
$background_type = get_post_meta( $program_id, 'cps_program_bg_type', true );

$classes = array(
	'block-hero',
	'block-hero--left',
	'has-black-background-color',
	'block-hero--image-align-center',
	'block-hero--fixed',
);

if ( 'color' === $background_type ) {
	$classes[] = 'has-white-color';
} else {
	$classes[] = 'block-hero--image';

	$text_color      = get_post_meta( $program_id, 'cps_program_nav_color', true );
	$overlay_color   = get_post_meta( $program_id, 'cps_program_overlay_color', true );
	$overlay_opacity = get_post_meta( $program_id, 'cps_program_overlay_opacity', true );

	if ( empty( $overlay_color ) ) {
		$overlay_color = 'black';
	}

	if ( empty( $overlay_opacity ) ) {
		$overlay_opacity = 40;
	}

	if ( empty( $text_color ) ) {
		$text_color = 'white';
	}

	$classes[] = 'block-hero--overlay-' . $overlay_color;
	$classes[] = 'has-' . $text_color . '-color';

	if ( 'image' === $background_type ) {

		$image = json_decode( get_post_meta( $program_id, 'cps_program_image', true ), true );

		if ( is_array( $image ) && ! empty( $image['id'] ) ) {
			/*
			 * @todo A future update might get an image tag using the appropriate
			 *       srcset and sizes attributes in stead.
			 */
			$image_url = wp_get_attachment_url( $image['id'], 'large' );
		}
	} else { // Assuming background type is 'pim'.

		$image = get_post_meta( $program_id, 'nu_pim_hero_image', true );

		if ( is_array( $image[0] ) && isset( $image[0]['uri'] ) && ! empty( $image[0]['uri'] ) ) {
			$image_url = $image[0]['uri'];
		}
	}
}

$program_initials = get_post_meta( $program_id, 'cps_program_initials', true );

if ( empty( $program_initials ) ) {
	$program_name     = get_post_meta( $program_id, 'nu_pim_formatted_title', true );
	$program_initials = preg_match_all( '/(?<=\s|^)[a-z]/i', $program_name, $matches );
	$program_initials = strtoupper( implode( '', $matches[0] ) );
}

?>
<div class="<?php echo esc_attr( join( ' ', $classes ) ); ?>">
	<?php if ( ! empty( $image_url ) ) : ?>
		<div class="block-hero__image">
			<img src="<?php echo esc_url( $image_url ); ?>" alt="" />
		</div>
	<?php endif; ?>
	<?php if ( 'color' !== $background_type ) : ?>
		<div class="block-hero__overlay" style="opacity:<?php echo (int) $overlay_opacity / 100; ?>"></div>
	<?php endif; ?>
	<div class="block-hero__content block-hero__content--badge">
		<div class="program-badge program-badge--hero">
			<?php echo esc_html( $program_initials ); ?>
		</div>
		<div class="block-hero__content-text">
			<h1><?php the_title(); ?></h1>
			<?php
			if ( ! empty( $description ) ) {
				echo wpautop( wp_kses_post( $description ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
			?>
		</div>
	</div>
</div>
