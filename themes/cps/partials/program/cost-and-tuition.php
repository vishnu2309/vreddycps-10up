<?php
/**
 * Renders the cost and tuition section
 *
 * @package CPS
 */

$items    = get_post_meta( get_the_ID(), 'nu_pim_finance_blocks', true );
$footnote = '';

if ( empty( $items ) || ! is_array( $items ) ) {
	return;
}

/*
 * Fun fact: $items can be an array of values representing a single item, or an array containing multiple items.
 *           Let's check if the 'value' key is present in the array and assume it's a single item if it is.
 */
if ( array_key_exists( 'value', $items ) ) {
	$items = array( $items );
}
?>
<div class="wp-block-group" id="cost-and-tuition" data-nav-title="<?php esc_attr_e( 'Cost and Tuition', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">
		<h2><?php esc_html_e( 'Cost and Tuition', 'cps' ); ?></h2>
		<div class="block-carousel" data-items="3">
			<div class="block-carousel__slider">
				<?php
				foreach ( $items as $item ) {
					$attributes = array(
						'statType'    => 'text',
						'statText'    => $item['value'],
						'headline'    => $item['label'],
						'className'   => 'is-style-date',
						'colorScheme' => 'red',
					);

					if ( 'Tuition' === $item['label'] && ! empty( $item['modal'] ) ) {
						$footnote               = $item['modal'];
						$attributes['statText'] = $attributes['statText'] . '<sup>*</sup>';
					}

					include locate_template( 'partials/block-stat.php' );
				}
				?>
			</div>
			<?php
			if ( ! empty( $footnote ) ) {
				printf(
					'<div class="footnote is-style-small-description">%s</div>',
					wp_kses_post( $footnote )
				);
			}
			?>
		</div>
	</div>
</div>
