<?php
/**
 * Renders the research section on the program detail page
 *
 * @package CPS
 */

$program_id = get_the_ID();
$items      = get_post_meta( $program_id, 'cps_program_research_items', true );

if ( empty( $items ) ) {
	return;
}

$title     = get_post_meta( $program_id, 'cps_program_research_title', true );
$nav_label = get_post_meta( $program_id, 'cps_program_research_nav_label', true );


if ( empty( $title ) ) {
	$title = __( 'Research', 'cps' );
}

if ( empty( $nav_label ) ) {
	$nav_label = __( 'Learn Through Research', 'cps' );
}
?>

<?php
printf(
	'<div class="wp-block-group"%s%s>',
	empty( $nav_label ) ? '' : ' data-nav-title="' . esc_attr( $nav_label ) . '"', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	empty( $nav_label ) ? '' : ' id="' . sanitize_title( $nav_label ) . '"' // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
);
?>
	<div class="wp-block-group__inner-container">
		<h2><?php echo esc_html( $title ); ?></h2>
	</div>
	<div class="block-accordion">
		<div class="block-accordion__items js-block-accordion">
			<?php foreach ( $items as $item ) : ?>
				<?php
				if ( empty( $item['title'] ) || empty( $item['copy'] ) ) {
					continue;
				}
				?>
				<button class="accordion-header is-style-h3" type="button" aria-label="<?php esc_attr_e( 'Expand/Close', 'cps' ); ?> <?php echo esc_attr( $item['title'] ); ?>">
					<?php echo esc_html( $item['title'] ); ?>
				</button>
				<div class="accordion-content">
					<h2 class="accordion-label screen-reader-text"><?php echo esc_html( $item['title'] ); ?></h2>
					<?php
					/*
					 * Note: not escaping the content. It may contain embeds and all kinds of other HTML elements so we're going
					 *       to have to trust what comes from the API.
					 */
					echo $item['copy']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
					?>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
