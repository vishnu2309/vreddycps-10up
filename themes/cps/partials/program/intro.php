<?php
/**
 * Renders the program intro (eg. "Take a quick look") section on the program detail page.
 *
 * @package CPS
 */

$program_id     = get_the_ID();
$intro_items    = [];
$intro_defaults = [
	[
		'title' => __( 'Customize your plan', 'cps' ),
		'copy'  => __( 'We work with you to map your path to your goal.', 'cps' ),
	],
	[
		'title' => __( 'Learn from experience', 'cps' ),
		'copy'  => __( 'From real-world case studies to employer-based projects, we prepare you to manage what comes next.', 'cps' ),
	],
	[
		'title' => __( 'Choose your focus', 'cps' ),
		'copy'  => __( 'Whatever you’re passionate about, you’ll find the classes to sharpen your specialty.', 'cps' ),
	],
	[
		'title' => __( 'Study anywhere, on your time', 'cps' ),
		'copy'  => __( 'Part-time, full-time, on-campus or online, we move at your speed, to get where you’re going.', 'cps' ),
	],
];

foreach ( $intro_defaults as $k => $intro_default ) {

	$index = $k + 1;
	$title = get_post_meta( $program_id, 'cps_program_intro_col_' . $index . '_title', true );
	$copy  = get_post_meta( $program_id, 'cps_program_intro_col_' . $index . '_copy', true );

	$intro_default['title'] = empty( $title ) ? $intro_default['title'] : $title;
	$intro_default['copy']  = empty( $copy ) ? $intro_default['copy'] : $copy;

	$intro_items[] = $intro_default;
}

?>
<div class="wp-block-group has-background has-red-background-color has-white-color" id="take-a-quick-look" data-nav-title="<?php esc_html_e( 'Take a quick look', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">

		<h2><?php esc_html_e( 'Take a quick look', 'cps' ); ?></h2>
		<p><?php esc_html_e( 'We’re committed to creating an education as unique as your career path. So, whether your goal is a new career or moving up in your field, our innovative programs will get you going your way.', 'cps' ); ?></p>

		<div class="grid grid--4">
			<?php foreach ( $intro_items as $k => $item ) : ?>
				<div class="grid-item grid-item--number">
					<p class="grid-item__number">0<?php echo absint( $k + 1 ); ?></p>
					<div class="grid-item__content">
						<h3 class="mb-zero"><?php echo esc_html( $item['title'] ); ?></h3>
						<p class="mt-zero is-style-small"><?php echo wpautop( wp_kses_post( $item['copy'] ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
