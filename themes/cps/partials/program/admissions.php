<?php
/**
 * Renders the admissions section on the program detail page.
 *
 * @package CPS
 */

$accordion_items = [];
$accordion_items = \CPS\Program\Helpers\get_admissions();
?>
<?php if ( ! empty( $accordion_items ) ) : ?>
	<div class="wp-block-group" id="course-plan" data-nav-title="<?php esc_attr_e( 'Admissions', 'cps' ); ?>">
		<div class="wp-block-group__inner-container">
			<h2><?php esc_html_e( "Now Let's Talk Admissions", 'cps' ); ?></h2>
			<p>You know where you are headed and you've seen how our program will lead you there. So let's get going. Here's what you need to know before you enroll.</p>
			<div class="block-accordion">
				<div class="block-accordion__items js-block-accordion">
					<?php foreach ( $accordion_items as $accordion_item ) : ?>
						<button class="accordion-header is-style-h3" type="button" aria-label="<?php esc_attr_e( 'Expand/Close', 'cps' ); ?> <?php echo esc_attr( $accordion_item['title'] ); ?>">
							<?php echo esc_html( $accordion_item['title'] ); ?>
						</button>
						<div class="accordion-content">
							<h2 class="accordion-label screen-reader-text"><?php echo esc_html( $accordion_item['title'] ); ?></h2>
							<?php
							/*
							 * Note: not escaping the content. It may contain embeds and all kinds of other HTML elements so we're going
							 *       to have to trust what comes from the API.
							 */
							echo $accordion_item['content']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
