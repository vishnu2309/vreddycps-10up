<?php
/**
 * Renders the career opportunities and services section
 *
 * @package CPS
 */

$post_meta = get_post_meta( get_the_ID(), 'nu_pim_sidebar', true );

if ( empty( $post_meta ) || ! is_array( $post_meta ) ) {
	return;
}
?>
<div class="wp-block-group" id="student-profile" data-nav-title="<?php esc_attr_e( 'Explore Career Opportunities', 'cps' ); ?>">
	<div class="wp-block-group__inner-container">
		<h2><?php esc_html_e( 'Career Opportunities and Services', 'cps' ); ?></h2>

		<div class="block-accordion">
			<div class="block-accordion__items js-block-accordion">
				<button class="accordion-header is-style-h3" type="button" aria-label="<?php esc_attr_e( 'Expand/Close', 'cps' ); ?> <?php esc_attr_e( 'Overview', 'cps' ); ?>">
					<?php esc_html_e( 'Overview', 'cps' ); ?>
				</button>
				<div class="accordion-content">
					<h2 class="accordion-label screen-reader-text"><?php esc_attr_e( 'Overview', 'cps' ); ?></h2>
					<?php
					foreach ( $post_meta as $item ) {

						if ( isset( $item['field_display_title'] ) && is_array( $item['field_display_title'] ) && ! empty( $item['field_display_title'] ) ) {

							$value = $item['field_display_title'][0]['value'];

							if ( ! empty( $value ) ) {
								echo '<h4>' . esc_html( $value ) . '</h4>';
							}
						}

						if ( isset( $item['body'] ) && is_array( $item['body'] ) && ! empty( $item['body'] ) ) {

							$value = $item['body'][0]['value'];

							if ( ! empty( $value ) ) {
								// wp_kses_post() does not like html comments.
								$value = preg_replace( '/<!--(.|\s)*?-->/', '', $value );
								echo wp_kses_post( $value );
							}
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
