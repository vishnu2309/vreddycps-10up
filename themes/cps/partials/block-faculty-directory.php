<?php
/**
 * Renders the Faculty Directory block
 *
 * @package CPS
 */

$faculty_members = cps_get_faculty_members(
	array(
		'tax_query' => array(
			array(
				'taxonomy' => 'person_cat',
				'field'    => 'slug',
				'terms'    => [ 'faculty', 'staff' ],
			),
		),
	)
);

if ( empty( $faculty_members ) ) {
	return;
}

$faculty_member_initials = array_keys( $faculty_members );

if ( empty( $faculty_member_initials ) ) {
	return;
}

?>

<div class="block-faculty-directory">

	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h2 class="faculty-directory-title">
			<?php echo wp_kses_post( $attributes['headline'] ); ?>
		</h2>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<div>
			<?php echo wp_kses_post( $attributes['copy'] ); ?>
		</div>
	<?php endif; ?>

	<ul class="initials">
		<?php foreach ( $faculty_member_initials as $initial ) : ?>
			<li>
				<button class="initial initial-<?php echo esc_attr( $initial ); ?>" data-initial="<?php echo esc_attr( $initial ); ?>" aria-controls="faculty-members-<?php echo esc_attr( $initial ); ?>">
					<?php echo esc_html( strtoupper( $initial ) ); ?>
				</button>
			</li>
		<?php endforeach; ?>
	</ul>

	<div class="faculty-directory-search">
		<label for="faculty-directory-search-input"><?php esc_html_e( 'Find by Name', 'cps' ); ?></label>
		<select name="faculty-member-select" id="faculty-member-select" class="js-faculty-member" multiple="multiple">
			<?php
			foreach ( $faculty_members as $initial => $faculty_member_list ) :
				?>
				<optgroup label="<?php echo esc_attr( strtoupper( $initial ) ); ?>">
					<?php
					foreach ( $faculty_member_list as $i => $faculty_member ) :
						?>
						<option value="<?php echo esc_url( $faculty_member['link'] ); ?>" data-role="<?php echo esc_attr( $faculty_member['role'] ); ?>">
							<?php echo esc_html( "{$faculty_member['first_name']} {$faculty_member['last_name']}" ); ?>
						</option>
						<?php
					endforeach;
					?>
				</optgroup>
				<?php
			endforeach;
			?>
		</select>
	</div>


	<div class="faculty-member-list">
		<?php
		$index = 0;

		foreach ( $faculty_members as $initial => $faculty_member_list ) :

			$class = 0 === $index ? 'active' : '';
			?>
			<button class="small-screen-initial initial initial-<?php echo esc_attr( $initial ); ?>" data-initial="<?php echo esc_attr( $initial ); ?>" aria-controls="faculty-members-<?php echo esc_attr( $initial ); ?>">
				<?php echo esc_html( strtoupper( $initial ) ); ?>
			</button>

			<table id="faculty-members-<?php echo esc_attr( $initial ); ?>" class="faculty-members faculty-members-<?php echo esc_attr( $initial ); ?> <?php echo esc_attr( $class ); ?>" data-initial="<?php echo esc_attr( $initial ); ?>">
				<thead>
					<tr>
						<th class="is-style-h3"><?php esc_html_e( 'Name', 'cps' ); ?></th>
						<th class="is-style-h3"><?php esc_html_e( 'Title', 'cps' ); ?></th>
					</tr>
				</thead>
				<tbody>
					<tr class="initial-heading">
						<td colspan="2"><h2><?php echo esc_html( strtoupper( $initial ) ); ?></h2></td>
					</tr>
					<?php foreach ( $faculty_member_list as $i => $faculty_member ) : ?>
						<tr data-last-name="<?php echo esc_attr( $faculty_member['last_name'] ); ?>" data-first-name="<?php echo esc_attr( $faculty_member['first_name'] ); ?>" data-link="<?php echo esc_attr( $faculty_member['link'] ); ?>">
							<td>
								<a href="<?php echo esc_url( $faculty_member['link'] ); ?>"><?php echo esc_html( "{$faculty_member['first_name']} {$faculty_member['last_name']}" ); ?></a>
							</td>
							<td><?php echo esc_html( $faculty_member['role'] ); ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php
			$index++;

		endforeach;
		?>
	</div>
</div>
