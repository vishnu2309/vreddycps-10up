<?php
/**
 * Renders the Faculty Grid block
 *
 * @package CPS
 *
 * @global $attributes
 * @global $content
 */

?>

<div class="block-faculty-grid">
	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h2><?php echo wp_kses_post( $attributes['headline'] ); ?></h2>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore ?>
	<?php endif; ?>

	<div class="faculty-grid">
		<div class="faculty-grid__slider">
			<?php if ( ! empty( $content ) ) : ?>
				<?php echo $content; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			<?php endif; ?>
		</div>
	</div>

	<?php
	if ( ! empty( $attributes['ctaLink'] ) && ! empty( $attributes['ctaText'] ) ) {
		printf(
			'<div class="wp-block-button aligncenter"><a class="wp-block-button__link has-white-background-color has-background" href="%s">%s</a></div>',
			esc_url( $attributes['ctaLink'] ),
			wp_kses_post( $attributes['ctaText'] )
		);
	}
	?>
</div>
