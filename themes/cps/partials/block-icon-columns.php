<?php
/**
 * Renders the Icon Columns block
 *
 * @package CPS\Blocks
 *
 * @global $attributes
 */

?>
<div class="block-icon-columns">

	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h2><?php echo esc_html( $attributes['headline'] ); ?></h2>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore ?>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['columns'] ) ) : ?>

		<div class="grid grid--icons grid--<?php echo esc_attr( $attributes['columns'] ); ?>">

			<?php if ( ! empty( $content ) ) : ?>
				<?php echo $content; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			<?php endif; ?>

		</div>

	<?php endif; ?>

</div>
