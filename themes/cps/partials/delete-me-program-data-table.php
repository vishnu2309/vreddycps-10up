<?php // phpcs:disable
/**
 * Temporary table to visualize fields stored via the FIM API plugin.
 *
 * This should be deleted.
 *
 * @package CPS
 */

$fields             = get_post_meta( get_the_ID() );
$whitelisted_fields = \NU_PIM\Helpers\get_whitelisted_fields();
?>
<style>
#temp-table {
	width: 100%;
}

#temp-table, #temp-table th, #temp-table td {
	border: 1px solid black;
	text-align: left;
	vertical-align: top;
}

#temp-table pre {
	font-size: 13px;
}
</style>
<table id="temp-table">
	<tbody>
		<?php if ( ! empty( get_post_thumbnail_id( get_the_ID() ) ) ) : ?>
			<tr>
				<th>Featured Image</th>
				<td>
					<?php the_post_thumbnail(); ?>
				</td>
			</tr>
		<?php endif; ?>
		<tr>
			<th>
				<strong>Location</strong><br>
			</th>
			<td>
				<?php
				$location_terms = wp_get_post_terms( get_the_ID(), 'nu_pim_location' );
				$location_terms = wp_list_pluck( $location_terms, 'name' );
				echo join( ', ', $location_terms );
				?>
			</td>
		</tr>
		<tr>
			<th>
				<strong>Area of Study</strong><br>
			</th>
			<td>
				<?php
				$aos_terms = wp_get_post_terms( get_the_ID(), 'nu_pim_area_of_study' );
				$aos_terms = wp_list_pluck( $aos_terms, 'name' );
				echo join( ', ', $aos_terms );
				?>
			</td>
		</tr>
		<tr>
			<th>
				<strong>Program Level</strong><br>
				<small style="font-size:12px;font-weight:normal;">Note: the import script uses both the 'field_degree_type' and 'field_undergrad_degree_type' to create the "program level" value for a project.</small>
			</th>
			<td>
				<?php
				$pl_terms = wp_get_post_terms( get_the_ID(), 'nu_pim_degree_type' );
				$pl_terms = wp_list_pluck( $pl_terms, 'name' );
				echo join( ', ', $pl_terms );
				?>
			</td>
		</tr>
		<?php foreach ( $whitelisted_fields as $key => $value ) : ?>

			<?php
			if ( array_key_exists( 'nu_pim_' . $key, $fields ) ) {
				$key = 'nu_pim_' . $key;
			} else {
				$key = 'nu_pim_' . str_replace( 'field_', '', $key );
			}
			?>
			<?php if ( array_key_exists( $key, $fields ) ) : ?>

				<tr>
					<th>
						<strong><?php echo esc_html( $value['label'] ); ?></strong>
						<br>
						<span style="font-weight: normal;"><?php echo esc_html( str_replace( 'nu_pim_', '', $key ) ); ?></span>
					</th>
					<td>
						<?php
						$meta_value = $fields[ $key ][0];
						$meta_value = maybe_unserialize( $meta_value );
						?>
						<?php if ( is_array( $meta_value ) ) : ?>

							<?php
							if ( in_array( $key, [ 'nu_pim_program_thumbnail', 'nu_pim_hero_image' ] ) ) {
								if ( isset( $meta_value[0]['uri'] ) ) {
									echo '<img src="' . esc_url( $meta_value[0]['uri'] ) . '" />';
								}
							}
							if ( in_array( $key, [ 'nu_pim_program_family', 'nu_pim_related_program', 'nu_pim_related_program_2' ] ) ) {

								if ( ! empty( $meta_value ) ) {
									echo '<ul>';
									foreach ( $meta_value as $pid ) {

										$_post = \NU_PIM\Helpers\get_post_by_pid( $pid );

										if ( is_a( $_post, '\WP_Post' ) ) {
											printf(
												'<li><a href="%s">%s</a></li>',
												get_permalink( $_post->ID ),
												get_the_title( $_post->ID )
											);
										}
									}
									echo '</ul>';
								}
							}

							if ( in_array( $key, [ 'nu_pim_entry_terms', 'nu_pim_commitment', 'nu_pim_study_options' ] ) ) {

								if ( ! empty( $meta_value ) ) {
									$entry_terms = [];
									foreach ( $meta_value as $_value ) {
										$entry_terms[] = $_value['name']['0']['value'];
									}
									echo '<strong>' . join( ', ', $entry_terms ) . '</strong>';
								}
							}
							?>

							<pre><?php echo var_export( $meta_value, true ); // phpcs:ignore ?></pre>

						<?php else : ?>
							<pre><?php echo htmlspecialchars( $meta_value ); // phpcs:ignore ?></pre>
						<?php endif; ?>

					</td>
				</tr>
			<?php endif; ?>
		<?php endforeach; ?>
	</tbody>
</table>
