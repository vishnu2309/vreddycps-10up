<?php
/**
 * Renders the Program Grid Item block
 *
 * @package CPS
 *
 * @global $program
 */

if ( empty( $program ) ) {
	return;
}
?>

<div class="grid-item">
	<div class="grid-item__badge">
		<div class="program-badge program-badge--center">
			<?php echo esc_html( $program['initials'] ); ?>
		</div>
	</div>
	<div class="grid-item__content">
		<h3>
			<?php
			$item_title = array( $program['title'] );

			if ( ! empty( $program['level'] ) ) {
				$item_title[] = $program['level'];
			}

			if ( ! empty( $program['location'] ) ) {
				$item_title[] = $program['location'];
			}

			echo esc_html( join( ' — ', $item_title ) );
			?>
		</h3>

		<p class="is-style-small">
			<?php echo wp_kses_post( $program['body'] ); ?>
		</p>

		<p class="is-style-small">
			<a href="<?php echo esc_url( $program['link'] ); ?>">
				<?php esc_html_e( 'Learn more', 'cps' ); ?>

				<span class="screen-reader-text">
					<?php
					printf(
						'%1$s %2$s',
						esc_html__( 'about', 'cps' ),
						esc_html( $program['title'] )
					);
					?>
				</span>
			</a>
		</p>
	</div>
</div>
