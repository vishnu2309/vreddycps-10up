<?php
/**
 * Renders the Accordion block
 *
 * @package CPS
 *
 * @global $attributes
 * @global $content
 */

if ( empty( $attributes['title'] ) ) {
	return;
}
?>
<button class="accordion-header is-style-h3" type="button" aria-label="Expand/close <?php echo esc_attr( $attributes['title'] ); ?>">
	<?php echo wp_kses_post( $attributes['title'] ); ?>
</button>
<div class="accordion-content">
	<h2 class="accordion-label screen-reader-text"><?php echo wp_kses_post( $attributes['title'] ); ?></h2>
	<?php echo wp_kses_post( $content ); ?>
</div>
