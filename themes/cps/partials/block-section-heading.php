<?php
/**
 * Renders the Section Heading block
 *
 * @package CPS
 *
 * @global $attributes
 */

?>

<div class="block-section-heading">

	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h2><?php echo esc_html( $attributes['headline'] ); ?></h2>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore ?>
	<?php endif; ?>

</div>
