<?php
/**
 * Renders the Faculty Column block
 *
 * @package CPS
 *
 * @global $post_data
 */

if ( empty( $post_data ) ) {
	return;
}

?>

<div class="faculty-card">
	<?php if ( ! empty( $post_data['imageID'] ) ) : ?>
		<div class="faculty-card__image aspect-ratio">
			<?php
			echo wp_kses_post(
				wp_get_attachment_image(
					$post_data['imageID'],
					'full',
					false,
					[
						'class' => 'aspect-ratio__element',
						// @todo Use proper sizes.
						'sizes' => '(max-width: 767px) 100vw, 46vw',
					]
				)
			);
			?>
		</div>
	<?php endif; ?>

	<div class="faculty-card__content">
		<?php if ( ! empty( $post_data['titleText'] ) ) : ?>
			<h3><?php echo esc_html( $post_data['titleText'] ); ?></h3>
		<?php endif; ?>
		<?php if ( ! empty( $post_data['customProfExp'] ) && is_array( $post_data['customProfExp'] ) ) : ?>
			<ul class="is-style-small-description">
				<?php foreach ( $post_data['customProfExp'] as $profile_item ) : ?>
					<li><?php echo esc_html( $profile_item ); ?></li>
				<?php endforeach; ?>
			</ul>
		<?php elseif ( ! empty( $post_data['profExp'] ) && is_array( $post_data['profExp'] ) ) : ?>
			<ul class="is-style-small-description">
				<?php foreach ( $post_data['profExp'] as $profile_item ) : ?>
					<li><?php echo esc_html( $profile_item ); ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif ?>
		<?php
		if ( ! empty( $post_data['ctaLink'] ) ) {
			printf(
				'<p class="is-style-small"><a href="%s">%s</a></p>',
				esc_url( $post_data['ctaLink'] ),
				esc_html__( 'See more', 'cps' )
			);
		}
		?>
	</div>
</div>
