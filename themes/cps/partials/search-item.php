<?php
/**
 * Search item template.
 *
 * @package CPS
 */

$post_id            = get_the_ID();
$post_type          = get_post_type( $post_id );
$content_type_label = cps_get_content_type_label( $post_type );
?>
<article class="search-item single-item">
	<div class="search-item__content">
		<p class="is-style-h4">
			<?php echo esc_html( $content_type_label ); ?>
		</p>

		<h2 class="post-title">
			<a href="<?php the_permalink(); ?>" class="post-link">
				<?php the_title(); ?>
			</a>
		</h2>

		<div class="post-excerpt">
			<?php the_excerpt(); ?>
		</div>
	</div>
</article>
