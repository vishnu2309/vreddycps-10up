<?php
/**
 * Renders the Program Finder block
 *
 * @package CPS
 *
 * @global $attributes
 */

?>

<div class="block-program-finder has-red-background-color has-white-color">

	<div class="block-program-finder__inner">

		<div class="block-program-finder__headline">
			<?php if ( ! empty( $attributes['headline'] ) ) : ?>
				<h2><?php echo esc_html( $attributes['headline'] ); ?></h2>
			<?php endif; ?>
		</div>

		<div class="block-program-finder__finder">
			<form action="#" class="block-program-finder__form">
				<select class="js-select2" name="programs" id="programs">
					<?php if ( ! empty( $attributes['dropdownLabel'] ) ) : ?>
						<option selected disabled value=""><?php echo esc_html( $attributes['dropdownLabel'] ); ?></option>
					<?php endif; ?>
					<?php if ( ! empty( $attributes['primaryLinksLabel'] ) && ! empty( $attributes['primaryLinks'] ) ) : ?>
						<optgroup label="<?php echo esc_attr( $attributes['primaryLinksLabel'] ); ?>">
						<?php
						$primary_links = json_decode( $attributes['primaryLinks'], true );
						if ( ! empty( $primary_links ) ) {
							foreach ( $primary_links as $link ) {
								if ( ! empty( $link['label'] ) && ! empty( $link['url'] ) ) {
									printf(
										'<option value="%s">%s</option>',
										esc_url( $link['url'] ),
										esc_html( $link['label'] )
									);
								}
							}
						}
						?>
						</optgroup>
					<?php endif; ?>
					<?php if ( ! empty( $attributes['secondaryLinksLabel'] ) && ! empty( $attributes['secondaryLinks'] ) ) : ?>
						<optgroup label="<?php echo esc_attr( $attributes['secondaryLinksLabel'] ); ?>">
						<?php
						$secondary_links = json_decode( $attributes['secondaryLinks'], true );
						if ( ! empty( $secondary_links ) ) {
							foreach ( $secondary_links as $link ) {
								if ( ! empty( $link['label'] ) && ! empty( $link['url'] ) ) {
									printf(
										'<option value="%s">%s</option>',
										esc_url( $link['url'] ),
										esc_html( $link['label'] )
									);
								}
							}
						}
						?>
						</optgroup>
					<?php endif; ?>
				</select>
				<button class="button-tertiary" type="submit"><?php echo esc_html( $attributes['buttonLabel'] ); ?></button>
			</form>
		</div>

	</div>

</div>
