<?php
/**
 * Renders the Quote block
 *
 * @package CPS
 *
 * @global $attributes
 */

?>

<blockquote class="block-cps-quote">

	<?php if ( ! empty( $attributes['quote'] ) ) : ?>
		<p><?php echo wp_kses_post( $attributes['quote'] ); ?></p>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['name'] ) || ! empty( $attributes['subhead'] ) || ! empty( $attributes['link'] ) ) : ?>

		<cite>

			<?php if ( ! empty( $attributes['link'] ) ) : ?>
				<a href="<?php echo esc_url( $attributes['link'] ); ?>">
			<?php endif; ?>

			<?php if ( ! empty( $attributes['name'] ) ) : ?>
				<span class="is-style-h5 has-red-color"><?php echo wp_kses_post( $attributes['name'] ); ?></span>
			<?php endif; ?>

			<?php if ( ! empty( $attributes['subhead'] ) ) : ?>
				<span class="is-style-small-description"><?php echo wp_kses_post( $attributes['subhead'] ); ?></span>
			<?php endif; ?>

			<?php if ( ! empty( $attributes['link'] ) ) : ?>
				</a>
			<?php endif; ?>

		</cite>

	<?php endif; ?>

</blockquote>
