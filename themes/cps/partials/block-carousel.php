<?php
/**
 * Renders the Carousel block
 *
 * @package CPS
 *
 * @global $attributes
 * @global $content
 */

if ( empty( $content ) ) {
	return;
}

?>
<div
	class="block-carousel"
	data-items="<?php echo esc_attr( $attributes['columnCount'] ); ?>"
>
	<div class="block-carousel__slider">
		<?php echo $content; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
	</div>
</div>
