<?php
/**
 * Renders the Hero block
 *
 * @package CPS
 *
 * @global $attributes
 */

$is_faculty_spotlight = is_page() && 'faculty_spotlight' === get_page_template_slug();
$faculty_id           = $is_faculty_spotlight ? get_post_meta( get_the_ID(), 'cps_faculty_id', true ) : false;

$align            = empty( $attributes['textAlignment'] ) ? 'left' : $attributes['textAlignment'];
$background_color = empty( $attributes['backgroundColor'] ) ? 'white' : $attributes['backgroundColor'];
$background_type  = empty( $attributes['backgroundType'] ) ? 'color' : $attributes['backgroundType'];
$image_align      = empty( $attributes['imageAlignment'] ) ? 'center' : $attributes['imageAlignment'];
$navigation_color = empty( $attributes['navigationColor'] ) ? 'black' : $attributes['navigationColor'];
$overlay_color    = empty( $attributes['overlayColor'] ) ? 'none' : $attributes['overlayColor'];
$overlay_opacity  = empty( $attributes['overlayOpacity'] ) ? 0 : $attributes['overlayOpacity'];
$fixed_ratio      = isset( $attributes['fixedRatio'] ) ? true : false;
$text_color       = '';

if ( 'color' !== $background_type ) {
	$background_color = 'white';
}

if ( 'white' !== $background_color ) {
	$text_color = 'white';
}
?>

<?php
printf(
	'<div class="block-hero block-hero--%s block-hero--%s has-%s-background-color has-%s-color block-hero--overlay-%s block-hero--image-align-%s block-hero--%s">',
	esc_attr( $align ),
	esc_attr( $background_type ),
	'color' === $background_type ? esc_attr( $background_color ) : '',
	'color' === $background_type ? esc_attr( $text_color ) : esc_attr( $navigation_color ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	esc_attr( $overlay_color ),
	'color' !== $background_type ? esc_attr( $image_align ) : '', // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	$fixed_ratio ? esc_attr( 'fixed' ) : esc_attr( 'free' ) // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
);
?>

	<?php if ( 'image' === $background_type && ! empty( $attributes['image'] ) ) : ?>
		<div class="block-hero__image">
		<?php
		echo wp_get_attachment_image(
			$attributes['image'],
			'hero-large',
			false,
			[
				'sizes' => '100vw',
			]
		);
		?>
		</div>
	<?php endif; ?>

	<?php if ( 'video' === $background_type && ! empty( $attributes['video'] ) ) : ?>
		<div class="block-hero__video">
			<?php
			/*
			 * @todo Render actual video.
			 *
			 * @see https://wistia.com/learn/culture/fullscreen-video-homepage
			 */
			echo $attributes['video']; // phpcs:ignore
			?>

			<?php if ( ! empty( $attributes['image'] ) ) : ?>
				<?php
				echo wp_get_attachment_image(
					$attributes['image'],
					'hero-large',
					false,
					[
						'sizes' => '100vw',
					]
				);
				?>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php
	/*
	 * The Hero block allows selecting a color and opacity value
	 * for a transparent overlay on top of the background image
	 * or video.
	 */
	if ( 'none' !== $overlay_color ) {
		printf(
			'<div class="block-hero__overlay" style="opacity:%s"></div>',
			$overlay_opacity !== 100 ? $overlay_opacity/100 : 100 // phpcs:ignore
		);
	}
	?>

	<?php
	if ( ! is_home() && ! is_front_page() && function_exists( 'breadcrumb_trail' ) ) {
		$breadcrumb_args = array(
			'labels' => array(
				'browse' => false,
			),
		);
		breadcrumb_trail( $breadcrumb_args );
	}
	?>

	<div class="block-hero__content <?php echo esc_attr( $faculty_id ? 'block-hero__content--badge' : '' ); ?>">

		<?php if ( $is_faculty_spotlight ) : ?>
			<?php
			if ( $faculty_id ) :
				$faculty_name     = get_the_title( $faculty_id );
				$faculty_initials = preg_match_all( '/(?<=\s|^)[a-z]/i', $faculty_name, $matches );
				$faculty_initials = strtoupper( implode( '', $matches[0] ) );
				?>
				<div class="program-badge program-badge--hero">
					<?php echo esc_html( $faculty_initials ); ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>

		<div class="block-hero__content-text">

			<?php if ( ! empty( $attributes['headline'] ) ) : ?>
				<h1><?php echo wp_kses_post( $attributes['headline'] ); ?></h1>
			<?php endif; ?>

			<?php if ( ! empty( $attributes['copy'] ) ) : ?>
				<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			<?php endif; ?>

			<div class="wp-block-buttons">
				<?php if ( ! empty( $attributes['ctaPrimaryLabel'] ) && ! empty( $attributes['ctaPrimaryUrl'] ) ) : ?>
					<?php
					printf(
						'<div class="wp-block-button"><a href="%s" class="wp-block-button__link has-white-background-color has-background">%s</a></div>',
						esc_url( $attributes['ctaPrimaryUrl'] ),
						esc_html( $attributes['ctaPrimaryLabel'] )
					);
					?>
				<?php endif; ?>

				<?php if ( ! empty( $attributes['ctaSecondaryLabel'] ) && ! empty( $attributes['ctaSecondaryUrl'] ) ) : ?>
					<?php
					printf(
						'<div class="wp-block-button"><a href="%s" class="wp-block-button__link has-white-background-color has-background">%s</a></div>',
						esc_url( $attributes['ctaSecondaryUrl'] ),
						esc_html( $attributes['ctaSecondaryLabel'] )
					);
					?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php
/*
 * Adding post-date here.
 *
 * We cannot add in the the_content filter, reason is we're adding hero block at first position
 * if it is not present in the content, and the date should come after it.
 */
if ( 'post' === get_post_type( get_the_ID() ) ) {
	echo '<p class="post-date">' . esc_html( get_the_date( 'F d, Y' ) ) . '</p>';
}
