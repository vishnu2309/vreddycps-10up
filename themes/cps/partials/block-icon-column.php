<?php
/**
 * Renders the single Icon Column block
 *
 * @package CPS\Blocks
 *
 * @global $attributes
 */

if ( empty( $attributes['title'] ) && $attributes['copy'] ) {
	return;
}
?>
<div class="grid-item grid-item--icon">
	<?php if ( ! empty( $attributes['icon'] ) ) : ?>
		<div class="grid-item__icon">
			<?php cps_svg_icon( $attributes['icon'], true ); ?>
		</div>
	<?php endif; ?>

	<div class="grid-item__content">
		<?php if ( ! empty( $attributes['title'] ) ) : ?>
			<h3><?php echo esc_html( $attributes['title'] ); ?></h3>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['copy'] ) ) : ?>
			<div class="is-style-small">
				<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			</div>
		<?php endif; ?>
	</div>
</div>
