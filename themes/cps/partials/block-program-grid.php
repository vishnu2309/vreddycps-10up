<?php
/**
 * Renders the Program Grid block
 *
 * @package CPS
 *
 * @global $attributes
 * @global $content
 */

?>
<div class="block-program-grid">

	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h2><?php echo esc_html( $attributes['headline'] ); ?></h2>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<?php echo wp_kses_post( wpautop( $attributes['copy'] ) ); ?>
	<?php endif; ?>

	<div class="grid grid--3 grid--gutter-lg grid--align-left">
		<?php
		if ( 'automatic' === $attributes['mode'] && ! empty( $attributes['programLevel'] ) ) :

			$args = [
				'programLevel' => $attributes['programLevel'],
			];

			$programs = cps_get_programs( $args );

			if ( ! empty( $programs ) ) :

				foreach ( $programs as $program ) :

					include locate_template( 'partials/block-program-grid-item.php' );

				endforeach;

			endif;

		elseif ( 'curated' === $attributes['mode'] && ! empty( $content ) ) :

			echo $content; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		endif;
		?>
	</div>
</div>
