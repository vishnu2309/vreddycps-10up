<?php
/**
 * Renders the favicons and other mobile 'app' settings
 *
 * @package CPS
 */

$apple_touch_icon_sizes = array(
	'57x57',
	'60x60',
	'72x72',
	'76x76',
	'114x114',
	'120x120',
	'144x144',
	'152x152',
);
?>
<?php
foreach ( $apple_touch_icon_sizes as $apple_touch_icon_size ) {
	printf(
		'<link rel="apple-touch-icon" sizes="%1$s" href="https://brand.northeastern.edu/global/assets/favicon/apple-touch-%1$s.png?v=2" />',
		esc_attr( $apple_touch_icon_size ),
		esc_attr( $apple_touch_icon_size )
	);
}
?>
<link rel="icon" sizes="144x144" type="image/png" href="https://brand.northeastern.edu/global/assets/favicon/android-chrome-144x144.png?v=2" />
<link rel="icon" sizes="32x32" type="image/png" href="https://brand.northeastern.edu/global/assets/favicon/favicon-32x32.png?v=2" />
<link rel="icon" sizes="16x16" type="image/png" href="https://brand.northeastern.edu/global/assets/favicon/favicon-16x16.png?v=2" />
<link rel="manifest" href="https://brand.northeastern.edu/global/assets/favicon/manifest.json" />
<meta name="theme-color" content="#ffffff" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="msapplication-tap-highlight" content="no" />
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="https://brand.northeastern.edu/global/assets/favicon/mstile-144x144.png?v=2" />
