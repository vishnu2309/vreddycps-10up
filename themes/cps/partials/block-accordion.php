<?php
/**
 * Renders the Accordion block
 *
 * @package CPS
 *
 * @global $attributes
 * @global $content
 */

?>
<div class="block-accordion">
	<?php if ( ! empty( $attributes['headline'] ) ) : ?>
		<h2 class="block-accordion__title"><?php echo wp_kses_post( $attributes['headline'] ); ?></h2>
	<?php endif; ?>

	<?php if ( ! empty( $attributes['copy'] ) ) : ?>
		<?php echo wpautop( wp_kses_post( $attributes['copy'] ) ); // phpcs:ignore ?>
	<?php endif; ?>

	<div class="block-accordion__items js-block-accordion">
		<?php echo wp_kses_post( $content ); ?>
	</div>
</div>
