<?php
/**
 * Renders the Card block
 *
 * @package CPS
 *
 * @global $attributes
 */

?>
<div class="block-card">

	<?php if ( ! empty( $attributes['image'] ) ) : ?>
		<div class="block-card__image">
			<?php echo wp_get_attachment_image( $attributes['image']['id'], 'full' ); ?>
		</div>
	<?php endif; ?>

	<div class="block-card__content">
		<?php if ( ! empty( $attributes['headline'] ) ) : ?>
			<h3><?php echo wp_kses_post( $attributes['headline'] ); ?></h3>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['copy'] ) ) : ?>
			<p><?php echo wp_kses_post( $attributes['copy'] ); ?></p>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['ctaLabelOne'] ) && ! empty( $attributes['ctaLinkOne'] ) ) : ?>
			<p>
				<a href="<?php echo esc_url( $attributes['ctaLinkOne'] ); ?>"><?php echo wp_kses_post( $attributes['ctaLabelOne'] ); ?></a>
			</p>
		<?php endif; ?>

	</div>

</div>
