<?php
/**
 * Renders the Image & Text block
 *
 * @package CPS
 *
 * @global $attributes
 */

$has_image     = ! empty( $attributes['image'] );
$block_classes = array(
	'single-item',
	'block-image-text',
	'block-image-text--align' . $attributes['mediaPosition'],
	$has_image ? 'block-image-text--has-image' : 'block-image-text--no-image',
);
?>
<div class="<?php echo esc_attr( implode( ' ', $block_classes ) ); ?>">

	<?php if ( $has_image ) : ?>
		<div class="block-image-text__image">
			<?php echo wp_get_attachment_image( $attributes['image']['id'], 'full' ); ?>
		</div>
	<?php endif; ?>

	<div class="block-image-text__content">
		<?php if ( ! empty( $attributes['headline'] ) ) : ?>
			<h3><?php echo wp_kses_post( $attributes['headline'] ); ?></h3>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['datetime'] ) ) : ?>
			<p class="block-image-text__date"><?php cps_svg_icon( 'calendar', true ); ?><strong><?php echo esc_html( $attributes['datetime'] ); ?></strong></p>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['copy'] ) ) : ?>
			<p class="is-style-small block-image-text__copy"><?php echo wp_kses_post( $attributes['copy'] ); ?></p>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['ctaLabelOne'] ) && ! empty( $attributes['ctaLinkOne'] ) ) : ?>
			<p class="is-style-small">
				<a href="<?php echo esc_url( $attributes['ctaLinkOne'] ); ?>"><?php echo wp_kses_post( $attributes['ctaLabelOne'] ); ?></a>
			</p>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['ctaLabelTwo'] ) && ! empty( $attributes['ctaLinkTwo'] ) ) : ?>
			<p class="is-style-small">
				<a href="<?php echo esc_url( $attributes['ctaLinkTwo'] ); ?>"><?php echo wp_kses_post( $attributes['ctaLabelTwo'] ); ?></a>
			</p>
		<?php endif; ?>

		<?php if ( ! empty( $attributes['ctaLabelThree'] ) && ! empty( $attributes['ctaLinkThree'] ) ) : ?>
			<p class="is-style-small">
				<a href="<?php echo esc_url( $attributes['ctaLinkThree'] ); ?>"><?php echo wp_kses_post( $attributes['ctaLabelThree'] ); ?></a>
			</p>
		<?php endif; ?>
	</div>

</div>
