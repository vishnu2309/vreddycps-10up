<?php
/**
 * Renders the CTA links.
 *
 * These links will be rendered below a Hero block on the Home page and the Program Detail pages.
 * When a site visitor scrolls down the page, these links remain visible by sticking to the top of the page.
 *
 * These links can be set here: /wp-admin/options-general.php?page=call-to-action
 *
 * @package CPS
 */

$cta_links = get_option( 'cps_floating_cta' );

if ( empty( $cta_links ) ) {
	return;
}

?>
<ul class="floating-cta">
	<?php
	if ( ! empty( $cta_links['first_link_label'] ) && ! empty( $cta_links['first_link_url'] ) ) :

		?>
		<li>
			<a href="<?php echo esc_url( $cta_links['first_link_url'] ); ?>"><?php echo esc_html( $cta_links['first_link_label'] ); ?></a>
		</li>
		<?php

	endif;

	if ( ! empty( $cta_links['second_link_label'] ) && ! empty( $cta_links['second_link_url'] ) ) :

		?>
		<li>
			<a href="<?php echo esc_url( $cta_links['second_link_url'] ); ?>"><?php echo esc_html( $cta_links['second_link_label'] ); ?></a>
		</li>
		<?php

	endif;
	?>
</ul>
