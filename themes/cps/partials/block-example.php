<?php
/**
 * Renders the Example block
 *
 * @package CPS\Blocks
 *
 * @global $attributes
 */

if ( empty( $attributes['customTitle'] ) ) {
	return;
}
?>

<div class="block-example">
	<h2><?php echo esc_html( $attributes['customTitle'] ); ?></h2>
</div>
