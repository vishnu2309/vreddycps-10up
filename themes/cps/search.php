<?php
/**
 * The template for displaying search results pages.
 *
 * @package CPS
 */

global $wp_query;

get_header();
?>

	<div class="block-hero block-hero--color">
		<div class="block-hero__content">
			<h1>
				<?php esc_html_e( 'Search Results', 'cps' ); ?>
			</h1>
			<p>Optional copy</p>
		</div>
	</div>

	<section itemscope itemtype="https://schema.org/SearchResultsPage">

		<div class="search-results-search-form">
			<?php get_search_form(); ?>

			<div class="search-count-display is-style-h3">
				<?php printf( '%d Results Found', esc_html( $wp_query->found_posts ) ); ?>
			</div>
		</div>

		<?php if ( have_posts() ) : ?>

			<div id="load-more-ajax-container">
				<?php
				while ( have_posts() ) {

					the_post();

					get_template_part( 'partials/search-item' );
				}
				?>
			</div>

			<?php if ( $wp_query->max_num_pages > 1 ) : ?>
				<div class="load-more">
					<button class="button-primary">
						<?php esc_html_e( 'Load more', 'cps' ); ?>
						<div class="cps-spinner spinner"></div>
					</button>
				</div>
			<?php endif; ?>

		<?php else : ?>

			<h2>
				<?php
				printf(
					/* translators: the search query */
					esc_html__( 'We\'re Sorry, there are no results that match "%s".', 'cps' ),
					'<span>' . esc_html( get_search_query() ) . '</span>'
				);
				?>
			</h2>

			<?php if ( has_nav_menu( 'navigation-serp' ) ) : ?>
				<p>
					<?php esc_html_e( 'Please try your search again or check out suggested pages below:', 'cps' ); ?>
				</p>
			<?php endif; ?>

			<?php
			wp_nav_menu(
				[
					'theme_location'  => 'navigation-serp',
					'container'       => 'nav',
					'container_class' => 'search-nav',
					'container_id'    => 'search-menu',
					'depth'           => 1,
					'fallback_cb'     => false,
				]
			);
			?>

		<?php endif; ?>
	</section>

<?php
get_footer();
