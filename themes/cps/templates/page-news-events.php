<?php
/**
 * Template Name: News & Events
 *
 * @package CPS
 */

$news_events_query  = cps_get_news_events();
$news_events_months = cps_get_events_months( $news_events_query );
$topic              = get_query_var( 'topic', '' );
$month_query        = get_query_var( 'month', '' );
?>

<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php cps_the_default_hero(); ?>
			<?php the_content(); ?>

			<section class="news_event--filters">
				<label for="js-news_event--filters--topic"><?php echo esc_html__( 'Topic', 'cps' ); ?></label>
				<select id="js-news_event--filters--topic" class="news_event--filters--select">
					<option value=""
						<?php
						if ( empty( $topic ) ) {
							echo ' selected';
						}
						?>
					><?php echo esc_html__( 'News', 'cps' ); ?></option>
					<option value="events"
						<?php
						if ( 'events' === $topic ) {
							echo ' selected';
						}
						?>
					><?php echo esc_html__( 'Events', 'cps' ); ?></option>
				</select>

				<?php if ( 'events' === $topic ) : ?>
					<label for="js-news_event--filters--month"><?php echo esc_html__( 'Month', 'cps' ); ?></label>
					<select id="js-news_event--filters--month" class="news_event--filters--select">
						<option value=""
						<?php
						if ( empty( $month_query ) ) {
							echo ' selected';
						}
						?>
						><?php echo esc_html__( 'All', 'cps' ); ?></option>
						<?php foreach ( $news_events_months as $month_value => $month_label ) : ?>
							<?php
							echo '<option value="' . esc_html( $month_value ) . '"';
							if ( $month_query === $month_value ) {
								echo ' selected ';
							}
							echo '">' . esc_html( $month_label ) . '</option>';
							?>
						<?php endforeach; ?>
					</select>
				<?php endif; ?>
			</section> <!-- ./news_event--filters -->

			<?php if ( $news_events_query->have_posts() ) : ?>
				<div id="load-more-ajax-container" data-template="page-news"
					<?php
					if ( 'events' === $topic ) {
						echo ' data-post-type="cps-event"';
						if ( ! empty( $month_query ) && preg_match( '/^[0-9]{4}-[0-9]{2}\z/', $month_query ) ) {
							echo ' data-month="' . esc_attr( $month_query ) . '"';
						}
					} else {
						echo 'data-post-type="post"';
					}
					?>
				>
					<?php
					$index = 0;

					while ( $news_events_query->have_posts() ) :

						$news_events_query->the_post();

						$media_position = ( 0 === $index % 2 ) ? 'left' : 'right';

						$attributes = array(
							'mediaPosition' => $media_position,
							'headline'      => get_the_title(),
							'copy'          => get_the_excerpt(),
							'ctaLabelOne'   => __( 'Learn More', 'cps' ) . '<span class="screen-reader-text">' . __( 'about ', 'cps' ) . get_the_title() . '</span>',
							'ctaLinkOne'    => get_the_permalink(),
						);

						if ( has_post_thumbnail() ) {
							$attributes['image']['id'] = get_post_thumbnail_id();
						}

						if ( 'cps-event' === get_post_type() ) {
							$attributes['datetime'] = \CPS\Event\Helpers\cps_the_event_date( false );
						}

						include locate_template( 'partials/block-image-text.php' );

						$index++;

					endwhile;

					wp_reset_postdata();
					?>
				</div>

				<?php if ( 'events' !== $topic && $news_events_query->max_num_pages > 1 ) : ?>
					<div class="load-more">
						<button class="button-primary">
							<?php esc_html_e( 'Load more', 'cps' ); ?>
							<div class="cps-spinner spinner"></div>
						</button>
					</div>
				<?php elseif ( 'events' === $topic ) : ?>
					<div class="load-more">
						<a href="<?php echo esc_url( get_post_type_archive_link( 'cps-event' ) ); ?>" class="wp-block-button__link">
							<?php esc_html_e( 'Events Archive', 'cps' ); ?>
						</a>
					</div>
				<?php endif; ?>

			<?php else : ?>

				<h2>
					<?php esc_html_e( 'We\'re sorry, we can\'t find anything.', 'cps' ); ?>
				</h2>

				<?php if ( has_nav_menu( 'navigation-serp' ) ) : ?>
					<p>
						<?php esc_html_e( 'Please try another filter or check out suggested pages below:', 'cps' ); ?>
					</p>
				<?php endif; ?>

				<?php
				wp_nav_menu(
					[
						'theme_location'  => 'navigation-serp',
						'container'       => 'nav',
						'container_class' => 'search-nav',
						'container_id'    => 'search-menu',
						'depth'           => 1,
						'fallback_cb'     => false,
					]
				);
				?>

			<?php endif; ?>

		<?php endwhile; ?>
	<?php endif; ?>

<?php
get_footer();
