<?php
/**
 * Archive template for past events.
 *
 * @package CPS
 */

?>

<?php get_header(); ?>

<div class="block-hero block-hero--color">

	<?php
	if ( function_exists( 'breadcrumb_trail' ) ) {
		$breadcrumb_args = array(
			'labels' => array(
				'browse' => false,
			),
		);
		breadcrumb_trail( $breadcrumb_args );
	}
	?>

	<div class="block-hero__content">
		<h1>
			<?php esc_html_e( 'Events Archive', 'cps' ); ?>
		</h1>
	</div>
</div>

<?php
if ( have_posts() ) :

	$index = 0;

	while ( have_posts() ) :

		the_post();

		$media_position = ( 0 === $index % 2 ) ? 'left' : 'right';

		$attributes = array(
			'mediaPosition' => $media_position,
			'headline'      => get_the_title(),
			'copy'          => get_the_excerpt(),
			'ctaLabelOne'   => __( 'Learn More', 'cps' ),
			'ctaLinkOne'    => get_the_permalink(),
			'datetime'      => \CPS\Event\Helpers\cps_the_event_date( false ),
		);

		if ( has_post_thumbnail() ) {
			$attributes['image']['id'] = get_post_thumbnail_id();
		}

		include locate_template( 'partials/block-image-text.php' );

		$index++;

	endwhile;

	the_posts_pagination();

endif;

get_footer();
