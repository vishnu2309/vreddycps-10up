<?php
/**
 * WP Theme constants and setup functions
 *
 * @package CPS
 */

// Useful global constants.
define( 'CPS_VERSION', '1.0.1' );
define( 'CPS_TEMPLATE_URL', get_template_directory_uri() );
define( 'CPS_PATH', get_template_directory() . '/' );
define( 'CPS_INC', CPS_PATH . 'includes/' );

require_once CPS_INC . 'helpers.php';
require_once CPS_INC . 'core.php';
require_once CPS_INC . 'fim.php';
require_once CPS_INC . 'pim.php';
require_once CPS_INC . 'overrides.php';
require_once CPS_INC . 'template-tags.php';
require_once CPS_INC . 'utility.php';
require_once CPS_INC . 'admin.php';
require_once CPS_INC . 'blocks/core.php';
require_once CPS_INC . 'content/post-types/contact/helpers.php';
require_once CPS_INC . 'content/post-types/contact/contact-post-type.php';
require_once CPS_INC . 'content/post-types/event/helpers.php';
require_once CPS_INC . 'content/post-types/event/event-post-type.php';
require_once CPS_INC . 'content/post-types/event/ics-generator.php';
require_once CPS_INC . 'content/post-types/news.php';
require_once CPS_INC . 'content/post-types/person/helpers.php';
require_once CPS_INC . 'content/post-types/person/person-post-type.php';
require_once CPS_INC . 'content/post-types/program/helpers.php';
require_once CPS_INC . 'content/post-types/program/program-post-type.php';
require_once CPS_INC . 'content/taxonomies/person-category.php';
require_once CPS_INC . 'menu.php';
require_once CPS_INC . 'classes/navigation-main-walker.php';
require_once CPS_INC . 'floating-cta.php';
require_once CPS_INC . 'routes.php';
require_once CPS_INC . 'search.php';
require_once CPS_INC . 'redirects.php';
require_once CPS_INC . 'rest.php';
require_once CPS_INC . 'content/pages/news-events.php';

if ( is_admin() ) {
	require_once CPS_INC . 'content/post-types/contact/admin/core.php';
	require_once CPS_INC . 'content/post-types/event/admin/core.php';
	require_once CPS_INC . 'content/post-types/event/admin/meta-box.php';
	require_once CPS_INC . 'content/post-types/person/admin/core.php';
	require_once CPS_INC . 'content/post-types/person/admin/metaboxes.php';
	require_once CPS_INC . 'content/post-types/program/admin/core.php';
	require_once CPS_INC . 'content/post-types/program/admin/metaboxes.php';
}

// Run the setup functions.
CPS\Core\setup();
CPS\Blocks\setup();
CPS\Overrides\setup();
CPS\Contact\PostType\setup();
CPS\Event\PostType\setup();
CPS\Event\ICSGenerator\setup();
CPS\News\setup();
CPS\Person\PostType\setup();
CPS\Program\PostType\setup();
CPS\Taxonomy\PersonCategory\setup();
CPS\Menu\setup();
CPS\FloatingCTA\setup();
CPS\Routes\setup();
CPS\Search\setup();
CPS\Redirects\setup();
CPS\REST\setup();
CPS\Page\NewsEvents\setup();

if ( is_admin() ) {
	CPS\Admin\setup();
	CPS\Contact\Admin\setup();
	CPS\Person\Admin\setup();
	CPS\Person\Admin\MetaBoxes\setup();
	CPS\Program\Admin\setup();
	CPS\Program\Admin\MetaBoxes\setup();
	CPS\Event\Admin\setup();
	CPS\Event\Admin\MetaBox\setup();
}
