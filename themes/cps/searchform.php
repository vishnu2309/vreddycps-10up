<?php
/**
 * The template for displaying the search form.
 *
 * @package CPS
 */

$content_types = cps_get_searchable_content_types();
$active_type   = filter_input( INPUT_GET, 'type', FILTER_SANITIZE_STRING );
$active_type   = empty( $active_type ) ? 'all' : $active_type;
?>

<div itemscope itemtype="http://schema.org/WebSite" class="search-form-wrapper">
	<form role="search" class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<meta itemprop="target" content="<?php echo esc_url( home_url() ); ?>/?s={s}" />

		<label for="type" class="screen-reader-text"><?php esc_html_e( 'Type', 'cps' ); ?></label>
		<select name="type" class="js-select2-search">
			<?php
			foreach ( $content_types as $key => $content_type ) :

				?>
				<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $active_type, $key ); ?>>
					<?php echo esc_html( $content_type['label'] ); ?>
				</option>
				<?php

			endforeach;
			?>
		</select>

		<label for="s" class="screen-reader-text">
			<?php echo esc_html_x( 'Search for:', 'label', 'cps' ); ?>
		</label>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder', 'cps' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'cps' ); ?>" />

		<button class="search-submit button-primary" type="submit">
			<?php cps_svg_icon( 'icon-search', true ); ?>
			<span class="screen-reader-text"><?php echo esc_attr_x( 'Search', 'submit button', 'cps' ); ?></span>
		</button>
	</form>
</div>
