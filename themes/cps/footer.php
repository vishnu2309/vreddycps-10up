<?php
/**
 * The template for displaying the footer.
 *
 * @package CPS
 */

$social_links = cps_get_social_links();
?>
		</main>

		<?php
		if ( ! is_page_template( 'templates/page-program.php' ) && 'cps-program' !== get_post_type() ) {
			get_template_part( 'partials/footer-contact' );
		} else {
			?>
			<div class="dialog" id="program-dialog" aria-hidden="true">
				<div class="dialog-overlay" tabindex="-1" data-a11y-dialog-hide></div>
				<div class="dialog-content" aria-labelledby="dialog-title" aria-describedby="dialog-description" role="dialog">
					<button class="dialog-close" type="button" data-a11y-dialog-hide aria-label="<?php esc_attr_e( 'Close this dialog window', 'cps' ); ?>">
						&times;
					</button>
					<h1 id="dialog-title" class="is-style-h3"></h1>
					<div>
						<span id="dialog-credit-low"></span>
						<span id="dialog-credit-high"></span>
					</div>
					<p id="dialog-description"></p>
				</div>
			</div>
		<?php } ?>

		<footer class="site-footer" role="contentinfo">
			<div class="site-footer__inner">
				<div class="site-footer__address" itemscope itemtype="http://schema.org/Organization">

					<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-footer__logo">
						<?php cps_svg_icon( 'logo', true ); ?>
					</a>
					<span class="screen-reader-text" itemprop="name"><?php bloginfo( 'name' ); ?></span>

					<div id="footer-social">
						<ul>
							<?php foreach ( $social_links as $network => $url ) : ?>
							<li>
								<a href="<?php echo esc_url( $url ); ?>" target="_blank" rel="noopener noreferrer">
									<?php cps_svg_icon( $network, true ); ?>
									<span class="screen-reader-text"><?php echo esc_html( $network ); ?></span>
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>

					<div class="address is-style-small" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<span itemprop="streetAddress"><?php esc_html_e( '360 Huntington Ave', 'cps' ); ?></span><br />
						<span itemprop="addressLocality"><?php esc_html_e( 'Boston, Massachusetts', 'cps' ); ?></span>
						<span itemprop="postalCode"><?php esc_html_e( '02115', 'cps' ); ?></span>
						<meta itemprop="telephone" content="<?php esc_attr_e( '617.373.2000', 'cps' ); ?>">
						<meta itemprop="email" content="<?php esc_attr_e( 'cps@neu.edu', 'cps' ); ?>">
					</div>

				</div>

				<div class="site-footer__menus">
					<?php
					wp_nav_menu(
						[
							'theme_location'  => 'navigation-footer',
							'container'       => 'div',
							'container_class' => 'site-footer__menu',
							'container_id'    => 'footer-menu',
							'depth'           => 1,
							'fallback_cb'     => false,
						]
					);
					?>
				</div>

				<div class="site-footer__contact">
					<h2 class="is-style-h4">
						<?php esc_html_e( 'Contact Us', 'cps' ); ?>
					</h2>
					<div class="is-style-small">
						<a href="mailto:cps@neu.edu" target="_blank"><?php esc_html_e( 'cps@neu.edu', 'cps' ); ?></a><br />
						<?php esc_html_e( '617.373.2000', 'cps' ); ?>
					</div>
				</div>

			</div>
		</footer>

		<?php if ( function_exists( 'NUML_globalfooter' ) ) : ?>
			<?php NUML_globalfooter(); ?>
		<?php endif; ?>
		<?php wp_footer(); ?>
	</body>
</html>
