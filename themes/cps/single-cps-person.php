<?php
/**
 * The main template file
 *
 * @package CPS
 */

get_header();

if ( have_posts() ) :

	while ( have_posts() ) :

		the_post();

		$faculty_id = get_the_ID();

		$position     = \CPS\Person\Helpers\get_faculty_position( $faculty_id );
		$address      = \CPS\Person\Helpers\get_faculty_address( $faculty_id );
		$email        = \CPS\Person\Helpers\get_faculty_email( $faculty_id );
		$phone_number = \CPS\Person\Helpers\get_faculty_phone_number( $faculty_id );
		$about        = \CPS\Person\Helpers\get_faculty_about( $faculty_id );
		$education    = \CPS\Person\Helpers\get_faculty_education( $faculty_id );
		$prof_exp     = \CPS\Person\Helpers\get_faculty_prof_exp( $faculty_id );
		$publications = \CPS\Person\Helpers\get_faculty_publications( $faculty_id );
		$linkedin     = \CPS\Person\Helpers\get_faculty_linkedin( $faculty_id );
		$twitter      = \CPS\Person\Helpers\get_faculty_twitter( $faculty_id );
		?>

		<div class="block-hero has-black-background-color has-white-color block-hero--color">
			<?php
			if ( ! is_home() && function_exists( 'breadcrumb_trail' ) ) {
				$breadcrumb_args = array(
					'labels' => array(
						'browse' => false,
					),
				);
				breadcrumb_trail( $breadcrumb_args );
			}
			?>

			<div class="block-hero__content">
				<h1><?php the_title(); ?></h1>

				<?php if ( ! empty( $position ) ) : ?>
					<p><?php echo esc_html( $position ); ?></p>
				<?php endif; ?>
			</div>
		</div>

		<div class="person-data">
			<?php if ( has_post_thumbnail() ) : ?>
				<div class="person-data__image">
					<?php the_post_thumbnail( 'profile' ); ?>
				</div>
			<?php endif; ?>

			<div class="person-data__contact">
				<ul>
					<?php if ( ! empty( $address ) ) : ?>
						<li>
							<?php cps_svg_icon( 'location', true ); ?>

							<span class="screen-reader-text">
								<?php esc_html_e( 'Location:', 'cps' ); ?>
							</span>

							<?php echo esc_html( $address ); ?>
						</li>
					<?php endif; ?>

					<?php if ( ! empty( $email ) ) : ?>
						<li>
							<?php cps_svg_icon( 'profile-email', true ); ?>

							<span class="screen-reader-text">
								<?php esc_html_e( 'Email:', 'cps' ); ?>
							</span>

							<a href="mailto:<?php echo esc_html( $email ); ?>" target="_blank">
								<?php echo esc_html( $email ); ?>
							</a>
						</li>
					<?php endif; ?>

					<?php if ( ! empty( $phone_number ) ) : ?>
						<li>
							<?php cps_svg_icon( 'profile-phone', true ); ?>

							<span class="screen-reader-text">
								<?php esc_html_e( 'Phone:', 'cps' ); ?>
							</span>

							<?php echo esc_html( $phone_number ); ?>
						</li>
					<?php endif; ?>

					<?php
					if ( ! empty( $linkedin ) && ! empty( $linkedin['url'] ) ) :

						$title = empty( $linkedin['title'] ) ? __( 'LinkedIn', 'cps' ) : $linkedin['title'];
						?>
						<li>
							<?php cps_svg_icon( 'profile-linkedin', true ); ?>

							<span class="screen-reader-text">
								<?php esc_html_e( 'Linkedin:', 'cps' ); ?>
							</span>

							<a href="<?php echo esc_url( $linkedin['url'] ); ?>" target="_blank" rel="nofollow">
								@<?php echo esc_html( $title ); ?>
							</a>
						</li>
						<?php

					endif;

					if ( ! empty( $twitter ) && ! empty( $twitter['url'] ) ) :

						$title = empty( $twitter['title'] ) ? __( 'Twitter', 'cps' ) : $twitter['title'];
						?>
						<li>
							<?php cps_svg_icon( 'profile-twitter', true ); ?>

							<span class="screen-reader-text">
								<?php esc_html_e( 'Twitter:', 'cps' ); ?>
							</span>

							<a href="<?php echo esc_url( $twitter['url'] ); ?>" target="_blank" rel="nofollow">
								@<?php echo esc_html( $title ); ?>
							</a>
						</li>
						<?php

					endif;
					?>
				</ul>
			</div>
		</div>

		<?php if ( ! empty( $about ) ) : ?>
			<div class="about">
				<h3>
					<?php esc_html_e( 'About', 'cps' ); ?>
				</h3>

				<?php echo wp_kses_post( $about ); ?>
			</div>
		<?php endif; ?>

		<?php if ( ! empty( $education ) ) : ?>
			<div class="education">
				<h3>
					<?php esc_html_e( 'Education', 'cps' ); ?>
				</h3>

				<?php echo wp_kses_post( $education ); ?>
			</div>
		<?php endif; ?>

		<?php if ( ! empty( $prof_exp ) ) : ?>
			<div class="prof_exp">
				<h3>
					<?php esc_html_e( 'Professional Experience', 'cps' ); ?>
				</h3>

				<?php echo wp_kses_post( $prof_exp ); ?>
			</div>
		<?php endif; ?>

		<?php if ( ! empty( $publications ) ) : ?>
			<div class="publications">
				<h3>
					<?php esc_html_e( 'Publications', 'cps' ); ?>
				</h3>

				<ul>
					<?php
					foreach ( $publications as $publication ) :

						if ( ! empty( $publication['url'] ) ) :

							$title = empty( $publication['title'] ) ? $publication['url'] : $publication['title'];
							?>
							<li>
								<a href="<?php echo esc_url( $publication['url'] ); ?>" target="_blank" rel="nofollow">
									<?php echo esc_html( $title ); ?>
								</a>
							</li>
							<?php

						endif;

					endforeach;
					?>
				</ul>
			</div>
			<?php

		endif;
	endwhile;

endif;

get_footer();
