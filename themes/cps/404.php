<?php
/**
 * The 404 page template file.
 *
 * @package CPS
 */

get_header(); ?>

<div class="block-hero block-hero--color">
	<div class="block-hero__content">
		<h1>
			<?php esc_html_e( '404 Page Not Found', 'cps' ); ?>
		</h1>
		<p><?php esc_html_e( 'The page you\'re looking for isn\'t here.', 'cps' ); ?></p>
	</div>
</div>

<?php if ( has_nav_menu( 'navigation-serp' ) ) : ?>
	<p>
		<?php esc_html_e( 'Check out suggested pages below:', 'cps' ); ?>
	</p>
<?php endif; ?>

<?php
wp_nav_menu(
	[
		'theme_location'  => 'navigation-serp',
		'container'       => 'nav',
		'container_class' => 'search-nav',
		'container_id'    => 'search-menu',
		'depth'           => 1,
		'fallback_cb'     => false,
	]
);
?>

<?php
get_footer();
