/**
 * Unregister core Pullquote styles.
 */
const unRegisterPullquoteStyles = () => {

	wp.blocks.unregisterBlockStyle( 'core/pullquote', 'default' );
	wp.blocks.unregisterBlockStyle( 'core/pullquote', 'solid-color' );
};

export default unRegisterPullquoteStyles;
