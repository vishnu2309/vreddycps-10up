/**
 * Unregister core image styles
 *
 * @return {void}
 */
const unregisterImageStyles = () => {
	wp.domReady( () => {
		wp.blocks.unregisterBlockStyle( 'core/image', 'rounded' );
		wp.blocks.unregisterBlockStyle( 'core/image', 'default' );
	} );
};

export default unregisterImageStyles;
