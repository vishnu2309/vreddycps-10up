/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

/**
 * Register custom Paragraph styles.
 */
const registerParagraphStyles = () => {

	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'small',
		label: __( 'Small (body 2)', 'cps' ),
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'small-description',
		label: __( 'Small Description', 'cps' ),
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/paragraph', {
		name: 'h6',
		label: __( 'Regular Description', 'cps' ),
		isDefault: false
	} );
};

export default registerParagraphStyles;
