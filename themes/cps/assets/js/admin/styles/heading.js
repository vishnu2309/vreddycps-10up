/**
 * Register custom Heading styles.
 */
const registerHeadingStyles = () => {
	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'h1',
		label: 'H1',
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'h2',
		label: 'H2',
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'h3',
		label: 'H3',
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'h4',
		label: 'H4',
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'h5',
		label: 'H5',
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/heading', {
		name: 'h6',
		label: 'H6',
		isDefault: false
	} );
};

export default registerHeadingStyles;
