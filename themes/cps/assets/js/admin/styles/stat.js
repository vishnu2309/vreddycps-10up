/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

/**
 * Register custom Stat styles.
 */
const registerStatStyles = () => {

	wp.blocks.registerBlockStyle( 'cps/stat', {
		name: 'stat',
		label: __( 'Stat', 'cps' ),
		isDefault: true
	} );

	wp.blocks.registerBlockStyle( 'cps/stat', {
		name: 'date',
		label: __( 'Date', 'cps' ),
		isDefault: false
	} );
};

export default registerStatStyles;
