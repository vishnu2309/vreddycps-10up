/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

/**
 * Register custom Columns styles.
 */
const registerColumnsStyles = () => {

	wp.blocks.registerBlockStyle( 'core/columns', {
		name: 'default',
		label: __( 'Default', 'cps' ),
		isDefault: true
	} );

	wp.blocks.registerBlockStyle( 'core/columns', {
		name: 'logo-grid',
		label: __( 'Logo grid', 'cps' ),
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/columns', {
		name: 'two-columns',
		label: __( 'Two columns with image', 'cps' ),
		isDefault: false
	} );
};

export default registerColumnsStyles;
