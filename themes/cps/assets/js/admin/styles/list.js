/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

/**
 * Register custom List styles.
 */
const registerListStyles = () => {

	wp.blocks.registerBlockStyle( 'core/list', {
		name: 'default',
		label: __( 'Default', 'cps' ),
		isDefault: true
	} );

	wp.blocks.registerBlockStyle( 'core/list', {
		name: 'small',
		label: __( 'Small (body 2)', 'cps' ),
		isDefault: false
	} );

	wp.blocks.registerBlockStyle( 'core/list', {
		name: 'linear',
		label: __( 'Linear', 'cps' ),
		isDefault: false
	} );
};

export default registerListStyles;
