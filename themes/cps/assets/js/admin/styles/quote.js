/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

/**
 * Register custom Quote styles.
 */
const registerQuoteStyles = () => {

	wp.blocks.registerBlockStyle( 'core/quote', {
		name: 'no-border',
		label: __( 'No Border', 'cps' ),
		isDefault: false
	} );
};

export default registerQuoteStyles;
