/* global eventsVars, jQuery */
( function ( $ ) {

	'use strict';

	/**
	 * Initialize.
	 */
	function init() {
		eventHandlers();
		initDatePickers();
	}

	/**
	 * Set up event handlers.
	 */
	function eventHandlers() {

		$( '#event-all-day' ).on( 'change.cpsEvents', function() {
			$( '.all-day-hidden' ).toggleClass( 'all-day-visible' );
		} );

		$( '#event-start-hh, #event-end-hh' ).on( 'change.cpsEvents', function() {
			validateTimeInput( this, 24 );
		} );

		$( '#event-start-mm, #event-end-mm' ).on( 'change.cpsEvents', function() {
			validateTimeInput( this, 60 );
		} );
	}

	/**
	 * Validate time inputs, toggle submit button accordingly.
	 *
	 * @param {obj} input Input element.
	 * @param {int} max   Maximum input value, eg. 24 for hours, 60 for minutes.
	 */
	function validateTimeInput( input, max ) {

		const $publishButton = $( '#publish' ), $input = $( input );

		if ( '' === $input.val() ) {
			$input.val( '00' );
		} else if ( ! $input.val().match( /^[0-9]{1,2}?$/ ) || $input.val() >= max ) {
			$input.addClass( 'error' );
			$publishButton.attr( 'disabled', 'disabled' );
		} else {
			$input.removeClass( 'error' );
			$publishButton.removeAttr( 'disabled' );
		}
	}

	/**
	 * Initialize date pickers.
	 */
	function initDatePickers() {

		const $startInput = $( '[name="event-start-string"]' );
		const $endInput   = $( '[name="event-end-string"]' );

		$startInput.datepicker( {
			dateFormat: 'mm/dd/yy',
			altField: '[name="event-start"]',
			altFormat: $.datepicker.ISO_8601,
			firstDay: eventsVars.firstDay,
			numberOfMonths: 3,
			/**
			 * On datepicker close.
			 *
			 * @param {string} selectedDate Selected date string.
			 */
			onClose: function( selectedDate ) {
				$endInput.datepicker( 'option', 'minDate', selectedDate );
			}
		} );

		$endInput.datepicker( {
			dateFormat: 'mm/dd/yy',
			altField: '[name="event-end"]',
			altFormat: $.datepicker.ISO_8601,
			firstDay: eventsVars.firstDay,
			minDate: new Date( $startInput.data( 'date' ) ),
			numberOfMonths: 3
		} );
	}

	$( function() {
		init();
	} );

} ( jQuery ) );
