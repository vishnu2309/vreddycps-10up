/* eslint require-jsdoc: 0 */
/**
 * Renders an image uploader with preview.
 *
 * This component is meant to be used inside
 * a panel in an InspectorControl
 *
 * @package CPS
 */

/**
 * WordPress dependencies
 */
const {
	Button,
} = wp.components;

const { __ } = wp.i18n;

const {
	MediaPlaceholder,
	MediaUpload,
	MediaUploadCheck
} = wp.blockEditor;

const {
	Component,
} = wp.element;

/**
 * External dependencies
 */
import classnames from 'classnames';
import pick from 'lodash/pick';

import icon from './icon';

export class ImageSelectForInspector extends Component {

	constructor() {
		super( ...arguments );

		this.onChangeImage = this.onChangeImage.bind( this );
		this.removeImage = this.removeImage.bind( this );
	}

	removeImage() {
		this.props.onChange( {
			alt: undefined,
			id: undefined,
			url: undefined,
		} );
	}

	onChangeImage( media ) {
		if ( ! media || ! media.url ) {
			this.props.onChange( {
				alt: undefined,
				id: undefined,
				url: undefined,
			} );
			return;
		}
		this.props.onChange( {
			...pick( media, [ 'alt', 'id', 'url' ] ),
		} );
	}

	renderPlaceholder() {
		const {
			className,
			labels = {},
		} = this.props;

		return(
			<MediaPlaceholder
				icon={ icon }
				labels={ labels }
				className={ className }
				onSelect={ this.onChangeImage }
				notices=''
				onError=''
				accept="image/*"
				allowedTypes={ ['image'] }
				multiple={ false }
			/>
		);
	}

	renderToolbar() {
		const { image } = this.props;
		return(
			<MediaUploadCheck>
				<MediaUpload
					addToGallery={ false }
					allowedTypes={ ['image'] }
					gallery={ false }
					multiple={ false }
					onSelect={ this.onChangeImage }
					value={ image.id || 0 }
					render={ ( { open } ) => (
						<Button
							className="components-button is-button is-default is-small"
							onClick={ ( event ) => {
								event.target.blur();
								event.stopPropagation();
								open();
							} }
						>
							{ __( 'Edit' ) }
						</Button>
					)}
				/>
				<Button
					className="cps-image-uploader-inspector-delete"
					isDestructive="true"
					isSmall="true"
					onClick={ this.removeImage }
				>
					{ __( 'Remove' ) }
				</Button>
			</MediaUploadCheck>
		);
	}

	render() {

		const { image } = this.props;

		const className = classnames( {
			'is-transient': image && image.url && 0 === image.url.indexOf( 'blob:' ),
		} );

		if ( image && image.url ) {
			return(
				<div class="cps-image-uploader-inspector">
					<div class="cps-image-uploader-inspector-edit-wrap">
						<div class="cps-image-uploader-inspector-image">
							<figure
								className={ `cps-image-uploader ${className}` }
								tabIndex="-1"
								onKeyDown={ this.onRemoveImage }
								onClick={ this.onClickImage }
							>
								<img
									src={ image.url }
									alt={ image.alt }
									data-id={ image.id }
									tabIndex="0"
									aria-label={ __( 'image upload' ) }
								/>
							</figure>
						</div>
						<div class="cps-image-uploader-inspector-toolbar">
							{ this.renderToolbar() }
						</div>
					</div>
				</div>
			);
		}

		return(
			<div class="cps-image-uploader-inspector">
				{ this.renderPlaceholder() }
			</div>
		);
	}
}

export default ImageSelectForInspector;
