/* eslint require-jsdoc: 0 */
/**
 * External dependencies
 */
import { map, throttle, isEmpty } from 'lodash';
import classnames from 'classnames';
import scrollIntoView from 'dom-scroll-into-view';

/**
 * WordPress dependencies
 */
const { __, sprintf, _n } = wp.i18n;
const { Component, createRef } = wp.element;
const { UP, DOWN, ENTER, TAB } = wp.keycodes;
const { Spinner, withSpokenMessages, Popover } = wp.components;
const { withInstanceId, withSafeTimeout, compose } = wp.compose;
const { apiFetch } = wp;
const { addQueryArgs } = wp.url;
const { decodeEntities } = wp.htmlEntities;

// Since PostFinder is rendered in the context of other inputs, but should be
// considered a separate modal node, prevent keyboard events from propagating
// as being considered from the input.
const stopEventPropagation = ( event ) => event.stopPropagation();

class PostFinder extends Component {
	constructor( { autocompleteRef } ) {
		super( ...arguments );

		this.onChange = this.onChange.bind( this );
		this.onKeyDown = this.onKeyDown.bind( this );
		this.autocompleteRef = autocompleteRef || createRef();
		this.inputRef = createRef();
		this.updateSuggestions = throttle( this.updateSuggestions.bind( this ), 200 );
		this.fetchLinkSuggestions = this.fetchLinkSuggestions.bind( this );

		this.suggestionNodes = [];

		this.state = {
			suggestions: [],
			showSuggestions: false,
			selectedSuggestion: null,
		};
	}

	componentDidUpdate() {
		const { showSuggestions, selectedSuggestion } = this.state;
		// only have to worry about scrolling selected suggestion into view
		// when already expanded
		if ( showSuggestions && null !== selectedSuggestion && ! this.scrollingIntoView ) {
			this.scrollingIntoView = true;
			scrollIntoView( this.suggestionNodes[ selectedSuggestion ], this.autocompleteRef.current, {
				onlyScrollIfNeeded: true,
			} );

			this.props.setTimeout( () => {
				this.scrollingIntoView = false;
			}, 100 );
		}
	}

	componentWillUnmount() {
		delete this.suggestionsRequest;
	}

	bindSuggestionNode( index ) {
		return ( ref ) => {
			this.suggestionNodes[ index ] = ref;
		};
	}

	updateSuggestions( value ) {
		// Show the suggestions after typing at least 2 characters
		// and also for URLs
		if ( 2 > value.length ) {
			this.setState( {
				showSuggestions: false,
				selectedSuggestion: null,
				loading: false,
			} );

			return;
		}

		this.setState( {
			showSuggestions: true,
			selectedSuggestion: null,
			loading: true,
		} );

		const request = this.fetchLinkSuggestions( value );

		request.then( ( suggestions ) => {

			// A fetch Promise doesn't have an abort option. It's mimicked by
			// comparing the request reference in on the instance, which is
			// reset or deleted on subsequent requests or unmounting.
			if ( this.suggestionsRequest !== request ) {
				return;
			}

			this.setState( {
				suggestions,
				loading: false,
			} );

			if ( suggestions.length ) {
				this.props.debouncedSpeak( sprintf( _n(
					'%d result found, use up and down arrow keys to navigate.',
					'%d results found, use up and down arrow keys to navigate.',
					suggestions.length
				), suggestions.length ), 'assertive' );
			} else {
				this.props.debouncedSpeak( __( 'No results.', 'cps' ), 'assertive' );
			}
		} ).catch( () => {
			if ( this.suggestionsRequest === request ) {
				this.setState( {
					loading: false,
				} );
			}
		} );

		this.suggestionsRequest = request;
	}

	async fetchLinkSuggestions( search ) {

		const args = {
			search,
			per_page: 20, // eslint-disable-line camelcase
			type: 'post',
			/*
			 * Note: Adding 'postfinder=true' will enable us to identify the search
			 *       coming from this component.
			 */
			postfinder: 'true',
		};

		// Note: The V2 search api does not allow searching across multiple specific post types.
		if ( ! isEmpty( this.props.postType ) ) {
			args.subtype = this.props.postType;
		}

		const posts = await apiFetch( {
			path: addQueryArgs( '/wp/v2/search', args ),
		} );

		return map( posts, ( post ) => ( {
			id: post.id,
			url: post.url,
			title: decodeEntities( post.title ) || __( '(no title)' ),
			type: post.subtype.replace( 'cps-', '' ),
		} ) );
	}

	onChange( event ) {
		this.props.value = event.target.value;
		this.updateSuggestions( this.props.value );
	}

	onKeyDown( event ) {
		const { showSuggestions, selectedSuggestion, suggestions, loading } = this.state;
		// If the suggestions are not shown or loading, we shouldn't handle the arrow keys
		// We shouldn't preventDefault to allow block arrow keys navigation
		if ( ! showSuggestions || ! suggestions.length || loading ) {
			// In the Windows version of Firefox the up and down arrows don't move the caret
			// within an input field like they do for Mac Firefox/Chrome/Safari. This causes
			// a form of focus trapping that is disruptive to the user experience. This disruption
			// only happens if the caret is not in the first or last position in the text input.
			// See: https://github.com/WordPress/gutenberg/issues/5693#issuecomment-436684747
			switch ( event.keyCode ) {
					// When UP is pressed, if the caret is at the start of the text, move it to the 0
					// position.
					case UP: {
						if ( 0 !== event.target.selectionStart ) {
							event.stopPropagation();
							event.preventDefault();

							// Set the input caret to position 0
							event.target.setSelectionRange( 0, 0 );
						}
						break;
					}
					// When DOWN is pressed, if the caret is not at the end of the text, move it to the
					// last position.
					case DOWN: {
						if ( this.props.value.length !== event.target.selectionStart ) {
							event.stopPropagation();
							event.preventDefault();

							// Set the input caret to the last position
							event.target.setSelectionRange( this.props.value.length, this.props.value.length );
						}
						break;
					}
			}

			return;
		}

		const suggestion = this.state.suggestions[ this.state.selectedSuggestion ];

		switch ( event.keyCode ) {
				case UP: {
					event.stopPropagation();
					event.preventDefault();
					const previousIndex = ! selectedSuggestion ? suggestions.length - 1 : selectedSuggestion - 1;
					this.setState( {
						selectedSuggestion: previousIndex,
					} );
					break;
				}
				case DOWN: {
					event.stopPropagation();
					event.preventDefault();
					const nextIndex = null === selectedSuggestion || ( selectedSuggestion === suggestions.length - 1 ) ? 0 : selectedSuggestion + 1;
					this.setState( {
						selectedSuggestion: nextIndex,
					} );
					break;
				}
				case TAB: {
					if ( null !== this.state.selectedSuggestion ) {
						this.selectPost( suggestion );
						// Announce a link has been selected when tabbing away from the input field.
						this.props.speak( __( 'Post selected.', 'cps' ) );
					}
					break;
				}
				case ENTER: {
					if ( null !== this.state.selectedSuggestion ) {
						event.stopPropagation();
						this.selectPost( suggestion );
					}
					break;
				}
		}
	}

	selectPost( suggestion ) {

		this.props.onChange( suggestion );
		this.inputRef.current.value = '';
		this.setState( {
			selectedSuggestion: null,
			showSuggestions: false,
		} );
	}

	handleOnClick( suggestion ) {
		this.selectPost( suggestion );
		// Move focus to the input field when a link suggestion is clicked.
		this.inputRef.current.focus();
	}

	static getDerivedStateFromProps( { disableSuggestions }, { showSuggestions } ) {
		return {
			showSuggestions: true === disableSuggestions ? false : showSuggestions,
		};
	}

	render() {
		const { autoFocus = true, instanceId, className, inputClassName, id, isFullWidth, hasBorder, placeholder } = this.props;
		const { showSuggestions, suggestions, selectedSuggestion, loading } = this.state;
		const suggestionsListboxId = `block-editor-url-input-suggestions-${ instanceId }`;
		const suggestionOptionIdPrefix = `block-editor-url-input-suggestion-${ instanceId }`;

		return (
			<div className={ classnames( 'cps-post-finder-input', className, {
				'is-full-width': isFullWidth,
				'has-border': hasBorder,
			} ) }>
				<input
					id={ id }
					className={ inputClassName }
					autoFocus={ autoFocus }
					type="text"
					aria-label={ __( 'Search', 'cps' ) }
					required
					onChange={ this.onChange }
					onInput={ stopEventPropagation }
					placeholder={ placeholder }
					onKeyDown={ this.onKeyDown }
					role="combobox"
					aria-expanded={ showSuggestions }
					aria-autocomplete="list"
					aria-owns={ suggestionsListboxId }
					aria-activedescendant={ null !== selectedSuggestion ? `${ suggestionOptionIdPrefix }-${ selectedSuggestion }` : undefined }
					ref={ this.inputRef }
				/>

				{ ( loading ) && <Spinner /> }

				{ showSuggestions && !! suggestions.length &&
					<Popover
						position="bottom"
						noArrow
						focusOnMount={ false }
					>
						<div
							className={ classnames(
								'editor-url-input__suggestions',
								'block-editor-url-input__suggestions',
								`${ className }__suggestions`
							) }
							id={ suggestionsListboxId }
							ref={ this.autocompleteRef }
							role="listbox"
						>
							{ suggestions.map( ( suggestion, index ) => (
								<button
									key={ suggestion.id }
									role="option"
									tabIndex="-1"
									id={ `${ suggestionOptionIdPrefix }-${ index }` }
									ref={ this.bindSuggestionNode( index ) }
									className={ classnames( 'editor-url-input__suggestion block-editor-url-input__suggestion', {
										'is-selected': index === selectedSuggestion,
									} ) }
									onClick={ () => this.handleOnClick( suggestion ) }
									aria-selected={ index === selectedSuggestion }
								>
									<span>
										{ ( 'post' !== suggestion.type ) &&
											<em class="post-type" style={{float: 'right'}}>[{ suggestion.type }]</em>
										}
										{ suggestion.title }
									</span>
								</button>
							) ) }
						</div>
					</Popover>
				}
			</div>
		);
	}
}

/**
 * @see https://github.com/WordPress/gutenberg/blob/master/packages/block-editor/src/components/url-input/README.md
 */
export default compose(
	withSafeTimeout,
	withSpokenMessages,
	withInstanceId,
)( PostFinder );
