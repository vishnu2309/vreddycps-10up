/* eslint require-jsdoc: 0 */
/**
 * Renders a custom ColorPalette using CPS colors.
 *
 * Note that the component returns a color name, eg. "red", not a color value.
 *
 * @package CPS
 *
 * Example:
 *
 * <ColorSelect
 *     value={ color }
 *     onChange={ ( value ) => {
 *         // Do something with value.
 *     } }
 * />
 */

/**
 * WordPress dependencies
 */
const { ColorPalette } = wp.components;
const { Component } = wp.element;

import { find, isFunction } from 'lodash';

const colors = [
	{ name: 'white', color: '#fff' },
	{ name: 'black', color: '#000' },
	{ name: 'red', color: '#dd1228' },
];

export class ColorSelect extends Component {

	constructor( props ) {

		super( props );

		this.onChange = this.onChange.bind( this );

		if ( ! props.value ) {
			props.value = colors[0].color;
		}

		this.state = { color: this.getColorByValue( props.value ) };
	}

	onChange( color ) {

		if ( ! this.props.onChange || ! isFunction( this.props.onChange ) ) {
			// eslint-disable-next-line no-console
			console.error(
				'[ColorSelect] The "onChange" property must be specified and must be a valid function.'
			);
			return;
		}

		this.props.onChange( this.getValueByColor( color ) );
	}

	getValueByColor( color ) {

		const result = find( colors, ( object ) => {
			return object.color === color;
		} );

		if ( ! result ) {
			return colors[0].name;
		}

		return result.name;
	}

	getColorByValue( value ) {

		const result = find( colors, ( object ) => {
			return object.name === value;
		} );

		if ( ! result ) {
			return colors[0].color;
		}

		return result.color;
	}

	render() {

		return (
			<>
				{

					<label>Select Color:</label>
				}
				<ColorPalette
					colors={ colors }
					clearable={ false }
					disableCustomColors={ true }
					value={ this.state.color }
					onChange={ ( color ) => {
						this.onChange( color );
						this.setState( { color: color } );
					} }
				/>
			</>
		);
	}
}

export default ColorSelect;
