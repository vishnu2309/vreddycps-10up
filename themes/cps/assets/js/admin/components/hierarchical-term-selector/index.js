/* eslint require-jsdoc: 0 */
/**
 * Renders a term selector component.
 *
 * Based on gutenberg/packages/editor/src/components/post-taxonomies/hierarchical-term-selector.js
 *
 * @package CPS
 *
 * <HierarchicalTermSelector
 *     terms={ terms } // An array of term IDs
 *     slug="category" // Taxonomy slug
 *     onUpdateTerms={ onUpdateTerms } // Callback
 * />
 */

/**
  * External dependencies
  */
import { get, invoke, unescape as unescapeString, without } from 'lodash';

/**
  * WordPress dependencies
  */
const { __, _n, sprintf } = wp.i18n;
const { Component } = wp.element;
const { Spinner, withSpokenMessages } = wp.components;
const { withSelect } = wp.data;
const { withInstanceId, compose } = wp.compose;
const { apiFetch } = wp;
const { addQueryArgs } = wp.url;

/**
 * Internal dependencies
 */
import { buildTermsTree } from './utils/terms';

/**
 * Module Constants
 */
const DEFAULT_QUERY = {
	per_page: -1, // eslint-disable-line camelcase
	orderby: 'name',
	order: 'asc',
	_fields: 'id,name,parent',
};

const MIN_TERMS_COUNT_FOR_FILTER = 8;

export class HierarchicalTermSelector extends Component {

	constructor() {
		super( ...arguments );

		this.onChange = this.onChange.bind( this );
		this.setFilterValue = this.setFilterValue.bind( this );
		this.sortBySelected = this.sortBySelected.bind( this );
		this.state = {
			loading: true,
			availableTermsTree: [],
			availableTerms: [],
			filterValue: '',
			filteredTermsTree: [],
		};
	}

	onChange( event ) {

		const { onUpdateTerms, terms = [] } = this.props;
		const termId = parseInt( event.target.value, 10 );

		const hasTerm = -1 !== terms.indexOf( termId );
		const newTerms = hasTerm ?
			without( terms, termId ) :
			[ ...terms, termId ];

		onUpdateTerms( newTerms );
	}

	componentDidMount() {
		this.fetchTerms();
	}

	componentWillUnmount() {
		invoke( this.fetchRequest, [ 'abort' ] );
		invoke( this.addRequest, [ 'abort' ] );
	}

	componentDidUpdate( prevProps ) {
		if ( this.props.taxonomy !== prevProps.taxonomy ) {
			this.fetchTerms();
		}
	}

	fetchTerms() {
		const { taxonomy } = this.props;
		if ( ! taxonomy ) {
			return;
		}
		this.fetchRequest = apiFetch( {
			path: addQueryArgs( `/wp/v2/${ taxonomy.rest_base }`, DEFAULT_QUERY ),
		} );
		this.fetchRequest.then(
			( terms ) => { // resolve
				const availableTermsTree = this.sortBySelected( buildTermsTree( terms ) );

				this.fetchRequest = null;
				this.setState( {
					loading: false,
					availableTermsTree,
					availableTerms: terms,
				} );
			},
			( xhr ) => { // reject
				if ( 'abort' === xhr.statusText ) {
					return;
				}
				this.fetchRequest = null;
				this.setState( {
					loading: false,
				} );
			}
		);
	}

	sortBySelected( termsTree ) {
		const { terms } = this.props;
		const treeHasSelection = ( termTree ) => {
			if ( -1 !== terms.indexOf( termTree.id ) ) {
				return true;
			}
			if ( undefined === termTree.children ) {
				return false;
			}
			const anyChildIsSelected = 0 < termTree.children.map( treeHasSelection ).filter( ( child ) => child ).length;
			if ( anyChildIsSelected ) {
				return true;
			}
			return false;
		};
		const termOrChildIsSelected = ( termA, termB ) => {
			const termASelected = treeHasSelection( termA );
			const termBSelected = treeHasSelection( termB );

			if ( termASelected === termBSelected ) {
				return 0;
			}

			if ( termASelected && ! termBSelected ) {
				return -1;
			}

			if ( ! termASelected && termBSelected ) {
				return 1;
			}

			return 0;
		};
		termsTree.sort( termOrChildIsSelected );
		return termsTree;
	}

	setFilterValue( event ) {
		const { availableTermsTree } = this.state;
		const filterValue = event.target.value;
		const filteredTermsTree = availableTermsTree.map( this.getFilterMatcher( filterValue ) ).filter( ( term ) => term );
		const getResultCount = ( terms ) => {
			let count = 0;
			for ( let i = 0; i < terms.length; i++ ) {
				count++;
				if ( undefined !== terms[ i ].children ) {
					count += getResultCount( terms[ i ].children );
				}
			}
			return count;
		};
		this.setState(
			{
				filterValue,
				filteredTermsTree,
			}
		);

		const resultCount = getResultCount( filteredTermsTree );
		const resultsFoundMessage = sprintf(
			_n( '%d result found.', '%d results found.', resultCount ),
			resultCount
		);
		this.props.debouncedSpeak( resultsFoundMessage, 'assertive' );
	}

	getFilterMatcher( filterValue ) {
		const matchTermsForFilter = ( originalTerm ) => {
			if ( '' === filterValue ) {
				return originalTerm;
			}

			const term = { ...originalTerm };

			if ( 0 < term.children.length ) {
				term.children = term.children.map( matchTermsForFilter ).filter( ( child ) => child );
			}

			if ( -1 !== term.name.toLowerCase().indexOf( filterValue ) || 0 < term.children.length ) {
				return term;
			}

			return false;
		};
		return matchTermsForFilter;
	}

	renderTerms( renderedTerms ) {
		const { terms = [], taxonomy } = this.props;
		const klass = 'undefined' !== typeof taxonomy && taxonomy.hierarchical ? 'hierarchical' : 'non-hierarchical';
		return renderedTerms.map( ( term ) => {
			const id = `column-taxonomies-${ klass }-term-${ term.id }`;
			return (
				<div key={ term.id } className="editor-post-taxonomies__hierarchical-terms-choice">
					<input
						id={ id }
						className="editor-post-taxonomies__hierarchical-terms-input"
						type="checkbox"
						checked={ -1 !== terms.indexOf( term.id ) }
						value={ term.id }
						onChange={ this.onChange }
						name={ `checkbox_tax_input-${this.props.slug}` }
					/>
					<label htmlFor={ id }>{ unescapeString( term.name ) }</label>
					{ !! term.children.length && (
						<div className="editor-post-taxonomies__hierarchical-terms-subchoices">
							{ this.renderTerms( term.children ) }
						</div>
					) }
				</div>
			);
		} );
	}

	render() {
		const { taxonomy, instanceId } = this.props;
		const klass = 'undefined' !== typeof taxonomy && taxonomy.hierarchical ? 'hierarchical' : 'non-hierarchical';

		const { availableTermsTree, availableTerms, filteredTermsTree, loading, filterValue } = this.state;
		const filterInputId = `editor-post-taxonomies__${ klass }-terms-filter-${ instanceId }`;
		const filterLabel = get(
			this.props.taxonomy,
			[ 'labels', 'search_items' ],
			__( 'Search Terms' )
		);
		const groupLabel = get(
			this.props.taxonomy,
			[ 'name' ],
			__( 'Terms' )
		);
		const showFilter = availableTerms.length >= MIN_TERMS_COUNT_FOR_FILTER;

		return [
			showFilter && <label
				key="filter-label"
				htmlFor={ filterInputId }>
				{ filterLabel }
			</label>,
			showFilter && <input
				type="search"
				id={ filterInputId }
				value={ filterValue }
				onChange={ this.setFilterValue }
				className="editor-post-taxonomies__hierarchical-terms-filter"
				key="term-filter-input"
			/>,
			<div
				className="editor-post-taxonomies__hierarchical-terms-list"
				key="term-list"
				tabIndex="0"
				role="group"
				aria-label={ groupLabel }
			>
				{ ( loading ) && <Spinner /> }
				{ this.renderTerms( '' !== filterValue ? filteredTermsTree : availableTermsTree ) }
			</div>
		];
	}
}

export default compose( [
	withSelect( ( select, { slug = 'category', terms, onChange } ) => {
		const { getTaxonomy } = select( 'core' );
		const taxonomy = getTaxonomy( slug );
		return {
			onChange: onChange,
			terms: terms,
			taxonomy,
		};
	} ),
	withSpokenMessages,
	withInstanceId,
] )( HierarchicalTermSelector );
