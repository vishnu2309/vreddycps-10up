/* eslint require-jsdoc: 0 */
/**
 * Renders the post type toggle.
 *
 * @package CPS
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;

const {
	CheckboxControl,
} = wp.components;

const {
	Component
} = wp.element;

import { filter, isFunction } from 'lodash';

/**
 * Renders a list of checkboxes to select the column post type.
 *
 * @param {obj} props Block properties.
 */
class PostTypeToggle extends Component {

	constructor( props ) {
		super( props );

		this.onChange = this.onChange.bind( this );
		this.toggle = this.toggle.bind( this );

		/*
		 * Getting a list of public post types seems a bit of a hassle using the
		 * JSON API, so lets just define a sensible set of default options.
		 */
		this.options = {
			'post': __( 'Posts', 'cps' ),
			'page': __( 'Pages', 'cps' ),
		};

		if ( this.props.options ) {
			this.options = this.props.options;
		}

		this.state = {
			'types': this.props.value
		};
	}

	onChange( value ) {

		if ( ! this.props.onChange || ! isFunction( this.props.onChange ) ) {
			// eslint-disable-next-line no-console
			console.error(
				'[PostTypeToggle] The "onChange" property must be specified and must be a valid function.'
			);
			return;
		}

		this.props.onChange( value );
	}

	toggle( isChecked, value ) {

		if ( false === isChecked ) {
			if ( 2 > this.state.types.length ) {
				return;
			}
			if (  -1 !== this.state.types.indexOf( value ) ) {

				const newTypes = filter( this.state.types, ( curValue ) => {
					return value !== curValue;
				} );

				this.setState( { types: newTypes } );
				this.onChange( newTypes );
			}
		} else {
			if ( -1 === this.state.types.indexOf( value ) ) {
				this.state.types.push( value );
				this.onChange( this.state.types );
			}
		}
	}

	render() {

		/*
		 * There is no checkbox group in Gutenberg unfortunately, and a massive bottom margin is
		 * added to checkboxes. Let's go ahead and add some styles here in stead of adding an
		 * additional stylesheet.
		 */
		const css = `
			.edit-post-settings-sidebar__panel-block .components-panel__body .cps-component-post-type-toggle .components-base-control {
		        margin-bottom: 0;
		    }`;

		return(
			<div className="cps-component-post-type-toggle">
				<style>{ css }</style>
				{
					Object.keys( this.options ).map( key =>
						<CheckboxControl
							label={ this.options[ key ] }
							checked={
								( -1 !== this.state.types.indexOf( key ) )
							}
							onChange={ ( isChecked ) => {
								this.toggle( isChecked, key );
							} }
						/>
					)
				}
			</div>
		);
	}
}

export default PostTypeToggle;
