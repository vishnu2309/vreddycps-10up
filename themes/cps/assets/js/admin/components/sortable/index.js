/* eslint require-jsdoc: 0 */
const {
	Button,
	Toolbar
} = wp.components;

const {
	Component
} = wp.element;

const {
	__,
} = wp.i18n;

import classnames from 'classnames';
import isFunction from 'lodash/isFunction';
import { v4 as uuid } from 'uuid';
import icon from './icon';

import {
	DragDropContext,
	Droppable,
	Draggable,
} from 'react-beautiful-dnd';

class Sortable extends Component {

	constructor( props ) {
		super( ...arguments );
		this.props = props;

		this.getItems = this.getItems.bind( this );
		this.addItem = this.addItem.bind( this );
		this.removeItem = this.removeItem.bind( this );
		this.onChange = this.onChange.bind( this );
		this.renderItem = this.renderItem.bind( this );
		this.moveItem = this.moveItem.bind( this );
		this.moveItemUp = this.moveItemUp.bind( this );
		this.moveItemDown = this.moveItemDown.bind( this );
		this.onDragEnd = this.onDragEnd.bind( this );
		this.renderToolbar = this.renderToolbar.bind( this );
	}

	/**
	 * Get all items in list.
	 *
	 * @return {array}
	 */
	getItems() {

		let { items } = this.props;

		if ( ! items ) {
			items = [];
		} else {
			if ( 'object' !== typeof items ) {
				items = JSON.parse( items );
			}
		}

		items.map( ( item, index ) => {

			const timestr = new Date().getTime();

			// This is not ideal, but better than nothing.
			if ( 'undefined' === typeof item.id ) {
				item.id = timestr + index;
			}
		} );

		return items;
	}

	/**
	 * Add a new item.
	 */
	addItem() {

		if ( ! this.props.addItem || ! isFunction( this.props.addItem ) ) {

			// eslint-disable-next-line no-console
			console.error(
				'[Sortable] The "addItem" property must be specified and must be a valid function.'
			);
			return;
		}

		this.props.addItem();
	}

	/**
	 * Remove an item.
	 *
	 * @param {int} index Index value of the item to remove.
	 */
	removeItem( index ) {

		const newItems = this.getItems();

		newItems.splice( index, 1 );

		this.props.onChange( newItems );
	}

	/**
	 * Call parent component onChange callback.
	 *
	 * @param {array} items List containing current items.
	 */
	onChange( items ) {

		if ( ! this.props.onChange || ! isFunction( this.props.onChange ) ) {

			// eslint-disable-next-line no-console
			console.error(
				'[Sortable] The "onChange" property must be specified and must be a valid function.'
			);
			return;
		}

		this.props.onChange( items );
	}

	/**
	 * Call parent component renderItem callback.
	 *
	 * @param {array} item List of item attributes.
	 */
	renderItem( item ) {

		if ( ! this.props.renderItem || ! isFunction( this.props.renderItem ) ) {

			// eslint-disable-next-line no-console
			console.error(
				'[Sortable] The "renderItem" property must be specified and must be a valid function.'
			);
			return;
		}

		return this.props.renderItem( item );
	}

	/**
	 * Move an item into another position.
	 *
	 * Will call the onChange calllback to enable persisting the new item positions.
	 *
	 * @param {int} index       The position of the item to move.
	 * @param {int} destination The position we want to move the item to.
	 */
	moveItem( index, destination ) {

		const items     = Array.from( this.getItems() );
		const [removed] = items.splice( index, 1 );

		items.splice( destination, 0, removed );

		this.props.onChange( items );
	}

	/**
	 * Move an item up one position in the list.
	 *
	 * @param {int} index The position of the item to move.
	 */
	moveItemUp( index ) {

		if ( 0 < index ) {
			this.moveItem( index, parseInt( index, 10 ) - 1 );
		}
	}

	/**
	 * Move an item down one position in the list.
	 *
	 * @param {int} index The position of the item to move.
	 */
	moveItemDown( index ) {
		this.moveItem( index, parseInt( index, 10 ) + 1 );
	}

	/**
	 * Callback for the DragDropContext component.
	 *
	 * The react-beautiful-dnd library provides the drag and drop interface but
	 * does not control the state of this component. A callback on dragEnd is
	 * required that is expected to synchronously reorder the list.
	 *
	 * @param {object} result
	 */
	onDragEnd( result ) {

		// Item is dropped outside of the list.
		if ( ! result.destination ) {
			return;
		}

		this.moveItem( result.source.index, result.destination.index );
	}

	renderToolbar( index, provided ) {

		const direction = this.props.direction || 'vertical',
			iconUp      = 'vertical' === direction ? 'arrow-up-alt2' : 'arrow-left-alt2',
			iconDown    = 'vertical' === direction ? 'arrow-down-alt2' : 'arrow-right-alt2',
			labelUp     = 'vertical' === direction ? __( 'Move Up' ) : __( 'Move Left' ),
			labelDown   = 'vertical' === direction ? __( 'Move Down' ) : __( 'Move Right' ),
			items       = this.getItems(),
			maxIndex    = items.length - 1;

		return[
			<Toolbar
				className="cps-sortable-item-toolbar"
			>
				<Button
					className="components-toolbar__control"
					disabled={ 0 === index ? 'disabled' : null }
					icon={ iconUp }
					key="up-item"
					label={ labelUp }
					onClick={ ( event ) => {
						event.target.blur();
						this.moveItemUp( event.target.closest( 'li' ).dataset.id );
					} }
				/>
				{ ! this.props.disableDrag &&
					<div
						className="cps-sortable-handle"
						{...provided.dragHandleProps}
					>
						<svg width="18" height="18" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
							<path d="M13,8c0.6,0,1-0.4,1-1s-0.4-1-1-1s-1,0.4-1,1S12.4,8,13,8z M5,6C4.4,6,4,6.4,4,7s0.4,1,1,1s1-0.4,1-1S5.6,6,5,6z M5,10
								c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S5.6,10,5,10z M13,10c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S13.6,10,13,10z M9,6
								C8.4,6,8,6.4,8,7s0.4,1,1,1s1-0.4,1-1S9.6,6,9,6z M9,10c-0.6,0-1,0.4-1,1s0.4,1,1,1s1-0.4,1-1S9.6,10,9,10z" />
						</svg>
					</div>
				}
				<Button
					className="components-toolbar__control"
					disabled={ maxIndex === index ? 'disabled' : null }
					icon={ iconDown }
					key="down-item"
					label={ labelDown }
					onClick={ ( event ) => {
						event.target.blur();
						this.moveItemDown( event.target.closest( 'li' ).dataset.id );
					} }
				/>
				<Button
					className="components-toolbar__control"
					icon="trash"
					key="remove-item"
					label={ __( 'Remove Item' ) }
					onClick={ ( event ) => {
						this.removeItem( event.target.closest( 'li' ).dataset.id );
					} }
				/>
			</Toolbar>
		];
	}

	/**
	 * Render sortable component
	 *
	 */
	render() {

		const direction = this.props.direction || 'vertical',
			disableDrag = this.props.disableDrag || false,
			items       = this.getItems(),
			droppableId = `cps-droppable-${ uuid() }`;

		return [
			<div className="cps-sortable">
				<DragDropContext onDragEnd={ this.onDragEnd } >
					<Droppable droppableId={ droppableId } direction={ direction }>
						{ ( provided ) => (
							<ul
								ref={ provided.innerRef }
								className={ classnames(
									'cps-sortable-items',
									this.props.className || null
								) }
							>
								{
									items.map( ( item, index ) => {

										if ( ! item || 'undefined' === typeof item ) {
											return;
										}

										return (
											<Draggable
												draggableId={ item.id.toString() }
												index={ index }
												key={ item.id }
												isDragDisabled={ disableDrag }
											>
												{ ( provided, snapshot ) => (
													<li
														className={ classnames(
															'cps-sortable-item',
															snapshot.isDragging ? 'cps-sortable-item-dragging' : ''
														) }
														data-id={ index }
														ref={ provided.innerRef }
														{...provided.draggableProps}
													>
														{ ! this.props.isSelected &&
															this.renderToolbar( index, provided )
														}
														<div className="cps-sortable-item-wrap">
															{ this.renderItem( item ) }
														</div>
													</li>
												) }
											</Draggable>
										);
									} )
								}
								{ provided.placeholder }
							</ul>
						) }
					</Droppable>
				</DragDropContext>

				{ isFunction( this.props.addItem ) &&
					<div className="cps-sortable-inserter">
						<Button
							className="cps-sortable-inserter-button"
							icon={ icon }
							key="add-item"
							label={ __( 'Add Item', 'cps' ) }
							onClick={ ( event ) => {
								event.stopPropagation();
								this.addItem();
								event.target.blur();
							} }
						/>
					</div>
				}

			</div>
		];
	}
}

export default Sortable;
