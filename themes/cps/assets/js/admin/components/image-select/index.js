/* eslint require-jsdoc: 0 */
/**
 * Renders an image uploader with preview.
 *
 * @package CPS
 *
 * Example:
 *
 * <ImageSelect
 *     image={ JSON.parse( image ) }
 *     isSelected={ isSelected }
 *     labels={ {
 *         title: __( 'Image' ),
 *         instructions: __( 'Upload an image file or pick one from your media library.', 'cps' ),
 *      } }
 *      onChange={ ( value ) => {
 *          onChangeImage( value ); // Note: your component needs to define the onChangeImage callback.
 *      } }
 * />
 *
 * const onChangeImage = ( attr ) => {
 *     const value = ( isEmpty( attr ) ? null : attr );
 *     setAttributes( { image: JSON.stringify( value ) } );
 * };
 */

/**
 * WordPress dependencies
 */
const {
	IconButton,
	Toolbar
} = wp.components;

const { __ } = wp.i18n;

const {
	BACKSPACE,
	DELETE
} = wp.keycodes;

const {
	MediaPlaceholder,
	MediaUpload,
	MediaUploadCheck
} = wp.blockEditor;

const {
	Component,
} = wp.element;

/**
 * External dependencies
 */
import classnames from 'classnames';
import pick from 'lodash/pick';

import icon from './icon';

export class ImageSelect extends Component {

	constructor() {
		super( ...arguments );

		this.bindContainer = this.bindContainer.bind( this );
		this.onClickImage = this.onClickImage.bind( this );
		this.onRemoveImage = this.onRemoveImage.bind( this );
		this.onChangeImage = this.onChangeImage.bind( this );
		this.removeImage = this.removeImage.bind( this );

		this.state = {
			isSelected: false,
		};
	}

	bindContainer( ref ) {
		this.container = ref;
	}

	onClickImage() {
		this.setState( {
			isSelected: ! this.state.isSelected,
		} );
	}

	onRemoveImage( event ) {
		if (
			this.container === document.activeElement &&
			this.state.isSelected && -1 !== [ BACKSPACE, DELETE ].indexOf( event.keyCode )
		) {
			event.stopPropagation();
			event.preventDefault();
			this.removeImage();
		}
	}

	removeImage() {
		this.props.onChange( {
			alt: undefined,
			id: undefined,
			url: undefined,
		} );

		this.setState( {
			isSelected: false,
		} );
	}

	onChangeImage( media ) {
		if ( ! media || ! media.url ) {
			this.props.onChange( {
				alt: undefined,
				id: undefined,
				url: undefined,
			} );
			return;
		}
		this.props.onChange( {
			...pick( media, [ 'alt', 'id', 'url' ] ),
		} );
		this.setState( {
			isSelected: false,
		} );
	}

	componentDidUpdate( prevProps ) {
		// Deselect image when deselecting the block it is contained within.
		if ( ! this.props.isSelected && prevProps.isSelected ) {
			this.setState( {
				isSelected: false,
			} );
		}
	}

	renderPlaceholder() {
		const {
			className,
			labels = {},
		} = this.props;

		return(
			<MediaPlaceholder
				icon={ icon }
				labels={ labels }
				className={ className }
				onSelect={ this.onChangeImage }
				notices=''
				onError=''
				accept="image/*"
				allowedTypes={ ['image'] }
			/>
		);
	}

	renderToolbar() {
		const { image } = this.props;
		return(
			<MediaUploadCheck>
				<Toolbar>
					<MediaUpload
						addToGallery={ false }
						gallery={ false }
						multiple={ false }
						onSelect={ this.onChangeImage }
						allowedTypes={ ['image'] }
						value={ image.id || 0 }
						render={ ( { open } ) => (
							<IconButton
								icon="edit"
								onClick={ ( event ) => {
									event.target.blur();
									event.stopPropagation();
									open();
								} }
								label={ __( 'Edit image' ) }
							/>
						)}
					/>
					<IconButton
						icon="no-alt"
						onClick={ this.removeImage }
						label={ __( 'Remove Image' ) }
					/>
				</Toolbar>
			</MediaUploadCheck>
		);
	}

	render() {

		const { image } = this.props;

		const className = classnames( {
			'is-selected': this.state.isSelected,
			'is-transient': image && image.url && 0 === image.url.indexOf( 'blob:' ),
		} );

		if ( image && image.url ) {
			return(
				<figure
					className={ `cps-image-uploader ${className}` }
					tabIndex="-1"
					onKeyDown={ this.onRemoveImage }
					onClick={ this.onClickImage }
					ref={ this.bindContainer }
				>
					{ this.state.isSelected &&
						this.renderToolbar()
					}
					<img
						src={ image.url }
						alt={ image.alt }
						data-id={ image.id }
						tabIndex="0"
						aria-label={ __( 'image upload' ) }
					/>
				</figure>
			);
		}

		return this.renderPlaceholder();
	}
}

export default ImageSelect;
