/**
 * Faculty Directory Block
 */

const { __ }                = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText }          = wp.blockEditor;

import icon from './icon';

/**
 * Register block
 */
export default registerBlockType(
	'cps/faculty-directory',
	{
		title: __( 'Faculty & Staff Directory', 'cps' ),
		icon: icon,
		description: __( 'A searchable list of faculty and staff.' ),
		category: 'cps-blocks',
		attributes: {
			headline: {
				type: 'string'
			},
			copy: {
				type: 'string'
			}
		},
		supports: {
			html: false,
			className: false,
			customClassName: false,
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {

			const {
				attributes: {
					copy,
					headline
				},
				setAttributes,
			} = props;

			return (
				<div class="block-faculty-directory">
					<RichText
						allowedFormats={ [] }
						format="string"
						onChange={ headline => setAttributes( { headline } ) }
						keepPlaceholderOnFocus="true"
						placeholder={ __( 'Add headline', 'cps' ) }
						tagName="h2"
						value={ headline }
						className="faculty-directory-title"
					/>
					<div>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							keepPlaceholderOnFocus="true"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Add text', 'cps' ) }
							tagName="div"
							value={ copy }
						/>
					</div>
					<div class="description">
						<p>
							{ __( 'The faculty directory table will be shown here.', 'cps' ) }
						</p>
					</div>
				</div>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		}
	}
);
