/**
 * Stat Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const {
	PanelBody,
	RadioControl,
	RangeControl,
} = wp.components;

const {
	InspectorControls,
	PanelColorSettings,
	RichText,
	withColors,
} = wp.blockEditor;

import isEmpty from 'lodash/isEmpty';
import icon from './icon';
import ImageSelect from '../../components/image-select';

/**
 * Register block
 */
export default registerBlockType(
	'cps/stat',
	{
		title: __( 'Stat', 'cps' ),
		description: __( 'Content element used for displaying a statistic or number.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'card', 'cps' ),
			__( 'number', 'cps' ),
		],
		attributes: {
			copy: {
				type: 'string'
			},
			headline: {
				type: 'string'
			},
			statImage: {
				type: 'string',
				default: '{}',
			},
			statImageAltText: {
				type: 'string'
			},
			statText: {
				type: 'string'
			},
			statTextSize: {
				type: 'int',
				default: 107
			},
			statType: {
				type: 'string',
				default: 'text'
			},
			colorScheme: {
				type: 'string',
			},
			className: {
				type: 'string',
			}
		},
		supports: {
			html: false,
			className: false,
			customClassName: false,
		},
		example: {
			attributes: {
				statText: '505k',
				headline: 'New PM roles within the next seven years',
				copy: 'That\'s across the globe, and across industries',
				statType: 'text',
			},
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: withColors( 'colorScheme' )( props => {
			const {
				attributes: {
					copy,
					headline,
					statImage,
					statText,
					statTextSize,
					statType
				},
				isSelected,
				setAttributes,
				colorScheme,
				setColorScheme,
				className,
			} = props;

			/**
			 * Callback for the ImageSelect component.
			 *
			 * @return {string}
			 */
			const onChangeImage = ( attr ) => {
				/*
				 * Note: The ImageSelect component returns an object
				 *       containing alt, id and url
				 */
				const value = ( isEmpty( attr ) ? null : attr );
				setAttributes( { statImage: JSON.stringify( value ) } );
			};

			/**
			 * Render block content.
			 *
			 * @return {string}
			 */
			const renderBlockContent = () => {

				const textSizeEm = statTextSize;
				const textStyle  = 107 !== textSizeEm ? {fontSize: `${textSizeEm}px`} : {};

				return (
					<div className={ `block-stat ${className}`}>
						{ 'image' === statType &&
							<ImageSelect
								image={ JSON.parse( statImage ) }
								isSelected={ isSelected }
								labels={ {
									title: __( 'Image' ),
									instructions: __( 'Upload an image file or pick one from your media library.', 'cps' ),
								} }
								onChange={ ( value ) => {
									onChangeImage( value );
								} }
							/>
						}
						{ 'text' === statType &&
							<div className={`block-stat__number has-${colorScheme.slug}-color`} style={ textStyle } >
								<RichText
									allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
									format="string"
									keepPlaceholderOnFocus="true"
									onChange={ statText => setAttributes( { statText } ) }
									placeholder={ __( 'Write statistic…', 'cps' ) }
									tagName="div"
									value={ statText }
								/>
							</div>
						}
						<RichText
							allowedFormats={ [] }
							format="string"
							onChange={ headline => setAttributes( { headline } ) }
							keepPlaceholderOnFocus="true"
							multiline="false"
							placeholder={ __( 'Write headline…', 'cps' ) }
							tagName="h3"
							value={ headline }
						/>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							keepPlaceholderOnFocus="true"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Write copy…', 'cps' ) }
							tagName="div"
							value={ copy }
						/>
					</div>
				);
			};

			return (

				<Fragment>
					<InspectorControls>
						<PanelBody
							title={ __( 'Block Settings', 'cps' ) }
							initialOpen={ true }
						>
							<RadioControl
								label={ __( 'Type' ) }
								selected={ statType }
								options={ [
									{ label: 'Image', value: 'image' },
									{ label: 'Text', value: 'text' },
								] }
								onChange={ ( statType ) => { setAttributes( { statType } ); } }
							/>
							{ 'text' === statType &&
								<div className="components-base-control">
									<RangeControl
										label={ __( 'Stat Size', 'cps' ) }
										value={ statTextSize }
										onChange={ statTextSize => setAttributes( { statTextSize } ) }
										min={ 10 }
										max={ 100 }
									/>
								</div>
							}
						</PanelBody>
						<PanelColorSettings
							title={__( 'Color Settings', 'cps' )}
							colorSettings={[
								{
									value: colorScheme.color,
									label: __(
										'Stat Color',
										'cps'
									),
									onChange: setColorScheme,
								},
							]}
						></PanelColorSettings>
					</InspectorControls>
					{
						renderBlockContent()
					}
				</Fragment>
			);
		} ),
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
