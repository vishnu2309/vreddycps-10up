/**
 * Faculty Grid block
 */

const { __ }                    = wp.i18n;
const { registerBlockType }     = wp.blocks;
const { InnerBlocks, RichText } = wp.blockEditor;
const { Fragment }              = wp.element;
const { TextControl }           = wp.components;

import icon from './icon';

/**
 * Returns the template for the block.
 *
 * @return {Object[]} Block template.
 */
const getColumnsTemplate = () => {
	return [
		[ 'cps/faculty-grid-column' ],
		[ 'cps/faculty-grid-column' ],
		[ 'cps/faculty-grid-column' ]
	];
};

/**
 * Register block
 */
export default registerBlockType(
	'cps/faculty-grid',
	{
		title: __( 'Faculty Grid', 'cps' ),
		description: __( 'A three-column grid highlighting faculty members', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'card', 'cps' )
		],
		attributes: {
			copy: {
				type: 'string'
			},
			ctaLink: {
				type: 'string'
			},
			ctaText: {
				type: 'string'
			},
			headline: {
				type: 'string'
			}
		},
		supports: {
			html: false,
			customClassName: false
		},

		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					copy,
					ctaLink,
					ctaText,
					headline
				},
				setAttributes,
				isSelected
			} = props;

			return (
				<div className="block-faculty-grid">
					<RichText
						allowedFormats={ [] }
						format="string"
						onChange={ headline => setAttributes( { headline } ) }
						keepPlaceholderOnFocus="true"
						placeholder={ __( 'Add headline', 'cps' ) }
						tagName="h2"
						value={ headline }
						className="faculty-grid-title"
					/>
					<div>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							keepPlaceholderOnFocus="true"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Add text', 'cps' ) }
							tagName="div"
							value={ copy }
						/>
					</div>
					<div className="faculty-grid">
						<div className="faculty-grid__slider">
							<InnerBlocks
								allowedBlocks={ ['cps/faculty-grid-column'] }
								orientation='horizontal'
								template={ getColumnsTemplate() }
								renderAppender={() => <InnerBlocks.ButtonBlockAppender />}
							/>
						</div>
					</div>
					<RichText
						allowedFormats={ [] }
						format="string"
						onChange={ ctaText => setAttributes( { ctaText } ) }
						keepPlaceholderOnFocus="true"
						placeholder={ __( 'Add Call to Action', 'cps' ) }
						tagName="p"
						value={ ctaText }
						className="button-tertiary"
					/>
					{ isSelected &&
						<TextControl
							id=""
							label={ __( 'CTA Link', 'cps' ) }
							value={ ctaLink }
							onChange={ ctaLink => setAttributes( { ctaLink } ) }
						/>
					}
				</div>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * Output the block's inner blocks so we can parse them in PHP.
		 */
		save: () => {
			return (
				<Fragment>
					<InnerBlocks.Content />
				</Fragment>
			);
		}
	}
);
