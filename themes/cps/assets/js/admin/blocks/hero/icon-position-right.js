/**
 * WordPress dependencies
 */
const { Path, SVG } = wp.components;

export default (
	<SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
		<Path d="M6 15h14V9H6v6zm6-10.8v1.5h8V4.2h-8zm0 15.6h8v-1.5h-8v1.5z" />
	</SVG>
);
