/**
 * Hero Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import icon from './icon';
import edit from './edit';

/**
 * Register block
 */
export default registerBlockType(
	'cps/hero',
	{
		title: __( 'Hero', 'cps' ),
		description: __( 'Page header with background color, image or video.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'header', 'cps' ),
			__( 'title', 'cps' ),
		],
		attributes: {
			backgroundType: {
				type: 'string',
				default: 'color'
			},
			backgroundColor: {
				type: 'string',
				default: 'white'
			},
			copy: {
				type: 'string'
			},
			ctaPrimaryLabel: {
				type: 'string'
			},
			ctaPrimaryUrl: {
				type: 'string'
			},
			ctaSecondaryLabel: {
				type: 'string'
			},
			ctaSecondaryUrl: {
				type: 'string'
			},
			headline: {
				type: 'string'
			},
			image: {
				type: 'string',
				default: '{}',
			},
			imageAlignment: {
				type: 'string',
				default: 'center',
			},
			navigationColor: {
				type: 'string',
				default: 'black',
			},
			overlayColor: {
				type: 'string',
				default: 'none',
			},
			overlayOpacity: {
				type: 'int',
				default: 0,
			},
			textAlignment: {
				type: 'string'
			},
			videoUrl: {
				type: 'string'
			},
			fixedRatio: {
				type: 'boolean',
				default: true,
			},
		},
		supports: {
			html: false,
			className: false,
			customClassName: false,
		},

		/**
		 * Display the block full-width without showing the alignment control.
		 *
		 * @param attributes
		 * @returns {object}
		 */
		getEditWrapperProps( attributes ) {
			attributes['data-align'] = 'full';
			return attributes;
		},
		edit,
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
