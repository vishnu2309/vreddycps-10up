/* eslint require-jsdoc: 0 */

const { __ } = wp.i18n;

const { Component } = wp.element;

const {
	Button,
	ButtonGroup,
	PanelBody,
	PanelRow,
	RangeControl,
	SelectControl,
	TextControl,
	ToggleControl,
} = wp.components;

const {
	AlignmentToolbar,
	BlockControls,
	InspectorControls,
	RichText,
} = wp.blockEditor;

import ColorSelect from '../../components/color-select';
import ImageSelectInspector from '../../components/image-select-inspector';
import iconAlignCenter from './icon-align-center';
import iconAlignLeft from './icon-align-left';
import iconPositionCenter from './icon-position-center';
import iconPositionLeft from './icon-position-left';
import iconPositionRight from './icon-position-right';
import isEmpty from 'lodash/isEmpty';

const backgroundTypeOptions = [
	{
		label: __( 'Color', 'cps' ),
		value: 'color'
	},
	{
		label: __( 'Image', 'cps' ),
		value: 'image'
	},
	{
		label: __( 'Video', 'cps' ),
		value: 'video'
	},
];

const imageAlignmentControls = [
	{
		icon: iconPositionLeft,
		title: __( 'Align image left', 'cps' ),
		align: 'left',
	},
	{
		icon: iconPositionCenter,
		title: __( 'Align image center', 'cps' ),
		align: 'center',
	},
	{
		icon: iconPositionRight,
		title: __( 'Align image right', 'cps' ),
		align: 'right',
	},
];

const navigationColorOptions = [
	{
		label: __( 'Black', 'cps' ),
		value: 'black'
	},
	{
		label: __( 'White', 'cps' ),
		value: 'white'
	},
];

const overlayColorOptions = [
	{
		label: __( 'None', 'cps' ),
		value: 'none'
	},
	{
		label: __( 'Black', 'cps' ),
		value: 'black'
	},
	{
		label: __( 'White', 'cps' ),
		value: 'white'
	},
];

const textAlignmentControls = [
	{
		icon: iconAlignLeft,
		title: __( 'Align text left', 'cps' ),
		align: 'left',
	},
	{
		icon: iconAlignCenter,
		title: __( 'Align text center', 'cps' ),
		align: 'center',
	},
];

/**
 * Hero block edit function.
 */
class HeroEdit extends Component {

	/**
	 * Component constructor.
	 *
	 * @return {void}
	 */
	constructor() {
		super( ...arguments );
	}

	/**
	 * Render component.
	 */
	render() {

		const {
			attributes: {
				backgroundColor,
				backgroundType,
				headline,
				copy,
				ctaPrimaryLabel,
				ctaPrimaryUrl,
				ctaSecondaryLabel,
				ctaSecondaryUrl,
				image,
				imageAlignment,
				navigationColor,
				overlayColor,
				overlayOpacity,
				textAlignment,
				videoUrl,
				fixedRatio,
			},
			className,
			isSelected,
			setAttributes,
		} = this.props;

		/**
		 * Return placeholder image or selected image.
		 *
		 * @return {string}
		 */
		const imageSrc = () => {

			const imageObj = JSON.parse( image );

			if ( isEmpty( imageObj ) ) {
				return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPc/erFfwAIgwOOjoLB7gAAAABJRU5ErkJggg==';
			} else {
				return imageObj.url;
			}
		};

		const onChangeImage = ( attr ) => {
			/*
			 * Note: The ImageSelect component returns an object
			 *       containing alt, id and url
			 */
			const value = ( isEmpty( attr ) ? null : attr );
			setAttributes( { image: JSON.stringify( value ) } );
		};

		const renderBlockContent = () => {

			const textColor = 'white' !== backgroundColor ? 'white' : '';
			const navColor = 'video' === backgroundType ? '' : navigationColor;

			return (
				<div className={ `block-hero block-hero--${backgroundType} block-hero--overlay-${overlayColor} has-${'color' === backgroundType ? textColor : navColor}-color has-${backgroundColor}-background-color block-hero--${fixedRatio ? 'fixed' : 'free'} ${className}` }>
					{ 'image' === backgroundType &&
						<div className="block-hero__image">
							<img src={ imageSrc() } />
						</div>
					}
					{ 'video' === backgroundType &&
						<div className="block-hero__video">
							<img src="https://placehold.it/1600x900" />
						</div>
					}
					{ 'none' !== overlayColor &&
						<div className="block-hero__overlay" style={{opacity: `${overlayOpacity/100}`}}>
						</div>
					}
					<div className="block-hero__content">
						<RichText
							allowedFormats={ [] }
							format="string"
							onChange={ headline => setAttributes( { headline } ) }
							keepPlaceholderOnFocus="true"
							multiline="false"
							placeholder={ __( 'Write headline…', 'cps' ) }
							tagName="h1"
							value={ headline }
							className="cps-block-input-title"
						/>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							keepPlaceholderOnFocus="true"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Write copy…', 'cps' ) }
							tagName="div"
							value={ copy }
						/>
						<TextControl
							id=""
							label={ __( 'Primary CTA Label', 'cps' ) }
							value={ ctaPrimaryLabel }
							onChange={ ctaPrimaryLabel => setAttributes( { ctaPrimaryLabel } ) }
						/>
						{ isSelected &&
							<TextControl
								id=""
								label={ __( 'Primary CTA Link', 'cps' ) }
								value={ ctaPrimaryUrl }
								onChange={ ctaPrimaryUrl => setAttributes( { ctaPrimaryUrl } ) }
							/>
						}
						<TextControl
							id=""
							label={ __( 'Secondary CTA Label', 'cps' ) }
							value={ ctaSecondaryLabel }
							onChange={ ctaSecondaryLabel => setAttributes( { ctaSecondaryLabel } ) }
						/>
						{ isSelected &&
							<TextControl
								id=""
								label={ __( 'Secondary CTA Link', 'cps' ) }
								value={ ctaSecondaryUrl }
								onChange={ ctaSecondaryUrl => setAttributes( { ctaSecondaryUrl } ) }
							/>
						}
					</div>

				</div>
			);
		};

		return (
			<>
				<BlockControls>
					<AlignmentToolbar
						value={ textAlignment }
						alignmentControls = { textAlignmentControls }
						label ={ __( 'Change text alignment', 'cps' ) }
						onChange={ textAlignment => setAttributes( { textAlignment } ) }
					/>
					<AlignmentToolbar
						value={ imageAlignment }
						alignmentControls = { imageAlignmentControls }
						label ={ __( 'Change image alignment', 'cps' ) }
						onChange={ imageAlignment => setAttributes( { imageAlignment } ) }
					/>
				</BlockControls>
				<InspectorControls>
					<PanelBody
						title={ __( 'Block Settings', 'cps' ) }
						initialOpen={ true }
					>
						<PanelRow>
							<label>{ __( 'Type', 'cps' ) }</label>
							<ButtonGroup>
								{
									backgroundTypeOptions.map( ( { label, value } ) => {
										return <Button
											value={ value }
											isPrimary={ value === backgroundType }
											isLarge={ true }
											aria-pressed={ value === backgroundType }
											onClick={ () => {
												setAttributes( { backgroundType: value } );
											} }
										>
											{ label }
										</Button>;
									} )
								}
							</ButtonGroup>
						</PanelRow>

						{ 'color' === backgroundType &&
							<PanelRow>
								<ColorSelect
									value={ backgroundColor }
									onChange={ backgroundColor => setAttributes( { backgroundColor } ) }
								/>
							</PanelRow>
						}
						{ ( 'image' === backgroundType || 'video' === backgroundType ) &&
							<PanelRow>
								<div className="cps-image-upload">
									{ 'video' === backgroundType &&
										<p>{__( 'Image to use on mobile', 'cps' )}</p>
									}
									<ImageSelectInspector
										image={ JSON.parse( image ) }
										isSelected={ isSelected }
										labels={ {
											title: __( 'Image' ),
											instructions: __( 'Upload an image file or pick one from your media library.', 'cps' ),
										} }
										onChange={ ( value ) => {
											onChangeImage( value );
										} }
									/>
								</div>
							</PanelRow>
						}
						{ 'image' === backgroundType &&
							<PanelRow>
								<ToggleControl
									label={__(
										'Fixed Aspect Ratio',
										'cps'
									)}
									checked={!!fixedRatio}
									onChange={() => setAttributes( { fixedRatio: !fixedRatio } )}
								/>
							</PanelRow>
						}
						{ 'video' === backgroundType &&
							<PanelRow>
								<TextControl
									id=""
									label={ __( 'Video URL', 'cps' ) }
									value={ videoUrl }
									onChange={ videoUrl => setAttributes( { videoUrl } ) }
								/>
							</PanelRow>
						}
					</PanelBody>
					<PanelBody
						title={ __( 'Navigation & Overlay', 'cps' ) }
						initialOpen={ true }
					>
						<PanelRow>
							<div>
								<SelectControl
									label={ __( 'Navigation Color', 'cps' ) }
									value={ navigationColor }
									onChange={ (  navigationColor ) => {
										setAttributes( { navigationColor } );
									} }
									options={ navigationColorOptions }
								/>
								{ ( 'video' === backgroundType || 'image' === backgroundType ) &&
								<div>
									<SelectControl
										label={ __( 'Overlay Color', 'cps' ) }
										value={ overlayColor }
										onChange={ (  overlayColor ) => {
											setAttributes( { overlayColor } );
										} }
										options={ overlayColorOptions }
									/>
									{ 'none' !== overlayColor &&
										<RangeControl
											label={ __( 'Overlay Opacity', 'cps' ) }
											value={ overlayOpacity }
											onChange={ overlayOpacity => setAttributes( { overlayOpacity } ) }
											min={ 0 }
											max={ 100 }
										/>
									}
								</div>
								}
							</div>
						</PanelRow>
					</PanelBody>
				</InspectorControls>
				{
					renderBlockContent()
				}
			</>
		);
	}
}

export default HeroEdit;
