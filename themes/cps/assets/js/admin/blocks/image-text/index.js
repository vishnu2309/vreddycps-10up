/**
 * Stat Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { PanelBody, PanelRow,TextControl, ToolbarGroup } = wp.components;
const { BlockControls, InspectorControls, RichText } = wp.blockEditor;
const { Fragment } = wp.element;

import isEmpty from 'lodash/isEmpty';
import icon from './icon';
import pullLeft from './pull-left';
import pullRight from './pull-right';
import ImageSelectInspector from '../../components/image-select-inspector';

/**
 * Register block
 */
export default registerBlockType(
	'cps/image-text',
	{
		title: __( 'Image & Text', 'cps' ),
		description: __( 'Set an image and words side-by-side', 'cps' ),
		icon: icon,
		category: 'media',
		keywords: [
			__( 'card', 'cps' ),
		],
		attributes: {
			copy: {
				type: 'string'
			},
			ctaLabelOne: {
				type: 'string'
			},
			ctaLinkOne: {
				type: 'string',
			},
			ctaLabelTwo: {
				type: 'string'
			},
			ctaLinkTwo: {
				type: 'string',
			},
			ctaLabelThree: {
				type: 'string'
			},
			ctaLinkThree: {
				type: 'string',
			},
			headline: {
				type: 'string'
			},
			image: {
				type: 'string',
				default: '{}',
			},
			mediaPosition: {
				type: 'string',
				default: 'right',
			}
		},
		supports: {
			customClassName: false,
			html: false,
			defaultStylePicker: false
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					copy,
					ctaLabelOne,
					ctaLinkOne,
					ctaLabelTwo,
					ctaLinkTwo,
					ctaLabelThree,
					ctaLinkThree,
					headline,
					image,
					mediaPosition
				},
				isSelected,
				setAttributes,
			} = props;

			/**
			 * Return placeholder image or selected image.
			 *
			 * @return {string}
			 */
			const imageSrc = () => {

				const imageObj = JSON.parse( image );

				if ( isEmpty( imageObj ) ) {
					return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPc/erFfwAIgwOOjoLB7gAAAABJRU5ErkJggg==';
				} else {
					return imageObj.url;
				}
			};

			const toolbarControls = [
				{
					icon: pullLeft,
					title: __( 'Show media on left', 'cps' ),
					isActive: 'left' === mediaPosition,
					onClick: () => setAttributes( { mediaPosition: 'left' } ),
				},
				{
					icon: pullRight,
					title: __( 'Show media on right', 'cps' ),
					isActive: 'right' === mediaPosition,
					onClick: () => setAttributes( { mediaPosition: 'right' } ),
				},
			];

			/**
			 * Callback for the ImageSelect component.
			 *
			 * @return {string}
			 */
			const onChangeImage = ( attr ) => {
				/*
				 * Note: The ImageSelect component returns an object
				 *       containing alt, id and url
				 */
				const value = ( isEmpty( attr ) ? null : attr );
				setAttributes( { image: JSON.stringify( value ) } );
			};

			return (
				<>
					<BlockControls>
						<ToolbarGroup controls={ toolbarControls } />
					</BlockControls>
					<InspectorControls>
						<PanelBody
							title={ __( 'Block Settings', 'cps' ) }
							initialOpen={ true }
						>
							<PanelRow>
								<div>
									<ImageSelectInspector
										image={ JSON.parse( image ) }
										isSelected={ isSelected }
										labels={ {
											title: __( 'Image' ),
											instructions: __( 'Upload an image file or pick one from your media library.', 'cps' ),
										} }
										onChange={ ( value ) => {
											onChangeImage( value );
										} }
									/>
									<TextControl
										id=""
										label={ __( 'CTA Label One', 'cps' ) }
										value={ ctaLabelOne }
										onChange={ ctaLabelOne => setAttributes( { ctaLabelOne } ) }
									/>
									<TextControl
										id=""
										label={ __( 'CTA Link One', 'cps' ) }
										value={ ctaLinkOne }
										onChange={ ctaLinkOne => setAttributes( { ctaLinkOne } ) }
									/>
									<TextControl
										id=""
										label={ __( 'CTA Label Two', 'cps' ) }
										value={ ctaLabelTwo }
										onChange={ ctaLabelTwo => setAttributes( { ctaLabelTwo } ) }
									/>
									<TextControl
										id=""
										label={ __( 'CTA Link Two', 'cps' ) }
										value={ ctaLinkTwo }
										onChange={ ctaLinkTwo => setAttributes( { ctaLinkTwo } ) }
									/>
									<TextControl
										id=""
										label={ __( 'CTA Label Three', 'cps' ) }
										value={ ctaLabelThree }
										onChange={ ctaLabelThree => setAttributes( { ctaLabelThree } ) }
									/>
									<TextControl
										id=""
										label={ __( 'CTA Link Three', 'cps' ) }
										value={ ctaLinkThree }
										onChange={ ctaLinkThree => setAttributes( { ctaLinkThree } ) }
									/>
								</div>
							</PanelRow>
						</PanelBody>
					</InspectorControls>
					<div className={ `block-image-text block-image-text--align${mediaPosition}` }>
						<div className="block-image-text__image">
							<img src={ imageSrc() } />
						</div>
						<div className="block-image-text__content">
							<RichText
								allowedFormats={ [] }
								format="string"
								onChange={ headline => setAttributes( { headline } ) }
								keepPlaceholderOnFocus="true"
								multiline="false"
								placeholder={ __( 'Write headline…', 'cps' ) }
								tagName="h3"
								value={ headline }
								className="cps-block-input-title"
							/>
							<RichText
								allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ copy => setAttributes( { copy } ) }
								placeholder={ __( 'Write copy…', 'cps' ) }
								tagName="div"
								value={ copy }
							/>
							{ ! isEmpty( ctaLabelOne ) && ! isEmpty( ctaLinkOne ) &&
								<Fragment>
									<p><a href={ ctaLinkOne }>{ ctaLabelOne }</a></p>
								</Fragment>
							}
							{ ! isEmpty( ctaLabelTwo ) && ! isEmpty( ctaLinkTwo ) &&
								<Fragment>
									<p><a href={ ctaLinkTwo }>{ ctaLabelTwo }</a></p>
								</Fragment>
							}
							{ ! isEmpty( ctaLabelThree ) && ! isEmpty( ctaLinkThree ) &&
								<Fragment>
									<p><a href={ ctaLinkThree }>{ ctaLabelThree }</a></p>
								</Fragment>
							}
						</div>
					</div>
				</>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
