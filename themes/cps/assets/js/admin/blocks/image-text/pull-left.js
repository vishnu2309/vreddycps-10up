/**
 * WordPress dependencies
 */
const { Path, SVG } = wp.components;

export default (
	<SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
		<Path d="M4 18h6V6H4v12zm9-10v1.5h7V8h-7zm0 7.5h7V14h-7v1.5z" />
	</SVG>
);
