/**
 * Carousel Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;
const { useDispatch, useSelect } = wp.data;

/**
 * The list of allowed blocks in a carousel item.
 *
 * @constant
 * @type {string[]}
 */
const ALLOWED_BLOCKS = [
	'cps/quote',
	'cps/stat',
	'core/image',
	'core-embed/youtube',
];

import icon from './icon-item';

/**
 * Register block
 */
export default registerBlockType(
	'cps/carousel-item',
	{
		title: __( 'Slide', 'cps' ),
		description: __( 'A slide for the carousel block.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'image', 'cps' ),
			__( 'video', 'cps' ),
			__( 'item', 'cps' ),
		],
		parent: [ 'cps/carousel' ],
		supports: {
			html: false,
			customClassName: false,
			defaultStylePicker: false,
			reusable: false,
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: ( props ) => {

			const { clientId } = props;

			const { blockCount } = useSelect( select => ( {
				blockCount: select( 'core/block-editor' ).getBlockCount( props.clientId )
			} ) );

			const { updateBlockAttributes } = useDispatch( 'core/block-editor' );

			const { getBlocksByClientId } = useSelect( select => ( {
				getBlocksByClientId: select( 'core/block-editor' ).getBlocksByClientId( clientId )
			} ) );

			getBlocksByClientId[0].innerBlocks.forEach( function ( block ) {
				updateBlockAttributes( block.clientId, { align: 'center' } );
			} );

			const extraClassName = 0 === blockCount ? 'empty' : '';

			return (
				<div className={ `block-carousel-item ${extraClassName}` }>
					{ 0 === blockCount &&
						<InnerBlocks
							allowedBlocks={ ALLOWED_BLOCKS }
							renderAppender={ () => <InnerBlocks.ButtonBlockAppender /> }
						/>
					}
					{ 0 < blockCount &&
						<InnerBlocks
							allowedBlocks={ ALLOWED_BLOCKS }
							renderAppender={ false }
							templateLock="all"
						/>
					}
				</div>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return (
				<InnerBlocks.Content />
			);
		},
	},
);
