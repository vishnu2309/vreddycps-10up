/**
 * Carousel Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

const {
	Button,
	PanelBody,
	PanelRow,
	ToggleControl,
	RangeControl
} = wp.components;

const {
	BlockControls,
	InnerBlocks,
	InspectorControls
} = wp.blockEditor;

const {
	Fragment,
} = wp.element;

import classnames from 'classnames';
import SlidesPreview from './slides-preview';

/**
 * Returns the template for the block.
 *
 * @return {Object[]} Block template.
 */
const getColumnsTemplate = () => {
	return [
		[ 'cps/carousel-item' ],
	];
};

/**
 * The list of allowed blocks in carousel.
 *
 * @constant
 * @type {string[]}
 */
const ALLOWED_BLOCKS = [
	'cps/carousel-item'
];

import icon from './icon';

/**
 * Register block
 */
export default registerBlockType(
	'cps/carousel',
	{
		title: __( 'Carousel', 'cps' ),
		description: __( 'A slider element that contains images, videos or stats.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'gallery', 'cps' ),
			__( 'image', 'cps' ),
			__( 'video', 'cps' ),
		],
		attributes: {
			columnCount: {
				type: 'number',
				default: 1,
			},
			/*
			 * In stead of using a state, we'll add an attribute that tells us
			 * whether the editor should render a preview or not.
			 */
			adminIsPreview: {
				type: 'boolean',
				default: false,
			}
		},
		supports: {
			html: false,
			customClassName: false
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					columnCount,
					adminIsPreview,
				},
				setAttributes
			} = props;

			const colCountMin = 1;
			const colCountMax = 3;

			/**
			 * Set number of columns in each slide of the carousel.
			 *
			 * @param {int} count    Interval time.
			 * @param {int} colCountMin Minimum columns.
			 * @param {int} colCountMax Maximum columns.
			 */
			const setColumnCount = ( count, colCountMin, colCountMax ) => {
				count = Math.min( Math.max( parseInt( count, 10 ), colCountMin ), colCountMax );

				setAttributes( { columnCount: count } );
			};

			/**
			 * Render appender.
			 *
			 * @return {mixed} Appender or false.
			 */
			const appender = () => {

				if ( ! adminIsPreview ) {
					return (
						() => <InnerBlocks.ButtonBlockAppender />
					);
				}

				return false;
			};

			const className = classnames( { 'is-selected': ! adminIsPreview } );

			return (
				<Fragment>
					<BlockControls>
						<div className="components-toolbar">
							<Button
								icon='visibility'
								aria-label={ __( 'Toggle Preview', 'cps' ) }
								aria-pressed={ adminIsPreview }
								onClick={ ( event ) => {
									event.stopPropagation();
									setAttributes( { adminIsPreview: ! adminIsPreview } );
									event.target.blur();
								} }
								className={ classnames( 'components-button components-toolbar-button has-icon', {
									'is-pressed': adminIsPreview,
								} ) }
							/>
						</div>
					</BlockControls>
					<InspectorControls>
						<PanelBody
							title={__(
								'Carousel Settings',
								'cps'
							)}
						>
							<PanelRow>
								<div>
									<ToggleControl
										label={ __( 'Show Preview', 'cps' ) }
										checked={ adminIsPreview }
										onChange={ () => setAttributes( { adminIsPreview: ! adminIsPreview } ) }
									/>
									{ ! adminIsPreview && // Slider preview does not update (yet), so let's hide this.
										<RangeControl
											label={__(
												'Columns',
												'cps'
											)}
											value={ columnCount }
											onChange={( value ) => setColumnCount( value, colCountMin, colCountMax )}
											min={ colCountMin }
											max={ colCountMax }
										/>
									}
								</div>
							</PanelRow>
						</PanelBody>
					</InspectorControls>
					{ adminIsPreview ? (
						<SlidesPreview
							clientId = { props.clientId }
							columnCount = { columnCount }
						/>
					) : (
						<div
							className={ `cps-edit-block-carousel cps-edit-block-carousel-columns-${columnCount} ${className}`}
						>
							<InnerBlocks
								allowedBlocks={ ALLOWED_BLOCKS }
								template={ getColumnsTemplate() }
								renderAppender={ appender() }
							/>
						</div>
					) }
				</Fragment>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-carousel.php
		 */
		save: () => {
			return (
				<InnerBlocks.Content />
			);
		},
	},
);
