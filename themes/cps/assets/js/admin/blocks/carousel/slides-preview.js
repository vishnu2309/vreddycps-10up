/* eslint require-jsdoc: 0 */
/**
 * WordPress dependencies
 */
const { select } = wp.data;
const { BlockPreview } = wp.blockEditor;
const {
	Component,
	createRef
} = wp.element;

import { tns } from 'tiny-slider/src/tiny-slider';

/**
 * Renders a preview of all slides in the carousel.
 *
 * @param {obj} props Block properties.
 */
class SlidesPreview extends Component {

	constructor( props ) {
		super( props );

		this.node = createRef();
		this.getSlideBlocks = this.getSlideBlocks.bind( this );
		this.slider = null;
	}

	getSlideBlocks() {

		const slideBlocks = [];
		const parentBlock = select( 'core/block-editor' ).getBlocksByClientId( this.props.clientId );

		parentBlock[0].innerBlocks.map( ( block ) => {
			slideBlocks.push( block.innerBlocks[0] );
		} );

		return slideBlocks;
	}

	componentDidMount() {

		const items = this.props.columnCount || 1;

		this.slider = tns( {
			container: this.node.current,
			controlsPosition: 'bottom',
			items: 1,
			loop: false,
			slideBy: 1,
			nav: false,
			arrowKeys: true,
			preventScrollOnTouch: true,
			controlsText: [
				'<svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13 25"><polygon fill="currentColor" fill-rule="evenodd" points="205 433 222 433 222 450" transform="scale(-1 1) rotate(45 618.349 -45.806)"/></svg>',
				'<svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13 25"><polygon fill="currentColor" fill-rule="evenodd" points="1223 433 1240 433 1240 450" transform="rotate(45 1133.849 -1258.948)"/></svg>'
			],
			responsive: {
				768: {
					items: 2,
					slideBy: 2,
				},
				1024: {
					items: items,
					slideBy: items,
				}
			},
		} );
	}

	componentDidUnmount() {
		this.slider.destroy();
	}

	render() {

		const slides = this.getSlideBlocks();

		return(
			<div
				className="block-carousel"
				dataItems={ this.props.columnCount }
			>
				<div
					className="block-carousel__slider"
					ref={ this.node }
				>
					{
						slides.map( ( block ) => {
							return(
								<div class="block-carousel-item">
									<BlockPreview
										blocks={ block }
										__experimentalLive={ true }
										live={ true }
									/>
								</div>
							);
						} )
					}
				</div>
			</div>
		);
	}
}

export default SlidesPreview;
