/**
 * WordPress dependencies
 */
const { Path, SVG } = wp.components;

export default (
	<SVG viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><Path d="M18 6h4v11h-4zM7 19h10V4H7v15zM9 6h6v11H9V6zM2 6h4v11H2z" fill="#000" fill-rule="nonzero"/></SVG>
);
