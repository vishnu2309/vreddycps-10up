
/**
 * Icon Columns block
 */
const { InnerBlocks, InspectorControls, RichText } = wp.blockEditor;
const { createBlock, registerBlockType }           = wp.blocks;
const { Button, ButtonGroup, PanelBody, PanelRow } = wp.components;
const { Fragment }                                 = wp.element;
const { __ }                                       = wp.i18n;

import { dropRight, times } from 'lodash';

import icon from './icon';

/**
 * Returns the template for the block.
 *
 * @return {Object[]} Block template.
 */
const getColumnsTemplate = () => {
	return [
		[ 'cps/icon-column' ],
		[ 'cps/icon-column' ],
		[ 'cps/icon-column' ]
	];
};

/**
 * Update Icon columns based on the column selection.
 *
 * @param previousColumns Previous selected columns number.
 * @param newColumns      Newly selected columns number.
 * @param clientId        Current block's unique id.
 */
const updateIconColumns = ( previousColumns, newColumns, clientId ) => {

	let innerBlocks      = wp.data.select( 'core/block-editor' ).getBlocks( clientId );
	const isAddingColumn = newColumns > previousColumns;

	if ( isAddingColumn ) {

		// Append new block at the last position.
		innerBlocks = [
			...innerBlocks,
			...times( newColumns - previousColumns, () => {
				return createBlock( 'cps/icon-column' );
			} ),
		];

	} else {

		// Remove the block from the right side.
		innerBlocks = dropRight(
			innerBlocks,
			previousColumns - newColumns
		);
	}

	wp.data.dispatch( 'core/block-editor' ).replaceInnerBlocks( clientId, innerBlocks );
};

/**
 * Register block
 */
export default registerBlockType(
	'cps/icon-columns',
	{
		title: __( 'Icon Columns', 'cps' ),
		description: __( 'One or more columns with an icon image.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'cards', 'cps' ),
			__( 'list', 'cps' )
		],
		attributes: {
			columns: {
				type: 'number',
				default: 3
			},
			copy: {
				type: 'string'
			},
			headline: {
				type: 'string'
			}
		},
		supports: {
			html: false,
			customClassName: false
		},

		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					columns,
					copy,
					headline
				},
				clientId,
				hasInnerBlocks,
				setAttributes
			} = props;

			return (
				<>
					<InspectorControls>
						<PanelBody
							title={ __( 'Number of Columns', 'cps' ) }
							initialOpen={ true }
						>
							<PanelRow>
								<ButtonGroup>
									{
										[ 1, 2, 3, 4 ].map( ( value ) => {
											return <Button
												key={`setting-key-${value}`}
												value={ value }
												isPrimary={ value === columns }
												isLarge={ true }
												aria-pressed={ value === columns }
												onClick={ () => {
													setAttributes( { columns: value } );
													updateIconColumns( columns, value, clientId );
												} }
											>
												{ value }
											</Button>;
										} )
									}
								</ButtonGroup>
							</PanelRow>
						</PanelBody>
					</InspectorControls>
					<div className="block-icon-columns">
						<RichText
							allowedFormats={ [] }
							format="string"
							onChange={ headline => setAttributes( { headline } ) }
							keepPlaceholderOnFocus="true"
							placeholder={ __( 'Add headline', 'cps' ) }
							tagName="h2"
							value={ headline }
							className="icon-columns-title"
						/>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Add text', 'cps' ) }
							tagName="p"
							value={ copy }
						/>
						<div className={ `grid grid--icons grid--${columns}` }>
							{ hasInnerBlocks &&
								<InnerBlocks
									allowedBlocks={ ['cps/icon-column'] }
									renderAppender={ false }
									orientation='horizontal'
								/>
							}
							{ ! hasInnerBlocks &&
								<InnerBlocks
									allowedBlocks={ ['cps/icon-column'] }
									template={ getColumnsTemplate() }
									renderAppender={ false }
									orientation='horizontal'
								/>
							}
						</div>
					</div>
				</>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * Output the block's inner blocks so we can parse them in PHP.
		 */
		save: () => {
			return (
				<Fragment>
					<InnerBlocks.Content />
				</Fragment>
			);
		}
	}
);
