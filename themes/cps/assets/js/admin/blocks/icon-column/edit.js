/* eslint require-jsdoc: 0 */

/**
 * WordPress dependencies
 */
const { RichText, InspectorControls } = wp.blockEditor;
const { SelectControl, PanelBody }    = wp.components;
const { Component }                   = wp.element;
const { __ }                          = wp.i18n;

import icons from './custom-icons';

const customIcons = [
	{
		label: __( 'None', 'cps' ),
		value: '',
	},
	{
		label: __( 'Calendar', 'cps' ),
		value: 'calendar',
	},
	{
		label: __( 'Clock', 'cps' ),
		value: 'clock',
	},
	{
		label: __( 'Enrollment', 'cps' ),
		value: 'enrollment',
	},
	{
		label: __( 'Location', 'cps' ),
		value: 'location',
	},
	{
		label: __( 'Target', 'cps' ),
		value: 'target',
	},
	{
		label: __( 'Transfer', 'cps' ),
		value: 'transfer',
	}
];


export default class IconColumnEdit extends Component {

	/**
	 * Component constructor.
	 *
	 * @return {void}
	 */
	constructor() {
		super( ...arguments );
	}

	/**
	 * Render component.
	 */
	render() {

		const {
			attributes: {
				copy,
				icon,
				title
			},
			setAttributes
		} = this.props;

		return (
			<div className="grid-item grid-item--icon">
				<InspectorControls>
					<PanelBody
						title={ __( 'Column Icon', 'cps' ) }
						initialOpen={ true }
					>
						<SelectControl
							label={ __( 'Select Icon', 'cps' ) }
							value={ icon }
							onChange={ ( icon ) => {
								setAttributes( { icon } );
							} }
							options={ customIcons }
						/>
					</PanelBody>
				</InspectorControls>
				{ icon &&
					<div className="grid-item__icon">
						{ icons[icon] }
					</div>
				}
				<div className="grid-item__content">
					<RichText
						allowedFormats={ [] }
						format="string"
						onChange={ title => setAttributes( { title } ) }
						placeholder={ __( 'Add title', 'cps' ) }
						tagName="h3"
						value={ title }
					/>
					<RichText
						allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
						format="string"
						onChange={ copy => setAttributes( { copy } ) }
						placeholder={ __( 'Add text', 'cps' ) }
						tagName="p"
						className="is-style-small"
						value={ copy }
					/>
				</div>
			</div>
		);
	}
}
