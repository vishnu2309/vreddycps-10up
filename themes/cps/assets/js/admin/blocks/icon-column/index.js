/**
 * A single faculty-column in the Faculty Grid block.
 */

/**
 * WordPress dependencies
 */
const { __ }                = wp.i18n;
const { registerBlockType } = wp.blocks;

import icon from './icon';
import edit from './edit';

export default registerBlockType( 'cps/icon-column', {
	title: __( 'Icon Column', 'cps' ),
	parent: [ 'cps/icon-column' ],
	icon: icon,
	description: __( 'A single icon block column.', 'cps' ),
	category: 'cps-blocks',
	supports: {
		html: false,
		reusable: false,
		className: false,
		customClassName: false
	},
	attributes: {
		copy: {
			type: 'string',
			default: ''
		},
		icon: {
			type: 'string',
			default: ''
		},
		title: {
			type: 'string',
			default: ''
		}
	},

	edit,

	save: () => {
		return null;
	}
} );
