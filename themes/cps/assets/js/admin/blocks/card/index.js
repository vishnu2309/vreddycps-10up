/**
 * Stat Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText, URLInput } = wp.blockEditor;
const { Fragment } = wp.element;

import isEmpty from 'lodash/isEmpty';
import icon from './icon';
import ImageSelect from '../../components/image-select-inspector';

/**
 * Register block
 */
export default registerBlockType(
	'cps/card',
	{
		title: __( 'Card', 'cps' ),
		description: __( 'A column containing an image, text and link. Intended to be used inside of a Columns block.', 'cps' ),
		icon: icon,
		category: 'widgets',
		parent: [ 'core/column' ],
		keywords: [
			__( 'column', 'cps' ),
		],
		attributes: {
			copy: {
				type: 'string'
			},
			ctaLabelOne: {
				type: 'string'
			},
			ctaLinkOne: {
				type: 'string',
			},
			headline: {
				type: 'string'
			},
			image: {
				type: 'string',
				default: '{}',
			},
		},
		supports: {
			customClassName: false,
			html: false,
			defaultStylePicker: false
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					copy,
					ctaLabelOne,
					ctaLinkOne,
					headline,
					image
				},
				isSelected,
				setAttributes,
			} = props;

			/**
			 * Return placeholder image or selected image.
			 *
			 * @return {string}
			 */
			const imageSrc = () => {

				const imageObj = JSON.parse( image );

				if ( isEmpty( imageObj ) ) {
					return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPc/erFfwAIgwOOjoLB7gAAAABJRU5ErkJggg==';
				} else {
					return imageObj.url;
				}
			};

			/**
			 * Callback for the ImageSelect component.
			 *
			 * @return {string}
			 */
			const onChangeImage = ( attr ) => {
				/*
				 * Note: The ImageSelect component returns an object
				 *       containing alt, id and url
				 */
				const value = ( isEmpty( attr ) ? null : attr );
				setAttributes( { image: JSON.stringify( value ) } );
			};

			/**
			 * Maybe render card link preview.
			 *
			 * @return {[type]} [description]
			 */
			const renderLink = () => {

				if ( ! isEmpty( ctaLabelOne ) && ! isEmpty( ctaLinkOne ) ) {
					return (
						<Fragment>
							<p><a href={ ctaLinkOne }>{ ctaLabelOne }</a></p>
						</Fragment>
					);
				}
			};

			return (
				<Fragment>
					<div className="block-card">
						{ isSelected ? (
							<ImageSelect
								image={ JSON.parse( image ) }
								isSelected={ isSelected }
								labels={ {
									title: __( 'Image' ),
									instructions: __( 'Upload an image file or pick one from your media library.', 'cps' ),
								} }
								onChange={ ( value ) => {
									onChangeImage( value );
								} }
							/>
						) : (
							<div className="block-card__image">
								<img src={ imageSrc() } />
							</div>
						) }
						<div className="block-card__content">
							<RichText
								allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
								format="string"
								onChange={ headline => setAttributes( { headline } ) }
								keepPlaceholderOnFocus="true"
								multiline="false"
								placeholder={ __( 'Write headline…', 'cps' ) }
								tagName="h3"
								value={ headline }
								className="cps-block-input-title"
							/>
							<RichText
								allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ copy => setAttributes( { copy } ) }
								placeholder={ __( 'Write copy…', 'cps' ) }
								tagName="div"
								value={ copy }
							/>
							{ isSelected ? (
								<Fragment>
									<p>
										<a href="#" className="block-card-cta-input">
											<RichText
												allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
												format="string"
												onChange={ ctaLabelOne => setAttributes( { ctaLabelOne } ) }
												placeholder={ __( 'Add call to action…', 'cps' ) }
												tagName="span"
												value={ ctaLabelOne }
											/>
										</a>
									</p>
									<URLInput
										autoFocus={ false }
										value={ ctaLinkOne }
										onChange={ ctaLinkOne => setAttributes( { ctaLinkOne } ) }
									/>
								</Fragment>
							) : (
								renderLink()
							) }
						</div>
					</div>
				</Fragment>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
