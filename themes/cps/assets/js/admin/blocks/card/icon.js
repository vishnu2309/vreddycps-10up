/**
 * WordPress dependencies
 */
const { Path, SVG } = wp.components;

export default (
	<SVG viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
		<Path d="M6 10.783h12V6H6v4.783zm0 3.39h12V12.74H6v1.435zM6 17h12v-1.435H6V17z" fill="#000" fill-rule="nonzero"/>
	</SVG>
);
