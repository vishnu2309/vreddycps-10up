/**
 * Section Heading Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.blockEditor;

import icon from './icon';

/**
 * Register block
 */
export default registerBlockType(
	'cps/section-heading',
	{
		title: __( 'Section Heading', 'cps' ),
		description: __( 'Header element with title and description.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'header', 'cps' ),
			__( 'title', 'cps' ),
		],
		attributes: {
			headline: {
				type: 'string'
			},
			copy: {
				type: 'string'
			},
		},
		example: {
			attributes: {
				headline: 'Catch up with Alumni',
				copy: 'Here\'s what our inspiring alumni are up to right now.'
			},
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					headline,
					copy,
				},
				className,
				setAttributes,
			} = props;

			// @todo Add proper class names to mirror front end styles.
			return (
				<div className={ `cps-block-section-heading ${className}` }>
					<RichText
						allowedFormats={ [] }
						format="string"
						onChange={ headline => setAttributes( { headline } ) }
						keepPlaceholderOnFocus="true"
						multiline="false"
						placeholder={ __( 'Add headline', 'cps' ) }
						tagName="h2"
						value={ headline }
						className="cps-block-input-title"
					/>
					<div>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							keepPlaceholderOnFocus="true"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Add text', 'cps' ) }
							tagName="div"
							value={ copy }
						/>
					</div>
				</div>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
