/* eslint require-jsdoc: 0 */

const { __ } = wp.i18n;
const { Component } = wp.element;

const {
	InnerBlocks,
	RichText
} = wp.blockEditor;

/**
 * Accorodion block edit function.
 */
class Edit extends Component {

	constructor() {
		super( ...arguments );

		this.state = {
			isSelectedDeep: false,
		};

		wp.data.subscribe( () => {

			this.setState( {
				isSelectedDeep: this.props.isSelected || this.isSelectedDeep( this.props )
			} );
		} );
	}

	/**
	 * Is this block or any of its children selected?
	 *
	 * @param {obj} props Block properties.
	 * @return {bool}
	 */
	isSelectedDeep( props ) {

		const select = wp.data.select( 'core/block-editor' );
		const selected = select.getBlockSelectionStart();

		if ( !selected ) {
			return false;
		}

		const inner = select.getBlock( props.clientId ).innerBlocks;

		for ( let i = 0; i < inner.length; i++ ) {
			if ( inner[i].clientId === selected || inner[i].innerBlocks.length && this.isSelectedDeep( inner[i] ) ) {
				return true;
			}
		}

		return false;
	}

	renderAppender() {

		if ( this.state.isSelectedDeep || this.props.isSelected ) {
			return(
				() => <InnerBlocks.ButtonBlockAppender />
			);
		}

		return false;
	}

	render() {

		const {
			attributes: {
				headline,
				copy
			},
			setAttributes
		} = this.props;

		return (
			<div className="block-accordion">
				<RichText
					allowedFormats={ [] }
					format="string"
					onChange={ headline => setAttributes( { headline } ) }
					keepPlaceholderOnFocus="true"
					placeholder={ __( 'Add headline', 'cps' ) }
					tagName="h2"
					value={ headline }
					className="block-accordion__title"
				/>
				<div className='block-accordion-edit-description'>
					<RichText
						allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
						format="string"
						keepPlaceholderOnFocus="true"
						onChange={ copy => setAttributes( { copy } ) }
						placeholder={ __( 'Add text', 'cps' ) }
						tagName="div"
						value={ copy }
					/>
				</div>
				<div className="block-accordion__items">
					<InnerBlocks
						allowedBlocks={ [ 'cps/accordion-item' ] }
						renderAppender={ this.renderAppender() }
					/>
				</div>
			</div>
		);
	}
}

export default Edit;
