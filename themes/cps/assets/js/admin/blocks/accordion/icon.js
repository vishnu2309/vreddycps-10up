/**
 * WordPress dependencies
 */
const { Path, SVG } = wp.components;

export default (
	<SVG xmlns="http://www.w3.org/2000/svg" viewBox="-2 -2 24 24">
		<Path d="M17 7V5H3v2h14zm0 4V9H3v2h14zm0 4v-2H3v2h14z" />
	</SVG>
);
