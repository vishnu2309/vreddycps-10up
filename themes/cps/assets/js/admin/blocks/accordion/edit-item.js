/* eslint require-jsdoc: 0 */

const { __ } = wp.i18n;
const { Component } = wp.element;
const { Fragment } = wp.element;

const {
	InnerBlocks,
	RichText
} = wp.blockEditor;

/**
 * The list of allowed blocks in accordion-item.
 *
 * @constant
 * @type {string[]}
 */
const ALLOWED_BLOCKS = [
	'core/columns',
	'core/file',
	'core/heading',
	'core/image',
	'core/list',
	'core/paragraph',
	'core/table'
];

/**
 * Accordion Item block edit function.
 */
class EditItem extends Component {

	constructor() {
		super( ...arguments );

		this.state = {
			isSelectedDeep: false,
		};

		wp.data.subscribe( () => {

			this.setState( {
				isSelectedDeep: this.props.isSelected || this.isSelectedDeep( this.props )
			} );
		} );
	}

	/**
	 * Is this block or any of its children selected?
	 *
	 * @param {obj} props Block properties.
	 * @return {bool}
	 */
	isSelectedDeep( props ) {

		const select = wp.data.select( 'core/block-editor' );
		const selected = select.getBlockSelectionStart();

		if ( !selected ) {
			return false;
		}

		const inner = select.getBlock( props.clientId ).innerBlocks;

		for ( let i = 0; i < inner.length; i++ ) {
			if ( inner[i].clientId === selected || inner[i].innerBlocks.length && this.isSelectedDeep( inner[i] ) ) {
				return true;
			}
		}

		return false;
	}

	render() {

		const {
			attributes: {
				title
			},
			setAttributes,
			isSelected,
		} = this.props;

		const isActiveClass = ( isSelected || this.state.isSelectedDeep ) ? ' is-active' : '';
		const toggleContent = ( isSelected || this.state.isSelectedDeep ) ? true : false;

		return (
			<Fragment>
				<RichText
					tagName='h3'
					multiline={false}
					className={ `accordion-header is-style-h3 ${isActiveClass}` }
					placeholder={ __( 'Accordion Item Title', 'cps' ) }
					value={ title }
					onChange={ title => setAttributes( { title } ) }
				/>
				{ toggleContent &&
					<div className="accordion-content">
						<InnerBlocks
							allowedBlocks={ ALLOWED_BLOCKS }
						/>
					</div>
				}
			</Fragment>
		);
	}
}

export default EditItem;
