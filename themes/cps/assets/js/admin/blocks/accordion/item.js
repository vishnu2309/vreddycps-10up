/**
 * A single accordion-item for the Accordion block.
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType} = wp.blocks;

const {
	InnerBlocks,
} = wp.blockEditor;

import icon from './icon-item';
import edit from './edit-item';

export default registerBlockType(
	'cps/accordion-item',
	{
		title: __( 'Accordion Item', 'cps' ),
		parent: [ 'cps/accordion' ],
		icon: icon,
		description: __( 'A single accordion-item block to build accordion list.', 'cps' ),
		category: 'cps-blocks',
		supports: {
			html: false,
			reusable: false,
			className: false,
			customClassName: false,
		},
		attributes: {
			title: {
				type: 'string'
			}
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit,
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * Output the block's inner blocks so we can parse them in PHP.
		 */
		save: () => {
			return (
				<InnerBlocks.Content />
			);
		}
	}
);
