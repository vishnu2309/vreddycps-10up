/**
 * Accordion Block
 */

const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;
const { InnerBlocks } = wp.blockEditor;

import icon from './icon';
import edit from './edit';
/**
 * Register block
 */
export default registerBlockType(
	'cps/accordion',
	{
		title: __( 'Accordion', 'cps' ),
		description: __( 'A group of items that can be expanded.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'faq', 'cps' ),
			__( 'list', 'cps' )
		],
		attributes: {
			copy: {
				type: 'string'
			},
			headline: {
				type: 'string'
			}
		},
		supports: {
			html: false,
			className: false,
			customClassName: false
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit,
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * Output the block's inner blocks so we can parse them in PHP.
		 */
		save: () => {
			return (
				<InnerBlocks.Content />
			);
		}
	}
);
