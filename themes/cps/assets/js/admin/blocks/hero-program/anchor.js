/**
 * Anchor Block
 *
 * Intended to be used on the Program detail page. The anchor block allows adding a named
 * anchor which automatically adds a link in the program's left menu.
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InspectorControls } = wp.blockEditor;
const { Fragment } = wp.element;

const {
	PanelBody,
	PanelRow,
	TextControl,
} = wp.components;

import icon from './icon-anchor';

/**
 * Register block
 */
export default registerBlockType(
	'cps/anchor',
	{
		title: __( 'Anchor', 'cps' ),
		description: __( 'Adds an internal link to the page.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		parent: [ 'cps/wrapper' ],
		keywords: [
			__( 'link', 'cps' ),
		],
		attributes: {
			titleAttr: {
				type: 'string',
				selector: 'div',
				attribute: 'data-nav-title',
			},
			idAttr: {
				type: 'string',
				selector: 'div',
				attribute: 'id',
			}
		},
		supports: {
			className: false,
			html: false,
			customClassName: false,
			reusable: false,
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {

			/**
			 * Create an id attribute from a string.
			 *
			 * Note: does not validate, eg. the id can start with a number.
			 *
			 * @param {string} value String value.
			 * @return {string} Formatted string
			 */
			const createPoorMansSlug = ( value ) => {

				return value.replace( /\W/g, '-' ).toLowerCase();
			};

			return (
				<Fragment>
					<InspectorControls>
						<PanelBody>
							<PanelRow>
								<TextControl
									label={ __( 'Title', 'cps' ) }
									value={ props.titleAttr }
									onChange={ ( value ) => {
										props.setAttributes( { titleAttr: value } );
										props.setAttributes( { idAttr: createPoorMansSlug( value ) } );
									}  }
								/>
							</PanelRow>
						</PanelBody>
					</InspectorControls>
					<div className="cps-anchor">
						<span />
						{ icon }
						<span />
					</div>
				</Fragment>
			);
		},
		save: props => {

			return (
				<div id={ props.attributes.idAttr } data-nav-title={ props.attributes.titleAttr } />
			);
		}
	},
);
