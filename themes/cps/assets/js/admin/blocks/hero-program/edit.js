/* global cpsProgramVars */
/* eslint require-jsdoc: 0 */

const { __ } = wp.i18n;
const { compose } = wp.compose;
const { withDispatch, withSelect } = wp.data;

const {
	Component,
	Fragment
} = wp.element;

const {
	Button,
	ButtonGroup,
	PanelBody,
	PanelRow,
	RangeControl,
	SelectControl,
} = wp.components;

const {
	InspectorControls,
	RichText,
} = wp.blockEditor;

import ImageSelectInspector from '../../components/image-select-inspector';
import isEmpty from 'lodash/isEmpty';

const backgroundTypeOptions = [
	{
		label: __( 'PIM', 'cps' ),
		value: 'pim'
	},
	{
		label: __( 'Image', 'cps' ),
		value: 'image'
	},
	{
		label: __( 'Black', 'cps' ),
		value: 'color'
	},
];

const navigationColorOptions = [
	{
		label: __( 'Black', 'cps' ),
		value: 'black'
	},
	{
		label: __( 'White', 'cps' ),
		value: 'white'
	},
];

const overlayColorOptions = [
	{
		label: __( 'None', 'cps' ),
		value: 'none'
	},
	{
		label: __( 'Black', 'cps' ),
		value: 'black'
	},
	{
		label: __( 'White', 'cps' ),
		value: 'white'
	},
];

const metafields = {
	backgroundType  : 'cps_program_bg_type',
	backgroundColor : 'cps_program_bg_color',
	badge           : 'cps_program_initials',
	navigationColor : 'cps_program_nav_color',
	overlayColor    : 'cps_program_overlay_color',
	overlayOpacity  : 'cps_program_overlay_opacity',
	copy            : 'cps_program_description_short',
	textColor       : 'cps_program_text_color',
	image           : 'cps_program_image',
};

const pixelImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPc/erFfwAIgwOOjoLB7gAAAABJRU5ErkJggg==';

/**
 * Hero block edit function.
 */
class HeroEdit extends Component {

	/**
	 * Component constructor.
	 *
	 * @return {void}
	 */
	constructor() {
		super( ...arguments );

		// https://github.com/WordPress/gutenberg/issues/11681
		wp.data.dispatch( 'core/block-editor' ).setTemplateValidity( true );

		const pimImage = 'undefined' === typeof cpsProgramVars.pimImage || '' === cpsProgramVars.pimImage ? pixelImage : cpsProgramVars.pimImage;
		const badge = 'undefined' === typeof cpsProgramVars.badge || '' === cpsProgramVars.badge ? '' : cpsProgramVars.badge;

		this.getMetaFieldValue = this.getMetaFieldValue.bind( this );
		this.onUpdateValue = this.onUpdateValue.bind( this );
		this.onChangeImage = this.onChangeImage.bind( this );

		this.state = {
			'backgroundType': 'pim',
			'backgroundColor': 'black',
			'badge': badge,
			'navigationColor': 'white',
			'overlayColor': 'black',
			'overlayOpacity': '40',
			'copy': '',
			'textColor': 'white',
			'headline': 'Need to fetch title from somewhere',
			'pimImage': pimImage,
			'image': '{}',
		};
	}

	componentDidMount() {

		for ( const [ key, value ] of Object.entries( metafields ) ) {

			const metaValue = this.getMetaFieldValue( value );

			if ( ! isEmpty( metaValue ) ) {
				this.setState( { [ key ]: metaValue } );
			}
		}
	}

	getMetaFieldValue( key ) {

		if ( 'undefined' === this.props.meta[ key ] ) {
			return '';
		} else {
			return this.props.meta[ key ];
		}

	}

	onUpdateValue( key, value ) {

		this.setState( { [key]: value } );
		this.props.setMetaFieldValue( metafields[ key ], value.toString() );

		// A bit of voodoo-ing.
		if ( 'backgroundType' === key && 'color' === value ) {
			this.setState( { textColor: 'white' } );
			this.setState( { navigationColor: 'white' } );
			this.props.setMetaFieldValue( metafields[ 'textColor' ], 'white' );
			this.props.setMetaFieldValue( metafields[ 'navigationColor' ], 'white' );
		}
	}

	onChangeImage( attr ) {
		/*
		 * Note: The ImageSelect component returns an object
		 *       containing alt, id and url
		 */
		const value = ( isEmpty( attr ) ? null : attr );
		this.onUpdateValue( 'image', JSON.stringify( value ) );
	}

	/**
	 * Render component.
	 */
	render() {

		const {
			isSelected,
		} = this.props;

		/**
		 * Return placeholder image or selected image.
		 *
		 * @return {string}
		 */
		const imageSrc = () => {

			const imageObj = JSON.parse( this.state.image );

			if ( isEmpty( imageObj ) ) {
				return pixelImage;
			} else {
				return imageObj.url;
			}
		};

		const renderBlockContent = () => {

			const title = 'undefined' === typeof cpsProgramVars.title || '' === cpsProgramVars.title ? 'No Title' : cpsProgramVars.title;

			const backgroundTypeForClass = 'image' === this.state.backgroundType || 'pim' === this.state.backgroundType ? 'image' : this.state.backgroundType;

			return (
				<div className={ `block-hero block-hero--${backgroundTypeForClass} block-hero--overlay-${this.state.overlayColor} has-${'color' === this.state.backgroundType ? this.state.textColor : this.state.navigationColor}-color has-${this.state.backgroundColor}-background-color block-hero--fixed` }>
					{ 'image' === this.state.backgroundType &&
						<div className="block-hero__image">
							<img src={ imageSrc() } />
						</div>
					}
					{ 'pim' === this.state.backgroundType &&
						<div className="block-hero__image">
							<img src={ this.state.pimImage } />
						</div>
					}
					{ 'image' === backgroundTypeForClass &&
						<div className="block-hero__overlay" style={{opacity: `${this.state.overlayOpacity/100}`}}>
						</div>
					}
					<div className="block-hero__content  block-hero__content--badge">
						<div className="program-badge program-badge--hero">
							<RichText
								allowedFormats={ [] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ value => this.onUpdateValue( 'badge', value ) }
								placeholder={ __( 'Add badge…', 'cps' ) }
								tagName="span"
								value={ this.state.badge }
							/>
						</div>
						<div className="block-hero__content-text">
							<h1>{ title }</h1>
							<RichText
								allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ value => this.onUpdateValue( 'copy', value ) }
								placeholder={ __( '(Optional) Write short description…', 'cps' ) }
								tagName="div"
								value={ this.state.copy }
							/>
						</div>
					</div>
				</div>
			);
		};

		return (
			<Fragment>
				<InspectorControls>
					<PanelBody
						title={ __( 'Block Settings', 'cps' ) }
						initialOpen={ true }
					>
						<PanelRow>
							<label>{ __( 'Type', 'cps' ) }</label>
							<ButtonGroup>
								{
									backgroundTypeOptions.map( ( { label, value } ) => {
										return <Button
											value={ value }
											isPrimary={ value === this.state.backgroundType }
											isLarge={ true }
											aria-pressed={ value === this.state.backgroundType }
											onClick={ () => {
												this.onUpdateValue( 'backgroundType', value );
											} }
										>
											{ label }
										</Button>;
									} )
								}
							</ButtonGroup>
						</PanelRow>
						{ ( 'image' === this.state.backgroundType ) &&
							<PanelRow>
								<div className="cps-image-upload">
									<ImageSelectInspector
										image={ JSON.parse( this.state.image ) }
										isSelected={ isSelected }
										labels={ {
											title: __( 'Image' ),
											instructions: __( 'Upload an image file or pick one from your media library.', 'cps' ),
										} }
										onChange={ ( value ) => {
											this.onChangeImage( value );
										} }
									/>
								</div>
							</PanelRow>
						}
					</PanelBody>
					<PanelBody
						title={ __( 'Navigation & Overlay', 'cps' ) }
						initialOpen={ true }
					>
						<PanelRow>
							<div>
								<SelectControl
									label={ __( 'Navigation Color', 'cps' ) }
									value={ this.state.navigationColor }
									onChange={ (  value ) => {
										this.onUpdateValue( 'navigationColor', value );
									} }
									options={ navigationColorOptions }
								/>
								{ ( 'image' === this.state.backgroundType || 'pim' === this.state.backgroundType ) &&
								<div>
									<SelectControl
										label={ __( 'Overlay Color', 'cps' ) }
										value={ this.state.overlayColor }
										onChange={ ( value ) => {
											this.onUpdateValue( 'overlayColor', value );
										} }
										options={ overlayColorOptions }
									/>
									{ 'none' !== this.state.overlayColor &&
										<RangeControl
											label={ __( 'Overlay Opacity', 'cps' ) }
											value={ parseInt( this.state.overlayOpacity, 10 ) }
											onChange={ value => this.onUpdateValue( 'overlayOpacity', value ) }
											min={ 0 }
											max={ 100 }
										/>
									}
								</div>
								}
							</div>
						</PanelRow>
					</PanelBody>
				</InspectorControls>
				{
					renderBlockContent()
				}
			</Fragment>
		);
	}
}

export default compose( [

	withDispatch( ( dispatch, props ) => { // eslint-disable-line no-unused-vars

		const { editPost } = dispatch( 'core/editor' );

		return {
			setMetaFieldValue: function ( key, value ) {

				editPost( { meta: { [key]: value } } );
			}
		};
	} ),
	withSelect( ( select, props ) => { // eslint-disable-line no-unused-vars

		const { getEditedPostAttribute } = select( 'core/editor' );

		return {
			meta: getEditedPostAttribute( 'meta' )
		};
	} )

] )( HeroEdit );
