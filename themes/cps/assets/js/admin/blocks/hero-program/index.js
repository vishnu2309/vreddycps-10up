/**
 * Hero Block
 *
 * Intended to be used on the Program detail page.
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import icon from './icon';
import edit from './edit';

/**
 * Register block
 */
export default registerBlockType(
	'cps/hero-program',
	{
		title: __( 'Hero', 'cps' ),
		description: __( 'Page header with background color or image.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'header', 'cps' ),
			__( 'title', 'cps' ),
		],
		supports: {
			html: false,
			className: false,
			customClassName: false,
			inserter: false,
			multiple: false,
			reusable: false,
		},

		/**
		 * Display the block full-width without showing the alignment control.
		 *
		 * @param attributes
		 * @returns {object}
		 */
		getEditWrapperProps( attributes ) {
			attributes['data-align'] = 'full';
			return attributes;
		},
		edit,
		save: () => {
			return null;
		},
	},
);
