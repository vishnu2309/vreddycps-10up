/**
 * Example Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { TextControl } = wp.components;

/**
 * Register block
 */
export default registerBlockType(
	'cps/example',
	{
		title: __( 'Example block', 'cps' ),
		description: __( 'Example block description', 'cps' ),
		icon: 'smiley',
		category: 'cps-blocks',
		keywords: [
			__( 'example', 'cps' ),
		],
		attributes: {
			customTitle: {
				type: 'string'
			},
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					customTitle
				},
				className,
				setAttributes,
				isSelected
			} = props;

			if ( isSelected ) {
				return (
					<div className={ className }>
						<TextControl
							id="example-block-text-field"
							label={ __( 'Custom Title', 'cps' ) }
							value={ customTitle }
							onChange={ customTitle => setAttributes( { customTitle } ) }
						/>
					</div>
				);
			} else {
				return (
					<h2 class="example-block-title">
						{ customTitle }
					</h2>
				);
			}
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
