/* eslint require-jsdoc: 0 */
/**
 * A content wrapper block intended to be used for the program post type edit screen.
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.blockEditor;
const { Fragment } = wp.element;

import icon from './icon';

/**
 * Register block
 */
export default registerBlockType(
	'cps/wrapper',
	{
		title: __( 'Content', 'cps' ),
		description: '',
		icon: icon,
		category: 'cps-blocks',
		supports: {
			html: false,
			reusable: false,
			customClassName: false,
			inserter: false,
		},
		/**
		 * Display the block full-width without showing the alignment control.
		 *
		 * @param attributes
		 * @returns {object}
		 */
		getEditWrapperProps( attributes ) {
			attributes['data-align'] = 'full';
			return attributes;
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: () => {

			// https://github.com/WordPress/gutenberg/issues/11681
			wp.data.dispatch( 'core/block-editor' ).setTemplateValidity( true );

			return (
				<InnerBlocks
					templateLock={false}
					renderAppender={ () => <InnerBlocks.ButtonBlockAppender />} />
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * Output the block's inner blocks so we can parse them in PHP.
		 */
		save: () => {
			return (
				<Fragment>
					<InnerBlocks.Content />
				</Fragment>
			);
		}
	},
);
