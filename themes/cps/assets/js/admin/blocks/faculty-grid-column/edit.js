/* global cpsGutenbergVars */
/* eslint require-jsdoc: 0 */

/**
 * WordPress dependencies
 */
const { __ }             = wp.i18n;
const { decodeEntities } = wp.htmlEntities;
const { apiFetch }       = wp;
const { Spinner }        = wp.components;
const { Component }      = wp.element;

import classnames from 'classnames';
import { isEmpty, isFinite } from 'lodash';
import PostFinder from '../../components/post-finder';

// Set nonce to be able to authenticate our requests in the rest handler in php.
if ( 'undefined' !== typeof cpsGutenbergVars.nonce ) {
	apiFetch.use( apiFetch.createNonceMiddleware( cpsGutenbergVars.nonce ) );
}

export default class FacultyColumnEdit extends Component {
	constructor() {
		super( ...arguments );

		this.pixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==';

		this.initContent   = this.initContent.bind( this );
		this.onChangePost  = this.onChangePost.bind( this );
		this.fetchPostData = this.fetchPostData.bind( this );

		this.state = {
			ctaText: __( 'See more', 'cps' ),
			image: this.pixel,
			isLoading: false,
			titleText: false,
			profExp: false,
			customProfExp: false
		};
	}

	componentDidMount() {
		this.isStillMounted = true;
		this.initContent();
	}

	componentWillUnmount() {
		this.isStillMounted = false;
	}

	initContent() {

		const {
			attributes: {
				postId
			}
		} = this.props;

		const id = parseInt( postId, 10 );

		if ( isFinite( id ) ) {
			this.fetchPostData( id );
		}
	}

	fetchPostData( id ) {

		this.setState( {
			isLoading: true
		} );

		const path = `/cps/v1/blocks/faculty-grid/post/${ parseInt( id, 10 ) }`;

		return apiFetch( { path } )
			.then( ( response ) => {

				if ( this.isStillMounted && response ) {
					this.setState( {
						titleText: response.titleText,
						profExp: response.profExp,
						customProfExp: response.customProfExp,
						image: response.image,
						isLoading: false
					} );
				}
			} )
			.catch( ( error ) => {
				if ( this.isStillMounted ) {
					this.setState( {
						isLoading: false,
						error: true,
						errorMsg: error.message
					} );
				}
			} );
	}

	/**
	 * On change faculty-member.
	 */
	onChangePost( post ) {

		const { setAttributes } = this.props;

		/*
		 * Note: The PostFinder component returns an object
		 *       containing id, title and url
		 *
		 * Todo: Display error.
		 */
		if ( ! isEmpty( post ) && 'undefined' !== typeof post.id ) {

			setAttributes( { postId: post.id } );

			// Set the titleText while we fetch the rest of the post data.
			this.setState( { titleText: post.title } );

			// Fetch post data.
			this.fetchPostData( post.id );
		}
	}

	render() {

		const {
			isSelected
		} = this.props;

		const className = classnames( {
			'is-loading': this.state.isLoading
		} );

		return (
			<div
				className={ `faculty-card ${ className }` }
			>
				{ this.state.isLoading &&
					<Spinner />
				}
				{ ( isSelected || ! this.state.titleText ) &&
					<PostFinder
						placeholder={ __( 'Search', 'cps' ) }
						autoFocus={ true }
						inputClassName="components-text-control__input"
						onChange={ value => this.onChangePost( value ) }
						postType={ 'cps-person' }
					/>
				}
				{ this.state.titleText &&
					<>
						<div className="faculty-card__image aspect-ratio">
							<img
								className="aspect-ratio__element"
								src={ this.state.image }
							/>
						</div>
						<div className="faculty-card__content">
							<h3 className="post-title">{ decodeEntities( this.state.titleText ) }</h3>
							{ ! isEmpty( this.state.customProfExp ) &&
								<ul className="is-style-small-description">
									{
										this.state.customProfExp.map( ( item, index ) => {
											return(
												<li key={`item-${index}`}>{ item }</li>
											);
										} )
									}
								</ul>
							}	
							{ isEmpty( this.state.customProfExp ) && ! isEmpty( this.state.profExp ) &&
								<ul className="is-style-small-description">
									{
										this.state.profExp.map( ( item, index ) => {
											return(
												<li key={`item-${index}`}>{ item }</li>
											);
										} )
									}
								</ul>
							}
							<p>
								<a href="#" className="post-link">{ decodeEntities( this.state.ctaText ) }</a>
							</p>
						</div>
					</>
				}
			</div>
		);
	}
}
