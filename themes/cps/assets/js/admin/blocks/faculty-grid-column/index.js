/**
 * A single faculty-column in the Faculty Grid block.
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import icon from './icon';
import edit from './edit';

export default registerBlockType( 'cps/faculty-grid-column', {
	title: __( 'Faculty Member', 'cps' ),
	parent: [ 'cps/faculty-grid' ],
	icon: icon,
	description: __( 'A single faculty grid block column for selecting faculty member.', 'cps' ),
	category: 'cps-blocks',
	supports: {
		html: false,
		reusable: false,
		className: false,
		customClassName: false,
	},
	attributes: {
		image: {
			type: 'string',
			default: '[]'
		},
		titleText: {
			type: 'string',
			default: '',
		},
		ctaLink: {
			type: 'string',
			default: '',
		},
		postId: {
			type: 'int',
			default: '',
		},
		profExp: {
			type: 'string',
			default: '',
		},
	},

	edit,

	save: () => {
		return null;
	}
} );
