/* eslint require-jsdoc: 0 */
/**
 * Renders the default contact block toggle.
 *
 * @package CPS
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Spinner, ToggleControl } = wp.components;
const { Component } = wp.element;
const { getCurrentPostId } = wp.data.select( 'core/editor' );

/**
 * Renders a list of checkboxes to select the column post type.
 *
 * @param {obj} props Block properties.
 */
class DefaultContactBlockToggleControl extends Component {

	constructor( props ) {

		super( props );

		this.state = {
			isAPILoaded: false,
			isAPISaving: false,
			isSaving: false,
			isChecked: false,
		};
	}

	componentDidMount() {

		/*
		 * Fetch current settings and set state.
		 */
		wp.api.loadPromise.then( () => {

			this.settings = new wp.api.models.Settings();

			if ( false === this.state.isAPILoaded ) {

				this.settings.fetch().then( response => {

					this.setState( {
						isChecked: parseInt( response.cps_default_contact_block, 10 ) === getCurrentPostId(),
						isAPILoaded: true
					} );
				} );
			}
		} );

		const that        = this; // Looks so oldschool, is there a better way to pass this to the subscribe callback?
		const unsubscribe = wp.data.subscribe( () => {
			/*
			 * Boldly stolen from:
			 * https://wordpress.stackexchange.com/questions/319054/trigger-javascript-on-gutenberg-block-editor-save
			 */
			const select = wp.data.select( 'core/editor' );
			const isSavingPost = select.isSavingPost();
			const isAutosavingPost = select.isAutosavingPost();
			const didPostSaveRequestSucceed = select.didPostSaveRequestSucceed();

			if ( isSavingPost && ! isAutosavingPost && didPostSaveRequestSucceed ) {
				that.saveOption();
				unsubscribe();
			}
		} );
	}

	saveOption() {

		this.setState( { isAPISaving: true } );

		const value = this.state.isChecked ? getCurrentPostId() : '0';
		const model = new wp.api.models.Settings( {
			// eslint-disable-next-line camelcase
			['cps_default_contact_block']: value.toString()
		} );

		model.save().then( () => {
			this.setState( { isAPISaving: false } );
		} );
	}

	render() {
		if ( this.state.isAPILoaded ) {
			return(
				<ToggleControl
					label={ this.state.isChecked ? __( 'Default' ) : __( 'Make Default' ) }
					help={ 'The default contact block is used on all pages that have the \'default\' option selected.' }
					checked={ this.state.isChecked }
					className='cps-default-contact-block-toggle'
					onChange={ () => {
						// Using a somewhat dirty hack to enable the post editor "Save" button.
						wp.data.dispatch( 'core/editor' ).editPost( { meta: { thisIsNotUsed: true } } );
						this.setState( { isChecked: !this.state.isChecked } );
					} }
				/>
			);
		} else {
			return(
				<Spinner />
			);
		}
	}
}

export default DefaultContactBlockToggleControl;
