/**
 * Footer Contact Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	RichText,
	InspectorControls,
	PanelColorSettings,
	withColors,
} = wp.blockEditor;
const {
	PanelBody,
	PanelRow,
} = wp.components;

import icon from './icon';
import DefaultContactBlockToggleControl from './toggle-default';

/**
 * Register block
 */
export default registerBlockType(
	'cps/footer-contact',
	{
		title: __( 'Footer Contact', 'cps' ),
		description: __( 'Footer element with contact information.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'header', 'cps' ),
			__( 'title', 'cps' ),
		],
		attributes: {
			headline: {
				type: 'string'
			},
			copy: {
				type: 'string'
			},
			columnOneCopy: {
				type: 'string'
			},
			columnTwoCopy: {
				type: 'string'
			},
			columnThreeCopy: {
				type: 'string'
			},
			colorScheme: {
				type: 'string',
			},
		},
		example: {
			attributes: {
				headline: 'Let\'s Go For Your Goals',
				copy: 'With our innovation, flexibility and expertise, we know you can get there.',
				columnOneCopy: '877.668.7727',
				columnTwoCopy: '<a href="#">cpsadmissions@neu.edu</a>',
				columnThreeCopy: '<a href="#" class="button">Request Info</a>',
			},
		},
		supports: {
			html: false,
			customClassName: false,
			inserter: false,
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: withColors( 'colorScheme' )( props => {
			const {
				attributes: {
					headline,
					copy,
					columnOneCopy,
					columnTwoCopy,
					columnThreeCopy,
				},
				setAttributes,
				colorScheme,
				setColorScheme,
			} = props;

			// @todo Add proper class names to mirror front end styles.
			return (
				<>
					<InspectorControls>
						<PanelBody
							title={ __( 'Default Contact Block', 'cps' ) }
							initialOpen={ true }
						>
							<PanelRow>
								<DefaultContactBlockToggleControl />
							</PanelRow>
						</PanelBody>
						<PanelColorSettings
							title={__( 'Color Settings', 'cps' )}
							colorSettings={[
								{
									value: colorScheme.color,
									label: __(
										'Background Color',
										'cps'
									),
									onChange: setColorScheme,
								},
							]}
						></PanelColorSettings>
					</InspectorControls>
					<div className={`block-footer-contact ${colorScheme.slug ? `has-${ colorScheme.slug }-background-color` : ''}`}>
						<div className="block-footer-contact__inner">
							<RichText
								allowedFormats={ [] }
								format="string"
								onChange={ headline => setAttributes( { headline } ) }
								keepPlaceholderOnFocus="true"
								multiline="false"
								placeholder={ __( 'Add headline', 'cps' ) }
								tagName="h2"
								value={ headline }
								className="block-footer-contact__headline is-style-h1"
							/>
							<RichText
								allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough', 'core/link' ] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ copy => setAttributes( { copy } ) }
								placeholder={ __( 'Add text', 'cps' ) }
								tagName="p"
								value={ copy }
							/>
							<div>
								<RichText
									allowedFormats={ [ 'cps/button', 'core/italic', 'core/bold', 'core/strikethrough', 'core/link' ] }
									format="string"
									keepPlaceholderOnFocus="true"
									onChange={ columnOneCopy => setAttributes( { columnOneCopy } ) }
									placeholder={ __( 'Add text', 'cps' ) }
									tagName="span"
									value={ columnOneCopy }
								/>
								<span>|</span>
								<RichText
									allowedFormats={ [ 'cps/button', 'core/italic', 'core/bold', 'core/strikethrough', 'core/link' ] }
									format="string"
									keepPlaceholderOnFocus="true"
									onChange={ columnTwoCopy => setAttributes( { columnTwoCopy } ) }
									placeholder={ __( 'Add text', 'cps' ) }
									tagName="span"
									value={ columnTwoCopy }
								/>
								<span>|</span>
								<RichText
									allowedFormats={ [ 'cps/button', 'core/italic', 'core/bold', 'core/strikethrough', 'core/link' ] }
									format="string"
									keepPlaceholderOnFocus="true"
									onChange={ columnThreeCopy => setAttributes( { columnThreeCopy } ) }
									placeholder={ __( 'Add text', 'cps' ) }
									tagName="span"
									value={ columnThreeCopy }
								/>
							</div>
						</div>
					</div>
				</>
			);
		} ),
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
