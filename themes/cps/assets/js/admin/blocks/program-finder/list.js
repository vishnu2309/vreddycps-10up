/* eslint require-jsdoc: 0 */
/**
 * WordPress dependencies
 */
const {
	Component,
} = wp.element;

/**
 * External dependencies
 */
import isFunction from 'lodash/isFunction';
import find from 'lodash/find';
import { v4 as uuid } from 'uuid';

/**
 * Internal dependencies
 */
import Item from './item';
import Sortable from '../../components/sortable';

class List extends Component {

	constructor() {
		super( ...arguments );
	}

	/**
	 * Render component.
	 *
	 */
	render() {

		const getItems = () => {

			let currentItems = this.props.items;

			if ( ! currentItems ) {
				currentItems = [];
			} else {
				currentItems = JSON.parse( currentItems );
			}

			return currentItems;
		};

		/**
		 * Call parent component onChange callback.
		 *
		 * @param {array} items List containing current items.
		 */
		const updateItems = ( items ) => {

			if ( ! this.props.onChange || ! isFunction( this.props.onChange ) ) {
				// eslint-disable-next-line no-console
				console.error(
					'[List] The "onChange" property must be specified and must be a valid function.'
				);
				return;
			}

			this.props.onChange( items );
		};

		const updateItem = ( id, key, value ) => {
			const curItems = getItems();
			const match = find( curItems, function( item ) { return item.id === id; } );

			if ( match ) {
				match[ key ] = value;
				updateItems( curItems );
			}
		};

		const addItem = () => {

			const items = getItems();

			items.push( {
				id: uuid(),
				label: null,
				url: null,
			} );

			updateItems( items );
		};

		const renderItem = ( item ) => {

			return (
				<Item
					attributes={ item }
					isSelected={ this.props.isSelected }
					onChange={ updateItem }
				/>
			);
		};

		return (
			<Sortable
				items={ this.props.items }
				addItem={ addItem }
				onChange={ updateItems }
				renderItem={ renderItem }
				className={ 'program-finder-items-wrap-inner' }
				direction="vertical"
				disableDrag={ false }
			/>
		);
	}
}

export default List;
