/* eslint require-jsdoc: 0 */
/**
 * Program Finder Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;

const {
	InspectorControls,
	RichText
} = wp.blockEditor;

const {
	PanelBody,
	PanelRow,
	TextControl,
} = wp.components;

import isEmpty from 'lodash/isEmpty';
import icon from './icon';
import List from './list';

/**
 * Register block
 */
export default registerBlockType(
	'cps/program-finder',
	{
		title: __( 'Program Finder', 'cps' ),
		description: __( 'A program search form.', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'search', 'cps' ),
		],
		attributes: {
			headline: {
				type: 'string'
			},
			buttonLabel: {
				type: 'string'
			},
			dropdownLabel: {
				type: 'string'
			},
			primaryLinks: {
				type: 'string',
				default: '[]'
			},
			primaryLinksLabel: {
				type: 'string'
			},
			secondaryLinks: {
				type: 'string',
				default: '[]'
			},
			secondaryLinksLabel: {
				type: 'string'
			},
		},
		supports: {
			html: false,
			className: false,
			customClassName: false,
		},
		example: {
			attributes: {
				headline: 'Let\'s find your path forward',
				buttonLabel: 'Let\'s Go',
				dropdownLabel: 'Explore our programs',
			},
		},
		/**
		 * Display the block full-width without showing the alignment control.
		 *
		 * @param attributes
		 * @returns {object}
		 */
		getEditWrapperProps( attributes ) {
			attributes['data-align'] = 'full';
			return attributes;
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					headline,
					buttonLabel,
					dropdownLabel,
					primaryLinks,
					primaryLinksLabel,
					secondaryLinks,
					secondaryLinksLabel
				},
				setAttributes,
				className,
				isSelected,
			} = props;

			const onChangeList = ( items, position ) => {

				const newItems = JSON.stringify( items );

				if ( 'primary' === position ) {
					setAttributes( { primaryLinks: newItems } );
				} else {
					setAttributes( { secondaryLinks: newItems } );
				}

			};

			const onChangeLabel = ( value, position ) => {

				if ( 'primary' === position ) {
					setAttributes( { primaryLinksLabel: value } );
				} else {
					setAttributes( { secondaryLinksLabel: value } );
				}
			};

			const itemColumns = [
				{
					items: primaryLinks,
					label: primaryLinksLabel,
					header: __( 'Primary Links', 'cps' ),
					position: 'primary',
				},
				{
					items: secondaryLinks,
					label: secondaryLinksLabel,
					header: __( 'Secondary Links', 'cps' ),
					position: 'secondary',
				}
			];

			const stateLabel  = __( 'Add dropdown label...', 'cps' );
			const firstOption = isEmpty( dropdownLabel ) ? stateLabel : dropdownLabel;

			return (
				<Fragment>
					<InspectorControls>
						<PanelBody
							title={ __( 'Block Settings', 'cps' ) }
							initialOpen={ true }
						>
							<PanelRow>
								<div>
									<TextControl
										label={ __( 'Dropdown Label', 'cps' ) }
										value={ dropdownLabel }
										onChange={ dropdownLabel => setAttributes( { dropdownLabel } ) }
									/>
									<p className="description">{ __( 'The dropdown label is shown when the user did not make a selection yet, eg. "Explore our Programs"', 'cps' ) }</p>
								</div>
							</PanelRow>
						</PanelBody>
					</InspectorControls>

					<div className={ `block-program-finder has-red-background-color has-white-color ${className}` }>
						<div className="block-program-finder__headline">
							<div className="block-program-finder__inner">
								<div className="block-program-finder__headline">
									<RichText
										allowedFormats={ [] }
										format="string"
										onChange={ headline => setAttributes( { headline } ) }
										keepPlaceholderOnFocus="true"
										multiline="false"
										placeholder={ __( 'Add headline', 'cps' ) }
										tagName="h2"
										value={ headline }
										className="cps-block-input-title"
									/>
								</div>
								<div className="block-program-finder__finder">
									<form action="#" className="block-program-finder__form">
										<select className="js-choices">
											<option selected disabled value="">{ firstOption }</option>

										</select>
										<RichText
											allowedFormats={ [] }
											format="string"
											onChange={ buttonLabel => setAttributes( { buttonLabel } ) }
											keepPlaceholderOnFocus="true"
											multiline="false"
											placeholder={ __( 'Add button label...', 'cps' ) }
											tagName="span"
											value={ buttonLabel }
											className="button-tertiary pseudo-button"
										/>
									</form>
								</div>
							</div>
						</div>
					</div>
					{ isSelected &&
						<div className="program-finder-items-wrap">
							{
								itemColumns.map( ( column ) => {
									return (
										<div className="program-finder-items">
											<div className="program-finder-items-header">
												<strong>{ column.header }</strong>
												<TextControl
													label={ __( 'Label', 'cps' ) }
													value={ column.label }
													onChange={ value => onChangeLabel( value, column.position ) }
												/>
											</div>
											<div className="program-finder-items-links">
												<div className="components-base-control">
													<div className="components-base-control__field">
														<label className="components-base-control__label">{ __( 'Links', 'cps' ) }</label>
														<List
															items={ column.items }
															onChange={ items => onChangeList( items, column.position ) }
															isSelected={ isSelected }
														/>
													</div>
												</div>
											</div>
										</div>
									);
								} )
							}
						</div>
					}
				</Fragment>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
