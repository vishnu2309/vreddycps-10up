/* eslint require-jsdoc: 0 */
/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { TextControl } = wp.components;
const { Component } = wp.element;
const {	URLInput } = wp.blockEditor;

/**
 * External dependencies
 */
import isFunction from 'lodash/isFunction';

export class Item extends Component {

	constructor() {
		super( ...arguments );
	}

	render() {

		const onChange = ( key, value ) => {

			if ( ! this.props.onChange || ! isFunction( this.props.onChange ) ) {
				// eslint-disable-next-line no-console
				console.error(
					'[Item] The "onChange" property must be specified and must be a valid function.'
				);
				return;
			}

			this.props.onChange( this.props.attributes.id, key, value );
		};

		return (
			<div className="program-finder-item" >
				<TextControl
					label=""
					value={ this.props.attributes.label }
					placeholder={ __( 'Add label...' ) }
					keepPlaceholderOnFocus="true"
					onChange={ ( value ) => {
						onChange( 'label', value );
					} }
				/>
				<URLInput
					autoFocus={ false }
					value={ this.props.attributes.url }
					onChange={ ( value ) => {
						onChange( 'url', value );
					} }
				/>
			</div>
		);
	}
}

export default Item;
