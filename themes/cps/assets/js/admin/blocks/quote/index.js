/**
 * Quote Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;
const {
	PanelBody,
	PanelRow,
	TextControl,
} = wp.components;

const {
	InspectorControls,
	RichText
} = wp.blockEditor;

import icon from './icon';

/**
 * Register block
 */
export default registerBlockType(
	'cps/quote',
	{
		title: __( 'CPS Quote', 'cps' ),
		description: __( 'A testimonial block with name and role.', 'cps' ),
		icon: icon,
		category: 'text',
		keywords: [
			__( 'testimonial', 'cps' ),
			__( 'reference', 'cps' ),
			__( 'pullquote', 'cps' ),
		],
		attributes: {
			quote: {
				type: 'string'
			},
			name: {
				type: 'string'
			},
			subhead: {
				type: 'string'
			},
			link: {
				type: 'string'
			}
		},
		supports: {
			customClassName: false,
			html: false,
			defaultStylePicker: false
		},
		example: {
			attributes: {
				quote: 'From start to finish, I felt like the faculty, the alumni, even my classmates, did everything to keep me motivated. There\'s a real sense of collaboration and community here.',
				name: 'Nicole Blanchard',
				subhead: 'Master\'s in Homeland Security, Class of 2019'
			}
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					quote,
					name,
					subhead,
					link
				},
				className,
				setAttributes,
			} = props;


			return (
				<Fragment>
					<InspectorControls>
						<PanelBody
							title={ __( 'Block Settings', 'cps' ) }
							initialOpen={ false }
						>
							<PanelRow>
								<TextControl
									id=""
									label={ __( 'Link (Optional)', 'cps' ) }
									value={ link }
									onChange={ link => setAttributes( { link } ) }
								/>
							</PanelRow>
						</PanelBody>
					</InspectorControls>
					<blockquote className={ `block-cps-quote ${className}` }>
						<RichText
							allowedFormats={ [] }
							format="string"
							onChange={ quote => setAttributes( { quote } ) }
							keepPlaceholderOnFocus="true"
							placeholder={ __( 'Add quote', 'cps' ) }
							tagName="p"
							value={ quote }
							className="cps-block-input-quote"
						/>
						<cite>
							<RichText
								allowedFormats={ [] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ name => setAttributes( { name } ) }
								placeholder={ __( 'Add name', 'cps' ) }
								tagName="span"
								multiline="false"
								value={ name }
								className="is-style-h5 has-red-color"
							/>
							<RichText
								allowedFormats={ [] }
								format="string"
								keepPlaceholderOnFocus="true"
								onChange={ subhead => setAttributes( { subhead } ) }
								placeholder={ __( 'Add role', 'cps' ) }
								tagName="span"
								multiline="false"
								value={ subhead }
								className="is-style-small-description"
							/>
						</cite>
					</blockquote>
				</Fragment>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return null;
		},
	},
);
