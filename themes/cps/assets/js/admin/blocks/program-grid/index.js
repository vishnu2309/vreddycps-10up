/**
 * Program Grid Block
 */

const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Fragment } = wp.element;

const {
	Button,
	ButtonGroup,
	PanelBody,
	PanelRow,
} = wp.components;

const {
	InnerBlocks,
	InspectorControls,
	RichText
} = wp.blockEditor;

import isEmpty from 'lodash/isEmpty';
import icon from './icon';
import HierarchicalTermSelector from '../../components/hierarchical-term-selector';

/**
 * Returns the template for the curated mode.
 *
 * @return {Object[]} Block template.
 */
const getColumnsTemplate = () => {
	return [
		[ 'cps/program-grid-item' ],
		[ 'cps/program-grid-item' ],
		[ 'cps/program-grid-item' ]
	];
};

/**
 * Register block
 */
export default registerBlockType(
	'cps/program-grid',
	{
		title: __( 'Program Grid', 'cps' ),
		description: __( 'A list of programs', 'cps' ),
		icon: icon,
		category: 'cps-blocks',
		keywords: [
			__( 'card', 'cps' )
		],
		attributes: {
			copy: {
				type: 'string'
			},
			headline: {
				type: 'string'
			},
			mode: {
				type: 'string',
				default: 'automatic'
			},
			programLevel: {
				type: 'string',
				default: '[]'
			}
		},
		supports: {
			html: false,
			className: false,
			customClassName: false,
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#edit
		 */
		edit: props => {
			const {
				attributes: {
					copy,
					headline,
					mode,
					programLevel
				},
				setAttributes
			} = props;

			const availableModes = {
				'automatic': {
					label: __( 'Automatic', 'cps' ),
					description: __( 'Show all programs from a selected program level.', 'cps' )
				},
				'curated': {
					label: __( 'Curated', 'cps' ),
					description: __( 'Curated mode allows you to Select specific programs to be included in the block.', 'cps' )
				}
			};

			let selectedProgramLevels = [];

			if ( null !== programLevel ) {
				selectedProgramLevels = JSON.parse( programLevel );
			}

			return (
				<Fragment>
					<InspectorControls>
						<PanelBody
							title={ __( 'Block Settings', 'cps' ) }
						>
							<PanelRow>
								<div>
									<label>{ __( 'Mode', 'cps' ) }</label>
									<ButtonGroup>
										{
											Object.keys( availableModes ).map( key =>
												<Button
													key={ key }
													label={ availableModes[ key ].label }
													isPrimary={ key === mode }
													isLarge={ true }
													aria-pressed={ key === mode }
													onClick={ () => {
														setAttributes( { mode: key } );
													} }
												>
													{ availableModes[ key ].label }
												</Button>
											)
										}
									</ButtonGroup>
									<div class="program-grid-option-description">
										<span class="description">
											{ availableModes[ mode ].description }
										</span>
									</div>
								</div>
							</PanelRow>
							{ 'automatic' === mode &&
								<PanelRow>
									<div>
										<HierarchicalTermSelector
											terms={ selectedProgramLevels }
											slug="nu_pim_degree_type"
											onUpdateTerms={ ( value ) => {
												setAttributes( { programLevel: JSON.stringify( value ) } );
											} }
										/>
									</div>
								</PanelRow>
							}
						</PanelBody>
					</InspectorControls>
					<div className="block-program-grid">
						<RichText
							allowedFormats={ [] }
							format="string"
							onChange={ headline => setAttributes( { headline } ) }
							keepPlaceholderOnFocus="true"
							multiline="false"
							placeholder={ __( 'Write headline…', 'cps' ) }
							tagName="h2"
							value={ headline }
						/>
						<RichText
							allowedFormats={ [ 'core/italic', 'core/bold', 'core/strikethrough' ] }
							format="string"
							keepPlaceholderOnFocus="true"
							onChange={ copy => setAttributes( { copy } ) }
							placeholder={ __( 'Write copy…', 'cps' ) }
							tagName="p"
							value={ copy }
						/>
						{ 'automatic' === mode &&
							<div className="description">
								{ isEmpty( JSON.parse( programLevel ) ) ? (
									<p>
										{ __( 'No program level selected.', 'cps' ) }
									</p>
								) : (
									<p>
										{ __( 'The list of programs will be shown here.', 'cps' ) }
									</p>
								) }
							</div>
						}
						{ 'curated' === mode &&
							<InnerBlocks
								allowedBlocks={ ['cps/program-grid-item'] }
								orientation='horizontal'
								template={ getColumnsTemplate() }
								renderAppender={() => <InnerBlocks.ButtonBlockAppender />}
							/>
						}
					</div>
				</Fragment>
			);
		},
		/**
		 * See https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-edit-save/#save
		 *
		 * This is a dynamic block, its output is generated via php in /includes/blocks/class-example.php
		 */
		save: () => {
			return (
				<Fragment>
					<InnerBlocks.Content />
				</Fragment>
			);
		}
	}
);
