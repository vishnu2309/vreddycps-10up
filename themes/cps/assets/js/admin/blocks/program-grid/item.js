/**
 * A single program grid item in the Program Grid block.
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

import icon from './item-icon';
import edit from './item-edit';

export default registerBlockType( 'cps/program-grid-item', {
	title: __( 'Program', 'cps' ),
	parent: [ 'cps/program-grid' ],
	icon: icon,
	description: __( 'A single program grid item.', 'cps' ),
	category: 'cps-blocks',
	supports: {
		html: false,
		reusable: false,
		className: false,
		customClassName: false,
	},
	attributes: {
		titleText: {
			type: 'string',
			default: '',
		},
		postId: {
			type: 'int',
			default: '',
		},
	},

	edit,

	save: () => {
		return null;
	}
} );
