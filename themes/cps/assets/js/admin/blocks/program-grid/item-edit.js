/* global cpsGutenbergVars */

/**
 * WordPress dependencies
 */
const { __ }             = wp.i18n;
const { decodeEntities } = wp.htmlEntities;
const { apiFetch }       = wp;
const { Spinner }        = wp.components;
const { Component }      = wp.element;

import classnames from 'classnames';
import { isEmpty, isFinite } from 'lodash';
import PostFinder from '../../components/post-finder';

// Set nonce to be able to authenticate our requests in the rest handler in php.
if ( 'undefined' !== typeof cpsGutenbergVars.nonce ) {
	apiFetch.use( apiFetch.createNonceMiddleware( cpsGutenbergVars.nonce ) );
}

/**
 * Single program edit component.
 */
export default class ProgramGridItemEdit extends Component {

	/**
	 * Class constructor.
	 */
	constructor() {

		super( ...arguments );

		this.initContent   = this.initContent.bind( this );
		this.onChangePost  = this.onChangePost.bind( this );
		this.fetchPostData = this.fetchPostData.bind( this );

		this.state = {
			isLoading: false,
			titleText: false,
			initials: false,
		};
	}

	/**
	 * Renders when the component is mounted from DOM.
	 */
	componentDidMount() {
		this.isStillMounted = true;
		this.initContent();
	}

	/**
	 * Renders when the component is unmounted from DOM.
	 */
	componentWillUnmount() {
		this.isStillMounted = false;
	}

	/**
	 * Initialise the content.
	 */
	initContent() {

		const {
			attributes: {
				postId
			}
		} = this.props;

		const id = parseInt( postId, 10 );

		if ( isFinite( id ) ) {
			this.fetchPostData( id );
		}
	}

	/**
	 * Fetch the program using a postID from the API.
	 *
	 * @param {int} id Post ID.
	 *
	 * @returns {*}
	 */
	fetchPostData( id ) {

		this.setState( {
			isLoading: true
		} );

		const path = `/wp/v2/program/${ parseInt( id, 10 ) }`;

		return apiFetch( { path } )
			.then( ( response ) => {

				if ( this.isStillMounted && response ) {
					this.setState( {
						titleText: response.title.rendered,
						isLoading: false
					} );
				}
			} )
			.catch( ( error ) => {
				if ( this.isStillMounted ) {
					this.setState( {
						isLoading: false,
						error: true,
						errorMsg: error.message
					} );
				}
			} );
	}

	/**
	 * On change program.
	 */
	onChangePost( post ) {

		const { setAttributes } = this.props;

		/*
		 * Note: The PostFinder component returns an object
		 *       containing id, title and url
		 *
		 * Todo: Display error.
		 */
		if ( ! isEmpty( post ) && 'undefined' !== typeof post.id ) {

			setAttributes( { postId: post.id } );

			// Set the titleText while we fetch the rest of the post data.
			this.setState( { titleText: post.title } );

			/*
			 * Fetch post data.
			 *
			 * TODO: No need to fetch extra data for now, as we're already getting the title.
			 */
			//this.fetchPostData( post.id );
		}
	}

	/**
	 * Render block's HTML markup in edit mode.
	 *
	 * @returns {JSX.Element}
	 */
	render() {

		const {
			isSelected
		} = this.props;

		const className = classnames( {
			'is-loading': this.state.isLoading
		} );

		return (
			<div
				className={ `grid-item ${ className }` }
			>
				{ this.state.isLoading &&
					<Spinner />
				}
				{ ( isSelected || ! this.state.titleText ) &&
					<PostFinder
						placeholder={ __( 'Search Program', 'cps' ) }
						autoFocus={ true }
						inputClassName="components-text-control__input"
						onChange={ value => this.onChangePost( value ) }
						postType={ 'cps-program' }
					/>
				}
				{ this.state.titleText &&
					<div className="grid-item__content">
						<h3 className="post-title">{ decodeEntities( this.state.titleText ) }</h3>
					</div>
				}
			</div>
		);
	}
}
