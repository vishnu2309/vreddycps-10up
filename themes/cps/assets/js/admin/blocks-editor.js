// Plugins
import './plugins/document-setting-person-block';
import './plugins/document-setting-contact-block';
import './plugins/richtext-button-format';

// Blocks
import './blocks/accordion';
import './blocks/accordion/item';
import './blocks/card';
import './blocks/carousel';
import './blocks/carousel/item.js';
import './blocks/faculty-directory';
import './blocks/faculty-grid';
import './blocks/faculty-grid-column';
import './blocks/footer-contact';
import './blocks/hero';
import './blocks/hero-program';
import './blocks/hero-program/anchor';
import './blocks/icon-columns';
import './blocks/icon-column';
import './blocks/image-text';
import './blocks/program-finder';
import './blocks/program-grid';
import './blocks/program-grid/item';
import './blocks/quote';
import './blocks/section-heading';
import './blocks/stat';
import './blocks/wrapper';


// LazySizes.
// import '../../../node_modules/lazysizes/lazysizes';

// Filters
import './filters/make-group-block-full-width';

// Block styles and filters.
import registerHeadingStyles from './styles/heading';
import registerListStyles from './styles/list';
import registerParagraphStyles from './styles/paragraph';
import registerStatStyles from './styles/stat';
import registerQuoteStyles from './styles/quote';
import registerColumnsStyles from './styles/columns';
import unRegisterImageStyles from './styles/image';
import unRegisterPullquoteStyles from './styles/pullquote';

/**
 * Init the Gutenberg block style registrations.
 */
wp.domReady( () => {
	registerHeadingStyles();
	registerListStyles();
	registerParagraphStyles();
	registerStatStyles();
	registerQuoteStyles();
	registerColumnsStyles();
	unRegisterImageStyles();
	unRegisterPullquoteStyles();
} );
