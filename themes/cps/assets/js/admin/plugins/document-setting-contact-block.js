/* global cpsContactBlockVars */

/**
 * WordPress dependencies
 */
const { registerPlugin } = wp.plugins;
const { PluginDocumentSettingPanel } = wp.editPost;
const { useSelect } = wp.data;
const { SelectControl } = wp.components;
const { useEntityProp } = wp.coreData;

/**
 * Add a contact block selector to the post edit screen.
 */
const ContactBlockSelect = () => {

	const postType = useSelect( select => select( 'core/editor' ).getCurrentPostType() );

	if ( 'cps-contact-block' === postType ) {
		return null;
	}

	const [ meta, setMeta ] = useEntityProp(
		'postType',
		postType,
		'meta'
	);

	let metaFieldValue = '';

	if ( meta && meta['cps_contact_block_id'] ) {
		metaFieldValue = meta['cps_contact_block_id'];
	}

	let selectOptions = [
		{ label: 'Default', value: '' },
	];

	if ( 'undefined' !== typeof cpsContactBlockVars && 'object' === typeof cpsContactBlockVars.options ) {
		selectOptions = selectOptions.concat( cpsContactBlockVars.options );
	}

	selectOptions.push( { label: 'None', value: 'none' } );

	/**
	 * Update post meta value.
	 *
	 * @param {string} newValue Updated meta value.
	 * @return {void}
	 */
	const updateMetaValue = ( newValue ) => {
		setMeta( { ...meta, 'cps_contact_block_id': newValue } );
	};

	return(
		<PluginDocumentSettingPanel
			name="contact-footer-panel"
			title="Contact Footer"
			className="contact-footer-panel"
		>
			<SelectControl
				label="Select a contact footer (Optional)"
				value={ metaFieldValue }
				options={ selectOptions }
				onChange={ updateMetaValue }
			/>
		</PluginDocumentSettingPanel>
	);
};

registerPlugin( 'plugin-contact-footer-select', {
	render: ContactBlockSelect,
	icon: ''
} );
