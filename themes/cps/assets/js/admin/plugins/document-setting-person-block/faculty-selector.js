/**
 * WordPress dependencies.
 */
const { apiFetch }                 = wp;
const { __ }                       = wp.i18n;
const { Button, Spinner }          = wp.components;
const { compose }                  = wp.compose;
const { withDispatch, withSelect } = wp.data;
const { Component }                = wp.element;

import PostFinder from '../../components/post-finder';
import { isEmpty } from 'lodash';

/**
 * Faculty Selector component.
 *
 * @since 1.0.0
 */
class FacultySelector extends Component {

	/**
	 * Component constructor.
	 *
	 * @param props
	 */
	constructor( props ) {

		super( props );

		this.onChangePost  = this.onChangePost.bind( this );
		this.fetchPostData = this.fetchPostData.bind( this );

		this.state = {
			name: false,
			isLoading: false
		};
	}

	/**
	 * Check if there's anything needs to be done, when component is mounted.
	 */
	componentDidMount() {
		this.isStillMounted = true;
		this.initContent();
	}

	/**
	 * Check if there's anything needs to be done, when component is unmounted.
	 */
	componentWillUnmount() {
		this.isStillMounted = false;
	}

	/**
	 * Check if faculty is already set.
	 */
	initContent() {

		const id = parseInt( this.props.cpsFacultyId, 10 );

		if ( isFinite( id ) ) {
			this.fetchPostData( id );
		}
	}

	/**
	 * Update post related data when changes.
	 *
	 * @param post Selected post object.
	 */
	onChangePost( post ) {

		/*
		 * Note: The PostFinder component returns an object
		 *       containing id, title, etc.
		 *
		 * Todo: Display error.
		 */
		if ( ! isEmpty( post ) && 'undefined' !== typeof post.id ) {

			this.props.setMetaFieldValue( post.id.toString() );

			// Set the name while we fetch the rest of the post data.
			this.setState( { name: post.title } );

		} else {

			this.props.setMetaFieldValue( post );

			this.state.name = false;
		}
	}

	/**
	 * Fetch the post using API.
	 *
	 * @param id Post ID.
	 * @returns {*}
	 */
	fetchPostData( id ) {

		this.setState( {
			isLoading: true
		} );

		const path = `/wp/v2/person/${ parseInt( id, 10 ) }`;

		return apiFetch( { path } )
			.then( ( response ) => {

				if ( this.isStillMounted && response ) {

					this.setState( {
						name: response.title.rendered,
						isLoading: false
					} );
				}
			} )
			.catch( ( error ) => {

				if ( this.isStillMounted ) {

					this.setState( {
						isLoading: false,
						error: true,
						errorMsg: error.message
					} );
				}
			} );
	}

	/**
	 * Render the component.
	 *
	 * @returns {*}
	 */
	render() {

		return (
			<>
				<PostFinder
					placeholder={ __( 'Search Faculty', 'cps' ) }
					autoFocus={ false }
					inputClassName="components-text-control__input"
					onChange={ value => this.onChangePost( value ) }
					postType="cps-person"
				/>
				{ this.state.isLoading &&
					<Spinner />
				}
				{ ! isEmpty( this.state.name ) &&
					<ul className="cps-faculty-panel-result">
						<li className="cps-faculty-panel-item">
							<span>{ this.state.name }</span>
							<Button
								className={ 'button-link button-link-delete' }
								onClick={ () => {
									this.onChangePost( '' );
								} }
							>
								<i className={ 'dashicons dashicons-no-alt' } />
								<span className="screen-reader-text">{ __( 'Remove Faculty', 'cps' ) }</span>
							</Button>
						</li>
					</ul>
				}
			</>
		);
	}
}

export default compose( [

	withDispatch( ( dispatch, props ) => { // eslint-disable-line no-unused-vars

		const { editPost } = dispatch( 'core/editor' );

		return {
			setMetaFieldValue: function ( value ) { // eslint-disable-line require-jsdoc
				editPost( { meta: { cps_faculty_id: value } } ); // eslint-disable-line camelcase
			}
		};
	} ),
	withSelect( ( select, props ) => { // eslint-disable-line no-unused-vars

		const { getEditedPostAttribute } = select( 'core/editor' );

		return {
			cpsFacultyId: getEditedPostAttribute( 'meta' )[ 'cps_faculty_id' ]
		};
	} )

] )( FacultySelector );
