/**
 * WordPress dependencies.
 */
const { select }                     = wp.data;
const { PluginDocumentSettingPanel } = wp.editPost;
const { __ }                         = wp.i18n;
const { registerPlugin }             = wp.plugins;

import FacultySelector from './faculty-selector';

/**
 * Add a faculty selector to the post edit screen.
 *
 * @returns {null|*}
 *
 * @constructor
 */
const FacultySelect = () => {

	const postType = select( 'core/editor' ).getCurrentPostType();

	if ( 'page' !== postType ) {
		return null;
	}

	const pageTemplate = select( 'core/editor' ).getEditedPostAttribute( 'template' );

	if ( 'faculty_spotlight' !== pageTemplate ) {
		return null;
	}

	return (
		<PluginDocumentSettingPanel
			name="faculty-select-panel"
			title={ __( 'Faculty Selector', 'cps' ) }
			className="faculty-select-panel"
		>
			<FacultySelector />
		</PluginDocumentSettingPanel>
	);
};


/**
 * Register the Faculty Selector plugin.
 */
registerPlugin( 'plugin-cps-faculty-select', {
	icon: '',
	render: FacultySelect
} );
