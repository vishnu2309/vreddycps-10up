/* eslint require-jsdoc: 0 */
/**
 * Registers a "Button" style for the richtext editor component.
 * Meant to be used in the Footer Contact block.
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerFormatType, toggleFormat } = wp.richText;
const { RichTextToolbarButton } = wp.blockEditor;

const name = 'cps/button';

export const buttonFormat = {
	name,
	title: __( 'Button' ),
	tagName: 'span',
	className: 'button-tertiary',
	edit( { isActive, value, onChange } ) {
		const onToggle = () => onChange( toggleFormat( value, { type: name } ) );

		return (
			<Fragment>
				<RichTextToolbarButton
					icon="button"
					title={ __( 'Button', 'cps' ) }
					onClick={ onToggle }
					isActive={ isActive }
				/>
			</Fragment>
		);
	},
};

registerFormatType( name , buttonFormat );
