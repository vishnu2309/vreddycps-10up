/**
 * Display the group block full-width in the edit screen without showing the alignment control.
 *
 * @package CPS
 */
const { createHigherOrderComponent } = wp.compose;

const makeGroupBlockFullWidth = createHigherOrderComponent( ( BlockListBlock ) => {

	return ( props ) => {

		if ( 'core/group' === props.name || 'core/block' === props.name ) {
			return <BlockListBlock { ...props } wrapperProps={ { 'data-align': 'full' } } />;
		}

		return <BlockListBlock { ...props } />;
	};
}, 'makeGroupBlockFullWidth' );

wp.hooks.addFilter( 'editor.BlockListBlock', 'cps/make-group-block-full-width', makeGroupBlockFullWidth );
