/* global jQuery */

jQuery( document ).ready( function() {
	// Variable to store the queried name
	let query = {};

	jQuery( '.js-faculty-member' ).select2( {
		allowClear: true,
		maximumSelectionLength: 1,
		multiple: true,
		placeholder: {
			id: '',
			text: 'Search by Name'
		},
		width: '100%',
		/**
		 * Format the dropdown result for faculty members
		 */
		templateResult: function( data ) {
			if ( ! data.id ) {
				return data.text;
			}

			// If the element has a role, let's grab it and display
			if ( data.element.dataset ) {
				const { role } = data.element.dataset;

				// If the user started searching, highlight the searched term
				const term = query.term || '';
				const regex = new RegExp( `${ term }`, 'i' );
				const highlight = data.text.replace( regex, `<strong>${ term }</strong>` );

				// HTML to display the faculty member name
				const $data = jQuery(
					`<div class="faculty-member-list-result"><span>${ highlight }</span><span>${ role }</span></div>`
				);

				return $data;
			}
		},
		language: {
			/**
			 * Store search query
			 */
			searching: function ( params ) {
				query = params;
				return 'Searching...';
			}
		}
	} );

	/**
	 * Redirects to Faculty Member page
	 *
	 * @returns {null}
	 */
	jQuery( '.js-faculty-member' ).on( 'select2:select', function( e ) {
		if ( e.params.data.id ) {
			// Clear select2 to avoid displaying a selection
			jQuery( '.js-faculty-member' ).val( null ).trigger( 'change' );

			// Make sure to reset search query
			query = {};

			// Redirect to the faculty member page
			window.location = e.params.data.id;
		}
	} );
} );
