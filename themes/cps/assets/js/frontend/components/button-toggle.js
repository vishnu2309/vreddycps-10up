/**
 * Toggle content
 */
export default class ButtonToggle {
	/**
	 * Check for small screen menu toggle
	 */
	constructor() {
		this.buttons = document.querySelectorAll( '.js-button-toggle' );

		if ( !this.buttons ) {
			return;
		}

		this.init();
	}

	/**
	 * Setup events
	 */
	init() {
		this.buttons.forEach( button => {
			const containerID = button.getAttribute( 'aria-controls' );
			const container = document.getElementById( containerID );
			const mq = window.matchMedia( `(${button.getAttribute( 'data-mq' )})` );

			this.setAriaAttributes( button, container, mq );
			mq.addListener( this.setAriaAttributes.bind( this, button, container, mq ) );

			button.addEventListener( 'click', this.toggleElement, false );
		} );
	}

	/**
	 * Toggle content
	 */
	setAriaAttributes( button, container, mq ) {

		if ( mq.matches ) {
			button.setAttribute( 'aria-expanded', false );
			container.setAttribute( 'aria-hidden', true );
		} else {
			button.removeAttribute( 'aria-expanded' );
			container.removeAttribute( 'aria-hidden' );
		}
	}

	/**
	 * Toggle content
	 */
	toggleElement( e ) {
		const button = e.currentTarget;
		const isExpanded = 'false' !== button.getAttribute( 'aria-expanded' );
		const containerID = button.getAttribute( 'aria-controls' );
		const container = document.getElementById( containerID );
		const textWrapper = button.querySelector( '.js-button-toggle__text' );
		const textExpand = button.getAttribute( 'data-text-expand' );
		const textExpanded = button.getAttribute( 'data-text-expanded' );

		if ( isExpanded ) {
			button.setAttribute( 'aria-expanded', false );
			container.setAttribute( 'aria-hidden', true );

			if ( textWrapper && textExpand ) {
				textWrapper.innerText = textExpand;
			}
		} else {
			button.setAttribute( 'aria-expanded', true );
			container.setAttribute( 'aria-hidden', false );

			if ( textWrapper && textExpanded ) {
				textWrapper.innerText = textExpanded;
			}
		}
	}
}
