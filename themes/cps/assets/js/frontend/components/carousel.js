import { tns } from 'tiny-slider/src/tiny-slider';
const carousels = document.querySelectorAll( '.block-carousel' );

/**
 * Init content carousel
 *
 * @returns {null}
 */
export const carousel = () => {

	if ( carousels ) {
		carousels.forEach( ( carousel ) => {
			const items = parseInt( carousel.getAttribute( 'data-items' ), 10 ) || 1;
			const slider = carousel.querySelector( '.block-carousel__slider' );
			if ( ! slider ) {
				return;
			}

			tns( {
				container: slider,
				controlsPosition: 'bottom',
				items: 1,
				loop: false,
				slideBy: 'page',
				nav: false,
				arrowKeys: true,
				preventScrollOnTouch: true,
				controlsText: [
					'<svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13 25"><polygon fill="currentColor" fill-rule="evenodd" points="205 433 222 433 222 450" transform="scale(-1 1) rotate(45 618.349 -45.806)"/></svg>',
					'<svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13 25"><polygon fill="currentColor" fill-rule="evenodd" points="1223 433 1240 433 1240 450" transform="rotate(45 1133.849 -1258.948)"/></svg>'
				],
				responsive: {
					768: {
						items: 2,
					},
					1024: {
						items: items,
					}
				},
			} );
		} );
	}
};
