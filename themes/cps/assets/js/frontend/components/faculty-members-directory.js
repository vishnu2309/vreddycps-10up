const initials           = document.querySelectorAll( '.initial' );
const facultyMemberLists = document.querySelectorAll( '.faculty-members' );

/**
 * Faculty members directory handler
 *
 * @param {Object} e The event object.
 *
 * @returns {null}
 */
const facultyMembersDirectoryHandler = ( e ) => {

	e.preventDefault();

	const mq = window.matchMedia( '(min-width: 48em)' );

	// Current clicked initial element.
	const initial = e.target;

	// Current clicked initial element's letter.
	const targetedListInitial = initial.getAttribute( 'data-initial' );

	// Get currently active faculty member's list's letter.
	const activeInitial = document.querySelector( '.faculty-members.active' );
	const activeListInitial = activeInitial ? activeInitial.getAttribute( 'data-initial' ) : '';

	// If clicked initial and currently active initial both are same, bail out.
	if ( targetedListInitial === activeListInitial && mq.matches ) {
		return;
	}

	[].forEach.call( facultyMemberLists, function ( el ) {
		const elInitial = el.getAttribute( 'data-initial' );
		if ( elInitial !== targetedListInitial || ( !mq.matches && elInitial === activeListInitial ) ) {
			// Remove active class from all the lists.
			el.classList.remove( 'active' );
			el.setAttribute( 'aria-hidden', true );
		} else {
			// Add active class to the requested initial list
			el.classList.add( 'active' );
			el.setAttribute( 'aria-hidden', false );

			const y = el.getBoundingClientRect().top - 200;
			window.scrollBy( {top: y, left: 0, behavior: 'smooth'} );
		}
	} );

	[].forEach.call( initials, function ( el ) {

		const elInitial = el.getAttribute( 'data-initial' );
		if ( elInitial !== targetedListInitial || ( !mq.matches && elInitial === activeListInitial ) ) {
			// Remove active class from the initial filters
			el.classList.remove( 'active' );
			el.setAttribute( 'aria-expanded', false );
		} else {
			// Add active class to the initial filter
			el.classList.add( 'active' );
			el.setAttribute( 'aria-expanded', true );
		}
	} );
};

/**
 * Init faculty members directory functionality
 *
 * @returns {null}
 */
export const facultyMembersDirectory = () => {

	if ( ! initials || ! facultyMemberLists ) {
		return;
	}

	initials.forEach( initial => {
		const elInitial = initial.getAttribute( 'data-initial' );

		initial.addEventListener( 'click', facultyMembersDirectoryHandler );
		initial.setAttribute( 'aria-expanded', false );

		if ( 'a' === elInitial ) {
			initial.classList.add( 'active' );
			initial.setAttribute( 'aria-expanded', true );
		}
	} );

	facultyMemberLists.forEach( facultyMemberList => {
		const elInitial = facultyMemberList.getAttribute( 'data-initial' );
		facultyMemberList.setAttribute( 'aria-hidden', true );

		if ( 'a' === elInitial ) {
			facultyMemberList.setAttribute( 'aria-hidden', false );
		}
	} );
};

