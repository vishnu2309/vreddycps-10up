/* global jQuery */

jQuery( document ).ready( function() {
	jQuery( '.js-select2-search' ).select2( {
		closeOnSelect: true,
		minimumResultsForSearch: Infinity,
		theme: 'default search-dropdown'
	} );
} );
