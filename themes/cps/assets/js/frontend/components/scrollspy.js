/**
 * Init scrollspy
 */
export default class Scrollspy {

	/**
	 * Check for scrollspy
	 */
	constructor() {
		// Scrollspy menu element
		this.scrollspyMenu = document.querySelector( '.js-scrollspy' );

		// If there is no scrollspy menu, bail out
		if ( !this.scrollspyMenu ) {
			return;
		}

		// Scrollspy area ID
		this.scrollspyAreaID = this.scrollspyMenu.getAttribute( 'data-scrollspy-area' ) || '';

		// Scrollspy area wrapper
		this.scrollspyArea = document.getElementById( this.scrollspyAreaID );

		// If there is no scrollspy area, bail out
		if ( !this.scrollspyArea ) {
			return;
		}

		// Current section for small screen menu toggle
		this.scrollspyCurrentName = document.getElementById( 'scrollspy-current-section' );

		// Scrollspy sections (need to be in wp-block-group)
		this.scrollspySections = this.scrollspyArea.querySelectorAll( '[data-nav-title]' );

		// Generate Scrollspy menu depending on sections
		this.scrollspySections.forEach( ( section, i ) => {
			const sectionTitle = section.getAttribute( 'data-nav-title' );
			const sectionID = section.id;

			const menuLink = document.createElement( 'a' );
			const menuLi = document.createElement( 'li' );

			// Create link
			menuLink.innerText = sectionTitle;
			menuLink.href = `#${sectionID}`;

			if ( 0 === i ) {
				menuLi.classList.add( 'is-active' );

				// Update current section for small screen toggle button
				this.scrollspyCurrentName.innerText = sectionTitle;
			}

			// Create menu element
			menuLi.append( menuLink );

			// Append to the scrollspy menu
			this.scrollspyMenu.append( menuLi );
		} );

		this.scrollspyLinks = this.scrollspyMenu.querySelectorAll( 'a' );

		// If the number of links is different from the number of sections, bail out
		if ( this.scrollspySections.length !== this.scrollspyLinks.length ) {
			return;
		}

		this.sectionOffset = 134;
		this.currentActive = 0;

		this.init();
	}

	/**
	 * Attach events
	 */
	init() {

		document.addEventListener( 'DOMContentLoaded', () => {

			this.scrollspyLinks.forEach( link => {
				link.addEventListener( 'click', ( e ) => {
					this.removeAllActive();
					e.target.parentNode.classList.add( 'is-active' );
				} );
			} );

			this.scrollHandler();

			window.addEventListener( 'scroll', this.debounce( this.scrollHandler.bind( this ), 50 ), false );
		} );
	}

	/**
	 * Make scrollspy link active
	 */
	makeActive( link ) {
		const currentSectionName = this.scrollspyLinks[link].text;

		if ( this.scrollspyCurrentName ) {
			this.scrollspyCurrentName.innerText = currentSectionName;
		}

		this.scrollspyLinks[link].parentNode.classList.add( 'is-active' );
	}

	/**
	 * Make scrollspy link inactive
	 */
	removeActive( link ) {
		this.scrollspyLinks[link].parentNode.classList.remove( 'is-active' );
	}

	/**
	 * Reset scrollspy links
	 */
	removeAllActive() {
		for ( let i = this.scrollspyLinks.length - 1; 0 <= i; i-- ) {
			this.removeActive( i );
		}
	}

	/**
	 * Scroll event
	 */
	scrollHandler() {

		let current = 0;
		const activeSection = [...this.scrollspySections].reverse().findIndex( ( section ) => {
			const viewpostOffset = section.getBoundingClientRect();
			const { top } = viewpostOffset;
			return window.scrollY >= top + window.scrollY - this.sectionOffset;
		} );

		if ( 0 <= activeSection ) {
			current = this.scrollspySections.length - activeSection - 1;
		}

		if ( current !== this.currentActive ) {
			this.removeAllActive();
			this.currentActive = current;
			this.makeActive( current );
		}
	}

	/**
	 * Debounce function
	 * https://davidwalsh.name/javascript-debounce-function
	 */
	debounce( func, wait, immediate ) {
		let timeout;
		return function() {
			const context = this, args = arguments;

			/**
			 * Later
			 */
			const later = function() {
				timeout = null;
				if ( !immediate ) func.apply( context, args );
			};
			const callNow = immediate && !timeout;
			clearTimeout( timeout );
			timeout = setTimeout( later, wait );
			if ( callNow ) func.apply( context, args );
		};
	}
}
