const mq = window.matchMedia( '(min-width: 80em)' ); // 1280px

const siteHeader = document.getElementById( 'site-header' );
const headerMenu = document.getElementById( 'header-menu' );
const siteHeaderNavigation = document.getElementById( 'site-header-navigation' );
const siteHeaderMenuButtons = Array.from( document.querySelectorAll( '.site-header__button--menu' ) );
const menuMainNavigation = document.getElementById( 'menu-main-navigation' );
const searchButton = document.querySelector( '.site-header__button--search' );
const searchWrapper = document.querySelector( '.site-header__search' );

/**
 * Setup markup
 */
const setupMarkup = () => {
	const menuItemsHaveChildren = Array.from( siteHeader.querySelectorAll( '.menu > .menu-item-has-children' ) );

	menuItemsHaveChildren.forEach( menuItem => {
		const { id } = menuItem;
		const wrapper = document.createElement( 'div' );
		const link = menuItem.querySelector( 'a' );
		const buttonOpen = createButtonFromLink( link );
		const buttonClose = createButtonFromLink( link );

		// Create wrapper
		for ( let index = 0; index < menuItem.childNodes.length; index++ ) {
			const element = menuItem.childNodes[index];

			wrapper.appendChild( element );
		}

		// Update Wrapper
		wrapper.setAttribute( 'id', `${id}-submenu-wrapper` );
		wrapper.setAttribute( 'aria-hidden', true );
		wrapper.setAttribute( 'data-open', false );
		wrapper.classList.add( 'menu-item__wrapper' );

		// Create open button
		const buttonOpenSpan = document.createElement( 'span' );
		buttonOpenSpan.classList.add( 'visually-hidden' );
		buttonOpenSpan.textContent = `Open ${buttonOpen.textContent} submenu`;
		buttonOpen.querySelector( 'span' ).setAttribute( 'aria-hidden', true );
		buttonOpen.appendChild( buttonOpenSpan );
		buttonOpen.setAttribute( 'data-submenu', 'open' );
		buttonOpen.setAttribute( 'aria-expanded', false );
		buttonOpen.setAttribute( 'aria-controls', `${id}-submenu-wrapper` );

		// Create close button
		const buttonCloseSpan = document.createElement( 'span' );
		buttonCloseSpan.classList.add( 'visually-hidden' );
		buttonCloseSpan.textContent = `Close ${buttonClose.textContent} submenu`;
		buttonClose.querySelector( 'span' ).setAttribute( 'aria-hidden', true );
		buttonClose.appendChild( buttonCloseSpan );
		buttonClose.setAttribute( 'data-submenu', 'close' );
		buttonClose.setAttribute( 'aria-expanded', false );
		buttonClose.setAttribute( 'aria-controls', `${id}-submenu-wrapper` );

		// Remove original link
		link.remove();

		// Add the button close to wrapper
		wrapper.insertBefore( buttonClose, wrapper.childNodes[0] || null );

		// Add button open to menu item
		menuItem.appendChild( buttonOpen );

		// Add the wrapper to the menu item
		menuItem.appendChild( wrapper );
	} );

	// Update search markup
	searchButton.setAttribute( 'aria-controls', 'site-header-search' );
	closeSearch();
};

/**
 * Update markup on load and resize
 */
const updateMarkup = () => {
	const isOpen = mq.matches;

	siteHeader.setAttribute( 'data-site-navigation-open', isOpen );
	siteHeader.setAttribute( 'data-submenu-open', false );

	siteHeaderNavigation.setAttribute( 'aria-hidden', !isOpen );
	siteHeaderNavigation.setAttribute( 'data-open', isOpen );

	siteHeaderMenuButtons.forEach( button => {
		button.setAttribute( 'aria-controls', 'site-header-navigation' );
		button.setAttribute( 'aria-expanded', isOpen );
	} );

	closeAllSubmenus();
};

/**
 * Debounce function
 * https://davidwalsh.name/javascript-debounce-function
 */
const debounce = ( func, wait, immediate ) => {
	let timeout;
	return function() {
		const context = this, args = arguments;

		/**
		 * Later
		 */
		const later = function() {
			timeout = null;
			if ( !immediate ) func.apply( context, args );
		};
		const callNow = immediate && !timeout;
		clearTimeout( timeout );
		timeout = setTimeout( later, wait );
		if ( callNow ) func.apply( context, args );
	};
};

/**
 * Update header class on scroll
 */
const updateHeader = () => {

	const windowScroll = window.scrollY || window.pageYOffset;
	const scrollPosition = Math.round( windowScroll );

	if ( 100 < scrollPosition ){
		siteHeader.classList.add( 'sticky' );
	}
	else {
		siteHeader.classList.remove( 'sticky' );
	}
};

/**
 * Setup listeners
 */
const setupListeners = () => {

	siteHeaderMenuButtons.forEach( button => {
		button.addEventListener( 'click', handleHeaderMenuButtonClick );
	} );

	siteHeaderNavigation.addEventListener( 'click', handleSiteHeaderNavigationClick );
	menuMainNavigation.addEventListener( 'click', handlemenuMainNavigationClick );

	document.addEventListener( 'click', handleDocumentClick );

	// Handle search events
	searchButton.addEventListener( 'keydown', handleDocumentClick );
	searchButton.addEventListener( 'click', handleSearchToggle );
	searchWrapper.addEventListener( 'click', ( e ) => {
		e.stopPropagation();
	} );

	// Add class sticky to tweak header appearance depending on scroll
	updateHeader();
	window.addEventListener( 'scroll', debounce( updateHeader, 100 ) );
};

/**
 * Handle search toggle click
 * @param {object} event the event object
 */
const handleSearchToggle = ( event ) => {
	const isSearchOpen = ( 'true' !== searchWrapper.getAttribute( 'aria-hidden' ) );

	event.stopPropagation();

	if ( isSearchOpen ) {
		closeSearch();
	} else {
		openSearch();
	}
};

/**
 * Handle site header navigation click
 * @param {object} event the event object
 */
const handleHeaderMenuButtonClick = ( event ) => {
	const { detail } = event;
	const isKeyboard = 0 === detail;
	const isOpen = ( 'true' === siteHeaderNavigation.getAttribute( 'data-open' ) );

	event.stopPropagation();

	if ( isOpen ) {
		closeSiteHeaderNavigation();
		document.querySelector( '.site-header__button--menu-open' ).focus();
	} else {
		openSiteHeaderNavigation( isKeyboard );
	}
};

/**
 * Handle site header navigation click
 * @param {object} event the event object
 */
const handleSiteHeaderNavigationClick = ( event ) => {
	event.stopPropagation();
};

/**
 * Handle main menu navigation click
 * @param {object} event the event object
 */
const handlemenuMainNavigationClick = ( event ) => {
	const { target, detail } = event;
	const isKeyboard = 0 === detail;

	if ( ! target.matches( '[data-submenu]' ) ) {
		return;
	}

	const submenu = document.getElementById( target.getAttribute( 'aria-controls' ) );
	const isOpen = ( 'true' === submenu.getAttribute( 'data-open' ) );

	if ( isOpen ) {
		closeSubmenu( submenu, isKeyboard );
	} else {
		closeAllSubmenus();
		openSubmenu( submenu, isKeyboard );
	}
};

/**
 * Handle document click
 * @param {object} event the event object
 */
const handleDocumentClick = ( e ) => {

	if ( 'keydown' === e.type ) {
		const isTabPressed = ( 'Tab' === e.key || 9 === e.keyCode );
		if ( !isTabPressed ) {
			return;
		}
		if ( ! e.shiftKey ) {
			closeAllSubmenus();

			if ( ! mq.matches ) {
				closeSiteHeaderNavigation();
			}
		}
	} else {
		closeAllSubmenus();
		closeSearch();

		if ( ! mq.matches ) {
			closeSiteHeaderNavigation();
		}
	}

};

/**
 * Opens search overlay
 */
const openSearch = () => {

	closeAllSubmenus();

	if ( ! mq.matches ) {
		closeSiteHeaderNavigation();
	}

	searchWrapper.setAttribute( 'aria-hidden', false );
	searchButton.setAttribute( 'aria-expanded', true );
	siteHeader.setAttribute( 'data-search-open', true );
	const searchField = searchWrapper.querySelector( '.search-field' );
	if ( searchField ) {
		setTimeout(
			() => {
				searchField.focus();
			},
			250
		);
	}
};

/**
 * Closes search overlay
 */
const closeSearch = () => {
	searchWrapper.setAttribute( 'aria-hidden', true );
	searchButton.setAttribute( 'aria-expanded', false );
	siteHeader.setAttribute( 'data-search-open', false );
};

/**
 * Opens the site header navigation
 */
const openSiteHeaderNavigation = () => {
	const isOpen = true;

	siteHeaderMenuButtons.forEach( button => {
		button.setAttribute( 'aria-expanded', isOpen );
	} );

	siteHeader.setAttribute( 'data-site-navigation-open', isOpen );
	siteHeaderNavigation.setAttribute( 'data-open', isOpen );
	siteHeaderNavigation.setAttribute( 'aria-hidden', !isOpen );

	closeSearch();

	document.querySelector( '.site-header__button--menu-close' ).focus();
};

/**
 * Closes the site header navigation
 */
const closeSiteHeaderNavigation = () => {
	const isOpen = false;

	siteHeaderMenuButtons.forEach( button => {
		button.setAttribute( 'aria-expanded', isOpen );
	} );

	siteHeader.setAttribute( 'data-site-navigation-open', isOpen );
	siteHeaderNavigation.setAttribute( 'data-open', isOpen );
	siteHeaderNavigation.setAttribute( 'aria-hidden', !isOpen );
};

/**
 * Open the submenu passed
 * @param {object} submenu The submenu node to use.
 * @param {boolean} isKeyboard Is the event from a keyboard interaction?
 */
const openSubmenu = ( submenu, isKeyboard = false ) => {
	const { id } = submenu;
	const buttons = Array.from( document.querySelectorAll( `[aria-controls="${id}"]` ) );
	const isOpen = true;
	const allLinks = submenu.querySelectorAll( 'a' );
	const lastLink = allLinks[allLinks.length - 1];
	const closeButton = document.querySelector( `[aria-controls="${id}"][data-submenu="close"]` );

	buttons.forEach( button => {
		button.setAttribute( 'aria-expanded', isOpen );
	} );

	submenu.setAttribute( 'aria-hidden', !isOpen );
	submenu.setAttribute( 'data-open', isOpen );
	siteHeader.setAttribute( 'data-submenu-open', isOpen );

	lastLink.addEventListener( 'keydown', closeSubmenuKeyboard );
	closeButton.addEventListener( 'keydown', submenuTrap );

	document.documentElement.style.setProperty( '--overlay-height', `${submenu.offsetHeight + 100}px` );

	closeSearch();

	if ( isKeyboard ) {
		setTimeout(
			() => {
				closeButton.focus();
			},
			250
		);
	}
};

/**
 * Close the submenu passed
 * @param {object} submenu The submenu node to use.
 * @param {boolean} isKeyboard Is the event from a keyboard interaction?
 */
const closeSubmenu = ( submenu, isKeyboard = false ) => {
	const { id } = submenu;
	const buttons = Array.from( document.querySelectorAll( `[aria-controls="${id}"]` ) );
	const isOpen = false;
	const allLinks = submenu.querySelectorAll( 'a' );
	const lastLink = allLinks[allLinks.length - 1];
	const closeButton = document.querySelector( `[aria-controls="${id}"][data-submenu="close"]` );

	buttons.forEach( button => {
		button.setAttribute( 'aria-expanded', isOpen );
	} );

	submenu.setAttribute( 'aria-hidden', !isOpen );
	submenu.setAttribute( 'data-open', isOpen );
	siteHeader.setAttribute( 'data-submenu-open', isOpen );

	lastLink.removeEventListener( 'keydown', closeSubmenuKeyboard );
	closeButton.removeEventListener( 'keydown', submenuTrap );

	if ( isKeyboard ) {
		setTimeout(
			() => {
				document.querySelector( `[aria-controls="${id}"][data-submenu="open"]` ).focus();
			},
			250
		);
	}
};

/**
 * Closes submenus on tabbing out
 */
const closeSubmenuKeyboard = ( e ) => {
	const isTabPressed = ( 'Tab' === e.key || 9 === e.keyCode );
	if ( !isTabPressed ) {
		return;
	}

	if ( ! e.shiftKey ) {
		const submenu = e.target.parentNode.parentNode.parentNode;

		if ( submenu ) {
			closeSubmenu( submenu );
		}
	}
};

/**
 * Trap focus inside submenu
 */
const submenuTrap = ( e ) => {
	const isTabPressed = ( 'Tab' === e.key || 9 === e.keyCode );
	if ( !isTabPressed ) {
		return;
	}

	if ( e.shiftKey ) {
		const submenu = e.target.parentNode;

		if ( submenu ) {
			closeSubmenu( submenu, true );
		}
	}
};

/**
 * Closes all submenus
 */
const closeAllSubmenus = () => {
	const submenus = Array.from( document.querySelectorAll( '.menu-item__wrapper' ) );

	submenus.forEach( submenu => { closeSubmenu( submenu ); } );
};

/**
 * Convert link to button
 * @param {object} link the node link to convert
 */
const createButtonFromLink = ( link ) => {

	const button = document.createElement( 'button' );
	const span = document.createElement( 'span' );

	button.setAttribute( 'type', 'button' );
	span.textContent = link.textContent;

	button.appendChild( span );

	return button;
};

/**
 * Init
 */
const init = () => {

	// Bail early
	if ( ! headerMenu ) {
		return;
	}

	setupMarkup();
	updateMarkup();
	setupListeners();
	mq.addListener( updateMarkup );
	siteHeader.classList.add( 'loaded' );
};

export default init;
