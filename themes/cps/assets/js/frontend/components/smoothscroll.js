const siteHeader = document.querySelector( '.site-header' );

/**
 * Link click handler
 *
 * @returns {null}
 */
const linkClickHandler = ( e ) => {
	if ( 'A' !== e.target.tagName ) {
		return;
	}

	if (
		( e.target.href && -1 != e.target.href.indexOf( '#' ) ) &&
		( ( e.target.pathname == location.pathname ) || ( `/${  e.target.pathname}` == location.pathname ) ) &&
		( e.target.search == location.search )
	) {
		scrollAnchors( e, e.target );
	}
};

/**
 * Get element distance to top
 *
 * @returns {int}
 */
const distanceToTop = ( el ) => {
	return Math.floor( el.getBoundingClientRect().top );
};

/**
 * Scroll to anchor
 *
 * @returns {null}
 */
const scrollAnchors = ( e, link = null, hash = null ) => {

	if ( e ) {
		e.preventDefault();
	}

	const targetID = ( link ) ? link.getAttribute( 'href' ) : ( hash ) ? hash : this.getAttribute( 'href' );
	const targetAnchor = document.querySelector( targetID );

	if ( !targetAnchor ) {
		return;
	}

	let originalTop = distanceToTop( targetAnchor );
	let siteHeaderHeight = 0;

	// Detect sticky site header height to stop scrolling at the right spot
	if ( siteHeader ) {
		siteHeaderHeight = siteHeader.getBoundingClientRect().height + 44;
		originalTop -= siteHeaderHeight;
	}

	window.scrollBy( { top: originalTop, left: 0, behavior: 'smooth' } );

	const checkIfDone = setInterval( () => {
		const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
		if ( 0 === parseInt( distanceToTop( targetAnchor ) - siteHeaderHeight, 10 ) || atBottom ) {
			targetAnchor.tabIndex = '-1';
			targetAnchor.focus();

			clearInterval( checkIfDone );
		}
	}, 100 );
};

/**
 * Init smooth scroll
 *
 * @returns {null}
 */
const smoothScroll = () => {
	const { location: { hash } } = window;

	document.addEventListener( 'click', linkClickHandler );

	// Client request to prevent the default browser behavior when loading a page with a hash
	// Need to reset the browser scroll
	// Then smooth scroll to the target
	if ( hash ) {
		window.scrollTo( 0, 0 );
		setTimeout( () => {
			window.scrollTo( 0, 0 ) ;
			scrollAnchors( null, null, hash );
		}, 1 );
	}
};

export default smoothScroll;
