/* global cpsLoadMore */
const { apiFetch } = wp;
const { addQueryArgs } = wp.url;

const loadMoreButtons = document.querySelectorAll( '.load-more button' );
const container       = document.getElementById( 'load-more-ajax-container' );

// Set nonce to be able to authenticate our requests in the rest handler in php.
if (
	'undefined' !== typeof cpsLoadMore &&
	'undefined' !== typeof cpsLoadMore.nonce
) {
	apiFetch.use( apiFetch.createNonceMiddleware( cpsLoadMore.nonce ) );
}

/**
 * Parse API response.
 *
 * @param {Object} response Response object.
 *
 * @return {Object} Parsed API response.
 */
const parseResponse = response =>
	response.json ? response.json() : Promise.reject( response );

/**
 * Enable button.
 *
 * @param {Element} button Submit button.
 */
const enableButton = button => {
	button.disabled = false;
	button.parentNode.classList.remove( 'loading' );
};

/**
 * Disable button.
 *
 * @param {Element} button Submit button.
 */
const disableButton = button => {
	button.disabled = true;
	button.parentNode.classList.add( 'loading' );
};

/**
 * Load more
 *
 * @param {Object} e The event object.
 * @returns {null}
 */
const loadMoreHandler = async e => {

	e.preventDefault();

	const button = e.target;
	const buttonWrapper = button.parentNode;

	// Disable button.
	disableButton( button );

	const queryArgs = {
		page: cpsLoadMore.current_page,
		query: cpsLoadMore.posts,
		partial: cpsLoadMore.partial,
	};

	const response = await apiFetch( {
		path: addQueryArgs( '/cps/v1/load-more', queryArgs ),
		parse: false
	} )
		.then( response => {
			return response;
		} )
		.catch( error => {
			enableButton( button );
			console.log( error ); // eslint-disable-line no-console
		} );

	const results = await parseResponse( response );

	if ( results ) {

		cpsLoadMore.current_page++;

		if ( container ) {

			const domparser = new DOMParser();
			const html = domparser.parseFromString( results, 'text/html' );
			const articles = html.querySelectorAll( '.single-item' );

			articles.forEach( article => {
				container.appendChild( article );
			} );
		}

		// Enable button.
		enableButton( button );

		// Hide the Load More button if the current page matches the total pages.
		if ( parseInt( cpsLoadMore.current_page, 10 ) === parseInt( cpsLoadMore.max_page, 10 ) ) {
			buttonWrapper.parentNode.removeChild( buttonWrapper );
		}
	}
};

/**
 * Init load more functionality
 *
 * @returns {null}
 */
export const loadMore = () => {

	if ( ! loadMoreButtons || 'undefined' === typeof cpsLoadMore ) {
		return;
	}

	loadMoreButtons.forEach( loadMoreButton => {
		loadMoreButton.addEventListener( 'click', loadMoreHandler );
	} );

};
