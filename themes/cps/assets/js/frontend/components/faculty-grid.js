import { tns } from 'tiny-slider/src/tiny-slider';
const carousels = document.querySelectorAll( '.faculty-grid' );

/**
 * Init faculty grid slider
 *
 * @returns {null}
 */
export const facultyGridSlider = () => {

	if ( carousels ) {
		carousels.forEach( ( carousel ) => {

			tns( {
				container: carousel.querySelector( '.faculty-grid__slider' ),
				controlsPosition: 'bottom',
				items: 1,
				loop: false,
				gutter: 30,
				slideBy: 1,
				nav: false,
				arrowKeys: true,
				preventScrollOnTouch: true,
				controlsText: [
					'<svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13 25"><polygon fill="currentColor" fill-rule="evenodd" points="205 433 222 433 222 450" transform="scale(-1 1) rotate(45 618.349 -45.806)"/></svg>',
					'<svg xmlns="http://www.w3.org/2000/svg" width="13" height="25" viewBox="0 0 13 25"><polygon fill="currentColor" fill-rule="evenodd" points="1223 433 1240 433 1240 450" transform="rotate(45 1133.849 -1258.948)"/></svg>'
				],
				responsive: {
					768: {
						items: 2,
						slideBy: 1,
					},
					1024: {
						items: 3,
						slideBy: 3,
					}
				},
			} );
		} );
	}
};
