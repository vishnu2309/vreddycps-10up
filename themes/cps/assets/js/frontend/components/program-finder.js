/* global jQuery */

jQuery( document ).ready( function() {
	jQuery( '.js-select2' ).select2( {
		closeOnSelect: true,
		minimumResultsForSearch: Infinity,
		width: 'style'
	} );
} );

const programFinderForm = document.querySelector( '.block-program-finder__form' );

/**
 * Redirects to Program URL
 *
 * @returns {null}
 */
const goToProgram = ( e ) => {
	e.preventDefault();

	const programSelected = jQuery( '.js-select2', programFinderForm ).select2( 'data' );

	if ( programSelected ) {
		const programLink = programSelected[0].id;

		if ( '' !== programLink ) {
			window.location = programLink;
		}
	}
};

if ( programFinderForm ) {
	programFinderForm.addEventListener( 'submit', goToProgram );
}
