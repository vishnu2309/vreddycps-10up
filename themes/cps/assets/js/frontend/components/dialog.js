import A11yDialog from 'a11y-dialog';

/**
 * Dialog / Modal
 */
export default class Dialog {
	/**
	 * Check for modals
	 */
	constructor() {
		this.programDialog = document.getElementById( 'program-dialog' );

		if ( !this.programDialog ) {
			return;
		}

		this.init();
	}

	/**
	 * Setup events
	 */
	init() {
		const dialog = new A11yDialog( this.programDialog );
		dialog.on( 'show', ( dialogEl, e ) => {
			const button = e.target;

			const titleEl = dialogEl.querySelector( '#dialog-title' );
			const descriptionEl = dialogEl.querySelector( '#dialog-description' );
			const creditHighEl = dialogEl.querySelector( '#dialog-credit-high' );
			const creditLowEl = dialogEl.querySelector( '#dialog-credit-low' );

			const title = button.getAttribute( 'data-title' );
			const description = button.getAttribute( 'data-description' );
			const creditHigh = button.getAttribute( 'data-credit-high' );
			const creditLow = button.getAttribute( 'data-credit-low' );

			if ( titleEl ) {
				titleEl.innerText = title;
			}

			if ( descriptionEl ) {
				descriptionEl.innerText = description;
			}

			if ( creditHighEl ) {
				creditHighEl.innerText = creditHigh;
			}

			if ( creditLowEl ) {
				creditLowEl.innerText = creditLow;
			}
		} );
	}
}
