/**
 * The select2.js dependency is only needed for the
 * program finder and faculty directory blocks.
 */
import 'select2';
import './components/program-finder';
import './components/faculty-members-directory-search';
import './components/search-dropdown';
import './pages/news-events';

import mainNavigation from './components/main-navigation';
import smoothScroll from './components/smoothscroll';
import Scrollspy from './components/scrollspy';
import ButtonToggle from './components/button-toggle';
import { facultyMembersDirectory } from './components/faculty-members-directory';
import { facultyGridSlider } from './components/faculty-grid';
import { carousel } from './components/carousel';
import Accordion from '@10up/component-accordion';
import { loadMore } from './components/load-more';
import Dialog from './components/dialog';

mainNavigation();
smoothScroll();
new Scrollspy();
new ButtonToggle();
facultyMembersDirectory();
facultyGridSlider();
carousel();
loadMore();

new Accordion( '.js-block-accordion' );
new Dialog();

const isIE11 = '-ms-scroll-limit' in document.documentElement.style && '-ms-ime-align' in document.documentElement.style;

if ( isIE11 ) {
	document.body.classList.add( 'ie11' );
}

// Open the Program Overview on load
const programOverview = document.getElementById( 'program-overview' );
if ( programOverview ) {
	const accordionHeader = programOverview.querySelector( '.accordion-header:first-of-type' );
	if ( accordionHeader ) {
		accordionHeader.click();
	}
}
