/* global jQuery */

jQuery( document ).ready( function() {
	jQuery( '.news_event--filters--select' ).select2( {
		closeOnSelect: true,
		minimumResultsForSearch: Infinity,
		theme: 'default search-dropdown'
	} );

	/**
	 * Redirects to events or news landing page
	 *
	 * @returns {null}
	 */
	jQuery( '.news_event--filters--select' ).on( 'select2:select', function( e ) {

		if ( 'js-news_event--filters--topic' === e.target.id ) {

			if ( e.params.data.id ) {
				window.location = `?topic=${e.params.data.id}`;
			} else {
				const { href } = window.location;
				const [newsLocation] = [href.split( '?' )[0]];
				window.location =  newsLocation;
			}
		} else {

			if ( e.params.data.id ) {
				window.location = `?topic=events&month=${e.params.data.id}`;
			} else {
				window.location = '?topic=events';
			}
		}
	} );
} );
