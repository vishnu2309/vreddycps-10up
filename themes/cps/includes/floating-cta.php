<?php
/**
 * Floating CTA functionality.
 *
 * @package CPS
 */

namespace CPS\FloatingCTA;

/**
 * Add actions and filters.
 *
 * @return void
 */
function setup() {
	/*
	 * Add floating-cta links to the content.
	 *
	 * Kept priority as 8, because WP converts the content into block markup on 9 priority.
	 */
	add_action( 'the_content', __NAMESPACE__ . '\add_floating_cta_links', 8 );
}

/**
 * Append floating-cta links after the first hero block on home/program-detail pages.
 *
 * @param atring $content Post content.
 *
 * @return string|string[]|null
 */
function add_floating_cta_links( $content ) {

	global $post;

	// If it's not front-page or program-detail page, bail out early.
	if ( ! is_front_page() && ! is_singular( 'cps-program' ) ) {
		return $content;
	}

	// If content does not have hero block, bail out.
	if ( ! has_block( 'cps/hero', $post ) ) {
		return $content;
	}

	ob_start();

	include locate_template( 'partials/floating-cta.php' );

	// Append Floating CTA links after the first her block.
	return preg_replace( '/<!--\swp:cps\/hero.*?\/-->/m', '\0' . ob_get_clean(), $content, 1 );
}
