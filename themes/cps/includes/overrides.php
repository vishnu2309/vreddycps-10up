<?php
/**
 * This file contains hooks and functions that override the behavior of WP Core.
 *
 * @package CPS\Overrides
 */

namespace CPS\Overrides;

/**
 * Registers instances where we will override default WP Core behavior.
 *
 * @link https://developer.wordpress.org/reference/functions/print_emoji_detection_script/
 * @link https://developer.wordpress.org/reference/functions/print_emoji_styles/
 * @link https://developer.wordpress.org/reference/functions/wp_staticize_emoji/
 * @link https://developer.wordpress.org/reference/functions/wp_staticize_emoji_for_email/
 * @link https://developer.wordpress.org/reference/functions/wp_generator/
 * @link https://developer.wordpress.org/reference/functions/wlwmanifest_link/
 * @link https://developer.wordpress.org/reference/functions/rsd_link/
 *
 * @return void
 */
function setup() {
	// Remove the Emoji detection script.
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

	// Remove inline Emoji detection script.
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );

	// Remove Emoji-related styles from front end and back end.
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Remove Emoji-to-static-img conversion.
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	add_filter( 'tiny_mce_plugins', __NAMESPACE__ . '\disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', __NAMESPACE__ . '\disable_emoji_dns_prefetch', 10, 2 );

	// Remove WordPress generator meta.
	remove_action( 'wp_head', 'wp_generator' );

	// Remove Windows Live Writer manifest link.
	remove_action( 'wp_head', 'wlwmanifest_link' );

	// Remove the link to Really Simple Discovery service endpoint.
	remove_action( 'wp_head', 'rsd_link' );

	// Remove wp-embed.js assuming we will not be embedding content from other WordPress blogs.
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\remove_wp_embed', 9999 );

	// Disable welcome panel.
	remove_action( 'welcome_panel', 'wp_welcome_panel' );

	// Remove WordPress Events and News widget from the dashboard.
	add_action( 'wp_network_dashboard_setup', __NAMESPACE__ . '\remove_core_events_and_news_widget', 20 );
	add_action( 'wp_user_dashboard_setup', __NAMESPACE__ . '\remove_core_events_and_news_widget', 20 );
	add_action( 'wp_dashboard_setup', __NAMESPACE__ . '\remove_core_events_and_news_widget', 20 );

	// Disable opening block editor in full screen mode by default.
	add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\disable_editor_fullscreen_by_default' );

	// Disable Yoast plugin structured data blocks.
	add_filter( 'wpseo_enable_structured_data_blocks', '__return_false' );

	// Filter disallowed block-types.
	add_filter( 'disallowed_block_types', __NAMESPACE__ . '\disallowed_block_types', 10, 2 );

	/**
	 * Show value inputs in WPForms Dropdown, checkboxes and Multiple Choice fields.
	 *
	 * @link https://wpforms.com/developers/add-field-values-for-dropdown-checkboxes-and-multiple-choice-fields/
	 */
	add_filter( 'wpforms_fields_show_options_setting', '__return_true' );

	// Enable unfiltered_html capability for Editors.
	add_filter( 'map_meta_cap', __NAMESPACE__ . '\add_unfiltered_html_capability_to_editors', 1, 3 );

	// Allow iframes in wp_kses_post, mostly to preserve videos when copied in a program description.
	add_filter( 'wp_kses_allowed_html', __NAMESPACE__ . '\wp_kses_allow_iframes', 10, 2 );
}

/**
 * Filter function used to remove the TinyMCE emoji plugin.
 *
 * @link https://developer.wordpress.org/reference/hooks/tiny_mce_plugins/
 *
 * @param  array $plugins An array of default TinyMCE plugins.
 * @return array          An array of TinyMCE plugins, without wpemoji.
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) && in_array( 'wpemoji', $plugins, true ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	}

	return $plugins;
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @link https://developer.wordpress.org/reference/hooks/emoji_svg_url/
 *
 * @param  array  $urls          URLs to print for resource hints.
 * @param  string $relation_type The relation type the URLs are printed for.
 * @return array                 Difference betwen the two arrays.
 */
function disable_emoji_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' === $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_values( array_diff( $urls, array( $emoji_svg_url ) ) );
	}

	return $urls;
}

/**
 * Remove wp-embed.js assuming we will not be embedding content from other WordPress blogs.
 */
function remove_wp_embed() {

	if ( ! is_admin() ) {
		wp_deregister_script( 'wp-embed' );
	}
}

/**
 * Remove WordPress Events and News widget from the dashboard.
 */
function remove_core_events_and_news_widget() {
	remove_meta_box( 'dashboard_primary', get_current_screen(), 'side' );
}

/**
 * Disable opening block editor in full screen mode by default.
 *
 * See: https://make.wordpress.org/core/2020/03/03/fullscreen-mode-enabled-by-default-in-the-editor/
 *
 * @since 1.0.0
 * @return void
 */
function disable_editor_fullscreen_by_default() {

	$script = "window.onload = function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } }";

	wp_add_inline_script( 'wp-blocks', $script );
}

/**
 * Disallow listed block-types from being inserted in the editor.
 *
 * @param bool|array $disallowed_block_types Array of block type slugs, or
 *                                           boolean to enable/disable all.
 * @param \WP_Post   $post                   The post resource data.
 *
 * @return string[]
 */
function disallowed_block_types( $disallowed_block_types, $post ) {

	return array(
		'core/verse',               // Verse (core)
		'core/cover',               // Cover (core)
		'core/media-text',          // Media & Text (core)
		'core/navigation',          // Navigation (core) (Both horizontal & vertical)
		'core/archives',            // Archives (core)
		'core/calendar',            // Calendar (core)
		'core/categories',          // Categories (core)
		'core/latest-comments',     // Latest Comments (core)
		'core/latest-posts',        // Latest Posts (core)
		'core/rss',                 // RSS (core)
		'core/search',              // Search (core)
		'core/social-links',        // Social Icons (core)
		'core/tag-cloud',           // Tag Cloud (core)
		'core-embed/wordpress',     // WordPress (core)
		'core-embed/wordpress-tv',  // WordPress.tv (core)
		'core-embed/soundcloud',    // SoundCloud (core)
		'core-embed/spotify',       // Spotify (core)
		'core-embed/flickr',        // Flickr (core)
		'core-embed/animoto',       // Animoto (core)
		'core-embed/cloudup',       // Cloudup (core)
		'core-embed/crowdsignal',   // Crowdsignal (core)
		'core-embed/dailymotion',   // Dailymotion (core)
		'core-embed/hulu',          // Hulu (core)
		'core-embed/imgur',         // Imgur (core)
		'core-embed/issuu',         // Issuu (core)
		'core-embed/kickstarter',   // Kickstarter (core)
		'core-embed/meetup-com',    // Meetup.com (core)
		'core-embed/mixcloud',      // Mixcloud (core)
		'core-embed/reddit',        // Reddit (core)
		'core-embed/reverbnation',  // ReverbNation (core)
		'core-embed/screencast',    // Screencast (core)
		'core-embed/scribd',        // Scribd (core)
		'core-embed/smugmug',       // SmugMug (core)
		'core-embed/tiktok',        // TikTok (core)
		'core-embed/tumblr',        // Tumblr (core)
		'core-embed/videopress',    // VideoPress (core)
		'core-embed/amazon-kindle', // Amazon Kindle (core)
	);
}

/**
 * Enable unfiltered_html capability for Editors.
 *
 * @param array  $caps The user's capabilities.
 * @param string $cap Capability name.
 * @param int    $user_id The user ID.
 * @return array $caps The user's capabilities, with 'unfiltered_html' potentially added.
 */
function add_unfiltered_html_capability_to_editors( $caps, $cap, $user_id ) {

	if ( 'unfiltered_html' === $cap && user_can( $user_id, 'editor' ) ) {
		$caps = array( 'unfiltered_html' );
	}

	return $caps;
}

/**
 * Allow iframes in wp_kses_post, mostly to preserve videos when copied in a program description.
 *
 * @param array  $tags Allowed tags, attributes, and/or entities.
 * @param string $context Context to judge allowed tags by. Allowed values are 'post'.
 *
 * @return array
 */
function wp_kses_allow_iframes( $tags, $context ) {

	if ( 'post' === $context ) {
		$tags['iframe'] = array(
			'allow'                 => true,
			'allowfullscreen'       => true,
			'frameborder'           => true,
			'height'                => true,
			'id'                    => true,
			'itemprop'              => true,
			'itemscope'             => true,
			'itemtype'              => true,
			'mozallowfullscreen'    => true,
			'src'                   => true,
			'webkitallowfullscreen' => true,
			'width'                 => true,
		);
	}

	return $tags;
}
