<?php
/**
 * General helper functions.
 *
 * @package CPS
 */

namespace CPS\Helpers;

/**
 * Check if the post has a hero block set, optionally as first item.
 *
 * @param int|WP_Post $post           Post ID or WP_Post object.
 * @param bool        $as_first_block Is the hero block the first block, default true.
 * @return bool
 */
function has_hero_block( $post = null, $as_first_block = true ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return false;
	}

	$has_block = has_block( 'cps/hero', $post );

	if ( $has_block && $as_first_block ) {

		$blocks = parse_blocks( $post->post_content );

		if ( 'cps/hero' !== $blocks[0]['blockName'] ) {
			$has_block = false;
		}
	}

	return $has_block;
}
