<?php
/**
 * Functionality that interacts with the NU FIM plugin
 *
 * The NU FIM plugin imports faculty members from the FIM API.
 *
 * @package CPS
 */

namespace CPS\Fim;

// Return the person post type name to override the default 'faculty' name.
add_filter( 'nu_fim_post_type', __NAMESPACE__ . '\filter_fim_post_type', 9 );

/**
 * Return the person post type name to override the default 'faculty' name.
 *
 * @since 1.0.0
 * @param  string $post_type_name Post type name provided by FIM API plugin.
 * @return string Post type name.
 */
function filter_fim_post_type( $post_type_name ) {

	return 'cps-person';
}
