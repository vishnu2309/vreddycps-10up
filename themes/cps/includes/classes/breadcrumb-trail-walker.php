<?php
/**
 * Contains a replacement breadcrumb trail class
 *
 * The breadcrumbs are generated using the Breadcrumb Trail must-use plugin.
 *
 * @package CPS
 */

/**
 * Creates a breadcrumbs menu for the site based on the current page that's being viewed by the user.
 *
 * @access public
 */
class CPS_Breadcrumb_Trail extends Breadcrumb_Trail {

	/**
	 * Magic method to use in case someone tries to output the layout object as a string.
	 * We'll just return the trail HTML.
	 *
	 * @access public
	 * @return string
	 */
	public function __toString() {
		return $this->trail();
	}

	/**
	 * Sets up the breadcrumb trail properties.  Calls the `Breadcrumb_Trail::add_items()` method
	 * to creat the array of breadcrumb items.
	 *
	 * @access public
	 * @param  array $args Options.
	 * @return void
	 */
	public function __construct( $args = array() ) {

		$defaults = array(
			'container'     => 'nav',
			'before'        => '',
			'after'         => '',
			'browse_tag'    => 'h2',
			'list_tag'      => 'ul',
			'item_tag'      => 'li',
			'show_on_front' => true,
			'network'       => false,
			'show_title'    => true,
			'show_browse'   => true,
			'labels'        => array(),
			'post_taxonomy' => array(),
			'echo'          => true,
		);

		// Parse the arguments with the deaults.
		$this->args = apply_filters( 'breadcrumb_trail_args', wp_parse_args( $args, $defaults ) );

		// Set the labels and post taxonomy properties.
		$this->set_labels();
		$this->set_post_taxonomy();

		// Let's find some items to add to the trail!
		$this->add_items();
	}

	/**
	 * Adds a specific post's hierarchy to the items array.  The hierarchy is determined by post type's
	 * rewrite arguments and whether it has an archive page.
	 *
	 * @access protected
	 * @param  int $post_id Post ID.
	 * @return void
	 */
	protected function add_post_hierarchy( $post_id ) {

		// Get the post type.
		$post_type        = get_post_type( $post_id );
		$post_type_object = get_post_type_object( $post_type );

		/*
		 * 10up enhancement:
		 *
		 * We might override the post type archive behaviour.
		 */
		$use_post_type_archive = true;

		// If this is the 'post' post type, get the rewrite front items and map the rewrite tags.
		if ( 'post' === $post_type ) {

			// Add $wp_rewrite->front to the trail.
			$this->add_rewrite_front_items();

			// Map the rewrite tags.
			$this->map_rewrite_tags( $post_id, get_option( 'permalink_structure' ) );

		} elseif ( in_array( $post_type, [ 'cps-event', 'cps-person' ], true ) ) {
			/*
			 * 10up enhancement:
			 *
			 * Add link to Faculty & Staff Directory page for cps-person post type.
			 */
			$page_templates = array(
				'cps-person' => 'faculty_staff_directory', // Defined in /inc/post-types/person/person-post-type.
				'cps-event'  => 'templates/page-news-events.php',
			);

			$cps_page = $this->get_page_by_template( $page_templates[ $post_type ] );

			if ( is_a( $cps_page, '\WP_Post' ) ) {

				$use_post_type_archive = false;

				$this->items[] = sprintf(
					'<a href="%s">%s</a>',
					esc_url( get_the_permalink( $cps_page ) ),
					get_the_title( $cps_page )
				);
			}
		} elseif ( false !== $post_type_object->rewrite ) { // If the post type has rewrite rules.

			// If 'with_front' is true, add $wp_rewrite->front to the trail.
			if ( $post_type_object->rewrite['with_front'] ) {
				$this->add_rewrite_front_items();
			}

			// If there's a path, check for parents.
			if ( ! empty( $post_type_object->rewrite['slug'] ) ) {
				$this->add_path_parents( $post_type_object->rewrite['slug'] );
			}
		}

		// If there's an archive page, add it to the trail.
		if ( $use_post_type_archive && $post_type_object->has_archive ) {

			// Add support for a non-standard label of 'archive_title' (special use case).
			$label = ! empty( $post_type_object->labels->archive_title ) ? $post_type_object->labels->archive_title : $post_type_object->labels->name;

			// Core filter hook.
			$label = apply_filters( 'post_type_archive_title', $label, $post_type_object->name );

			$this->items[] = sprintf( '<a href="%s">%s</a>', esc_url( get_post_type_archive_link( $post_type ) ), $label );
		}

		// Map the rewrite tags if there's a `%` in the slug.
		if ( 'post' !== $post_type && ! empty( $post_type_object->rewrite['slug'] ) && false !== strpos( $post_type_object->rewrite['slug'], '%' ) ) {
			$this->map_rewrite_tags( $post_id, $post_type_object->rewrite['slug'] );
		}
	}

	/**
	 * Get the WP_Post object for the page with the News & Events template.
	 *
	 * @param string $template Page template.
	 * @return WP_Post WP_Post object of the most recent published page that uses the 'News & Events' page template.
	 */
	protected function get_page_by_template( $template ) {

		$args = array(
			'no_found_rows'          => true,
			'meta_key'               => '_wp_page_template',
			'meta_value'             => $template,
			'posts_per_page'         => 1,
			'post_status'            => 'publish',
			'post_type'              => 'page',
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		);

		$query = new \WP_Query( $args );

		if ( $query->have_posts() ) {
			return $query->posts[0];
		}

		return false;
	}
}
