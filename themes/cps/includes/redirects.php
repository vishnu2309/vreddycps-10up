<?php
/**
 * Redirect functionality.
 *
 * @package CPS
 */

namespace CPS\Redirects;

/**
 * Set up redirects.
 *
 * @return void
 */
function setup() {
	add_filter( 'template_redirect', __NAMESPACE__ . '\redirect_old_news_and_events', 10, 2 );
}

/**
 * Redirect old news and events URLs to the new URL structure.
 */
function redirect_old_news_and_events() {

	$server_uri = filter_var( $_SERVER['REQUEST_URI'], FILTER_SANITIZE_STRING );

	// Redirect old news.
	if ( starts_with( $server_uri, '/blog/news/' ) ) {
		$new_location = substr( $server_uri, 10 );
		wp_safe_redirect( home_url( $new_location ), 301 );
		die;
	}

	// Redirect old events.
	if ( starts_with( $server_uri, '/blog/event/' ) ) {
		$new_location = substr( $server_uri, 5 );
		wp_safe_redirect( home_url( $new_location ), 301 );
		die;
	}
}

/**
 * String starts with substring.
 *
 * @param string $string      The string to be checked.
 * @param string $start_string The initial substring.
 *
 * @return bool
 */
function starts_with( $string, $start_string ) {
	$len = strlen( $start_string );
	return ( substr( $string, 0, $len ) === $start_string );
}
