<?php
/**
 * Menu customisation
 *
 * @package CPS\Menu
 */

namespace CPS\Menu;

/**
 * Add actions and filters.
 *
 * @return void
 */
function setup() {

	// Add tertiary menu select to nav menu item.
	add_action( 'wp_nav_menu_item_custom_fields', __NAMESPACE__ . '\add_item_custom_fields', 10, 5 );

	// Save tertiary menu custom field.
	add_action( 'wp_update_nav_menu_item', __NAMESPACE__ . '\update_item_custom_fields', 10, 3 );

	// Add custom field to nav menu item object to make it available in the menu Walker.
	add_filter( 'wp_setup_nav_menu_item', __NAMESPACE__ . '\add_tertiary_menu_id_to_nav_item_object' );

	// Add tertiary menu as a submenu to the menu item.
	add_filter( 'walker_nav_menu_end_el', __NAMESPACE__ . '\maybe_add_tertiary_submenu', 10, 3 );
}

/**
 * Add tertiary menu select to nav menu item.
 *
 * @param int       $item_id Menu item ID.
 * @param \WP_Post  $item    Menu item data object.
 * @param int       $depth   Depth of menu item. Used for padding.
 * @param \stdClass $args    An object of menu item arguments.
 * @param int       $id      Nav menu ID.
 */
function add_item_custom_fields( $item_id, $item, $depth, $args, $id ) {

	if ( 0 === (int) $depth ) :

		$menus = wp_get_nav_menus();
		?>
		<p class="field-cps-submenu description description-wide">
			<label for="edit-menu-item-cps-submenu-<?php echo (int) $item_id; ?>">
				<?php esc_html_e( 'Tertiary Submenu', 'cps' ); ?><br />
				<select id="edit-menu-item-cps-submenu-<?php echo (int) $item_id; ?>" class="widefat edit-menu-item-cps-submenu" name="<?php echo (int) $item_id; ?>_cps_submenu">
					<option value="0">
						<?php esc_html_e( 'None', 'cps' ); ?>
					</option>
					<?php
					foreach ( $menus as $menu ) {

						// Unfortunately $id is always 0, but let's check it anyway in case that ever changes.
						if ( 0 === $id ) {
							$id = filter_input( INPUT_GET, 'menu', FILTER_VALIDATE_INT );
						}

						if ( $id === (int) $menu->term_id ) {
							continue;
						}

						printf(
							'<option value="%d" %s>%s</option>',
							esc_attr( $menu->term_id ),
							selected( absint( $item->cps_submenu_id ), absint( $menu->term_id ), false ),
							esc_html( $menu->name )
						);
					}
					?>
				</select>
			</label>
		</p>
		<?php // This is cheating a bit, but negates the need for adding another hook to render the styles: ?>
		<style>.menu-item:not(.menu-item-depth-0) .field-cps-submenu{display:none;visibility:hidden;}</style>
		<?php

	endif;
}

/**
 * Save tertiary menu custom field.
 *
 * @param int   $menu_id         ID of the updated menu.
 * @param int   $menu_item_db_id ID of the new menu item.
 * @param array $args            An array of arguments used to update/add the menu item.
 */
function update_item_custom_fields( $menu_id, $menu_item_db_id, $args ) {

	$tertiary_menu_id = filter_input( INPUT_POST, "{$menu_item_db_id}_cps_submenu", FILTER_VALIDATE_INT );

	if ( isset( $tertiary_menu_id ) ) {

		if ( ! empty( $tertiary_menu_id ) ) {
			update_post_meta( $menu_item_db_id, 'cps_submenu_id', $tertiary_menu_id );
		} else {
			delete_post_meta( $menu_item_db_id, 'cps_submenu_id' );
		}
	}
}

/**
 * Add custom field to nav menu item object to make it available in the menu Walker.
 *
 * @param obj $menu_item The menu item object.
 *
 * @return mixed
 */
function add_tertiary_menu_id_to_nav_item_object( $menu_item ) {

	$menu_item->cps_submenu_id = get_post_meta( $menu_item->ID, 'cps_submenu_id', true );

	return $menu_item;
}

/**
 * Add tertiary menu as a submenu to the menu item.
 *
 * @param \WP_Post  $item  Menu item data object.
 * @param int       $depth Depth of menu item. Used for padding.
 * @param \stdClass $args  An object of wp_nav_menu() arguments.
 *
 * @return string
 */
function maybe_add_tertiary_submenu( $item, $depth, $args ) {
	$item_output = '';

	// If not main navigation, bail out.
	if ( 'navigation-main' !== $args->theme_location ) {
		return $item_output;
	}

	// Only add to top level items.
	if ( $depth > 0 ) {
		return $item_output;
	}

	// If submenu is not set for the menu item, bail out.
	if ( empty( $item->cps_submenu_id ) ) {
		return $item_output;
	}

	// Append tertiary menu as a submenu to the menu item.
	$item_output .= wp_nav_menu(
		[
			'menu'       => $item->cps_submenu_id,
			'container'  => '',
			'menu_class' => 'sub-menu-tertiary',
			'depth'      => 1,
			'echo'       => false,
		]
	);

	return $item_output;
}
