<?php
/**
 * Functionality that interacts with the NU PIM plugin
 *
 * The NU PIM plugin imports programs from the PIM API.
 *
 * @package CPS
 */

namespace CPS\Pim;

// Return the program post type name to override the default 'program' name.
add_filter( 'nu_pim_post_type', __NAMESPACE__ . '\filter_pim_post_type', 9 );

/**
 * Return the program post type name to override the default 'program' name.
 *
 * @since 1.0.0
 * @param  string $post_type_name Post type name returned by the PIM API plugin.
 * @return string Post type name.
 */
function filter_pim_post_type( $post_type_name ) {

	return 'cps-program';
}
