<?php
/**
 * Contains event custom post type functionality
 *
 * @package CPS
 */

namespace CPS\Event\PostType;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom post type.
	add_action( 'init', __NAMESPACE__ . '\register_post_type' );

	// Filter custom post type updated messages.
	add_filter( 'post_updated_messages', __NAMESPACE__ . '\post_updated_messages' );

	// Add custom post type to 'At a Glance' dashboard widget.
	add_action( 'dashboard_glance_items', __NAMESPACE__ . '\dashboard_glance_items' );

	// Change 'At a Glance' icon.
	add_action( 'admin_print_scripts', __NAMESPACE__ . '\dashboard_glance_items_style' );

	// Filter past events.
	add_action( 'pre_get_posts', __NAMESPACE__ . '\filter_past_events' );

	// Filter WPForms date values used in the event submission form.
	add_filter( 'wpforms_post_submissions_process_meta', __NAMESPACE__ . '\filter_wpforms_event_date', 10, 5 );
}

/**
 * Register event post type.
 */
function register_post_type() {

	$labels = array(
		'name'                     => esc_html__( 'Events', 'cps' ),
		'menu_name'                => esc_html__( 'Events', 'cps' ),
		'singular_name'            => esc_html__( 'Event', 'cps' ),
		'add_new'                  => esc_html__( 'Add New', 'cps' ),
		'add_new_item'             => esc_html__( 'Add Event', 'cps' ),
		'edit_item'                => esc_html__( 'Edit Event', 'cps' ),
		'new_item'                 => esc_html__( 'New Event', 'cps' ),
		'all_items'                => esc_html__( 'All Events', 'cps' ),
		'view_item'                => esc_html__( 'View Event', 'cps' ),
		'view_items'               => esc_html__( 'View Events', 'cps' ),
		'search_items'             => esc_html__( 'Search Events', 'cps' ),
		'not_found'                => esc_html__( 'No events found', 'cps' ),
		'not_found_in_trash'       => esc_html__( 'No events found in the trash', 'cps' ),
		'parent_item_colon'        => null,
		'archives'                 => esc_html__( 'Event Archives', 'cps' ),
		'attributes'               => esc_html__( 'Event Attributes', 'cps' ),
		'insert_into_item'         => esc_html__( 'Insert into content', 'cps' ),
		'uploaded_to_this_item'    => esc_html__( 'Uploaded to this event', 'cps' ),
		'featured_image'           => esc_html__( 'Event image', 'cps' ),
		'set_featured_image'       => esc_html__( 'Set event image', 'cps' ),
		'remove_featured_image'    => esc_html__( 'Remove event image', 'cps' ),
		'use_featured_image'       => esc_html__( 'Use as event image', 'cps' ),
		'filter_items_list'        => esc_html__( 'Filter event list', 'cps' ),
		'items_list_navigation'    => esc_html__( 'Event list navigation', 'cps' ),
		'items_list'               => esc_html__( 'Event list', 'cps' ),
		'item_published'           => esc_html__( 'Event published.', 'cps' ),
		'item_published_privately' => esc_html__( 'Event published privately.', 'cps' ),
		'item_reverted_to_draft'   => esc_html__( 'Event reverted to draft.', 'cps' ),
		'item_scheduled'           => esc_html__( 'Event scheduled.', 'cps' ),
		'item_updated'             => esc_html__( 'Event updated.', 'cps' ),
	);

	$args = array(
		'labels'              => $labels,
		'public'              => true,
		'hierarchical'        => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => false,
		'rest_base'           => 'event',
		'menu_position'       => 50,
		'menu_icon'           => 'dashicons-calendar',
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'supports'            => array(
			'title',
			'editor',
			'thumbnail',
			'cps-event-date-meta-box',
			'cps-event-admin',
		),
		'has_archive'         => 'events-achive',
		'rewrite'             => array(
			'slug'       => 'event',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => true,
		),
		'query_var'           => 'event',
		'can_export'          => true,
		'delete_with_user'    => false,
	);

	\register_post_type( 'cps-event', $args );
}

/**
 * Filter display messages.
 *
 * @param array $messages Post updated messages. For defaults @see wp-admin/edit-form-advanced.php.
 * @return array $messages Modified post updated messages.
 */
function post_updated_messages( $messages ) {

	global $post;

	$messages['event'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => esc_html__( 'Event updated.', 'cps' ),
		2  => esc_html__( 'Custom field updated.', 'cps' ),
		3  => esc_html__( 'Custom field deleted.', 'cps' ),
		4  => esc_html__( 'Event updated.', 'cps' ),
		/* translators: %s: previous revision */
		5  => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Event restored to revision from %s', 'cps' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		6  => esc_html__( 'Event published.', 'cps' ),
		7  => esc_html__( 'Event saved.', 'cps' ),
		8  => esc_html__( 'Event submitted.', 'cps' ),
		/* translators: %s: event date */
		9  => sprintf( esc_html__( 'Event scheduled for: <strong>%1$s</strong>.', 'cps' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
		10 => esc_html__( 'Event draft updated.', 'cps' ),
	);

	return $messages;
}

/**
 * Add custom post type to 'At a Glance' dashboard widget.
 */
function dashboard_glance_items() {

	$post_type_obj = get_post_type_object( 'cps-event' );

	if ( ! $post_type_obj ) {
		return;
	}

	if ( ! current_user_can( $post_type_obj->cap->edit_posts ) ) {
		return;
	}

	$num_posts = wp_count_posts( $post_type_obj->name );

	if ( $num_posts && $num_posts->publish ) {

		// phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralSingle
		// phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralPlural

		/* translators: %s: number of published posts */
		$label = _n(
			'%s ' . $post_type_obj->labels->singular_name,
			'%s ' . $post_type_obj->labels->name,
			$num_posts->publish
		);

		// phpcs:enable WordPress.WP.I18n.NonSingularStringLiteralSingle
		// phpcs:enable WordPress.WP.I18n.NonSingularStringLiteralPlural

		$label = sprintf( $label, number_format_i18n( $num_posts->publish ) );

		printf(
			'<li class="%1$s-count"><a class="%2$s" href="edit.php?post_type=%1$s">%3$s</a></li>',
			esc_attr( $post_type_obj->name ),
			esc_attr( $post_type_obj->menu_icon ),
			esc_html( $label )
		);
	}

	dashboard_glance_items_style( 'cps-event', $post_type_obj->menu_icon );
}

/**
 * Add inline style to change the 'At A Glance' link icon.
 */
function dashboard_glance_items_style() {

	$current_screen = get_current_screen();

	if ( 'dashboard' === $current_screen->base && current_user_can( 'edit_posts' ) ) {
		printf(
			'<style media="all">#dashboard_right_now .%s-count a:before { content: "%s"; }</style>',
			'cps-event',
			'\f145'
		);
	}
}

/**
 * Only allowed past events on the archive.
 *
 * @param \WP_Query $wp_query WP_Query object, passed by reference.
 */
function filter_past_events( $wp_query ) {

	if ( is_admin() || ! $wp_query->is_main_query() || ! is_post_type_archive( 'cps-event' ) ) {
		return;
	}

	$meta_query = $wp_query->get( 'meta_query', [] );

	$meta_query[] = array(
		'key'     => 'cps_event_end',
		'value'   => gmdate( 'Y-m-d H:i:s' ),
		'compare' => '<',
	);

	$wp_query->set( 'meta_query', $meta_query );
}

/**
 * Filter WPForms date values used in the event submission form.
 *
 * @link   https://wpforms.com/developers/wpforms_post_submissions_process_meta/
 *
 * @param  string $field_value  Field value from the form.
 * @param  string $meta_key     Custom field meta key.
 * @param  int    $field_id     Field ID.
 * @param  array  $fields       Sanitized entry field values/properties.
 * @param  array  $form_data    Processed form settings/data, prepared to be used later.
 *
 * @return string
 */
function filter_wpforms_event_date( $field_value, $meta_key, $field_id, $fields, $form_data ) {

	if ( in_array( $meta_key, [ 'cps_event_end', 'cps_event_start' ], true ) && ! empty( $fields[ $field_id ]['unix'] ) ) {
		/*
		 * @todo Update to make sure we're saving the date correctly related to the time zone, the WPForms plugin might use
		 *       the timestamp according to the sites's timezone setting.
		 */
		$field_value = gmdate( 'Y-m-d H:i:s', $fields[ $field_id ]['unix'] );
	}

	return $field_value;
}
