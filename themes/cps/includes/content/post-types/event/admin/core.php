<?php
/**
 * Contains the event core admin functionality
 *
 * @package CPS
 */

namespace CPS\Event\Admin;

/**
 * Enqueue scripts and styles, add date meta box.
 */
function setup() {

	// Add query vars used for filtering events in the admin.
	add_filter( 'query_vars', __NAMESPACE__ . '\add_query_vars' );

	// Modify requests to enable ordering and scope.
	add_action( 'pre_get_posts', __NAMESPACE__ . '\orderby_and_scope' );

	// Add filter dropdown to event post type list in admin.
	add_action( 'restrict_manage_posts', __NAMESPACE__ . '\add_event_scope_select' );

	// Maybe remove quick edit for non-hierarchical event post type.
	add_filter( 'post_row_actions', __NAMESPACE__ . '\remove_quick_edit', 10, 2 );

	// Maybe remove quick edit for hierarchical event post type.
	add_filter( 'page_row_actions', __NAMESPACE__ . '\remove_quick_edit', 10, 2 );

	/*
	 * The "manage_{post_type}_sortable_columns" filter needs a post type.
	 * We don't know the post types with 'cps-event-admin' support at this point
	 * and there is no alternative filter for all post types.
	 * Let's wrap that into a function (and add the other column related functions in it to while we're at it).
	 */
	add_action( 'init', __NAMESPACE__ . '\add_list_table_colums_actions_and_filters', 11 );
}

/**
 * Add actions and filters for custom list table columns.
 *
 * Wrapper for actions and filters hooked into 'init' so we know
 * the post types the filters should be applied to.
 */
function add_list_table_colums_actions_and_filters() {

	$post_types = get_post_types_by_support( 'cps-event-admin' );

	foreach ( $post_types as $post_type ) {

		add_filter(
			'manage_' . $post_type . '_posts_columns',
			__NAMESPACE__ . '\manage_columns'
		);
		add_action(
			'manage_' . $post_type . '_posts_custom_column',
			__NAMESPACE__ . '\manage_custom_columns',
			10,
			2
		);
		add_filter(
			'manage_edit-' . $post_type . '_sortable_columns',
			__NAMESPACE__ . '\manage_sortable_columns'
		);
	}
}

/**
 * Register query vars for the edit.php table filter.
 *
 * @since 0.0.1
 * @param array $query_vars The array of whitelisted query variables.
 */
function add_query_vars( $query_vars ) {
	if ( is_admin() ) {
		$query_vars[] = 'cps_events_event_scope';
	}
	return $query_vars;
}

/**
 * Modify requests to enable ordering and scope.
 *
 * @todo Ignores events without a date.
 *
 * @param WP_Query $wp_query WP_Query object, passed by reference.
 */
function orderby_and_scope( $wp_query ) {

	if ( ! is_admin() ) {
		return;
	}

	if ( ! $wp_query->is_main_query() ) {
		return;
	}

	$current_screen = get_current_screen();

	if ( 'edit' !== $current_screen->base ) {
		return;
	}

	if ( ! in_array( $current_screen->post_type, get_post_types_by_support( 'cps-admin' ), true ) ) {
		return;
	}

	if ( isset( $wp_query->query_vars['orderby'] ) ) {
		if ( in_array( $wp_query->query_vars['orderby'], array( 'event_start', 'event_end' ), true ) ) {
			$wp_query->set( 'meta_key', $wp_query->query_vars['orderby'] );
			$wp_query->set( 'orderby', 'meta_value' );
		}
	} else {
		$wp_query->set( 'meta_key', 'event_start' );
		$wp_query->set( 'orderby', 'meta_value' );
		$wp_query->set( 'order', 'DESC' );
	}

	// Show past, present, future or all events?
	if ( isset( $wp_query->query_vars['cps_events_event_scope'] ) ) {

		$now = current_time( 'mysql', 0 );

		switch ( $wp_query->query_vars['cps_events_event_scope'] ) {

			case 'past':
				$wp_query->set( 'meta_key', 'event_end' );
				$wp_query->set( 'meta_value', $now );
				$wp_query->set( 'meta_compare', '<' );
				break;
			case 'current':
				$wp_query->set(
					'meta_query',
					array(
						array(
							'key'     => 'event_start',
							'value'   => $now,
							'compare' => '<=',
						),
						array(
							'key'     => 'event_end',
							'value'   => $now,
							'compare' => '>',
						),
					)
				);
				break;
			default:
				$wp_query->set( 'meta_key', 'event_start' );
				$wp_query->set( 'meta_value', $now );
				$wp_query->set( 'meta_compare', '>' );
		}
	}
}

/**
 * Output <select> element to filter events in admin.
 *
 * Adds a dropdown filter to edit.php which allows displaying upcoming, current, past or all events.
 * Via: http://wordpress.stackexchange.com/questions/45/how-to-sort-the-admin-area-of-a-wordpress-custom-post-type-by-a-custom-field
 */
function add_event_scope_select() {

	$current_screen = get_current_screen();

	if ( 'edit' !== $current_screen->base ) {
		return;
	}

	if ( ! in_array( $current_screen->post_type, get_post_types_by_support( 'cps-admin' ), true ) ) {
		return;
	}

	$post_type_obj = get_post_type_object( $current_screen->post_type );

	$options = array(
		'all'      => esc_html( $post_type_obj->labels->all_items ),
		'upcoming' => esc_html__( 'Upcoming', 'cps' ),
		'current'  => esc_html__( 'Current', 'cps' ),
		'past'     => esc_html__( 'Past', 'cps' ),
	);

	$event_scope = 'all';

	if ( get_query_var( 'cps_events_event_scope' ) ) {
		$event_scope = get_query_var( 'cps_events_event_scope' );
	}

	$html = '<select name="cps_events_event_scope">';

	foreach ( $options as $k => $v ) {
		$html .= "<option value='{$k}' " . selected( $k, $event_scope, false ) . ">{$v}</option>";
	}

	$html .= '</select>';

	echo $html; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Maybe remove 'quick edit' action.
 *
 * @param array   $actions An array of row action links.
 * @param WP_Post $post    The post object.
 */
function remove_quick_edit( $actions, $post ) {

	if ( 'cps-event' === $post->post_type ) {
		unset( $actions['inline hide-if-no-js'] );
	}

	return $actions;
}

/**
 * Add start and end date columns.
 *
 * Inserts the columns at the end of the table and adds the post date column after that if it exists.
 *
 * @param array $post_columns An array of column names.
 */
function manage_columns( $post_columns ) {

	$date_column = false;

	if ( array_key_exists( 'date', $post_columns ) ) {
		$date_column = $post_columns['date'];
		unset( $post_columns['date'] );
	}

	$date_columns = array(
		'event-start' => esc_html__( 'Event Start', 'cps' ),
		'event-end'   => esc_html__( 'Event End', 'cps' ),
	);

	$post_columns = array_merge( $post_columns, $date_columns );

	if ( $date_column ) {
		$post_columns['date'] = $date_column;
	}

	return $post_columns;
}

/**
 * Content for custom columns.
 *
 * @param string $column_name Column name, eg. 'date'.
 * @param int    $post_id     Post ID.
 */
function manage_custom_columns( $column_name, $post_id ) {

	if ( ! in_array( $column_name, array( 'event-start', 'event-end' ), true ) ) {
		return;
	}

	$post = get_post( $post_id );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return;
	}

	$datestr = '-';

	if ( 'event-start' === $column_name && ! empty( $post->cps_event_start ) ) {
		$datestr = date_i18n( get_option( 'date_format' ), strtotime( $post->cps_event_start ) );
	}

	if ( 'event-end' === $column_name && ! empty( $post->cps_event_end ) ) {
		$datestr = date_i18n( get_option( 'date_format' ), strtotime( $post->cps_event_end ) );
	}

	echo esc_html( $datestr );
}

/**
 * Make our columns sortable.
 *
 * @param array $sortable_columns An array of sortable columns.
 */
function manage_sortable_columns( $sortable_columns ) {

	$sortable_columns['event-start'] = 'event_start';
	$sortable_columns['event-end']   = 'event_end';

	return $sortable_columns;
}
