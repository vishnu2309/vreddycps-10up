<?php
/**
 * Contains the meta box content
 *
 * @package Pehub_Events
 */

?>

<table>
	<tbody>
		<?php if ( false === $all_day_disable ) : ?>
			<tr>
				<td>
					<label for="event-all-day">
						<?php esc_html_e( 'All Day', 'cps' ); ?>
					</label>
				</td>
				<td colspan="2">
					<input id="event-all-day" name="event-all-day" type="checkbox" <?php checked( $all_day_event, 1 ); ?> />
				</td>
			</tr>
		<?php endif; ?>

		<tr>
			<td>
				<label for="event-start-string">
					<?php esc_html_e( 'From', 'cps' ); ?>
				</label>
			</td>
			<td>
				<input id="event-start-string" name="event-start-string" type="text" value="<?php echo esc_attr( mysql2date( 'm/d/Y', $start ) ); ?>" readonly="true"  />
				<input id="event-start" name="event-start" type="hidden" value="<?php echo esc_attr( mysql2date( 'Y-m-d', $start ) ); ?>" />
			</td>
			<?php $cps_all_day_visible_class = $all_day_event ? '' : 'all-day-visible'; ?>
			<td class="all-day-hidden <?php echo esc_attr( $cps_all_day_visible_class ); ?>">

				<input class="integer" id="event-start-hh" name="event-start-hh" type="text" size="2" maxlength="2" min="0" max="23" value="<?php echo esc_attr( $start_time[0] ); ?>" />
				:
				<input class="integer" id="event-start-mm" name="event-start-mm" type="text" size="2" maxlength="2" min="0" max="59" value="<?php echo esc_attr( $start_time[1] ); ?>" />

				<?php if ( 12 === (int) $clock ) : ?>
					<select name="event-start-ampm">
						<option value="am" <?php selected( $start_ampm, 'am' ); ?>>am</option>
						<option value="pm" <?php selected( $start_ampm, 'pm' ); ?>>pm</option>
					</select>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td>
				<label for="event-end-string">
					<?php esc_html_e( 'To', 'cps' ); ?>
				</label>
			</td>
			<td>
				<input id="event-end-string" name="event-end-string" type="text" value="<?php echo esc_attr( mysql2date( 'm/d/Y', $end ) ); ?>" readonly="true" />
				<input id="event-end" name="event-end" type="hidden" value="<?php echo esc_attr( mysql2date( 'Y-m-d', $end ) ); ?>" />
			</td>
			<td class="all-day-hidden <?php echo esc_attr( $cps_all_day_visible_class ); ?>">

				<input class="integer" id="event-end-hh" name="event-end-hh" type="text" size="2" maxlength="2" min="0" max="24" value="<?php echo esc_attr( $end_time[0] ); ?>" />
				:
				<input class="integer" id="event-end-mm" name="event-end-mm" type="text" size="2" maxlength="2" min="0" max="59" value="<?php echo esc_attr( $end_time[1] ); ?>" />

				<?php if ( 12 === (int) $clock ) : ?>
					<select name="event-end-ampm">
						<option value="am" <?php selected( $end_ampm, 'am' ); ?>>am</option>
						<option value="pm" <?php selected( $end_ampm, 'pm' ); ?>>pm</option>
					</select>
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<td>
				<label for="event-timezone">
					<?php esc_html_e( 'Timezone', 'cps' ); ?>
				</label>
			</td>
			<td colspan="2">
				<select id="cps_event_timezone" name="event-timezone">
				<?php
				for ( $i = -12; $i <= 12; $i++ ) {

					$utc_time = ( $i >= 0 ) ? 'UTC+' . $i : 'UTC' . $i;

					printf(
						'<option value="%s" %s>%s</option>',
						esc_attr( $utc_time ),
						selected( $timezone, $utc_time, false ),
						esc_html( $utc_time )
					);
				}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label for="event-campus">
					<?php esc_html_e( 'Campus', 'cps' ); ?>
				</label>
			</td>
			<td colspan="2">
				<input id="cps_event_campus" name="event-campus" type="text" size="60" value="<?php echo esc_html( $campus ); ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="event-location">
					<?php esc_html_e( 'Location', 'cps' ); ?>
				</label>
			</td>
			<td colspan="2">
				<textarea id="cps_event_location" name="event-location" type="text" cols="60" rows="6"><?php echo wp_kses_post( $location ); ?></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label for="event-public">
					<?php esc_html_e( 'Public event', 'cps' ); ?>
				</label>
			</td>
			<td colspan="2">
				<input id="cps_event_public" name="event-public" type="checkbox" <?php checked( $public, 1 ); ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="event-link">
					<?php esc_html_e( 'Link', 'cps' ); ?>
				</label>
			</td>
			<td colspan="2">
				<input id="event-link" name="event-link" type="text" size="60" value="<?php echo esc_url( $link ); ?>" />
			</td>
		</tr>
	</tbody>
</table>
