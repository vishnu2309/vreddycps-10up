<?php
/**
 * Contains functionality for the event date meta box
 *
 * @package CPS
 */

namespace CPS\Event\Admin\MetaBox;

use CPS\Event\Helpers as Helpers;

/**
 * Enqueue scripts and styles, add date meta box.
 */
function setup() {

	// Register and enqueue admin-specific JavaScript.
	add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\enqueue_scripts' );

	// Add a date meta box to the custom post type edit screen.
	add_action( 'add_meta_boxes', __NAMESPACE__ . '\add_date_meta_box' );

	// Render the date meta box.
	add_action( 'save_post', __NAMESPACE__ . '\save_date_meta_box', 10, 2 );
}

/**
 * Register and enqueue admin-specific JavaScript.
 *
 * @param string $hook_suffix The current admin page.
 * @return null Return early if no settings page is registered.
 */
function enqueue_scripts( $hook_suffix ) {

	if ( ! in_array( $hook_suffix, array( 'post-new.php', 'post.php' ), true ) ) {
		return;
	}

	$current_screen = get_current_screen();

	if ( ! in_array( $current_screen->post_type, get_post_types_by_support( 'cps-event-date-meta-box' ), true ) ) {
		return;
	}

	$options = Helpers\get_event_options();

	wp_enqueue_script(
		'cps-events',
		CPS_TEMPLATE_URL . '/dist/js/admin-events.js',
		array( 'jquery-ui-datepicker' ),
		CPS_VERSION,
		true
	);

	wp_localize_script(
		'cps-events',
		'eventsVars',
		array(
			'disableAllDay' => $options['all_day_disable'],
			'firstDay'      => get_option( 'start_of_week' ),
		)
	);
}

/**
 * Add a date meta box to the custom post type edit screen.
 */
function add_date_meta_box() {

	$options = Helpers\get_event_options();

	add_meta_box(
		'cps-event-date',
		$options['title'],
		__NAMESPACE__ . '\render_date_meta_box',
		'cps-event',
		$options['context'],
		$options['priority']
	);
}

/**
 * Render the date meta box.
 *
 * @param WP_Post $post WP_Post object.
 */
function render_date_meta_box( $post ) {

	wp_nonce_field( 'cps_events_save_date_nonce', 'cps_events_save_date_' . $post->ID );

	$options = Helpers\get_event_options();

	$all_day_disable = $options['all_day_disable'];
	$all_day_event   = Helpers\is_all_day_event( $post );

	$start = empty( $post->cps_event_start ) ? current_time( 'Y-m-d' ) . ' ' . $options['default_start_time'] : $post->cps_event_start;
	$end   = empty( $post->cps_event_end ) ? current_time( 'Y-m-d' ) . ' ' . $options['default_end_time'] : $post->cps_event_end;

	$start_ampm = 'am';
	$end_ampm   = $start_ampm;
	$clock      = get_clock_type();

	if ( $all_day_event ) {
		$start_time = explode( ':', $options['default_start_time'] );
		$end_time   = explode( ':', $options['default_end_time'] );
	} else {
		$start_time = explode( ':', substr( $start, 11, 5 ) );
		$end_time   = explode( ':', substr( $end, 11, 5 ) );
	}

	if ( 12 === (int) $clock ) {
		if ( $start_time[0] > 12 ) {
			$start_time[0] = str_pad( $start_time[0] - 12, 2, '0', STR_PAD_LEFT );
			$start_ampm    = 'pm';
		}
		if ( $end_time[0] > 12 ) {
			$end_time[0] = str_pad( $end_time[0] - 12, 2, '0', STR_PAD_LEFT );
			$end_ampm    = 'pm';
		}
	}

	$timezone     = empty( $post->cps_event_timezone ) ? 'UTC-5' : $post->cps_event_timezone;
	$campus       = empty( $post->cps_event_campus ) ? '' : $post->cps_event_campus;
	$location     = empty( $post->cps_event_location ) ? '' : $post->cps_event_location;
	$public       = empty( $post->cps_event_public ) ? '' : $post->cps_event_public;
	$link         = empty( $post->cps_event_link ) ? '' : $post->cps_event_link;
	$author_email = empty( $post->cps_event_author_email ) ? '' : $post->cps_event_author_email;
	$comments     = empty( $post->cps_event_author_comments ) ? '' : $post->cps_event_author_comments;

	require_once CPS_INC . 'content/post-types/event/admin/partials/meta-box.php';
}

/**
 * Save event date.
 *
 * @since 0.0.1
 *
 * @todo Error handling and date validation.
 *
 * @param int     $post_id ID of the post in question.
 * @param WP_Post $post    WP_Post object.
 */
function save_date_meta_box( $post_id, $post ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! in_array( $post->post_type, get_post_types_by_support( 'cps-event-date-meta-box' ), true ) ) {
		return;
	}

	if ( ! isset( $_POST[ 'cps_events_save_date_' . $post_id ] ) ) { // WPCS: input var okay.
		return;
	}

	if ( ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST[ 'cps_events_save_date_' . $post_id ] ) ), 'cps_events_save_date_nonce' ) ) { // WPCS: input var okay.
		return;
	}

	$post_type_obj = get_post_type_object( $post->post_type );

	if ( ! current_user_can( $post_type_obj->cap->edit_post, $post_id ) ) {
		return;
	}

	$postdata = $_POST; // WPCS: input var okay.

	// Maybe turn 12 hour into 24 hour.
	if ( isset( $postdata['event-start-ampm'] ) ) {
		if ( 'pm' === $postdata['event-start-ampm'] ) {
			$postdata['event-start-hh'] = $postdata['event-start-hh'] + 12;
		}
		if ( 'pm' === $postdata['event-end-ampm'] ) {
			$postdata['event-end-hh'] = $postdata['event-end-hh'] + 12;
		}
	}

	// Define start and end times depending on whether the 'All Day' option is selected.
	if ( isset( $postdata['event-all-day'] ) ) {
		$start_time = ' 00:00:00';
		$end_time   = ' 23:59:59';
	} else {
		$start_time = sprintf( ' %02d:%02d:00', $postdata['event-start-hh'], $postdata['event-start-mm'] );
		$end_time   = sprintf( ' %02d:%02d:00', $postdata['event-end-hh'], $postdata['event-end-mm'] );
	}

	// Timezone.
	if ( isset( $postdata['event-timezone'] ) ) {
		$event_timezone = esc_attr( $postdata['event-timezone'] );
	} else {
		$event_timezone = '';
	}

	// Campus.
	if ( isset( $postdata['event-campus'] ) ) {
		$event_campus = esc_attr( $postdata['event-campus'] );
	} else {
		$event_campus = '';
	}

	// Location.
	if ( isset( $postdata['event-location'] ) ) {
		$event_location = esc_attr( $postdata['event-location'] );
	} else {
		$event_location = '';
	}

	// Public event.
	if ( isset( $postdata['event-public'] ) ) {
		$event_public = 1;
	} else {
		$event_public = '';
	}

	// Event link.
	if ( isset( $postdata['event-link'] ) ) {
		$event_link = esc_url_raw( $postdata['event-link'] );
	} else {
		$event_link = '';
	}

	// Event author email.
	if ( isset( $postdata['event-author-email'] ) ) {
		$event_author_email = sanitize_email( $postdata['event-author-email'] );
	} else {
		$event_author_email = '';
	}

	// Author Comments.
	if ( isset( $postdata['event-author-comments'] ) ) {
		$event_comments = wp_kses_post( $postdata['event-author-comments'] );
	} else {
		$event_comments = '';
	}

	// Update metadata.
	update_post_meta( $post_id, 'cps_event_start', $postdata['event-start'] . $start_time );
	update_post_meta( $post_id, 'cps_event_end', $postdata['event-end'] . $end_time );
	update_post_meta( $post_id, 'cps_event_timezone', $event_timezone );
	update_post_meta( $post_id, 'cps_event_campus', $event_campus );
	update_post_meta( $post_id, 'cps_event_location', $event_location );
	update_post_meta( $post_id, 'cps_event_public', $event_public );
	update_post_meta( $post_id, 'cps_event_link', $event_link );
	update_post_meta( $post_id, 'cps_event_author_email', $event_author_email );
	update_post_meta( $post_id, 'cps_event_author_comments', $event_comments );

	return $post_id;
}

/**
 * Is the event an all day event?
 *
 * @param int|WP_Post $post Post ID or WP_Post object.
 * @return bool True if all day, else false.
 */
function is_all_day_event( $post ) {

	$post = get_post( $post );

	if ( ! $post ) {
		return false;
	}

	$all_day_event = false;

	$options = Helpers\get_event_options();

	if ( false === $options['all_day_disable'] ) {

		if ( empty( $post->cps_event_start ) || empty( $post->cps_event_end ) ) {
			$all_day_event = $options['all_day_checked'];
		} elseif ( substr( $post->cps_event_start, 11, 8 ) === '00:00:00' && substr( $post->cps_event_end, 11, 8 ) === '23:59:59' ) {
			$all_day_event = true;
		} else {
			$all_day_event = false;
		}
	}

	return $all_day_event;
}

/**
 * Get clock type, 12 or 24 hour.
 *
 * @return string Clock type, 12 or 24.
 */
function get_clock_type() {

	$options = Helpers\get_event_options();

	if ( 'auto' === $options['clock'] ) {
		if ( strtolower( substr( trim( get_option( 'time_format' ) ), -1 ) ) === 'a' ) {
			$clock = 12;
		} else {
			$clock = 24;
		}
	} else {
		$clock = $options['clock'];
	}

	return $clock;
}
