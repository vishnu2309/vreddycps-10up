<?php
/**
 * Contains functionality for ICS file generator for the event.
 *
 * @package CPS
 */

namespace CPS\Event\ICSGenerator;

/**
 * Add actions and filters.
 */
function setup() {

	// Add ICS rewrite rule to handle event/event-name/ics URLs.
	add_action( 'init', __NAMESPACE__ . '\add_ics_rewrite_rule' );

	// Add "ics" query var.
	add_filter( 'query_vars', __NAMESPACE__ . '\add_ics_query_var' );

	// Handle ICS file generation.
	add_action( 'template_redirect', __NAMESPACE__ . '\output_ics_file' );
}

/**
 * Add ICS rewrite rule to handle event/event-name/ics URLs.
 */
function add_ics_rewrite_rule() {

	add_rewrite_rule(
		'event/(.*?)/ics$',
		'index.php?post_type=cps-event&name=$matches[1]&ics=1',
		'top'
	);
}

/**
 * Add custom "ics" query var to handle the ICS file generation request.
 *
 * @param array $query_vars The array of whitelisted query variable names
 *
 * @return mixed
 */
function add_ics_query_var( $query_vars ) {

	// Add "ics" query var.
	$query_vars[] = 'ics';

	return $query_vars;
}

/**
 * Generate ICS file with event's data and output it.
 */
function output_ics_file() {

	// If "ics" query var is not hit, bail out.
	if ( empty( get_query_var( 'ics' ) ) ) {
		return;
	}

	// Get current event.
	$event = get_post( get_the_ID() );

	// If no event found or start-date/end-date is not set, bail out.
	if ( empty( $event ) || empty( $event->cps_event_start ) || empty( $event->cps_event_end ) ) {
		return;
	}

	// Generate ICS format calendar data.
	$ical  = "BEGIN:VCALENDAR\r\n";
	$ical .= "VERSION:2.0\r\n";
	$ical .= 'PRODID:-//' . get_bloginfo( 'name' ) . "//NONSGML v1.0//EN\r\n";
	$ical .= "CALSCALE:GREGORIAN\r\n";
	$ical .= "METHOD:PUBLISH\r\n";
	$ical .= 'BEGIN:VEVENT' . "\r\n";
	$ical .= 'DTSTART:' . gmdate( 'Ymd\THis\Z', strtotime( $event->cps_event_start ) ) . "\r\n";
	$ical .= 'DTEND:' . gmdate( 'Ymd\THis\Z', strtotime( $event->cps_event_end ) ) . "\r\n";
	$ical .= 'UID:' . esc_html( wp_generate_uuid4() ) . "\r\n";
	$ical .= 'SUMMARY:' . wp_strip_all_tags( $event->post_title ) . "\r\n";

	if ( ! empty( $event->cps_event_link ) ) {
		$ical .= "DESCRIPTION:{$event->cps_event_link}\r\n";
	}

	$ical .= 'URL:' . get_permalink( $event ) . "\r\n";
	$ical .= "END:VEVENT\r\n";
	$ical .= "END:VCALENDAR\r\n\r\n";

	header( 'HTTP/1.0 200 OK', true, 200 );
	header( 'Content-type: text/calendar; charset=UTF-8' );
	header( 'Content-Disposition: attachment; filename="' . $event->post_name . '.ics"' );

	exit( $ical ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}
