<?php
/**
 * Contains events helper functions
 *
 * @package CPS
 */

namespace CPS\Event\Helpers;

/**
 * Is the event an all day event?
 *
 * @param int|\WP_Post $post Post ID or WP_Post object.
 * @return bool True if all day, else false.
 */
function is_all_day_event( $post ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return false;
	}

	if ( substr( $post->cps_event_start, 11, 8 ) === '00:00:00' && substr( $post->cps_event_end, 11, 8 ) === '23:59:59' ) {
		return true;
	}

	return false;
}

/**
 * Get event options.
 *
 * Applies the 'cps_events_options' filter.
 */
function get_event_options() {

	$defaults = array(
		'all_day_disable'    => true,
		'all_day_checked'    => true, // 'All day' is checked by default
		'default_start_time' => '10:00',
		'default_end_time'   => '17:00',
		'clock'              => 'auto', // 12 (Shows am/pm select), 24 or auto
		'title'              => esc_html__( 'Event Info', 'cps' ),
		'context'            => 'normal',
		'priority'           => 'high',
		'meta_box_title'     => false,
	);

	return apply_filters( 'cps_events_options', $defaults );
}

/**
 * Display or return event timings.
 *
 * @param bool $echo Whether to display or return the datetime.
 *
 * @return false|string|void
 */
function cps_the_event_date( $echo = true ) {

	$event           = get_post( get_the_ID() );
	$start_timestamp = strtotime( $event->cps_event_start );
	$end_timestamp   = strtotime( $event->cps_event_end );

	/*
	 * If event is not found, bail out.
	 *
	 * TODO: Temporary bailing out even when start date-time or end date-time is not found.
	 *
	 */
	if ( empty( $event ) || empty( $start_timestamp ) || empty( $end_timestamp ) ) {
		return;
	}

	if ( is_all_day_event( $event ) ) {
		$event_datetime = gmdate( 'l F j', $start_timestamp );
	} elseif ( substr( $event->cps_event_start, 0, 10 ) === substr( $event->cps_event_end, 0, 10 ) ) {
		$event_datetime = gmdate( 'l F j, g:ia', $start_timestamp ) . gmdate( ' - g:ia', $end_timestamp );
	} else {
		$event_datetime = gmdate( 'l F j, g:ia', $start_timestamp ) . gmdate( ' - l F j, g:ia', $end_timestamp );
	}

	if ( $echo ) {
		echo esc_html( $event_datetime );
	} else {
		return $event_datetime;
	}
}
