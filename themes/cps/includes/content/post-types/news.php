<?php
/**
 * News post type
 *
 * Note: The core 'post' post type is used for news.
 *
 * @package CPS
 */

namespace CPS\News;

/**
 * Add actions and filters.
 *
 * @return void
 */
function setup() {

	// Use default 'post' post type for News.
	add_action( 'init', __NAMESPACE__ . '\re_register_post_type' );

	// Filter updated messages for the 'post' post type.
	add_filter( 'post_updated_messages', __NAMESPACE__ . '\post_updated_messages' );

	// Remove support for tags.
	add_action( 'init', __NAMESPACE__ . '\remove_taxonomy_support', 999 );

	// Remove post related help tabs.
	add_action( 'admin_head-post.php', __NAMESPACE__ . '\remove_help_tabs', 10, 3 );
	add_action( 'admin_head-edit.php', __NAMESPACE__ . '\remove_help_tabs', 10, 3 );

	// Filter the results for the link query used in the insert link dialog.
	add_filter( 'wp_link_query', __NAMESPACE__ . '\filter_link_query_results', 10, 2 );

	// Filter the label shown on the 'At a Glance' dashboard widget by adding a filter to ngettext on the dashboard.
	add_action( 'admin_head-index.php', __NAMESPACE__ . '\add_ngettext_filter_on_dashboard' );
}

/**
 * Use default 'post' post type for News.
 *
 * @global $wp_post_types
 * @global $_wp_post_type_features
 * @return void
 */
function re_register_post_type() {

	global $wp_post_types, $_wp_post_type_features;

	// Unregister core 'post' post type.
	if ( isset( $wp_post_types['post'] ) ) {
		unset( $wp_post_types['post'] );
	}

	// Remove 'post' support features, these will be re-added later on.
	if ( isset( $_wp_post_type_features['post'] ) ) {
		unset( $_wp_post_type_features['post'] );
	}

	$labels = [
		'name'               => __( 'News', 'cps' ),
		'name_admin_bar'     => _x( 'News Item', 'add new on admin bar', 'cps' ),
		'singular_name'      => __( 'News', 'cps' ),
		'add_new'            => __( 'Add New', 'cps' ),
		'add_new_item'       => __( 'Add New Item', 'cps' ),
		'edit_item'          => __( 'Edit Item', 'cps' ),
		'new_item'           => __( 'New News', 'cps' ),
		'all_items'          => __( 'All News', 'cps' ),
		'view_item'          => __( 'View News Item', 'cps' ),
		'search_items'       => __( 'Search News', 'cps' ),
		'not_found'          => __( 'No news found', 'cps' ),
		'not_found_in_trash' => __( 'No news found in the trash', 'cps' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'News', 'cps' ),
		'archive'            => __( 'All News', 'cps' ),
	];

	$args = [
		'labels'           => $labels,
		'public'           => true,
		'_builtin'         => true,
		'_edit_link'       => 'post.php?post=%d',
		'capability_type'  => 'post',
		'map_meta_cap'     => true,
		'menu_icon'        => 'dashicons-admin-post',
		'menu_position'    => 5,
		'hierarchical'     => false,
		'has_archive'      => true,
		'rewrite'          => [ 'slug' => 'post' ],
		'query_var'        => 'post',
		'delete_with_user' => false,
		'show_in_rest'     => true,
		'supports'         => [
			'title',
			'editor',
			'revisions',
			'thumbnail',
		],
		'taxonomies'       => [
			'category',
		],
	];

	\register_post_type( 'post', $args ); // phpcs:ignore WordPress.NamingConventions.ValidPostTypeSlug.Reserved
}

/**
 * Filter display messages.
 *
 * We're redefining the messages for the 'post' post type. The original messages
 * are defined in edit-form-advanced.php
 *
 * @param array $messages Post updated messages. For defaults @see wp-admin/edit-form-advanced.php.
 * @return array $messages Modified post updated messages.
 */
function post_updated_messages( $messages ) {

	global $post;

	$permalink = get_permalink( $post->ID );

	$messages['post'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: listing edit link */
		1  => sprintf( __( 'Item updated. <a href="%s">View news item</a>', 'cps' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'cps' ),
		3  => __( 'Custom field deleted.', 'cps' ),
		4  => __( 'News updated.', 'cps' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'News restored to revision from %s', 'cps' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore
		/* translators: %s: post link */
		6  => sprintf( __( 'News published. <a href="%s">View news item</a>', 'cps' ), esc_url( $permalink ) ),
		7  => __( 'News saved.', 'cps' ),
		/* translators: %s: post preview link */
		8  => sprintf( __( 'News submitted. <a target="_blank" href="%s">Preview news item</a>', 'cps' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: %1$s: date and time of the revision, %2$s: post preview link */
		9  => sprintf( __( 'News scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview news item</a>', 'cps' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post preview link */
		10 => sprintf( __( 'News draft updated. <a target="_blank" href="%s">Preview item</a>', 'cps' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

/**
 * Remove support for tags.
 */
function remove_taxonomy_support() {

	$taxonomies = [
		'post_tag',
	];

	foreach ( $taxonomies as $taxonomy ) {
		\unregister_taxonomy_for_object_type( $taxonomy, 'post' );
	}
}

/**
 * Remove default help tabs when viewing edit.php or post.php.
 *
 * The help tabs use references to the 'post' post type which we're hijacking for news.
 */
function remove_help_tabs() {

	$current_screen = get_current_screen();
	$ids            = array();

	switch ( $current_screen->id ) {
		case 'post':
			$ids = array(
				'customize-display',
				'inserting-media',
				'publish-box',
				'discussion-settings',
				'title-post-editor',
			);
			break;
		case 'edit-post':
			$ids = array(
				'overview',
				'screen-content',
				'action-links',
				'bulk-actions',
			);
			break;
	}

	foreach ( $ids as $id ) {
		if ( $current_screen->get_help_tab( $id ) ) {
			$current_screen->remove_help_tab( $id );
		}
	}
}

/**
 * Filter the link query results.
 *
 * The dialog for internal linking in the tinymce editor shows the post date next to posts
 * with type 'post'. We're change that to the post type name as it is done with other post types.
 *
 * @see 'wp_link_query_args' filter
 *
 * @param array $results {
 *     An associative array of query results.
 *
 *     @type array {
 *         @type int    $ID        Post ID.
 *         @type string $title     The trimmed, escaped post title.
 *         @type string $permalink Post permalink.
 *         @type string $info      A 'Y/m/d'-formatted date for 'post' post type,
 *                                 the 'singular_name' post type label otherwise.
 *     }
 * }
 * @param array $query  An array of WP_Query arguments.
 */
function filter_link_query_results( $results, $query ) {

	if ( empty( $results ) ) {
		return $results;
	}

	$get_posts = new \WP_Query();
	$posts     = $get_posts->query( $query );

	if ( empty( $posts ) ) {
		return $posts;
	}

	$posts         = wp_list_pluck( $posts, 'post_type', 'ID' );
	$post_type_obj = get_post_type_object( 'post' );
	$info          = esc_html( $post_type_obj->labels->singular_name );
	$count         = 0;

	foreach ( $results as $result ) {
		if ( 'post' === $posts[ $result['ID'] ] ) {
			$results[ $count ]['info'] = $info;
		}
		$count++;
	}

	return $results;
}

/**
 * Add ngettext filter on the Dashboard to filter the 'At a Glance' 'post' post type labels.
 *
 * Although the ngettext filter probably won't be applied on many occasions,
 * wrapping it in an action on the Dashboard limits its use.
 */
function add_ngettext_filter_on_dashboard() {
	add_filter( 'ngettext', __NAMESPACE__ . '\filter_ngettext', 10, 5 );
}

/**
 * Filter the '%s Post' and '%s Posts' labels used in the 'At a Glance' dashboard widget.
 *
 * The labels are hardcoded into the widget and unfortunately do not use the values defined in the post type object.
 *
 * @see  https://core.trac.wordpress.org/ticket/26066
 *
 * @param string $translation Translated text.
 * @param string $single      The text to be used if the number is singular.
 * @param string $plural      The text to be used if the number is plural.
 * @param string $number      The number to compare against to use either the singular or plural form.
 * @param string $domain      Text domain. Unique identifier for retrieving translated strings.
 * @return string Original or modified translation.
 */
function filter_ngettext( $translation, $single, $plural, $number, $domain ) {

	if ( ! is_admin() || '%s Post' !== $single || '%s Posts' !== $plural ) {
		return $translation;
	}
	/* translators: number of products */
	return _n( '%s News Item', '%s News Items', $number, 'cps' );
}
