<?php
/**
 * Contains the footer contact core admin functionality
 *
 * @package CPS
 */

namespace CPS\Contact\Admin;

/**
 * Add actions and filters.
 */
function setup() {

	// Maybe remove 'quick edit' and 'trash' actions for non-hierarchical contact post type.
	add_filter( 'post_row_actions', __NAMESPACE__ . '\remove_quick_edit_and_trash', 10, 2 );

	// Maybe remove 'quick edit' and 'trash' actions for hierarchical contact post type.
	add_filter( 'page_row_actions', __NAMESPACE__ . '\remove_quick_edit_and_trash', 10, 2 );

	// Maybe add 'Default' flag to admin post list.
	add_filter( 'display_post_states', __NAMESPACE__ . '\maybe_add_post_state', 10, 2 );
}

/**
 * Maybe remove 'quick edit' and 'trash' actions.
 *
 * @param array   $actions An array of row action links.
 * @param WP_Post $post    The post object.
 * @return array
 */
function remove_quick_edit_and_trash( $actions, $post ) {

	if ( 'cps-contact-block' === $post->post_type ) {

		unset( $actions['inline hide-if-no-js'] );

		$default_post_id = get_option( 'cps_default_contact_block', 0 );

		if ( (int) $post->ID === (int) $default_post_id ) {
			unset( $actions['trash'] );
		}
	}

	return $actions;
}

/**
 * Maybe add 'Default' flag to admin post list.
 *
 * @param string[] $post_states An array of post display states.
 * @param WP_Post  $post        The current post object.
 * @return array
 */
function maybe_add_post_state( $post_states, $post ) {

	if ( 'cps-contact-block' === $post->post_type ) {

		$default_post_id = get_option( 'cps_default_contact_block', 0 );

		if ( (int) $post->ID === (int) $default_post_id ) {
			$post_states['default'] = __( 'Default', 'cps' );
		}
	}

	return $post_states;
}
