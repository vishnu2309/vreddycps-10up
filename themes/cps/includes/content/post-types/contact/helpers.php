<?php
/**
 * Contains footer contact post type helper functions
 *
 * @package CPS
 */

namespace CPS\Contact\Helpers;

/**
 * Get footer contact HTML.
 *
 * @return string
 */
function get_footer_contact_block() {

	$footer_id = 0;

	if ( is_singular() ) {
		$footer_id = get_post_meta( get_the_ID(), 'cps_contact_id', true );
	} else {
		// @todo Add conditionals for other content types.
		$footer_id = false;
	}

	if ( 'none' === $footer_id ) {
		return;
	}

	if ( empty( $footer_id ) ) {
		$footer_id = get_option( 'cps_default_contact_block' );
	}

	if ( empty( $footer_id ) ) {
		return;
	}

	$contact_block_post = get_post( $footer_id );

	if ( ! $contact_block_post ) {
		return;
	}

	if ( 'publish' !== $contact_block_post->post_status ) {
		return;
	}

	return do_blocks( $contact_block_post->post_content );
}
