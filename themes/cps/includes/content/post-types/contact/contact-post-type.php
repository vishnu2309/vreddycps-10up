<?php
/**
 * Contains footer contact custom post type functionality
 *
 * @package CPS
 */

namespace CPS\Contact\PostType;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom post type.
	add_action( 'init', __NAMESPACE__ . '\register_post_type' );

	// Filter custom post type updated messages.
	add_filter( 'post_updated_messages', __NAMESPACE__ . '\post_updated_messages' );

	// Register 'cps_contact_block_id' custom field so we can use it in the block editor.
	add_action( 'init', __NAMESPACE__ . '\register_contact_block_id_post_meta', 99 );

	// Add global JS variable for the contact block select.
	add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\register_contact_block_js_var', 99 );

	// Register setting for the default contact block so we can use it in the block editor.
	add_action( 'init', __NAMESPACE__ . '\register_default_contact_block_setting' );

	// Filter the title field placeholder text.
	add_filter( 'enter_title_here', __NAMESPACE__ . '\enter_title_here', 10, 2 );
}

/**
 * Register contact block post type.
 */
function register_post_type() {

	$labels = array(
		'name'                     => esc_html__( 'Contact Blocks', 'cps' ),
		'menu_name'                => esc_html__( 'Contact', 'cps' ),
		'singular_name'            => esc_html__( 'Contact Block', 'cps' ),
		'add_new'                  => esc_html__( 'Add New', 'cps' ),
		'add_new_item'             => esc_html__( 'Add New Contact Block', 'cps' ),
		'edit_item'                => esc_html__( 'Edit Contact Block', 'cps' ),
		'new_item'                 => esc_html__( 'New Contact Block', 'cps' ),
		'view_item'                => esc_html__( 'View Contact Block', 'cps' ),
		'view_items'               => esc_html__( 'View Contact Blocks', 'cps' ),
		'search_items'             => esc_html__( 'Search Contact Blocks', 'cps' ),
		'not_found'                => esc_html__( 'No contact blocks found', 'cps' ),
		'not_found_in_trash'       => esc_html__( 'No contact blocks found in the trash', 'cps' ),
		'parent_item_colon'        => null,
		'all_items'                => esc_html__( 'All Contact Blocks', 'cps' ),
		'archives'                 => esc_html__( 'Contact Block Archives', 'cps' ),
		'attributes'               => esc_html__( 'Contact Block Attributes', 'cps' ),
		'insert_into_item'         => esc_html__( 'Insert into copy text', 'cps' ),
		'uploaded_to_this_item'    => esc_html__( 'Uploaded to this contact block', 'cps' ),
		'uploaded_to_this_item'    => esc_html__( 'Uploaded to this contact block', 'cps' ),
		'featured_image'           => esc_html__( 'Contact image', 'cps' ),
		'set_featured_image'       => esc_html__( 'Set contact image', 'cps' ),
		'remove_featured_image'    => esc_html__( 'Remove contact image', 'cps' ),
		'use_featured_image'       => esc_html__( 'Use as contact image', 'cps' ),
		'filter_items_list'        => esc_html__( 'Filter contact items list', 'cps' ),
		'items_list_navigation'    => esc_html__( 'Contact Block list navigation', 'cps' ),
		'items_list'               => esc_html__( 'Contact Blocks list', 'cps' ),
		'item_published'           => esc_html__( 'Contact Block published.', 'cps' ),
		'item_published_privately' => esc_html__( 'Contact Block published privately.', 'cps' ),
		'item_reverted_to_draft'   => esc_html__( 'Contact Block reverted to draft.', 'cps' ),
		'item_scheduled'           => esc_html__( 'Contact Block scheduled.', 'cps' ),
		'item_updated'             => esc_html__( 'Contact Block updated.', 'cps' ),
	);

	$args = array(
		'labels'              => $labels,
		'description'         => esc_html__( 'This post type is used to render contact information in the site\'s footer', 'cps' ),
		'public'              => false,
		'hierarchical'        => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true, // Enable block editor.
		'menu_position'       => 60,
		'menu_icon'           => 'dashicons-info',
		'supports'            => array(
			'editor',
			'title',
		),
		'has_archive'         => false,
		'rewrite'             => false,
		'template'            => array(
			array(
				'cps/footer-contact',
			),
		),
		'template_lock'       => 'all',
	);

	\register_post_type( 'cps-contact-block', $args );
}

/**
 * Filter display messages.
 *
 * @param array $messages Post updated messages. For defaults @see wp-admin/edit-form-advanced.php.
 * @return array $messages Modified post updated messages.
 */
function post_updated_messages( $messages ) {

	global $post;

	$messages['cps-contact-block'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => esc_html__( 'Contact block updated.', 'cps' ),
		2  => esc_html__( 'Custom field updated.', 'cps' ),
		3  => esc_html__( 'Custom field deleted.', 'cps' ),
		4  => esc_html__( 'Contact block updated.', 'cps' ),
		/* translators: %s: previous revision */
		5  => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Contact block restored to revision from %s', 'cps' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		6  => esc_html__( 'Contact Block published.', 'cps' ),
		7  => esc_html__( 'Contact Block saved.', 'cps' ),
		8  => esc_html__( 'Contact Block submitted.', 'cps' ),
		/* translators: %s: Contact Block date */
		9  => sprintf( esc_html__( 'Contact Block scheduled for: <strong>%1$s</strong>.', 'cps' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
		10 => esc_html__( 'Contact Block draft updated.', 'cps' ),
	);

	return $messages;
}

/**
 * Register 'cps_contact_block_id' custom field so we can use it in the block editor.
 *
 * @return void
 */
function register_contact_block_id_post_meta() {

	$args = array(
		'public' => true,
	);

	$post_types = get_post_types( $args, 'names' );

	foreach ( $post_types as $post_type ) {

		if ( 'attachment' === $post_type ) {
			continue;
		}

		register_post_meta(
			$post_type,
			'cps_contact_block_id',
			array(
				'show_in_rest' => true,
				'single'       => true,
				'type'         => 'string',
			)
		);
	}
}

/**
 * Add global JS variable for the contact block select.
 *
 * @return void
 */
function register_contact_block_js_var() {

	if ( ! is_admin() ) {
		return;
	}

	$posts = array();
	$args  = array(
		'post_type'   => 'cps-contact-block',
		'post_status' => 'any',
	);

	$query = new \WP_Query( $args );

	if ( $query->have_posts() ) {
		foreach ( $query->posts as $post ) {

			$label = empty( $post->post_title ) ? 'No Title' : $post->post_title;
			$label = 'publish' !== $post->post_status ? $label . ' [' . $post->post_status . ']' : $label;

			$posts[] = [
				'value' => $post->ID,
				'label' => $label,
			];
		}
	}

	wp_localize_script(
		'cps-blocks-editor',
		'cpsContactBlockVars',
		array(
			'options' => $posts,
		)
	);
}

/**
 * Register setting for the default contact block so we can use it in the block editor
 *
 * @return void
 */
function register_default_contact_block_setting() {

	register_setting(
		'cps_settings',
		'cps_default_contact_block',
		array(
			'type'         => 'string',
			'show_in_rest' => true,
			// phpcs:ignore
			// 'sanitize_callback' => 'absint',
		)
	);
}

/**
 * Filter the title field placeholder text.
 *
 * @param string  $text Placeholder text. Default 'Add title'.
 * @param WP_Post $post Post object.
 * @return string
 */
function enter_title_here( $text, $post ) {

	if ( 'cps-contact-block' === $post->post_type ) {
		$text = __( 'Add block name', 'cps' );
	}

	return $text;
}
