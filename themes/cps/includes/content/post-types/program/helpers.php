<?php
/**
 * Contains program post type helper functions
 *
 * @package CPS
 */

namespace CPS\Program\Helpers;

/**
 * Get related programs via field_program_family.
 *
 * @param int $post_id Post ID.
 * @return array A list of WP_Post objects.
 */
function get_related_programs( $post_id ) {

	// Note: there is also a related_program_2 field.
	$program_ids = get_post_meta( $post_id, 'nu_pim_program_family', true );

	if ( empty( $program_ids ) || ! is_array( $program_ids ) ) {
		return [];
	}

	$args = array(
		'post_type'   => 'cps-program',
		'post_status' => 'publish',
		'meta_query'  => array(
			array(
				'key'     => 'nu_pim_id',
				'value'   => $program_ids,
				'compare' => 'IN',
			),
		),
	);

	$query    = new \WP_Query( $args );
	$programs = [];

	if ( $query->have_posts() ) {

		foreach ( $query->posts as $program ) {

			if ( (int) $program->ID !== (int) $post_id ) {
				$programs[] = $program;
			}
		}
	}

	return $programs;
}

/**
 * Get enrollment, entry terms and completion time column values for the program detail page.
 *
 * @param int|WP_Post $post Post ID or WP_Post object.
 * @return array
 */
function get_program_icon_columns( $post = null ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	$columns     = array();
	$enrollment  = get_post_meta( $post->ID, 'nu_pim_commitment', true );
	$entry_terms = get_post_meta( $post->ID, 'nu_pim_entry_terms', true );
	$duration    = get_post_meta( $post->ID, 'nu_pim_duration', true );

	if ( ! empty( $enrollment ) ) {

		$enrollment_values = [];

		foreach ( $enrollment as $value ) {
			$enrollment_values[] = $value['name']['0']['value'];
		}

		if ( ! empty( $enrollment_values ) ) {
			$columns[] = array(
				'title' => __( 'Enrollment', 'cps' ),
				'icon'  => 'enrollment',
				'value' => join( ', ', $enrollment_values ),
			);
		}
	}

	if ( ! empty( $entry_terms ) ) {

		$entry_term_values = [];

		foreach ( $entry_terms as $value ) {
			$entry_term_values[] = $value['name']['0']['value'];
		}

		if ( ! empty( $entry_term_values ) ) {
			$columns[] = array(
				'title' => __( 'Entry Terms', 'cps' ),
				'icon'  => 'calendar',
				'value' => join( ', ', $entry_term_values ),
			);
		}
	}

	if ( ! empty( $duration ) ) {
		$columns[] = array(
			'title' => __( 'Completion Time', 'cps' ),
			'icon'  => 'clock',
			'value' => $duration,
		);
	}

	return $columns;
}

/**
 * Get program short description, either from the nu_pim_body field or its override value.
 *
 * @param int|WP_Post $post Post ID or WP_Post object.
 * @return array
 */
function get_program_short_description( $post = null ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	$description = get_post_meta( $post->ID, 'cps_program_description_short', true );

	if ( ! empty( $description ) ) {
		return $description;
	}

	$description = get_post_meta( get_the_ID(), 'nu_pim_body', true );

	return $description;
}


/**
 * Get program description, either from the nu_pim_program_description_long field or its override value.
 *
 * @param int|WP_Post $post Post ID or WP_Post object.
 * @return array
 */
function get_program_long_description( $post = null ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	/*
	 * Good to know: The field is called 'nu_pim_program_description_long_alt', but it does not come from the PIM API,
	 *               we're adding it via a meta box created by the Fieldmanager plugin. The naming convention used
	 *               for this field makes it easier to include the original 'nu_pim_program_description_long' field
	 *               in the meta box.
	 */
	$desciption = get_post_meta( $post->ID, 'nu_pim_program_description_long_alt', true );

	if ( ! empty( $desciption ) ) {
		return $desciption;
	}

	$desciption = get_post_meta( get_the_ID(), 'nu_pim_program_description_long', true );
	$desciption = str_replace( '<hr />', '', $desciption );

	return $desciption;
}

/**
 * Get HTML that renders the requirements tables.
 *
 * @todo Add caching.
 *
 * @param int|WP_Post $post WP_Post object or post ID.
 * @return string
 */
function get_requirements( $post = null ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	$post_meta = get_post_meta( $post->ID, 'nu_pim_curriculum', true );

	if ( empty( $post_meta ) ) {
		return '';
	}

	$requirements_text   = $post_meta[0]['field_level_1_group']['entities'][0]['field_group_level_2']['entities'][0]['field_requirements'][0]['value'];
	$requirements_tables = $post_meta[0]['field_level_1_group']['entities'][0]['field_group_level_2']['entities'][0]['field_course_table']['entities'];

	$tables = [];

	if ( is_array( $requirements_tables ) ) {
		foreach ( $requirements_tables as $k => $requirements_table ) {
			$tables[] = get_requirements_table_html( $requirements_table['field_courses']['entities'] );
		}
	}

	/*
	 * The requirements text contains placeholders
	 * that represent a table, eg. [course_table id=0].
	 *
	 * Replace those with the table HTML.
	 */
	foreach ( $tables as $k => $table_html ) {
		$requirements_text = str_replace( '[course_table id=' . $k . ']', $table_html, $requirements_text );
	}

	return $requirements_text;
}

/**
 * Get a list of items to render the concentrations tables.
 *
 * @todo Add caching.
 *
 * @param int|WP_Post $post WP_Post object or post ID.
 * @return array A list of items that contain a title and preformatted HTML content.
 */
function get_concentrations( $post = null ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	$post_meta = get_post_meta( $post->ID, 'nu_pim_curriculum', true );

	if ( empty( $post_meta ) ) {
		return '';
	}

	if ( empty( $post_meta[0]['field_level_1_group']['entities'][1] ) ) {
		return [];
	}

	$result = [];
	$items  = $post_meta[0]['field_level_1_group']['entities'][1]['field_group_level_2']['entities'];

	if ( is_array( $items ) ) {

		foreach ( $items as $item ) {

			// @todo check if array keys exists before using them.
			$title    = $item['title'][0]['value'];
			$content  = $item['field_requirements'][0]['value'];
			$entities = $item['field_course_table']['entities'][0]['field_courses']['entities'];
			$tables   = $item['field_course_table']['entities'];

			if ( is_array( $tables ) ) {
				foreach ( $tables as $k => $table ) {

					$entities   = $table['field_courses']['entities'];
					$table_html = get_requirements_table_html( $entities );
					$content    = str_replace( '[course_table id=' . $k . ']', $table_html, $content );
				}
			}

			$result[] = array(
				'title'   => $title,
				'content' => $content,
			);
		}
	}

	return $result;
}

/**
 * Get table HTML from API data.
 *
 * @todo Validate if keys are present in the data before using them.
 *
 * @param array $entities Single table data array from the curriculum field.
 * @return string Formatted HTML table.
 */
function get_requirements_table_html( $entities ) {

	$html = '<table class="requirements-table"><tbody>';

	foreach ( $entities as $entity ) {

		$credit_low  = 0;
		$credit_high = 0;

		if ( is_array( $entity['field_credit_low'] ) && isset( $entity['field_credit_low'][0]['value'] ) ) {
			$credit_low = $entity['field_credit_low'][0]['value'];
		}

		if ( is_array( $entity['field_credit_high'] ) && isset( $entity['field_credit_high'][0]['value'] ) ) {
			$credit_high = $entity['field_credit_high'][0]['value'];
		}

		if ( $credit_low === $credit_high || empty( floatval( $credit_high ) ) ) {
			$credit_high = $credit_low;
			$credit_low  = '';
		}

		$html .= sprintf(
			'<tr><td><button type="button" class="requirements-table__button" data-a11y-dialog-show="program-dialog" data-description="%1$s" data-title="%2$s" data-credit-low="%3$s%5$s" data-credit-high="%4$s">%2$s</button></td><td>%3$s</td><td>%4$s</td></tr>',
			wp_kses_post( $entity['field_description'][0]['value'] ),
			esc_html( $entity['title'][0]['value'] ),
			esc_html( $credit_low ),
			esc_html( $credit_high ),
			$credit_low && $credit_high ? ' - ' : ''
		);
	}

	$html .= '</tbody></table>';

	return $html;
}

/**
 * Get admissions accordion data.
 *
 * @param int|WP_Post $post Post ID or WP_Post object.
 * @return array
 */
function get_admissions( $post = null ) {

	$post = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	$result   = [];
	$sections = array(
		array(
			'meta_key' => 'nu_pim_requirements_transform',
			'title'    => __( 'Application Requirements' ),
			'type'     => 'html',
		),
		array(
			'meta_key' => 'nu_pim_admissions_links',
			'title'    => __( 'International Requirements' ),
			'type'     => 'array',
		),
		array(
			'meta_key' => 'nu_pim_financial_links',
			'title'    => __( 'Financing Requirements' ),
			'type'     => 'array',
		),
	);

	foreach ( $sections as $section ) {

		$content = get_post_meta( $post->ID, $section['meta_key'], true );

		if ( empty( $content ) ) {
			continue;
		}

		if ( 'array' === $section['type'] && is_array( $content ) ) {

			$content_new = '';

			foreach ( $content as $link_arr ) {

				$content_new .= sprintf(
					'<p><a href="%s">%s</a> %s</p>',
					$link_arr['url'],
					$link_arr['title'],
					$link_arr['text']
				);
			}

			$content = $content_new;
		}

		if ( ! empty( $content ) ) {

			$result[] = array(
				'title'   => $section['title'],
				'content' => $content,
			);
		}
	}

	// Add additional sections that are manually added via the post edit screen.
	$sections = get_post_meta( $post->ID, 'cps_program_admissions_items', true );

	if ( ! empty( $sections ) && is_array( $sections ) ) {

		foreach ( $sections as $section ) {

			if ( ! empty( $section['title'] ) && ! empty( $section['copy'] ) ) {

				$result[] = array(
					'title'   => $section['title'],
					'content' => wpautop( wp_kses_post( $section['copy'] ) ),
				);
			}
		}
	}

	return $result;
}
