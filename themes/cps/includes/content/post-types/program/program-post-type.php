<?php
/**
 * Contains program custom post type functionality
 *
 * @package CPS
 */

namespace CPS\Program\PostType;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom post type.
	add_action( 'init', __NAMESPACE__ . '\register_post_type' );

	// Filter custom post type updated messages.
	add_filter( 'post_updated_messages', __NAMESPACE__ . '\post_updated_messages' );

	// Add custom post type to 'At a Glance' dashboard widget.
	add_action( 'dashboard_glance_items', __NAMESPACE__ . '\dashboard_glance_items' );

	// Change 'At a Glance' icon.
	add_action( 'admin_print_scripts', __NAMESPACE__ . '\dashboard_glance_items_style' );

	// Order projects by project name.
	add_action( 'pre_get_posts', __NAMESPACE__ . '\order_projects_by_name' );

	// Register content filters.
	add_action( 'init', __NAMESPACE__ . '\add_content_filters', 999 );

	// Disable code editor.
	add_filter( 'block_editor_settings', __NAMESPACE__ . '\maybe_disable_code_editing', 10, 2 );

	// Register 'cps_faculty_id' custom field so we can use it in the block editor.
	add_action( 'init', __NAMESPACE__ . '\register_program_post_meta', 99 );

	// Register global JS variables for use in the editor,
	add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\localize_script', 11 );
}

/**
 * Register program post type.
 */
function register_post_type() {

	$labels = array(
		'name'               => esc_html__( 'Programs', 'cps' ),
		'singular_name'      => esc_html__( 'Program', 'cps' ),
		'add_new'            => esc_html__( 'Add New', 'cps' ),
		'add_new_item'       => esc_html__( 'Add Program', 'cps' ),
		'edit_item'          => esc_html__( 'Edit Program', 'cps' ),
		'new_item'           => esc_html__( 'New Program', 'cps' ),
		'all_items'          => esc_html__( 'All Programs', 'cps' ),
		'view_item'          => esc_html__( 'View Program', 'cps' ),
		'search_items'       => esc_html__( 'Search Programs', 'cps' ),
		'not_found'          => esc_html__( 'No programs found', 'cps' ),
		'not_found_in_trash' => esc_html__( 'No programs found in the trash', 'cps' ),
		'parent_item_colon'  => '',
		'menu_name'          => esc_html__( 'Programs', 'cps' ),
	);

	$args = array(
		'labels'              => $labels,
		'public'              => true,
		'hierarchical'        => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'rest_base'           => 'program',
		'menu_position'       => 50,
		'menu_icon'           => 'dashicons-welcome-learn-more',
		'capability_type'     => 'post',
		'capabilities'        => array(
			'create_posts' => 'do_not_allow',
		),
		'map_meta_cap'        => true,
		'supports'            => array(
			'editor',
			'thumbnail',
			'custom-fields',
		),
		'has_archive'         => true,
		'rewrite'             => array(
			'slug'       => 'program',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => false,
		),
		'query_var'           => 'program',
		'can_export'          => true,
		'delete_with_user'    => false,
		'template'            => array(
			array(
				'cps/hero-program',
			),
			array(
				'cps/wrapper',
			),
		),
		'template_lock'       => 'all',
	);

	/*
	 * Adding title support outside of admin to be able to access the
	 * title via a rest request.
	 */
	if ( ! is_admin() ) {
		$args['supports'][] = 'title';
	}

	\register_post_type( 'cps-program', $args );
}

/**
 * Filter display messages.
 *
 * @param array $messages Post updated messages. For defaults @see wp-admin/edit-form-advanced.php.
 * @return array $messages Modified post updated messages.
 */
function post_updated_messages( $messages ) {

	global $post;

	$messages['cps-program'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => esc_html__( 'Program updated.', 'cps' ),
		2  => esc_html__( 'Custom field updated.', 'cps' ),
		3  => esc_html__( 'Custom field deleted.', 'cps' ),
		4  => esc_html__( 'Program updated.', 'cps' ),
		/* translators: %s: previous revision */
		5  => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Program restored to revision from %s', 'cps' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		6  => esc_html__( 'Program published.', 'cps' ),
		7  => esc_html__( 'Program saved.', 'cps' ),
		8  => esc_html__( 'Program submitted.', 'cps' ),
		/* translators: %s: Program date */
		9  => sprintf( esc_html__( 'Program scheduled for: <strong>%1$s</strong>.', 'cps' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
		10 => esc_html__( 'Program draft updated.', 'cps' ),
	);

	return $messages;
}

/**
 * Add custom post type to 'At a Glance' dashboard widget.
 */
function dashboard_glance_items() {

	$post_type_obj = get_post_type_object( 'cps-program' );

	if ( ! $post_type_obj ) {
		return;
	}

	if ( ! current_user_can( $post_type_obj->cap->edit_posts ) ) {
		return;
	}

	$num_posts = wp_count_posts( $post_type_obj->name );

	if ( $num_posts && $num_posts->publish ) {

		// phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralSingle
		// phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralPlural

		/* translators: %s: number of published posts */
		$label = _n(
			'%s ' . $post_type_obj->labels->singular_name,
			'%s ' . $post_type_obj->labels->name,
			$num_posts->publish
		);

		// phpcs:enable WordPress.WP.I18n.NonSingularStringLiteralSingle
		// phpcs:enable WordPress.WP.I18n.NonSingularStringLiteralPlural

		$label = sprintf( $label, number_format_i18n( $num_posts->publish ) );

		printf(
			'<li class="%1$s-count"><a class="%2$s" href="edit.php?post_type=%1$s">%3$s</a></li>',
			esc_attr( $post_type_obj->name ),
			esc_attr( $post_type_obj->menu_icon ),
			esc_html( $label )
		);
	}

	dashboard_glance_items_style( 'cps-program', $post_type_obj->menu_icon );
}

/**
 * Add inline style to change the 'At A Glance' link icon.
 */
function dashboard_glance_items_style() {

	$current_screen = get_current_screen();

	if ( 'dashboard' === $current_screen->base && current_user_can( 'edit_posts' ) ) {
		printf(
			'<style media="all">#dashboard_right_now .%s-count a:before { content: "%s"; }</style>',
			'cps-program',
			'\f118'
		);
	}
}

/**
 * Order projects by project name.
 *
 * @todo Remove, temporary, this is used to show the program fields to A&G
 *
 * @since 1.0.0
 * @param WP_Query $wp_query WP_Query object, passed y reference.
 * @return void
 */
function order_projects_by_name( $wp_query ) {

	if ( is_admin() || ! $wp_query->is_main_query() || ! is_post_type_archive( 'cps-program' ) ) {
		return;
	}

	$wp_query->set( 'orderby', 'name' );
	$wp_query->set( 'order', 'ASC' );
}

/**
 * Register content filters.
 *
 * Adds the required hero block and removes it on save.
 *
 * This function is added late to the init hook to make sure
 * all post types are registered.
 */
function add_content_filters() {

	// Filter post content when retrieving it for the editor.
	add_filter( 'rest_prepare_cps-program', __NAMESPACE__ . '\filter_rest_prepare', 99, 3 );

	// Filter content before saving a post.
	add_filter( 'rest_pre_insert_cps-program', __NAMESPACE__ . '\filter_rest_pre_insert', 99, 2 );
}

/**
 * Filter post content when retrieving it for the editor.
 *
 * Adds a program hero block.
 *
 * Uses the "rest_prepare_{$this->post_type}" filter.
 *
 * @param WP_REST_Response $response The response object.
 * @param WP_Post          $post     Post object.
 * @param WP_REST_Request  $request  Request object.
 */
function filter_rest_prepare( $response, $post, $request ) {

	if ( 'GET' === $request->get_method() && 'edit' === $request->get_param( 'context' ) ) {

		$data = $response->get_data();

		if ( empty( $data['content']['raw'] ) ) {
			$data['content']['raw'] = '<!-- wp:cps/hero-program /--><!-- wp:cps/wrapper /-->';
		} else {
			$data['content']['raw'] = '<!-- wp:cps/hero-program /--><!-- wp:cps/wrapper -->' . $data['content']['raw'] . '<!-- /wp:cps/wrapper -->';
		}

		$response->set_data( $data );
	}

	return $response;
}

/**
 * Filter the post content before it is inserted via the REST API.
 *
 * Removes the hero block and unwraps our wrapper box block from the post content.
 *
 * Uses the "rest_pre_insert_{$this->post_type}" filter.
 *
 * @param stdClass        $prepared_post An object representing a single post prepared
 *                                       for inserting or updating the database.
 * @param WP_REST_Request $request       Request object.
 */
function filter_rest_pre_insert( $prepared_post, $request ) {

	$post_content = $prepared_post->post_content;
	$blocks       = [
		'<!-- wp:cps/hero-program -->',
		'<!-- wp:cps/hero-program /-->',
		'<!-- /wp:cps/wrapper -->',
		'<!-- wp:cps/wrapper /-->',
		'<!-- wp:cps/wrapper -->',
	];

	/*
	 * Poor man's block removal.
	 *
	 * A future update might use parse_blocks to remove the hero.
	 */
	foreach ( $blocks as $block ) {
		$post_content = str_replace( $block, '', $post_content );
	}

	$prepared_post->post_content = trim( $post_content );

	return $prepared_post;
}

/**
 * Filters the settings to pass to the block editor.
 *
 * @param array   $editor_settings Default editor settings.
 * @param WP_Post $post            Post being edited.
 */
function maybe_disable_code_editing( $editor_settings, $post ) {

	if ( 'cps-program' === $post->post_type ) {
		$editor_settings['codeEditingEnabled'] = false;
	}

	return $editor_settings;
}

/**
 * Register person detail custom fields so we can use them in the block editor.
 *
 * @return void
 */
function register_program_post_meta() {

	$fields = [
		'cps_program_bg_color',
		'cps_program_bg_type',
		'cps_program_nav_color',
		'cps_program_overlay_color',
		'cps_program_overlay_opacity',
		'cps_program_description_short',
		'cps_program_initials',
		'cps_program_image',
	];

	foreach ( $fields as $field ) {

		register_post_meta(
			'cps-program',
			$field,
			array(
				'show_in_rest' => true,
				'single'       => true,
				'type'         => 'string',
			)
		);
	}
}

/**
 * Register global JS variables for use in the editor,
 */
function localize_script() {

	global $post;

	if ( 'cps-program' !== $post->post_type ) {
		return;
	}

	$image     = get_post_meta( $post->ID, 'nu_pim_hero_image', true );
	$image_url = '';

	if ( is_array( $image ) && is_array( $image[0] ) && isset( $image[0]['uri'] ) && ! empty( $image[0]['uri'] ) ) {
		$image_url = $image[0]['uri'];
	}

	$copy = get_post_meta( $post->ID, 'nu_pim_body', true );

	$program_initials = get_post_meta( get_the_ID(), 'cps_program_initials', true );

	if ( empty( $program_initials ) ) {
		$program_name     = get_post_meta( get_the_ID(), 'nu_pim_formatted_title', true );
		$program_initials = preg_match_all( '/(?<=\s|^)[a-z]/i', $program_name, $matches );
		$program_initials = strtoupper( implode( '', $matches[0] ) );
	}

	wp_localize_script(
		'cps-blocks-editor',
		'cpsProgramVars',
		[
			'title'    => get_the_title( $post ),
			'pimImage' => $image_url,
			'copy'     => $copy,
			'badge'    => $program_initials,
		]
	);
}
