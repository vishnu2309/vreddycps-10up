<?php
/**
 * Contains the program core admin functionality
 *
 * @package CPS
 */

namespace CPS\Program\Admin;

/**
 * Add actions and filters.
 */
function setup() {

	// Maybe remove quick edit for non-hierarchical person post type.
	add_filter( 'post_row_actions', __NAMESPACE__ . '\remove_quick_edit', 10, 2 );

	// Maybe remove quick edit for hierarchical person post type.
	add_filter( 'page_row_actions', __NAMESPACE__ . '\remove_quick_edit', 10, 2 );

	// Clear caching.
	add_action( 'save_post_cps-program', __NAMESPACE__ . '\clear_cache', 10, 3 );
}

/**
 * Maybe remove 'quick edit' action.
 *
 * @param array   $actions An array of row action links.
 * @param WP_Post $post    The post object.
 */
function remove_quick_edit( $actions, $post ) {

	if ( 'cps-program' === $post->post_type ) {
		unset( $actions['inline hide-if-no-js'] );
	}

	return $actions;
}

/**
 * Clear caching.
 *
 * @param int      $post_ID Post ID.
 * @param \WP_Post $post    Post object.
 * @param bool     $update  Whether this is an existing post being updated or not.
 */
function clear_cache( $post_ID, $post, $update ) {

	$cache_key = 'cps_programs';

	$program_levels = get_the_terms( $post_ID, 'nu_pim_degree_type' );

	// Add program-level term-ids to the cache key.
	if ( ! empty( $program_levels ) ) {

		// Get all the term-ids.
		$program_level_ids = wp_list_pluck( $program_levels, 'term_id' );

		// Sort them to create proper cache key.
		sort( $program_level_ids );

		$cache_key .= '_' . implode( ',', $program_level_ids );
	}

	wp_cache_delete( $cache_key );
}
