<?php
/**
 * Metaboxes for the program detail admin edit page
 *
 * The metaboxes are created using the Fieldmanager plugin.
 *
 * @see https://fieldmanager.org/
 *
 * @package CPS
 */

namespace CPS\Program\Admin\Metaboxes;

/**
 * Add actions and filters.
 */
function setup() {

	// Register intro meta box.
	add_action( 'init', __NAMESPACE__ . '\register_quick_look_meta_box' );

	// Register overview meta box.
	add_action( 'init', __NAMESPACE__ . '\register_overview_meta_box' );

	// Register career prospects meta box.
	add_action( 'init', __NAMESPACE__ . '\register_career_prospects_meta_box' );

	// Register course plan meta box.
	add_action( 'init', __NAMESPACE__ . '\register_course_plan_meta_box' );

	// Register faculty meta box.
	add_action( 'init', __NAMESPACE__ . '\register_faculty_meta_box' );

	// Register quote meta box.
	add_action( 'init', __NAMESPACE__ . '\register_quote_meta_box' );

	// Register admissions meta box.
	add_action( 'init', __NAMESPACE__ . '\register_admissions_meta_box' );

	// Register research meta box.
	add_action( 'init', __NAMESPACE__ . '\register_research_meta_box' );

	// Register study abroad meta box.
	add_action( 'init', __NAMESPACE__ . '\register_study_abroad_meta_box' );

	// Register faq meta box.
	add_action( 'init', __NAMESPACE__ . '\register_faq_meta_box' );
}

/**
 * Register intro meta box.
 */
function register_quick_look_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_intro',
			'tabbed'         => 'vertical',
			'serialize_data' => false,
			'children'       => [
				'col_1' => new \Fieldmanager_Group(
					[
						'label'          => __( 'Column 1', 'cps' ),
						'serialize_data' => false,
						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
				'col_2' => new \Fieldmanager_Group(
					[
						'label'          => __( 'Column 2', 'cps' ),
						'serialize_data' => false,
						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
				'col_3' => new \Fieldmanager_Group(
					[
						'label'          => __( 'Column 3', 'cps' ),
						'serialize_data' => false,
						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
				'col_4' => new \Fieldmanager_Group(
					[
						'label'          => __( 'Column 4', 'cps' ),
						'serialize_data' => false,
						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Quick Look', 'cps' ), 'cps-program' );
}

/**
 * Register overview meta box.
 */
function register_overview_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'nu_pim', // Using the same field as imported from pim to be able to populate original description.
			'tabbed'         => 'vertical',
			'serialize_data' => false,
			'children'       => [
				'program_description_long' => new \Fieldmanager_Group(
					[
						'label'          => __( 'Description', 'cps' ),
						'serialize_data' => false,
						'children'       => [

							/*
							 * Results in 'nu_pim_program_description_long_alt', which is not ideal as it implies this is
							 * imported from the PIM API. However it is the most convenient way to fetch and display the original
							 * nu_pim_program_description_long alongside it in the next tab.
							 */
							'alt' => new \Fieldmanager_RichTextArea(
								[
									'label'        => null,
									'inline_label' => false,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),

				/*
				 * nu_pim_program_description_long comes from the PIM API. It is shown here for
				 * convenience but we don't want to overwrite it.
				 */
				'program_description'      => new \Fieldmanager_Group(
					[
						'label'          => __( 'Description (PIM)', 'cps' ),
						'serialize_data' => false,
						'children'       => [
							'long' => new \Fieldmanager_TextArea(
								[
									'skip_save'    => true, // No need to save this field.
									'label'        => null,
									'inline_label' => false,
									'attributes'   => [
										'disabled' => true,
										'rows'     => 12,
									],
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Overview', 'cps' ), 'cps-program' );
}

/**
 * Register career prospects meta box.
 */
function register_career_prospects_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_career_prospects',
			'serialize_data' => false,
			'children'       => [
				'title'     => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Title', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'nav_label' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Navigation Label', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'copy'      => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Content', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'items'     => new \Fieldmanager_Group(
					__( 'Column', 'cps' ),
					[
						'add_more_label' => __( 'Add Career Prospects Column', 'cps' ),
						'limit'          => 3,
						'sortable'       => true,
						'extra_elements' => 0,
						'children'       => [
							'number' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Number', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'title'  => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'   => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Career Prospects', 'cps' ), 'cps-program' );
}

/**
 * Register course plan meta box.
 */
function register_course_plan_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_course_plan',
			'serialize_data' => false,
			'children'       => [
				'title'     => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Title', 'cps' ),
						'inline_label' => true,
						'description'  => __( 'Optional, defaults to "Get Set with a Custom Course Plan"', 'cps' ),
						'sanitize'     => 'esc_html',
					]
				),
				'nav_label' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Navigation Label', 'cps' ),
						'inline_label' => true,
						'description'  => __( 'Optional, defaults to "Create a Course Plan"', 'cps' ),
						'sanitize'     => 'esc_html',
					]
				),
				'copy'      => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Content', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Course Plan', 'cps' ), 'cps-program' );
}

/**
 * Register faculty meta box.
 */
function register_faculty_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_faculty',
			'serialize_data' => false,
			'children'       => [
				'title'      => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Title', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'copy'       => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Content', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'nav_label'  => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Navigation Label', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'quote'      => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Quote', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'stat'       => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Stat Number', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'stat_title' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Stat Title', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'stat_copy'  => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Stat Copy', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'items'      => new \Fieldmanager_Group(
					__( 'Faculty', 'cps' ),
					[
						'add_more_label' => __( 'Add Faculty Member', 'cps' ),
						'sortable'       => true,
						'limit'          => 0,
						'children'       => [
							'post_id' => new \Fieldmanager_Autocomplete(
								[
									'attributes' => [
										'placeholder' => __( 'Type name to search faculty member', 'cps' ),
									],
									'datasource' => new \Fieldmanager_Datasource_Post(
										[
											'query_args' => [
												'post_type' => 'cps-person',
											],
										]
									),
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Faculty', 'cps' ), 'cps-program' );
}

/**
 * Register quote meta box.
 */
function register_quote_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed' => true,
			'name'      => 'cps_program_quote',
			'children'  => [
				'quote'   => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Quote', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'name'    => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Name', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'subhead' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Subhead (Optional)', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'link'    => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Link (Optional)', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_url',
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Quote', 'cps' ), 'cps-program' );
}

/**
 * Register admissions meta box.
 */
function register_admissions_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'                 => true,
			'name'                      => 'cps_program_admissions',
			'serialize_data'            => false,
			'description'               => __( 'Optional fields for the "Admissions" section, eg. for Transfer Credits.', 'cps' ),
			'description_after_element' => false,
			'children'                  => [
				'items' => new \Fieldmanager_Group(
					__( 'Field', 'cps' ),
					[
						'add_more_label' => __( 'Add Admissions Field', 'cps' ),
						'extra_elements' => 0,
						'limit'          => 0,
						'sortable'       => true,

						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Additional Admissions Fields', 'cps' ), 'cps-program' );
}

/**
 * Register research meta box.
 */
function register_research_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_research',
			'serialize_data' => false,
			'children'       => [
				'title'     => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Title', 'cps' ),
						'description'  => __( 'Optional, defaults to "Research"', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'nav_label' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Navigation Label', 'cps' ),
						'description'  => __( 'Optional, defaults to "Learn Through Research"', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'items'     => new \Fieldmanager_Group(
					__( 'Field', 'cps' ),
					[
						'add_more_label' => __( 'Add Research Field', 'cps' ),
						'extra_elements' => 0,
						'limit'          => 0,
						'sortable'       => true,

						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Research', 'cps' ), 'cps-program' );
}

/**
 * Register study abroad meta box.
 */
function register_study_abroad_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_study_abroad',
			'serialize_data' => false,
			'children'       => [
				'title'     => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Title', 'cps' ),
						'description'  => __( 'Optional, defaults to "Study Abroad"', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'nav_label' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Navigation Label', 'cps' ),
						'description'  => __( 'Optional, defaults to "Study Abroad"', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'items'     => new \Fieldmanager_Group(
					__( 'Field', 'cps' ),
					[
						'add_more_label' => __( 'Add Study Abroad Field', 'cps' ),
						'extra_elements' => 0,
						'limit'          => 0,
						'sortable'       => true,

						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Study Abroad', 'cps' ), 'cps-program' );
}

/**
 * Register faq meta box.
 */
function register_faq_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'collapsed'      => true,
			'name'           => 'cps_program_faq',
			'serialize_data' => false,
			'children'       => [
				'title'     => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Title', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'copy'      => new \Fieldmanager_RichTextArea(
					[
						'label'        => __( 'Content', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'wp_kses_post',
					]
				),
				'nav_label' => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Navigation Label', 'cps' ),
						'inline_label' => true,
						'sanitize'     => 'esc_html',
					]
				),
				'items'     => new \Fieldmanager_Group(
					__( 'Field', 'cps' ),
					[
						'add_more_label' => __( 'Add FAQ Field', 'cps' ),
						'extra_elements' => 0,
						'limit'          => 0,
						'sortable'       => true,

						'children'       => [
							'title' => new \Fieldmanager_TextField(
								[
									'label'        => __( 'Title', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'esc_html',
								]
							),
							'copy'  => new \Fieldmanager_RichTextArea(
								[
									'label'        => __( 'Content', 'cps' ),
									'inline_label' => true,
									'sanitize'     => 'wp_kses_post',
								]
							),
						],
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'FAQ', 'cps' ), 'cps-program' );
}
