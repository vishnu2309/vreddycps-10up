<?php
/**
 * Contains person custom post type functionality
 *
 * @package CPS
 */

namespace CPS\Person\PostType;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom post type.
	add_action( 'init', __NAMESPACE__ . '\register_post_type' );

	// Filter custom post type updated messages.
	add_filter( 'post_updated_messages', __NAMESPACE__ . '\post_updated_messages' );

	// Add custom post type to 'At a Glance' dashboard widget.
	add_action( 'dashboard_glance_items', __NAMESPACE__ . '\dashboard_glance_items' );

	// Change 'At a Glance' icon.
	add_action( 'admin_print_scripts', __NAMESPACE__ . '\dashboard_glance_items_style' );

	// Remove Yoast SEO meta boxes from person post type edit screen.
	add_action( 'add_meta_boxes', __NAMESPACE__ . '\remove_wp_seo_meta_box', 100 );

	// Order persons by last name.
	add_action( 'pre_get_posts', __NAMESPACE__ . '\order_persons_by_last_name' );

	// Register 'cps_faculty_id' custom field so we can use it in the block editor.
	add_action( 'init', __NAMESPACE__ . '\register_cps_faculty_id_post_meta', 99 );

	// Update post title based on the fname and lname meta field values.
	add_filter( 'rest_after_insert_cps-person', __NAMESPACE__ . '\maybe_update_post_title', 99, 3 );

	// Note: 'save_post_name' left in in for future reference.
	// add_action( 'save_post', __NAMESPACE__ . '\save_post_name', 10, 2 );

	// Add a 'Faculty Spotlight' and 'Faculty & Staff' option to the page templates dropdown.
	add_filter( 'theme_page_templates', __NAMESPACE__ . '\filter_page_templates' );
}

/**
 * Register person post type.
 */
function register_post_type() {

	$labels = array(
		'name'                     => esc_html__( 'Persons', 'cps' ),
		'menu_name'                => esc_html__( 'Persons', 'cps' ),
		'singular_name'            => esc_html__( 'Person', 'cps' ),
		'add_new'                  => esc_html__( 'Add New', 'cps' ),
		'add_new_item'             => esc_html__( 'Add New Person', 'cps' ),
		'edit_item'                => esc_html__( 'Edit Person', 'cps' ),
		'new_item'                 => esc_html__( 'New Person', 'cps' ),
		'view_item'                => esc_html__( 'View Person', 'cps' ),
		'view_items'               => esc_html__( 'View Persons', 'cps' ),
		'search_items'             => esc_html__( 'Search Persons', 'cps' ),
		'not_found'                => esc_html__( 'No persons found', 'cps' ),
		'not_found_in_trash'       => esc_html__( 'No persons found in the trash', 'cps' ),
		'parent_item_colon'        => null,
		'all_items'                => esc_html__( 'All Persons', 'cps' ),
		'archives'                 => esc_html__( 'Person Archives', 'cps' ),
		'attributes'               => esc_html__( 'Person Attributes', 'cps' ),
		'insert_into_item'         => esc_html__( 'Insert into content', 'cps' ),
		'uploaded_to_this_item'    => esc_html__( 'Uploaded to this person', 'cps' ),
		'featured_image'           => esc_html__( 'Profile image', 'cps' ),
		'set_featured_image'       => esc_html__( 'Set profile image', 'cps' ),
		'remove_featured_image'    => esc_html__( 'Remove profile image', 'cps' ),
		'use_featured_image'       => esc_html__( 'Use as profile image', 'cps' ),
		'filter_items_list'        => esc_html__( 'Filter persons list', 'cps' ),
		'items_list_navigation'    => esc_html__( 'Person list navigation', 'cps' ),
		'items_list'               => esc_html__( 'Persons list', 'cps' ),
		'item_published'           => esc_html__( 'Person published.', 'cps' ),
		'item_published_privately' => esc_html__( 'Person published privately.', 'cps' ),
		'item_reverted_to_draft'   => esc_html__( 'Person reverted to draft.', 'cps' ),
		'item_scheduled'           => esc_html__( 'Person scheduled.', 'cps' ),
		'item_updated'             => esc_html__( 'Person updated.', 'cps' ),
	);

	$args = array(
		'labels'              => $labels,
		'description'         => esc_html__( 'The person post type is used for faculty and other university staff profiles.', 'cps' ),
		'public'              => true,
		'hierarchical'        => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'rest_base'           => 'person',
		'menu_position'       => 50,
		'menu_icon'           => 'dashicons-groups',
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'supports'            => array(
			// Note: no 'title' support. The title will be created based on the first and last name fields.
			'editor',
			'thumbnail',
			'custom-fields',
			'metablock',
		),
		'has_archive'         => false,
		'rewrite'             => array(
			'slug'       => 'profile',
			'with_front' => false,
			'feeds'      => false,
			'pages'      => false,
		),
		'query_var'           => 'person',
		'can_export'          => true,
		'delete_with_user'    => false,
	);

	/*
	 * Adding title support outside of admin to be able to access the
	 * title via a rest request.
	 */
	if ( ! is_admin() ) {
		$args['supports'][] = 'title';
	}

	\register_post_type( 'cps-person', $args );
}

/**
 * Filter display messages.
 *
 * @param array $messages Post updated messages. For defaults @see wp-admin/edit-form-advanced.php.
 * @return array $messages Modified post updated messages.
 */
function post_updated_messages( $messages ) {

	global $post;

	$messages['cps-person'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => esc_html__( 'Person updated.', 'cps' ),
		2  => esc_html__( 'Custom field updated.', 'cps' ),
		3  => esc_html__( 'Custom field deleted.', 'cps' ),
		4  => esc_html__( 'Person updated.', 'cps' ),
		/* translators: %s: previous revision */
		5  => isset( $_GET['revision'] ) ? sprintf( esc_html__( 'Person restored to revision from %s', 'cps' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		6  => esc_html__( 'Person published.', 'cps' ),
		7  => esc_html__( 'Person saved.', 'cps' ),
		8  => esc_html__( 'Person submitted.', 'cps' ),
		/* translators: %s: Person date */
		9  => sprintf( esc_html__( 'Person scheduled for: <strong>%1$s</strong>.', 'cps' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
		10 => esc_html__( 'Person draft updated.', 'cps' ),
	);

	return $messages;
}

/**
 * Add custom post type to 'At a Glance' dashboard widget.
 */
function dashboard_glance_items() {

	$post_type_obj = get_post_type_object( 'cps-person' );

	if ( ! $post_type_obj ) {
		return;
	}

	if ( ! current_user_can( $post_type_obj->cap->edit_posts ) ) {
		return;
	}

	$num_posts = wp_count_posts( $post_type_obj->name );

	if ( $num_posts && $num_posts->publish ) {

		// phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralSingle
		// phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralPlural

		/* translators: %s: number of published posts */
		$label = _n(
			'%s ' . $post_type_obj->labels->singular_name,
			'%s ' . $post_type_obj->labels->name,
			$num_posts->publish
		);

		// phpcs:enable WordPress.WP.I18n.NonSingularStringLiteralSingle
		// phpcs:enable WordPress.WP.I18n.NonSingularStringLiteralPlural

		$label = sprintf( $label, number_format_i18n( $num_posts->publish ) );

		printf(
			'<li class="%1$s-count"><a class="%2$s" href="edit.php?post_type=%1$s">%3$s</a></li>',
			esc_attr( $post_type_obj->name ),
			esc_attr( $post_type_obj->menu_icon ),
			esc_html( $label )
		);
	}

	dashboard_glance_items_style( 'cps-person', $post_type_obj->menu_icon );
}

/**
 * Add inline style to change the 'At A Glance' link icon.
 */
function dashboard_glance_items_style() {

	$current_screen = get_current_screen();

	if ( 'dashboard' === $current_screen->base && current_user_can( 'edit_posts' ) ) {
		printf(
			'<style media="all">#dashboard_right_now .%s-count a:before { content: "%s"; }</style>',
			'cps-person',
			'\f307'
		);
	}
}

/**
 * Remove Yoast SEO meta boxes from person post type edit screen.
 *
 * @return void
 */
function remove_wp_seo_meta_box() {
	remove_meta_box( 'yoast_internal_linking', 'cps-person', 'side' );
	remove_meta_box( 'wpseo_meta', 'cps-person', 'normal' );
}

/**
 * Order persons by last name.
 *
 * @param WP_Query $wp_query The WP_Query instance (passed by reference).
 * @return void
 */
function order_persons_by_last_name( $wp_query ) {

	// @todo Order persons by last name in admin.
	if ( is_admin() || ! $wp_query->is_main_query() || ! is_post_type_archive( 'cps-person' ) ) {
		return;
	}

	$wp_query->set( 'meta_key', 'nu_fim_faculty_lname' );
	$wp_query->set( 'orderby', 'nu_fim_faculty_lname' );
	$wp_query->set( 'order', 'ASC' );
}

/**
 * Register 'cps_faculty_id' custom field so we can use it in the block editor.
 *
 * @return void
 */
function register_cps_faculty_id_post_meta() {

	$args = array(
		'public' => true,
	);

	$post_types = get_post_types( $args, 'names' );

	foreach ( $post_types as $post_type ) {

		if ( 'attachment' === $post_type ) {
			continue;
		}

		register_post_meta(
			$post_type,
			'cps_faculty_id',
			array(
				'show_in_rest' => true,
				'single'       => true,
				'type'         => 'string',
			)
		);
	}
}

/**
 * Prevent editing faculty imported via the FIM API.
 *
 * @param array  $caps    Array of the user's capabilities.
 * @param string $cap     Capability name.
 * @param int    $user_id The user ID.
 * @param array  $args    Adds the context to the cap. Typically the object ID.
 * @return array
 */
function prevent_faculty_edit( $caps, $cap, $user_id, $args ) {

	if ( 'edit_post' === $cap ) {

		$post = get_post( $args[0] );

		if ( ! is_a( $post, '\WP_Post' ) ) {
			return $caps;
		}

		if ( 'cps-person' === $post->post_type ) {

			$is_imported = \CPS\Person\Helpers\is_imported( $post->ID );

			if ( $is_imported ) {

				$caps   = array();
				$caps[] = 'do_not_allow';
			}
		}
	}

	return $caps;
}

/**
 * Update post title based on the fname and lname meta field values.
 *
 * @todo create post slug when publishing a post.
 *
 * Uses the "rest_after_insert_{$this->post_type}" filter.
 *
 * @param WP_Post         $post     Inserted or updated post object.
 * @param WP_REST_Request $request  Request object.
 * @param bool            $creating True when creating a post, false when updating.
 */
function maybe_update_post_title( $post, $request, $creating ) {

	if ( ! current_user_can( 'edit_post', $post->ID ) ) {
		return;
	}

	$is_imported = \CPS\Person\Helpers\is_imported( $post->ID );

	if ( $is_imported ) {
		return;
	}

	$fname      = get_post_meta( $post->ID, 'nu_fim_faculty_fname', true );
	$lname      = get_post_meta( $post->ID, 'nu_fim_faculty_lname', true );
	$post_title = $fname . ' ' . $lname;

	if ( empty( $post_title ) ) {
		return;
	}

	$postarr = array(
		'ID'         => $post->ID,
		'post_title' => $post_title,
	);

	// Maybe update post slug as well.
	if ( 'draft' !== $post->post_status && is_numeric( $post->post_name ) || empty( $post->post_name ) ) {

		$postarr['post_name'] = wp_unique_post_slug(
			sanitize_title( $post_title, $post->ID ),
			$post->ID,
			$post->post_status,
			$post->post_type,
			$post->post_parent
		);
	}

	wp_update_post( $postarr );
}

/**
 * Save post title. Only for manually created persons.
 *
 * @param  int     $post_id Post ID.
 * @param  WP_Post $post    Post object.
 */
function save_post_name( $post_id, $post ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	$is_imported = \CPS\Person\Helpers\is_imported( $post_id );

	if ( $is_imported ) {
		return;
	}

	if ( isset( $_POST['nu_fim_faculty'] ) ) {

		$fname = '';
		$lname = '';

		if ( isset( $_POST['nu_fim_faculty']['fname'] ) ) {
			$fname = sanitize_text_field( wp_unslash( $_POST['nu_fim_faculty']['fname'] ) );
		}

		if ( isset( $_POST['nu_fim_faculty']['lname'] ) ) {
			$lname = sanitize_text_field( wp_unslash( $_POST['nu_fim_faculty']['lname'] ) );
		}

		$name = $fname . ' ' . $lname;

		$args = array(
			'ID'         => $post_id,
			'post_title' => $name,
			'post_name'  => wp_unique_post_slug( sanitize_title( $name, '', 'save' ), $post_id, $post->post_status, $post->post_type, 0 ),
		);

		remove_action( 'save_post', __NAMESPACE__ . '\save_post_name', 10 );

		wp_update_post( $args );

		add_action( 'save_post', __NAMESPACE__ . '\save_post_name', 10, 2 );

	}
}

/**
 * Add a 'Faculty Spotlight' and 'Faculty & Staff Directory' to the page templates dropdown.
 *
 * This function hooks into the 'theme_page_templates' filter which is
 * documented in wp-includes/class-wp-theme.php.
 *
 * Filtering the page templates options negates the need for an
 * additional template file.
 *
 * - The 'Faculty Spotlight' page template automatically adds the person initials to the
 *   hero block and exposes a faculty ID select to the page admin screen. This select
 *   allows us to link a faculty spotlight to a person
 * - The 'Faculty & Staff' page template is solely used to be able to find the
 *   faculty overview page, eg. to populate the breadcrumbs for a person.
 *
 * @param array $page_templates  Array of page templates. Keys are filenames,
 *                               values are translated names.
 * @return array Array of page templates including our custom ones.
 */
function filter_page_templates( $page_templates ) {

	$page_templates['faculty_staff_directory'] = __( 'Faculty & Staff Directory', 'cps' );
	$page_templates['faculty_spotlight']       = __( 'Faculty Spotlight', 'cps' );

	return $page_templates;
}
