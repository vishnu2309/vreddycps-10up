<?php
/**
 * Metaboxes for the program detail admin edit page
 *
 * The metaboxes are created using the Fieldmanager plugin.
 *
 * @see https://fieldmanager.org/
 *
 * @package CPS
 */

namespace CPS\Person\Admin\Metaboxes;

/**
 * Add actions and filters.
 */
function setup() {

	// Register person details meta box.
	add_action( 'init', __NAMESPACE__ . '\register_profile_meta_box' );
	// Register person repeatable fields meta box.
	add_action( 'init', __NAMESPACE__ . '\register_custom_profile_meta_box' );
}

/**
 * Register person details meta box.
 */
function register_profile_meta_box() {

	$attributes          = array();
	$attributes_textarea = array(
		'columns' => 4,
	);

	if ( is_admin() && ! empty( $_GET['post'] ) ) { // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		$is_imported = \CPS\Person\Helpers\is_imported( intval( $_GET['post'] ) ); // phpcs:ignore WordPress.Security.NonceVerification.Recommended

		if ( $is_imported ) {
			$attributes = array(
				'disabled' => true,
			);

			$attributes_textarea = array(
				'columns'  => 4,
				'disabled' => true,
			);
		}
	}

	$group = new \Fieldmanager_Group(
		[
			'name'           => 'nu_fim',
			'serialize_data' => false,
			'children'       => [
				'faculty_fname'          => new \Fieldmanager_TextField(
					[
						'label'        => __( 'First Name', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
				'faculty_lname'          => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Last Name', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
				'academic_title'         => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Position', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
				'faculty_office_address' => new \Fieldmanager_TextArea(
					[
						'label'        => __( 'Address', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes_textarea,
					]
				),
				'faculty_email'          => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Email', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
				'faculty_phone'          => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Phone', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
				'faculty_linkedin'       => new \Fieldmanager_TextField(
					[
						'label'        => __( 'LinkedIn URL', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
				'faculty_twitter'        => new \Fieldmanager_TextField(
					[
						'label'        => __( 'Twitter URL', 'cps' ),
						'inline_label' => true,
						'attributes'   => $attributes,
					]
				),
			],
		]
	);

	$group->add_meta_box( __( 'Profile', 'cps' ), 'cps-person' );
}

/**
 * Register faculty grid profile info meta box.
 *
 * The items that are added via this meta box are rendered as a list in the
 * faculty grid item for a person.
 */
function register_custom_profile_meta_box() {

	$group = new \Fieldmanager_Group(
		[
			'name'           => 'nu_fim_custom_profile',
			'limit'          => 0,
			'label'          => __( 'List Item', 'cps' ),
			'add_more_label' => __( 'Add Item', 'cps' ),
			'sortable'       => true,
			'children'       => [
				'item' => new \Fieldmanager_TextField(),
			],
		]
	);

	$group->add_meta_box( __( 'Faculty Grid Profile Info', 'cps' ), 'cps-person' );
}
