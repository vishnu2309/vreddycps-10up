<?php
/**
 * Contains the person core admin functionality
 *
 * @package CPS
 */

namespace CPS\Person\Admin;

/**
 * Add actions and filters.
 */
function setup() {

	// Maybe remove quick edit for non-hierarchical person post type.
	add_filter( 'post_row_actions', __NAMESPACE__ . '\remove_quick_edit', 10, 2 );

	// Maybe remove quick edit for hierarchical person post type.
	add_filter( 'page_row_actions', __NAMESPACE__ . '\remove_quick_edit', 10, 2 );

	// Clear cache used for the faculty directory block.
	add_action( 'save_post_cps-person', __NAMESPACE__ . '\clear_cache', 10, 3 );
}

/**
 * Maybe remove 'quick edit' action.
 *
 * @param array   $actions An array of row action links.
 * @param WP_Post $post    The post object.
 */
function remove_quick_edit( $actions, $post ) {

	if ( 'cps-person' === $post->post_type ) {
		unset( $actions['inline hide-if-no-js'] );
	}

	return $actions;
}

/**
 * Clear cache used for the faculty directory block.
 *
 * @param int      $post_ID Post ID.
 * @param \WP_Post $post    Post object.
 * @param bool     $update  Whether this is an existing post being updated or not.
 */
function clear_cache( $post_ID, $post, $update ) {
	wp_cache_delete( 'cps_faculty_members' );
}
