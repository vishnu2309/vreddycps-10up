<?php
/**
 * Contains program post type helper functions
 *
 * @package CPS
 */

namespace CPS\Person\Helpers;

/**
 * Check whether the faculty member is imported vis FIM API.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return bool
 */
function is_imported( $faculty_id ) {
	return (bool) get_post_meta( $faculty_id, 'nu_fim_nuid', true );
}

/**
 * Get meta key of the faculty.
 *
 * @param int    $faculty_id Faculty (person) ID.
 * @param string $type       Which data is required. Default empty
 *
 * @return string
 */
function get_faculty_meta_key( $faculty_id, $type = '' ) {

	// Check whether faculty is imported through FIM API.
	$is_imported = is_imported( $faculty_id );

	switch ( $type ) {

		case 'position':
			$meta_key = 'nu_fim_academic_title';
			break;

		case 'address':
			$meta_key = 'nu_fim_faculty_office_address';
			break;

		case 'email':
			$meta_key = 'nu_fim_faculty_email';
			break;

		case 'phone_number':
			$meta_key = 'nu_fim_faculty_phone';
			break;

		case 'linkedin':
			$meta_key = 'nu_fim_faculty_linkedin';
			break;

		case 'twitter':
			$meta_key = 'nu_fim_faculty_twitter';
			break;

		case 'about':
			$meta_key = 'nu_fim_faculty_bio';
			break;

		case 'education':
			$meta_key = 'nu_fim_faculty_edu_degree';
			break;

		case 'prof_exp':
			$meta_key = 'nu_fim_faculty_prof_exp';
			break;

		case 'publications':
			$meta_key = 'nu_fim_faculty_research_articles';
			break;

		default:
			$meta_key = '';
			break;
	}

	return $meta_key;
}

/**
 * Get faculty member's position.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_position( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'position' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get faculty member's address.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return string|null
 */
function get_faculty_address( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'address' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	$office_address = get_post_meta( $faculty_id, $meta_key, true );

	if ( ! is_imported( $faculty_id ) ) {
		return $office_address;
	}

	$address  = empty( $office_address[0]['field_faculty_maildrop'] ) ? '' : $office_address[0]['field_faculty_maildrop'] . ' ';
	$address .= empty( $office_address[0]['field_faculty_building'] ) ? '' : $office_address[0]['field_faculty_building'] . ' ';
	$address .= empty( $office_address[0]['field_faculty_city'] ) ? '' : $office_address[0]['field_faculty_city'] . ' ';
	$address .= empty( $office_address[0]['field_faculty_state'] ) ? '' : $office_address[0]['field_faculty_state'] . ' ';
	$address .= empty( $office_address[0]['field_faculty_zip'] ) ? '' : $office_address[0]['field_faculty_zip'];

	return $address;
}

/**
 * Get faculty member's email.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_email( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'email' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get faculty member's phone number.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_phone_number( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'phone_number' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get faculty member's linkedin link.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_linkedin( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'linkedin' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	$meta_value = get_post_meta( $faculty_id, $meta_key, true );

	// The FIM API returns an array, for manually created persons it's a string.
	if ( ! is_imported( $faculty_id ) ) {
		$meta_value = array(
			'url' => $meta_value,
		);
	};

	return $meta_value;
}

/**
 * Get faculty member's twitter link.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_twitter( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'twitter' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	$meta_value = get_post_meta( $faculty_id, $meta_key, true );

	// The FIM API returns an array, for manually created persons it's a string.
	if ( ! is_imported( $faculty_id ) ) {
		$meta_value = array(
			'url' => $meta_value,
		);
	};

	return $meta_value;
}

/**
 * Get faculty member's bio.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|string
 */
function get_faculty_about( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'about' );

	if ( empty( $meta_key ) ) {
		return get_the_content( null, false, $faculty_id );
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get faculty member's education.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_education( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'education' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get faculty member's professional experience.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_prof_exp( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'prof_exp' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get faculty member's publication list.
 *
 * @param int $faculty_id Faculty (person) ID.
 *
 * @return mixed|null
 */
function get_faculty_publications( $faculty_id ) {

	$meta_key = get_faculty_meta_key( $faculty_id, 'publications' );

	if ( empty( $meta_key ) ) {
		return null;
	}

	return get_post_meta( $faculty_id, $meta_key, true );
}

/**
 * Get a link to either the faculty spotlight page or the
 * faculty detail page.
 *
 * Useful for the faculty grid item.
 *
 * @param int|WP_Post $faculty_id Post ID or WP_Post objet.
 * @return string URL
 */
function get_faculty_link( $faculty_id ) {

	$post = get_post( $faculty_id );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return '';
	}

	$args = array(
		'no_found_rows'          => true,
		'update_post_term_cache' => false,
		'post_type'              => 'page',
		'posts_per_page'         => 1,
		'fields'                 => 'ids',
		'meta_key'               => 'cps_faculty_id',
		'meta_value'             => $post->ID,
	);

	$query = new \WP_Query( $args );

	if ( $query->have_posts() ) {
		return get_the_permalink( $query->posts[0] );
	}

	return get_the_permalink( $post );
}
