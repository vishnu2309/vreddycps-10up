<?php
/**
 * "Person Category" custom taxonomy.
 *
 * The "Person Category" taxonomy is used to categorize faculty members.
 *
 * @package CPS
 */

namespace CPS\Taxonomy\PersonCategory;

/**
 * Add actions and filters.
 */
function setup() {

	// Register custom taxonomy.
	add_action( 'init', __NAMESPACE__ . '\register' );

	// Hide parent select from area of study meta box.
	add_action( 'admin_footer', __NAMESPACE__ . '\hide_parent_select' );

	// Assign "Faculty" term to the imported faculty members.
	add_action( 'nu_fim_after_insert_post', __NAMESPACE__ . '\assign_faculty_term', 10, 2 );
	add_action( 'nu_fim_after_update_post', __NAMESPACE__ . '\assign_faculty_term', 10, 2 );
}

/**
 * Register custom taxonomy.
 *
 * @since 1.0.0
 * @return void
 */
function register() {

	$labels = array(
		'name'                       => esc_html__( 'Category', 'cps' ),
		'singular_name'              => esc_html__( 'Category', 'cps' ),
		'search_items'               => esc_html__( 'Search Categories', 'cps' ),
		'popular_items'              => esc_html__( 'Categories', 'cps' ),
		'all_items'                  => esc_html__( 'All Categories', 'cps' ),
		'parent_item'                => esc_html__( 'Parent Category', 'cps' ),
		'parent_item_colon'          => esc_html__( 'Parent Category:', 'cps' ),
		'edit_item'                  => esc_html__( 'Edit Category', 'cps' ),
		'view_item'                  => esc_html__( 'View Category', 'cps' ),
		'update_item'                => esc_html__( 'Update Category', 'cps' ),
		'add_new_item'               => esc_html__( 'Add New Category', 'cps' ),
		'new_item_name'              => esc_html__( 'New Category', 'cps' ),
		'separate_items_with_commas' => esc_html__( 'Separate items with commas', 'cps' ),
		'add_or_remove_items'        => esc_html__( 'Add or remove categories', 'cps' ),
		'choose_from_most_used'      => esc_html__( 'Choose from the most used categories', 'cps' ),
		'not_found'                  => esc_html__( 'No categories found', 'cps' ),
		'no_terms'                   => esc_html__( 'No categories', 'cps' ),
		'items_list_navigation'      => esc_html__( 'Categories', 'cps' ),
		'items_list'                 => esc_html__( 'Categories', 'cps' ),
		'most_used'                  => esc_html__( 'Most Used', 'cps' ),
		'back_to_items'              => esc_html__( 'Back to categories', 'cps' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => esc_html__( 'The Person Category is used to categorize faculty members.', 'cps' ),
		'public'             => false,
		'publicly_queryable' => false,
		'hierarchical'       => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => false,
		'show_in_rest'       => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => false,
		'show_admin_column'  => false,
		'meta_box_cb'        => 'post_categories_meta_box',
		'query_var'          => 'person-category',
	);

	register_taxonomy(
		'person_cat',
		'cps-person',
		$args
	);
}

/**
 * Hide parent select from area of study meta box.
 *
 * The nu_fim_area_of_study is non-hierarchical but uses the core 'category'
 * meta box. This function adds an inline style element that hides
 * the parent term select field.
 *
 * @since 1.0.0
 * @return void
 */
function hide_parent_select() {

	$current_screen = get_current_screen();

	if ( 'post' === $current_screen->base && 'cps-person' === $current_screen->post_type ) {
		echo '<style>#newperson_cat_parent{display:none;}</style>';
	}
}

/**
 * Assign "Faculty" term to the imported faculty members using FIM API.
 *
 * @param int   $post_id Post ID.
 * @param array $data    Data from the FIM API used to insert or update the post.
 */
function assign_faculty_term( $post_id, $data ) {

	$term_name = 'Faculty';
	$taxonomy  = 'person_cat';

	// Check whether the term has already been created.
	$_term = get_term_by( 'name', $term_name, $taxonomy );

	if ( empty( $_term ) ) {

		// Create new term.
		$inserted_term = wp_insert_term( $term_name, $taxonomy );

		// If we fail to create new term, bail out.
		if ( is_wp_error( $inserted_term ) ) {
			return; // TODO: Check for error
		}

		if ( empty( $inserted_term['term_id'] ) ) {
			return; // TODO: Check for error
		}

		$term_id = $inserted_term['term_id'];

	} else {
		$term_id = $_term->term_id;
	}

	if ( ! empty( $term_id ) ) {
		wp_set_object_terms( $post_id, $term_id, $taxonomy, true );
	}
}
