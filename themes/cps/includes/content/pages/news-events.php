<?php
/**
 * Contains News & Events page functionality
 *
 * @package CPS
 */

namespace CPS\Page\NewsEvents;

/**
 * Add actions and filters.
 */
function setup() {

	add_filter( 'query_vars', __NAMESPACE__ . '\query_vars_filter' );
}

/**
 * Add query var 'filter'
 *
 * @param array $vars Query vars.
 * @return array Updated query vars.
 */
function query_vars_filter( $vars ) {
	$vars[] = 'topic';
	$vars[] = 'month';
	return $vars;
}
