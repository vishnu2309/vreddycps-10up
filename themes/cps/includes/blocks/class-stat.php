<?php
/**
 * Register the Stat block server-side.
 *
 * @package CPS\Blocks
 */

namespace CPS\Blocks;

/**
 * Initialize Ad Slot block.
 */
class Stat extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'stat';
		$attributes = [
			'copy'             => [
				'type' => 'text',
			],
			'headline'         => [
				'type' => 'text',
			],
			'statImage'        => [
				'type' => 'text',
			],
			'statImageAltText' => [
				'type' => 'text',
			],
			'statText'         => [
				'type' => 'text',
			],
			'statTextSize'     => [
				'type' => 'text',
			],
			'statType'         => [
				'type' => 'text',
			],
			'colorScheme'      => [
				'type' => 'string',
			],
			'className'        => [
				'type' => 'string',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		if ( ! empty( $attributes['statImage'] ) ) {
			$attributes['statImage'] = json_decode( $attributes['statImage'], true );
		}

		ob_start();

		include locate_template( 'partials/block-stat.php' );

		return ob_get_clean();
	}
}
