<?php
/**
 * Register the Section Heading block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Section Heading block.
 */
class SectionHeading extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'section-heading';
		$attributes = [
			'headline' => [
				'type' => 'text',
			],
			'copy'     => [
				'type' => 'text',
			],
		];

		$post_types = [ 'post', 'page' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		if ( empty( $attributes['headline'] ) && empty( $attributes['copy'] ) ) {
			return;
		}

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
