<?php
/**
 * Containt Abstract Block
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Block class.
 *
 * @todo Required parameters could be set using abstract methods in stead of
 *       using public parameters.
 *
 * Convenience class to prevent boilerplate code when registering a block.
 */
abstract class Block {

	/**
	 * Block name.
	 *
	 * @var string
	 */
	public $name;

	/**
	 * Block attributes.
	 *
	 * @var array
	 */
	public $attributes;

	/**
	 * Static reference to the single instance.
	 *
	 * @var object
	 */
	protected static $instance;

	/**
	 * Class constructor.
	 *
	 * @param string $name       Name of the block, will be namespaced with cps/...
	 * @param array  $attributes Block attributes. Required when using ServerSideRender to
	 *                           preview the block in the admin.
	 * @param array  $post_types Optional, List of post types the block may be used on. Defaults
	 *                           to all post types.
	 */
	protected function __construct( $name = '', $attributes = [], $post_types = [] ) {

		if ( empty( $name ) ) {
			return; // Fail silently.
		}

		$this->name       = $name;
		$this->attributes = $attributes;
		$this->post_types = $post_types;

		$this->init();
	}

	/**
	 * Initialize the block.
	 *
	 * @return void
	 */
	public function init() {

		// Register block.
		add_action( 'wp_loaded', [ $this, 'register' ] );

		// Restrict block to post types.
		add_filter( 'disallowed_block_types', [ $this, 'maybe_disallow_block_type' ], 10, 2 );
	}

	/**
	 * Get object instance.
	 *
	 * @return Block
	 */
	final public static function get_instance() {

		static $instances = array();

		$called_class = get_called_class();

		if ( ! isset( $instances[ $called_class ] ) ) {
			$instances[ $called_class ] = new $called_class();
		}

		return $instances[ $called_class ];
	}

	/**
	 * Register block server-side.
	 *
	 * @return void
	 */
	public function register() {

		if ( ! function_exists( 'register_block_type' ) ) {
			return;
		}

		register_block_type(
			$this->get_name(),
			array(
				'render_callback' => array( $this, 'render' ),
				'attributes'      => $this->attributes,
			)
		);
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		return ''; // Blocks will need to implement this.
	}

	/**
	 * Restrict block to post types.
	 *
	 * @param array   $disallowed_block_types List of block types.
	 * @param WP_Post $post                   Current post object.
	 * @return array
	 */
	public function maybe_disallow_block_type( $disallowed_block_types, $post ) {

		if ( ! empty( $this->post_types ) && is_array( $this->post_types ) ) {

			if ( ! in_array( $post->post_type, $this->post_types, true ) ) {

				if ( ! isset( $disallowed_block_types[ $this->get_name() ] ) ) {
					$disallowed_block_types[] = $this->get_name();
				}
			}
		}

		return $disallowed_block_types;
	}

	/**
	 * Get prefixed block name.
	 *
	 * @return string
	 */
	protected function get_name() {
		return 'cps/' . $this->name;
	}
}
