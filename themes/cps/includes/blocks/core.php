<?php
/**
 * Gutenberg Blocks setup
 *
 * @package CPS
 */

namespace CPS\Blocks;

// Base block class.
require_once CPS_INC . 'blocks/abstract-block.php';

// Include blocks.
require_once CPS_INC . 'blocks/class-accordion.php';
require_once CPS_INC . 'blocks/class-accordion-item.php';
require_once CPS_INC . 'blocks/class-card.php';
require_once CPS_INC . 'blocks/class-carousel.php';
require_once CPS_INC . 'blocks/class-example.php';
require_once CPS_INC . 'blocks/class-faculty-directory.php';
require_once CPS_INC . 'blocks/class-faculty-grid.php';
require_once CPS_INC . 'blocks/class-faculty-grid-column.php';
require_once CPS_INC . 'blocks/class-footer-contact.php';
require_once CPS_INC . 'blocks/class-hero.php';
require_once CPS_INC . 'blocks/class-icon-columns.php';
require_once CPS_INC . 'blocks/class-icon-column.php';
require_once CPS_INC . 'blocks/class-image-text.php';
require_once CPS_INC . 'blocks/class-program-finder.php';
require_once CPS_INC . 'blocks/class-program-grid.php';
require_once CPS_INC . 'blocks/class-program-grid-item.php';
require_once CPS_INC . 'blocks/class-quote.php';
require_once CPS_INC . 'blocks/class-section-heading.php';
require_once CPS_INC . 'blocks/class-stat.php';

/**
 * Set up blocks and API endpoints.
 *
 * @return void
 */
function setup() {

	// Register blocks.
	add_action( 'init', __NAMESPACE__ . '\init_blocks' );

	// Enqueue editor-only JavaScript/CSS for blocks.
	add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\blocks_editor_scripts' );

	// Add 'Custom Blocks' block category.
	add_filter( 'block_categories', __NAMESPACE__ . '\block_categories', 10, 2 );

	// Add custom 'disallowed_block_types' filter.
	add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\add_disallowed_block_types_filter' );

	// Disable selecting a custom color for a block.
	add_action( 'after_setup_theme', __NAMESPACE__ . '\disable_custom_colors' );

	// Maybe set a navigation color class on the body element.
	add_filter( 'body_class', __NAMESPACE__ . '\maybe_add_navigation_color_body_class' );
}

/**
 * Register all blocks.
 *
 * @return void
 */
function init_blocks() {

	Accordion::get_instance();
	Accordion_Item::get_instance();
	Card::get_instance();
	Carousel::get_instance();
	Faculty_Directory::get_instance();
	Faculty_Grid::get_instance();
	Faculty_Grid_Column::get_instance();
	FooterContact::get_instance();
	Hero::get_instance();
	Icon_Columns::get_instance();
	Icon_Column::get_instance();
	ImageText::get_instance();
	ProgramFinder::get_instance();
	Program_Grid::get_instance();
	Program_Grid_Item::get_instance();
	Quote::get_instance();
	SectionHeading::get_instance();
	Stat::get_instance();
}

/**
 * Enqueue editor-only JavaScript/CSS for blocks.
 *
 * @return void
 */
function blocks_editor_scripts() {

	wp_enqueue_script(
		'cps-blocks-editor', // Note: Do code search before renaming!
		CPS_TEMPLATE_URL . '/dist/js/blocks-editor.js',
		array(
			'wp-api',
			'wp-blocks',
			'wp-components',
			'wp-dom',
			'wp-editor',
			'wp-element',
			'wp-i18n',
		),
		CPS_VERSION,
		true
	);

	wp_localize_script(
		'cps-blocks-editor',
		'cpsGutenbergVars',
		[
			'nonce' => wp_create_nonce( 'wp_rest' ),
		]
	);

	wp_enqueue_style(
		'google-fonts',
		'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&display=swap',
		[],
		CPS_VERSION
	);

	wp_enqueue_style(
		'typekit',
		'//use.typekit.net/wme2ziw.css',
		[],
		CPS_VERSION
	);

	wp_enqueue_style(
		'cps-editor-style',
		CPS_TEMPLATE_URL . '/dist/css/editor-style.css',
		[],
		CPS_VERSION
	);

	wp_localize_script(
		'cps-blocks-editor',
		'cpsBlockVars',
		array(
			'nonce' => wp_create_nonce( 'wp_rest' ),
		)
	);
}

/**
 * Add 'CPS Blocks' block category.
 *
 * @param array  $categories Registered categories.
 * @param object $post       The post object.
 *
 * @return array Filtered categories.
 */
function block_categories( $categories, $post ) {

	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'cps-blocks',
				'title' => __( 'CPS Blocks', 'cps' ),
			),
		)
	);
}

/**
 * Add custom 'disallowed_block_types' filter.
 *
 * Gutenberg has an 'allowed_block_types' filter which is not useful for blacklisting
 * specific blocks. This function adds a 'disallowed_block_types' filter.
 *
 * The filter does not use the `wp.blocks.unregisterBlockType()` function, as blocks may
 * not have been registered at that point. In stead a filter is added that removes the disallowed
 * blocks from the block inserter.
 */
function add_disallowed_block_types_filter() {

	$post = get_post();

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return;
	}

	$disallowed_block_types = apply_filters( 'disallowed_block_types', [], $post );

	if ( ! is_array( $disallowed_block_types ) || empty( $disallowed_block_types ) ) {
		return;
	}

	$disallowed_block_types = wp_json_encode( $disallowed_block_types );

	if ( ! $disallowed_block_types ) {
		return;
	}

	$script = <<<TAG
(function(){
	var excludeBlocks = ${disallowed_block_types};
	wp.hooks.addFilter('blocks.registerBlockType', 'cps/exclude-blocks', function(settings, name) {
		if ( excludeBlocks.indexOf(name) !== -1 ) {
			return Object.assign({}, settings, {
				supports: Object.assign({}, settings.supports, {inserter: false})
			});
		}
		return settings;
	});
})();
TAG;

	wp_add_inline_script( 'wp-blocks', $script, 'after' );
}

/**
 * Disable selecting a custom background or text color for a block.
 *
 * @retun void
 */
function disable_custom_colors() {

	add_theme_support( 'disable-custom-colors', true );
	add_theme_support( 'disable-custom-gradients', true );
	add_theme_support( 'editor-gradient-presets', [] );
	add_theme_support(
		'editor-color-palette',
		[
			[
				'name'  => __( 'Red', 'cps' ),
				'slug'  => 'red',
				'color' => '#d41b2c',
			],
			[
				'name'  => __( 'Yellow', 'cps' ),
				'slug'  => 'yellow',
				'color' => '#ffbf3d',
			],
			[
				'name'  => __( 'Dark Gray', 'cps' ),
				'slug'  => 'dark-gray',
				'color' => '#272727',
			],
			[
				'name'  => __( 'Black', 'cps' ),
				'slug'  => 'black',
				'color' => '#000',
			],
			[
				'name'  => __( 'White', 'cps' ),
				'slug'  => 'white',
				'color' => '#fff',
			],
		]
	);
}

/**
 * Maybe set a navigation color class on the body element.
 *
 * The hero block contains a navigation color attribute. This filter:
 * - Checks if there is a hero block on the page
 * - If it is the first block on the page
 * - And assigns a navigation color class
 *
 * @todo A future update might save the navigation color to a post meta field in stead
 *       of as a block attribute.
 *
 * @param array $body_class A list of classnames
 * @return array
 */
function maybe_add_navigation_color_body_class( $body_class ) {

	if ( ! is_singular() ) {
		return $body_class;
	}

	$post = get_post();

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return $body_class;
	}

	if ( 'cps-program' === $post->post_type ) {

		$color = get_post_meta( get_the_ID(), 'cps_program_nav_color', true );

		if ( ! empty( $color ) ) {
			$body_class[] = 'navigation-color-' . (string) $color;
		} else {
			$body_class[] = 'navigation-color-white';
		}

		return $body_class;
	}

	if ( has_block( 'cps/hero', $post ) ) {

		$blocks = parse_blocks( $post->post_content );

		if ( 'cps/hero' === $blocks[0]['blockName'] ) {

			if ( isset( $blocks[0]['attrs']['navigationColor'] ) && ! empty( $blocks[0]['attrs']['navigationColor'] ) ) {
				$body_class[] = 'navigation-color-' . $blocks[0]['attrs']['navigationColor'];
			}

			if ( isset( $blocks[0]['attrs']['backgroundColor'] ) && ! empty( $blocks[0]['attrs']['backgroundColor'] ) ) {
				$body_class[] = 'hero-background-color-' . $blocks[0]['attrs']['backgroundColor'];
			}
		}
	} else {
		$body_class[] = 'navigation-color-white';
	}

	if ( 'cps-person' === get_post_type() ) {
		$body_class[] = 'navigation-color-white';
	}

	return $body_class;
}
