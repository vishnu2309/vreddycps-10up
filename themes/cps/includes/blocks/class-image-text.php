<?php
/**
 * Register the Image & Text block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Image & Text block.
 */
class ImageText extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'image-text';
		$attributes = [
			'copy'          => [
				'type' => 'text',
			],
			'ctaLabelOne'   => [
				'type' => 'text',
			],
			'ctaLinkOne'    => [
				'type' => 'text',
			],
			'ctaLabelTwo'   => [
				'type' => 'text',
			],
			'ctaLinkTwo'    => [
				'type' => 'text',
			],
			'ctaLabelThree' => [
				'type' => 'text',
			],
			'ctaLinkThree'  => [
				'type' => 'text',
			],
			'headline'      => [
				'type' => 'text',
			],
			'image'         => [
				'type' => 'string',
			],
			'mediaPosition' => [
				'type'    => 'string',
				'default' => 'right',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		if ( ! empty( $attributes['image'] ) ) {
			$attributes['image'] = json_decode( $attributes['image'], true );
		}

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
