<?php
/**
 * Register the Quote block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Quote block.
 */
class Quote extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'quote';
		$attributes = [
			'quote'   => [
				'type' => 'text',
			],
			'name'    => [
				'type' => 'text',
			],
			'subhead' => [
				'type' => 'text',
			],
			'link'    => [
				'type' => 'text',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event', 'cps-person' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
