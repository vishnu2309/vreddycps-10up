<?php
/**
 * Register the Footer Contact block server-side.
 *
 * Note: This block is intended to be used for the cps-contact-block post type only.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Footer Contact block.
 */
class FooterContact extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'footer-contact';
		$attributes = [
			'headline'        => [
				'type' => 'string',
			],
			'copy'            => [
				'type' => 'string',
			],
			'columneOneCopy'  => [
				'type' => 'string',
			],
			'columnTwoCopy'   => [
				'type' => 'string',
			],
			'columnThreeCopy' => [
				'type' => 'string',
			],
			'colorScheme'     => [
				'type' => 'string',
			],
		];

		$post_types = [ 'cps-contact-block' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
