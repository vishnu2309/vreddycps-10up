<?php
/**
 * Register the Carousel block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Carousel block.
 */
class Carousel extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'carousel';
		$attributes = [
			'columnCount' => [
				'type'    => 'int',
				'default' => 1,
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		// Add wrapper div to Carousel Item block.
		add_filter( 'render_block', [ $this, 'add_wrapper_div' ], 9999, 2 );

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		if ( empty( $attributes['columnCount'] ) ) {
			$column_count = 1;
		} else {
			$column_count = absint( $attributes['columnCount'] );
			$column_count = $attributes['columnCount'] > 3 ? 3 : $attributes['columnCount'];
		}

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}

	/**
	 * Add wrapper div to Carousel Item block.
	 *
	 * Adding the wrapper in PHP so we don't have to do that in the JS block definition,
	 * which allows for more flexibility should we ever need to change the markup.
	 *
	 * @param string $block_content The block content about to be appended.
	 * @param array  $block         The full block, including name and attributes.
	 * @return string
	 */
	public function add_wrapper_div( $block_content, $block ) {

		if ( 'cps/carousel-item' !== $block['blockName'] ) {
			return $block_content;
		}

		return '<div class="block-carousel-item">' . $block_content . '</div>';
	}
}
