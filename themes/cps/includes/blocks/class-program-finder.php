<?php
/**
 * Register the Program Finder block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Program Finder block.
 */
class ProgramFinder extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'program-finder';
		$attributes = [
			'headline'            => [
				'type' => 'text',
			],
			'buttonLabel'         => [
				'type' => 'text',
			],
			'dropdownLabel'       => [
				'type' => 'text',
			],
			'linksPrimary'        => [
				'type' => 'text',
			],
			'linksPrimaryLabel'   => [
				'type' => 'text',
			],
			'linksSecondary'      => [
				'type' => 'text',
			],
			'linksSecondaryLabel' => [
				'type' => 'text',
			],
		];

		parent::__construct( $name, $attributes );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		$attributes['buttonLabel'] = empty( $attributes['buttonLabel'] ) ? __( "Let's Go", 'cps' ) : $attributes['buttonLabel'];

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
