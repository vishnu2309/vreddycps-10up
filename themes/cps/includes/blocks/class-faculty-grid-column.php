<?php
/**
 * Register the Faculty Column block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Faculty Column block.
 */
class Faculty_Grid_Column extends Block {

	/**
	 * Column data.
	 *
	 * @var string
	 */
	protected $data;

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name = 'faculty-grid-column';

		$attributes = [
			'image'     => [
				'type'    => 'string',
				'default' => '[]',
			],
			'titleText' => [
				'type'    => 'string',
				'default' => '',
			],
			'ctaLink'   => [
				'type'    => 'string',
				'default' => '',
			],
			'postId'    => [
				'type'    => 'int',
				'default' => '',
			],
			'profExp'   => [
				'type'    => 'string',
				'default' => '',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		$post_data = false;

		if ( ! empty( $attributes['postId'] ) ) {
			$post_data = self::get_post_data( $attributes['postId'] );
		}

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}

	/**
	 * Get formatted post data array.
	 *
	 * Note: This static function is also used by the rest api endpoint
	 *       for the Faculty Grid block.
	 *
	 * @param int|\WP_Post $post Post ID or WP_Post object.
	 *
	 * @return false|array
	 */
	public static function get_post_data( $post ) {

		$post = get_post( $post );

		if ( ! is_a( $post, '\WP_Post' ) ) {
			return false;
		}

		$image_url         = self::get_placeholder_image();
		$post_thumbnail_id = get_post_thumbnail_id( $post );

		if ( ! empty( $post_thumbnail_id ) ) {

			$attachment_src = wp_get_attachment_image_src( $post_thumbnail_id, 'full', false );

			if ( $attachment_src ) {
				$image_url = $attachment_src[0];
			}
		}

		$data = [
			'titleText'     => get_the_title( $post ),
			'image'         => $image_url,
			'imageID'       => $post_thumbnail_id,
			'ctaLink'       => get_permalink( $post ),
			'profExp'       => self::get_details( $post ),
			'customProfExp' => self::get_custom_profile( $post ),
		];

		return $data;
	}

	/**
	 * Get a list of faculty member details.
	 *
	 * @param int|\WP_Post $post Post ID or WP_Post object.
	 * @return array
	 */
	public static function get_details( $post ) {

		$post = get_post( $post );

		if ( ! is_a( $post, '\WP_Post' ) ) {
			return [];
		}

		$details           = [];
		$faculty_positions = get_post_meta( $post->ID, 'nu_fim_faculty_positions', true );

		if ( ! empty( $faculty_positions ) && is_array( $faculty_positions ) ) {
			foreach ( $faculty_positions as $faculty_position ) {
				if ( ! empty( $faculty_position['field_faculty_academic_title'] ) ) {
					$details[] = $faculty_position['field_faculty_academic_title'];
				}
			}
		}

		if ( empty( $details ) ) {
			$academic_title = get_post_meta( $post->ID, 'nu_fim_academic_title', true );
			if ( ! empty( $academic_title ) ) {
				$details[] = $academic_title;
			}
		}

		return $details;
	}

	/**
	 * Get a list of faculty member custom details.
	 *
	 * @param int|\WP_Post $post Post ID or WP_Post object.
	 * @return array
	 */
	public static function get_custom_profile( $post ) {

		$post = get_post( $post );

		if ( ! is_a( $post, '\WP_Post' ) ) {
			return [];
		}

		$details = [];
		$items   = get_post_meta( $post->ID, 'nu_fim_custom_profile', true );

		if ( ! empty( $items ) && is_array( $items ) ) {
			foreach ( $items as $item ) {
				if ( ! empty( $item['item'] ) ) {
					$details[] = $item['item'];
				}
			}
		}

		return $details;
	}

	/**
	 * Get placeholder image.
	 *
	 * @return string Grey 1 x 1 placeholder gif image.
	 */
	public static function get_placeholder_image() {

		return 'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==';
	}
}
