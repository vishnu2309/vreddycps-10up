<?php
/**
 * Register the Accordion-Item block server-side.
 *
 * @package CPS\Blocks
 */

namespace CPS\Blocks;

/**
 * Initialize Accordion-Item block.
 */
class Accordion_Item extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name = 'accordion-item';

		$attributes = [
			'title' => [
				'type' => 'string',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
