<?php
/**
 * Register the Program Grid block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Program Grid block.
 */
class Program_Grid extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name = 'program-grid';

		$attributes = [
			'copy'         => [
				'type' => 'string',
			],
			'headline'     => [
				'type' => 'string',
			],
			'mode'         => [
				'type'    => 'string',
				'default' => 'automatic',
			],
			'programLevel' => [
				'type' => 'string',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		$attributes['programLevel'] = empty( $attributes['programLevel'] ) ? [] : json_decode( $attributes['programLevel'] );

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
