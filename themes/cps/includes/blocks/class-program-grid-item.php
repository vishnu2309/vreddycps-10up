<?php
/**
 * Register the Program Grid Item block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Program Grid Item block.
 */
class Program_Grid_Item extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name = 'program-grid-item';

		$attributes = [
			'postId'    => [
				'type'    => 'int',
				'default' => '',
			],
			'titleText' => [
				'type'    => 'string',
				'default' => '',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		$program = false;

		if ( ! empty( $attributes['postId'] ) ) {
			$program = cps_get_single_program( $attributes['postId'] );
		}

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
