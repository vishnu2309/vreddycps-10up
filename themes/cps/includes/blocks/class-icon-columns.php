<?php
/**
 * Register the Icon Columns block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Icon Columns block.
 */
class Icon_Columns extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name = 'icon-columns';

		$attributes = [
			'columns'  => [
				'type'    => 'int',
				'default' => 3,
			],
			'copy'     => [
				'type' => 'string',
			],
			'headline' => [
				'type' => 'string',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
