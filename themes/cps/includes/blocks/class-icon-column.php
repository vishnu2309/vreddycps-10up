<?php
/**
 * Register the Icon Column block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Icon Column block.
 */
class Icon_Column extends Block {

	/**
	 * Column data.
	 *
	 * @var string
	 */
	protected $data;

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name = 'icon-column';

		$attributes = [
			'icon'  => [
				'type'    => 'string',
				'default' => '',
			],
			'title' => [
				'type'    => 'string',
				'default' => '',
			],
			'copy'  => [
				'type'    => 'string',
				'default' => '',
			],
		];

		$post_types = [ 'post', 'page', 'cps-program', 'cps-event' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
