<?php
/**
 * Register the Faculty & Staff Directory block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Faculty & Staff Directory block.
 */
class Faculty_Directory extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'faculty-directory';
		$attributes = [
			'copy'     => [
				'type' => 'text',
			],
			'headline' => [
				'type' => 'text',
			],
		];

		$post_types = [ 'page' ];

		parent::__construct( $name, [], $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 *
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
