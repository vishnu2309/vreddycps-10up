<?php
/**
 * Register the Hero block server-side.
 *
 * @package CPS
 */

namespace CPS\Blocks;

/**
 * Initialize Hero block.
 */
class Hero extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'hero';
		$attributes = [
			'backgroundType'    => [
				'type' => 'text',
			],
			'backgroundColor'   => [
				'type' => 'text',
			],
			'copy'              => [
				'type' => 'text',
			],
			'ctaPrimaryLabel'   => [
				'type' => 'text',
			],
			'ctaPrimaryUrl'     => [
				'type' => 'text',
			],
			'ctaSecondaryLabel' => [
				'type' => 'text',
			],
			'ctaSecondaryUrl'   => [
				'type' => 'text',
			],
			'headline'          => [
				'type' => 'text',
			],
			'image'             => [
				'type' => 'text', // JSON object.
			],
			'imageAlignment'    => [
				'type' => 'text',
			],
			'navigationColor'   => [
				'type' => 'text',
			],
			'overlayColor'      => [
				'type' => 'text',
			],
			'overlayOpacity'    => [
				'type' => 'text',
			],
			'textAlignment'     => [
				'type' => 'text',
			],
			'videoUrl'          => [
				'type' => 'text',
			],
			'fixedRatio'        => [
				'type'    => 'boolean',
				'default' => true,
			],
		];

		$post_types = [ 'post', 'page' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Fetch video from Wistia.
	 *
	 * @see https://wistia.com/learn/culture/fullscreen-video-homepage
	 *
	 * @param array $attributes Block attributes.
	 * @return false|string Video HTML on success, else false.
	 */
	protected function fetch_video( $attributes ) {

		if ( empty( $attributes['videoUrl'] ) ) {
			return false;
		}

		$oembed = wp_oembed_get(
			$attributes['videoUrl'],
			array(
				'hero' => true,
			)
		);

		if ( ! $oembed ) {
			return false;
		}

		return $oembed;
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		$attributes['video'] = $this->fetch_video( $attributes );

		if ( empty( $attributes['image'] ) ) {
			$attributes['image'] = false;
		} else {

			$image_obj = json_decode( $attributes['image'] );

			if ( $image_obj ) {
				$attributes['image'] = $image_obj->id;
			}
		}

		ob_start();

		include locate_template( 'partials/block-' . $this->name . '.php' );

		return ob_get_clean();
	}
}
