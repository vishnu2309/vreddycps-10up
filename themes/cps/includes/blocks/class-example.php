<?php
/**
 * Register the Example block server-side.
 *
 * @package CPS\Blocks
 */

namespace CPS\Blocks;

/**
 * Initialize Ad Slot block.
 */
class Example extends Block {

	/**
	 * Class constructor.
	 */
	protected function __construct() {

		$name       = 'example';
		$attributes = [
			'customTitle' => [
				'type' => 'text',
			],
		];
		$post_types = [ 'post' ];

		parent::__construct( $name, $attributes, $post_types );
	}

	/**
	 * Render block.
	 *
	 * @param array  $attributes Block attributes.
	 * @param string $content    Block content, if available. A block can be dynamic
	 *                           but still implement its save function to return HTML.
	 *                           The save function output is returned in the $content
	 *                           variable.
	 * @return string Block HTML content.
	 */
	public function render( $attributes, $content ) {

		if ( empty( $attributes['customTitle'] ) ) {
			return;
		}

		ob_start();

		include locate_template( 'partials/block-example.php' );

		return ob_get_clean();
	}
}
