<?php
/**
 * REST API functionality.
 *
 * @package CPS
 */

namespace CPS\REST;

use CPS\Blocks\Faculty_Grid_Column;

/**
 * Set up blocks and API endpoints.
 *
 * @return void
 */
function setup() {

	// Filter Restricted Site Access plugin to allow REST API requests.
	add_filter( 'restricted_site_access_is_restricted', __NAMESPACE__ . '\unrestrict_rest_api', 10, 2 );

	// Register json api endpoints.
	add_action( 'rest_api_init', __NAMESPACE__ . '\register_endpoints' );
}

/**
 * Filter Restricted Site Access to allow REST API requests.
 *
 * @param bool   $is_restricted Whether access is restricted.
 * @param object $wp            The WordPress object.
 *
 * @return bool Whether access should be restricted.
 */
function unrestrict_rest_api( $is_restricted, $wp ) {

	if ( ! empty( $wp->query_vars['rest_route'] ) ) {
		return false;
	}

	return $is_restricted;
}

/**
 * Register json api endpoints.
 *
 * Note: Endpoints use the following path: /wp-json/cps/v1/...
 *
 * @see register_rest_route();
 */
function register_endpoints() {

	$namespace = 'cps/v1';

	// Register route for getting column data by post id for the faculty-grid block.
	register_rest_route(
		$namespace,
		'/blocks/faculty-grid/post/(?P<id>\d+)',
		array(
			array(
				'methods'             => 'GET',
				'callback'            => __NAMESPACE__ . '\endpoint_faculty_grid_get_post',
				'args'                => array(
					'id' => array(
						'validate_callback' => function ( $param ) {
							return is_numeric( $param );
						},
					),
				),
				'permission_callback' => function ( $request ) {
					return current_user_can( 'edit_posts' );
				},
			),
		)
	);

	// Register route for getting load-more data.
	register_rest_route(
		$namespace,
		'/load-more',
		array(
			array(
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => __NAMESPACE__ . '\load_more_posts',
				'permission_callback' => function ( $request ) {
					return true;
				},
				'args'                => array(
					'page'  => array(
						'validate_callback' => function ( $param ) {
							return is_numeric( $param );
						},
					),
					'query' => array(
						'validate_callback' => function ( $param ) {
							return is_string( $param );
						},
					),
				),
			),
		)
	);
}

/**
 * Get faculty-column data by post id for the faculty-grid block.
 *
 * @param \WP_REST_Request $request Request object.
 *
 * @return \WP_Error|\WP_REST_Response Error if no post found, else a list containing
 *                                     post title, link, featured image.
 */
function endpoint_faculty_grid_get_post( $request ) {

	$id   = absint( $request->get_param( 'id' ) );
	$data = Faculty_Grid_Column::get_post_data( $id );

	if ( ! $data ) {
		return new \WP_Error(
			'error',
			__( 'Post not found.', 'cps' ),
			array(
				'status' => 200,
			)
		);
	}

	return new \WP_REST_Response( $data, 200 );
}

/**
 * Get load-more posts data with its HTML markup.
 *
 * @param \WP_REST_Request $request Request object.
 *
 * @return \WP_Error|\WP_REST_Response Error if no post found, else a list containing
 *                                     post title, link, featured image.
 */
function load_more_posts( $request ) {

	$page    = absint( $request->get_param( 'page' ) );
	$query   = $request->get_param( 'query' );
	$partial = $request->get_param( 'partial' );

	$args                = json_decode( stripslashes( $query ), true );
	$args['paged']       = $page + 1;
	$args['post_status'] = 'publish';

	$query = new \WP_Query( $args );
	$html  = '';
	$index = get_option( 'posts_per_page' ) * $page;

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();

			if ( 'partials/search-item.php' === $partial ) {

				ob_start();

				include locate_template( $partial );

				$html .= ob_get_clean();

			} else {

				ob_start();

				$media_position = ( 0 === $index % 2 ) ? 'left' : 'right';

				$attributes = array(
					'mediaPosition' => $media_position,
					'headline'      => get_the_title(),
					'copy'          => get_the_excerpt(),
					'ctaLabelOne'   => __( 'Learn More', 'cps' ),
					'ctaLinkOne'    => get_the_permalink(),
				);

				if ( has_post_thumbnail() ) {
					$attributes['image']['id'] = get_post_thumbnail_id();
				}

				include locate_template( $partial );

				$index++;

				$html .= ob_get_clean();
			}
		}
	}

	return new \WP_REST_Response( $html, 200 );
}
