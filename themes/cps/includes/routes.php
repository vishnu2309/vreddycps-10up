<?php
/**
 * Admin functionality
 *
 * @package CPS
 */

namespace CPS\Routes;

/**
 * Add actions and filters.
 *
 * @return void
 */
function setup() {

	// Remove unused rewrite rules.
	add_filter( 'date_rewrite_rules', '__return_empty_array' );
	add_filter( 'author_rewrite_rules', '__return_empty_array' );
	add_filter( 'rewrite_rules_array', __NAMESPACE__ . '\remove_rewrite_rules', 999 );

}

/**
 * Remove unused rewrite rules.
 *
 * @param array $rules The compiled array of rewrite rules.
 * @return array
 */
function remove_rewrite_rules( $rules ) {
	$rules_to_remove = array(
		'attachment',
		'comment',
		'embed',
		'feed',
		'tag',
		'type',
		'[^/]+/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$', // Attachments.
		'[^/]+/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$',
		'[^/]+/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/print(/(.*))?/?$',
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$',
		'[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/print(/(.*))?/?$',
		'[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$',
		'[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$',
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$', // Date based archives.
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$',
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/print(/(.*))?/?$',
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$',
		'([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$',
		'([0-9]{4})/([0-9]{1,2})/print(/(.*))?/?$',
		'([0-9]{4})/([0-9]{1,2})/?$',
		'([0-9]{4})/page/?([0-9]{1,})/?$',
		'([0-9]{4})/print(/(.*))?/?$',
		'([0-9]{4})/?$',
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$',
		'([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/print(/(.*))?/?$',
	);

	foreach ( $rules as $rule => $rewrite ) {
		foreach ( $rules_to_remove as $rule_to_remove ) {
			if ( false !== strpos( $rule, $rule_to_remove ) ) {
				unset( $rules[ $rule ] );
			}
		}
	}
	return $rules;
}
