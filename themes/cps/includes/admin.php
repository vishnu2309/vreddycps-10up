<?php
/**
 * Admin functionality
 *
 * @package CPS
 */

namespace CPS\Admin;

/**
 * Add actions and filters.
 *
 * @return void
 */
function setup() {

	// Enqueue styles for the admin.
	add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\enqueue_styles' );

	// Add "Call To Action" submenu for Floating CTA.
	add_action( 'admin_menu', __NAMESPACE__ . '\add_cta_settings_page' );
	add_action( 'admin_init', __NAMESPACE__ . '\register_settings' );
}

/**
 * Enqueue styles for the admin.
 *
 * @return void
 */
function enqueue_styles() {

	wp_enqueue_style(
		'cps-styles',
		CPS_TEMPLATE_URL . '/dist/css/admin-style.css',
		[],
		CPS_VERSION
	);

	$current_screen = get_current_screen();

	// Datepicker styles for the event calendar on the event edit screen.
	if ( 'cps-event' === $current_screen->id ) {

		wp_enqueue_style(
			'jquery-ui',
			CPS_TEMPLATE_URL . '/assets/css/admin/vendor/jquery-ui.min.css',
			[],
			CPS_VERSION
		);

		wp_enqueue_style(
			'datepicker',
			CPS_TEMPLATE_URL . '/assets/css/admin/vendor/datepicker.css',
			[],
			CPS_VERSION
		);
	}
}

/**
 * Add settings page.
 *
 * @return void
 */
function add_cta_settings_page() {

	add_options_page(
		__( 'Call To Action', 'cps' ),
		__( 'Call To Action', 'cps' ),
		'edit_posts',
		'call-to-action',
		__NAMESPACE__ . '\render_settings_page'
	);
}

/**
 * Render the settings page.
 *
 * @return void
 */
function render_settings_page() {

	?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<p>
			<?php esc_html_e( 'The call-to-action links will be rendered below a Hero block on the Home and Program Detail pages.', 'cps' ); ?>
		</p>
		<form action="options.php" method="post">
			<?php
			settings_fields( 'cps_floating_cta' );
			do_settings_sections( 'call-to-action' );
			?>
			<br />
			<?php submit_button(); ?>
		</form>
	</div>
	<?php
}

/**
 * Register all the setting fields.
 *
 * @return void
 */
function register_settings() {

	// Add settings group.
	register_setting(
		'cps_floating_cta',
		'cps_floating_cta',
		__NAMESPACE__ . '\fields_validation'
	);

	// Add settings section inside the main group.
	add_settings_section(
		'first_link',
		__( 'First Link', 'cps' ),
		'',
		'call-to-action'
	);

	// Add "Label" field.
	add_settings_field(
		'cps_first_link_label',
		__( 'Label', 'cps' ),
		__NAMESPACE__ . '\settings_field_first_link_label',
		'call-to-action',
		'first_link'
	);

	// Add "URL" field.
	add_settings_field(
		'cps_first_link_url',
		__( 'URL', 'cps' ),
		__NAMESPACE__ . '\settings_field_first_link_url',
		'call-to-action',
		'first_link'
	);

	// Add settings section inside the main group.
	add_settings_section(
		'second_link',
		__( 'Second Link', 'cps' ),
		'',
		'call-to-action'
	);

	// Add "Label" field.
	add_settings_field(
		'cps_second_link_label',
		__( 'Label', 'cps' ),
		__NAMESPACE__ . '\settings_field_second_link_label',
		'call-to-action',
		'second_link'
	);

	// Add "URL" field.
	add_settings_field(
		'cps_second_link_url',
		__( 'URL', 'cps' ),
		__NAMESPACE__ . '\settings_field_second_link_url',
		'call-to-action',
		'second_link'
	);
}

/**
 * Validates all values from the settings fields before saving them to the database.
 *
 * @param array $input Entered field values in the settings page.
 *
 * @return array Sanitized field values.
 */
function fields_validation( $input ) {

	$sanitized_values = [];

	if ( isset( $input['first_link_label'] ) ) {
		$sanitized_values['first_link_label'] = sanitize_text_field( $input['first_link_label'] );
	}

	if ( isset( $input['first_link_url'] ) ) {
		$sanitized_values['first_link_url'] = esc_url_raw( $input['first_link_url'] );
	}

	if ( isset( $input['second_link_label'] ) ) {
		$sanitized_values['second_link_label'] = sanitize_text_field( $input['second_link_label'] );
	}

	if ( isset( $input['second_link_url'] ) ) {
		$sanitized_values['second_link_url'] = esc_url_raw( $input['second_link_url'] );
	}

	return $sanitized_values;
}

/**
 * Output the "First Link Label" field.
 *
 * @return void
 */
function settings_field_first_link_label() {

	$options          = get_option( 'cps_floating_cta', [] );
	$first_link_label = ! empty( $options['first_link_label'] ) ? $options['first_link_label'] : '';

	printf(
		'<input size="50" id="cps_cta_first_link_label" name="cps_floating_cta[first_link_label]" type="text" value="%s" />',
		esc_attr( $first_link_label )
	);
}

/**
 * Output the "First Link URL" field.
 *
 * @return void
 */
function settings_field_first_link_url() {

	$options        = get_option( 'cps_floating_cta', [] );
	$first_link_url = ! empty( $options['first_link_url'] ) ? $options['first_link_url'] : '';

	printf(
		'<input size="50" id="cps_cta_first_link_url" name="cps_floating_cta[first_link_url]" type="text" value="%s" />',
		esc_attr( $first_link_url )
	);
}

/**
 * Output the "Second Link Label" field.
 *
 * @return void
 */
function settings_field_second_link_label() {

	$options           = get_option( 'cps_floating_cta', [] );
	$second_link_label = ! empty( $options['second_link_label'] ) ? $options['second_link_label'] : '';

	printf(
		'<input size="50" id="cps_cta_second_link_label" name="cps_floating_cta[second_link_label]" type="text" value="%s" />',
		esc_attr( $second_link_label )
	);
}

/**
 * Output the "Second Link URL" field.
 *
 * @return void
 */
function settings_field_second_link_url() {

	$options         = get_option( 'cps_floating_cta', [] );
	$second_link_url = ! empty( $options['second_link_url'] ) ? $options['second_link_url'] : '';

	printf(
		'<input size="50" id="cps_cta_second_link_url" name="cps_floating_cta[second_link_url]" type="text" value="%s" />',
		esc_attr( $second_link_url )
	);
}
