<?php
/**
 * Search functionality customization.
 *
 * @package CPS\Search
 */

namespace CPS\Search;

/**
 * Add actions and filters.
 *
 * @return void
 */
function setup() {

	// Filter search query.
	add_action( 'pre_get_posts', __NAMESPACE__ . '\filter_search_query' );
}

/**
 * Modify search query.
 *
 * @param \WP_Query $wp_query \WP_Query object.
 */
function filter_search_query( $wp_query ) {

	// If not a search query, bail out early.
	if ( is_admin() || ! $wp_query->is_main_query() || ! $wp_query->is_search() ) {
		return;
	}

	$active_type  = filter_input( INPUT_GET, 'type', FILTER_SANITIZE_STRING );
	$active_type  = empty( $active_type ) ? 'all' : $active_type;
	$content_type = cps_get_searched_content_type( $active_type );

	// If searched type is not matched with the provided one, bail out.
	if ( empty( $content_type ) ) {

		$wp_query->set( 'post_type', '' );

		return;
	}

	// Set post-types.
	$wp_query->set( 'post_type', $content_type['post-types'] );

	// If terms are also set, include in the query.
	if ( ! empty( $content_type['terms'] ) ) {

		$tax_query = $wp_query->get( 'tax_query', [] );

		foreach ( $content_type['terms'] as $taxonomy => $terms ) {

			$tax_query[] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $terms,
			);
		}

		$wp_query->set( 'tax_query', $tax_query );
	}

	// If meta are also set, include in the query.
	if ( ! empty( $content_type['meta'] ) ) {

		$searched_term = get_search_query();

		$meta_query = $wp_query->get( 'meta_query', [] );

		foreach ( $content_type['meta'] as $meta_key ) {

			$meta_query[] = array(
				'key'     => $meta_key,
				'value'   => $searched_term,
				'compare' => 'LIKE',
			);
		}

		$meta_query['relation'] = 'OR';

		$wp_query->set( 'meta_query', $meta_query );
	}
}
