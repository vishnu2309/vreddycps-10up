<?php
/**
 * Core setup, site hooks and filters.
 *
 * @package CPS\Core
 */

namespace CPS\Core;

/**
 * Set up theme defaults and register supported WordPress features.
 *
 * @return void
 */
function setup() {

	// Make Theme available for translation.
	add_action( 'after_setup_theme', __NAMESPACE__ . '\i18n' );

	// Set up theme defaults and registers support for various WordPress features.
	add_action( 'after_setup_theme', __NAMESPACE__ . '\theme_setup' );

	// Enqueue scripts for front-end.
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_scripts' );

	// Maybe add async or defer attribute on script tag.
	add_filter( 'script_loader_tag', __NAMESPACE__ . '\maybe_add_async_or_defer_attribute', 10, 3 );

	// Move jQuery to footer.
	add_action( 'wp_default_scripts', __NAMESPACE__ . '\move_jquery_to_footer', 9999 );

	// Dequeue jQuery Migrate.
	add_filter( 'wp_default_scripts', __NAMESPACE__ . '\dequeue_jquery_migrate' );

	// Enqueue styles for front-end.
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\enqueue_styles' );

	// Add JavaScript detection.
	add_action( 'wp_head', __NAMESPACE__ . '\js_detection', 0 );

	// Append a link tag used to add a manifest.json to the head.
	add_action( 'wp_head', __NAMESPACE__ . '\add_manifest', 10 );

	// Add async/defer attributes to enqueued scripts that have the specified script_execution flag.
	add_filter( 'script_loader_tag', __NAMESPACE__ . '\script_loader_tag', 10, 2 );

	// Maybe add body classes if the NU global header and footer are activated.
	add_filter( 'body_class', __NAMESPACE__ . '\maybe_add_global_navigation_body_classes' );

	// Generate custom excerpt.
	add_filter( 'get_the_excerpt', __NAMESPACE__ . '\generate_custom_excerpt', 10, 2 );

	// Tweak oembed result for hero block
	add_filter( 'oembed_result', __NAMESPACE__ . '\hero_oembed', 10, 3 );

	// Prevent saving empty metadata fields, fieldmanager likes doing this.
	add_action( 'added_post_meta', __NAMESPACE__ . '\dont_save_empty_metadata', 11, 999 );

	// Use our own breadcrumb trail class.
	add_filter( 'breadcrumb_trail_object', __NAMESPACE__ . '\filter_breadcrumb_trail_object', 10, 2 );
}

/**
 * Make Theme available for translation.
 *
 * Translations can be added to the /languages directory.
 * If you're building a theme based on "cps", change the
 * filename of '/languages/cps.pot' to the name of your project.
 *
 * @return void
 */
function i18n() {
	load_theme_textdomain( 'cps', CPS_PATH . '/languages' );
}

/**
 * Set up theme defaults and registers support for various WordPress features.
 */
function theme_setup() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support(
		'html5',
		array(
			'search-form',
			'gallery',
		)
	);

	// This theme uses wp_nav_menu() in three locations.
	register_nav_menus(
		array(
			'navigation-main'   => esc_html__( 'Main Navigation', 'cps' ),
			'navigation-footer' => esc_html__( 'Footer Navigation', 'cps' ),
			'navigation-social' => esc_html__( 'Social Networks', 'cps' ),
			'navigation-serp'   => esc_html__( 'Search Page Navigation', 'cps' ),
		)
	);

	// Register image sizes
	add_image_size( 'hero-large-2x', 2880, 1100, false );
	add_image_size( 'hero-large', 1440, 550, false );
	add_image_size( 'hero-small-2x', 720, 275, false );
	add_image_size( 'hero-small', 360, 137, false );
	add_image_size( 'profile-2x', 1026, 712, false );
	add_image_size( 'profile', 513, 356, false );
}

/**
 * Enqueue scripts for front-end.
 *
 * Note: The program finder and faculty directory blocks
 *       enqueue their scripts in the block template.
 *
 * @return void
 */
function enqueue_scripts() {

	global $wp_query;

	wp_enqueue_script(
		'cps',
		CPS_TEMPLATE_URL . '/dist/js/frontend.js',
		[ 'wp-api-fetch', 'wp-url', 'jquery' ],
		CPS_VERSION,
		true
	);

	if ( is_page_template( 'templates/page-styleguide.php' ) ) {
		wp_enqueue_script(
			'styleguide',
			CPS_TEMPLATE_URL . '/dist/js/styleguide.js',
			[],
			CPS_VERSION,
			true
		);
	}

	if ( is_page_template( 'templates/page-news-events.php' ) ) {
		$news_events_query = cps_get_news_events();
		wp_localize_script(
			'cps',
			'cpsLoadMore',
			[
				'nonce'        => wp_create_nonce( 'wp_rest' ),
				'posts'        => wp_json_encode( $news_events_query->query_vars ),
				'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
				'max_page'     => $news_events_query->max_num_pages,
				'partial'      => 'partials/block-image-text.php',
			]
		);
	} else {
		wp_localize_script(
			'cps',
			'cpsLoadMore',
			[
				'nonce'        => wp_create_nonce( 'wp_rest' ),
				'posts'        => wp_json_encode( $wp_query->query_vars ),
				'current_page' => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
				'max_page'     => $wp_query->max_num_pages,
				'partial'      => 'partials/search-item.php',
			]
		);
	}
}

/**
 * Maybe add async or defer attribute to enqueued script.
 *
 * @param string $tag    The <script> tag for the enqueued script.
 * @param string $handle The script's registered handle.
 * @param string $src    The script's source URL.
 *
 * @return mixed
 */
function maybe_add_async_or_defer_attribute( $tag, $handle, $src ) {

	$handles = [
		'frontend' => 'defer',
		'jquery'   => 'defer',
	];

	// Add async or defer attribute.
	if ( array_key_exists( $handle, $handles ) ) {
		$tag = str_replace( '<script', '<script ' . $handles[ $handle ], $tag );
	}

	return $tag;
}

/**
 * Dequeue jQuery migrate.
 *
 * @param WP_Scripts $scripts Default scripts.
 */
function dequeue_jquery_migrate( $scripts ) {
	if ( ! is_admin() ) {
		$scripts->remove( 'jquery' );
		$scripts->add( 'jquery', false, array( 'jquery-core' ), false );
	}
}

/**
 * Move jQuery to footer.
 *
 * @param WP_Scripts $wp_scripts WP_Scripts instance, passed by reference.
 */
function move_jquery_to_footer( $wp_scripts ) {

	if ( is_admin() ) {
		return;
	}

	$wp_scripts->add_data( 'jquery', 'group', 1 );
	$wp_scripts->add_data( 'jquery-core', 'group', 1 );
	$wp_scripts->add_data( 'jquery-migrate', 'group', 1 );
}

/**
 * Enqueue styles for front-end.
 *
 * @return void
 */
function enqueue_styles() {

	wp_enqueue_style(
		'styles',
		CPS_TEMPLATE_URL . '/dist/css/style.css',
		[],
		CPS_VERSION
	);

	wp_enqueue_style(
		'google-fonts',
		'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&display=swap',
		[],
		CPS_VERSION
	);

	wp_enqueue_style(
		'typekit',
		'//use.typekit.net/wme2ziw.css',
		[],
		CPS_VERSION
	);

	if ( is_page_template( 'templates/page-styleguide.php' ) ) {
		wp_enqueue_style(
			'styleguide',
			CPS_TEMPLATE_URL . '/dist/css/styleguide-style.css',
			[],
			CPS_VERSION
		);
	}
}

/**
 * Add JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @return void
 */
function js_detection() {

	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}


/**
 * Append a link tag used to add a manifest.json to the head.
 *
 * @return void
 */
function add_manifest() {
	echo "<link rel='manifest' href='" . esc_url( CPS_TEMPLATE_URL . '/manifest.json' ) . "' />";
}

/**
 * Add async/defer attributes to enqueued scripts that have the specified script_execution flag.
 *
 * @link https://core.trac.wordpress.org/ticket/12009
 * @param string $tag    The script tag.
 * @param string $handle The script handle.
 * @return string
 */
function script_loader_tag( $tag, $handle ) {
	$script_execution = wp_scripts()->get_data( $handle, 'script_execution' );

	if ( ! $script_execution ) {
		return $tag;
	}

	if ( 'async' !== $script_execution && 'defer' !== $script_execution ) {
		return $tag;
	}

	// Abort adding async/defer for scripts that have this script as a dependency. _doing_it_wrong()?
	foreach ( wp_scripts()->registered as $script ) {
		if ( in_array( $handle, $script->deps, true ) ) {
			return $tag;
		}
	}

	// Add the attribute if it hasn't already been added.
	if ( ! preg_match( ":\s$script_execution(=|>|\s):", $tag ) ) {
		$tag = preg_replace( ':(?=></script>):', " $script_execution", $tag, 1 );
	}

	return $tag;
}

/**
 * Maybe add body classes if the NU global header and footer are activated.
 *
 * The NU Loader plugin is added as an mu-plugin and allows activating the
 * university-wide naviation menu.
 *
 * @param array $classes List of body classes
 * @return array
 */
function maybe_add_global_navigation_body_classes( $classes ) {

	$classes[] = 'cps-theme';

	if ( ! empty( get_option( 'global_header' ) ) && 'on' === get_option( 'global_header' ) ) {
		$classes[] = 'has-global-header';
	}

	if ( ! empty( get_option( 'global_footer' ) ) && 'on' === get_option( 'global_footer' ) ) {
		$classes[] = 'has-global-footer';
	}

	return $classes;
}

/**
 * Generate custom excerpt.
 *
 * @param string   $excerpt The post excerpt.
 * @param \WP_Post $post    Post object.
 *
 * @return string
 */
function generate_custom_excerpt( $excerpt, $post ) {

	$custom_excerpt = '';

	if ( 'cps-program' === $post->post_type ) {
		$custom_excerpt = get_post_meta( $post->ID, 'nu_pim_body', true );
	} elseif ( 'cps-person' === $post->post_type ) {
		$custom_excerpt = get_post_meta( $post->ID, 'nu_fim_faculty_short_bio', true );
	}

	if ( ! empty( $custom_excerpt ) ) {

		$custom_excerpt = strip_shortcodes( $custom_excerpt );
		$custom_excerpt = excerpt_remove_blocks( $custom_excerpt );
		$custom_excerpt = str_replace( ']]>', ']]&gt;', $custom_excerpt );

		/* translators: Maximum number of words used in a post excerpt. */
		$excerpt_length = intval( _x( '55', 'excerpt_length' ) );
		$excerpt_length = (int) apply_filters( 'excerpt_length', $excerpt_length );

		$excerpt_more = apply_filters( 'excerpt_more', ' [&hellip;]' );

		$excerpt = wp_trim_words( $custom_excerpt, $excerpt_length, $excerpt_more );
	}

	return $excerpt;
}

/**
 * Add arguments to embedded Youtube videos in Hero block
 *
 * @param string|false $data The returned oEmbed HTML (false if unsafe).
 * @param string       $url  URL of the content to be embedded.
 * @param array        $args Optional arguments, usually passed from a shortcode.
 *
 * @return string|false The UNSANITIZED (and potentially unsafe) HTML that should be used to embed on success, false on failure.
 */
function hero_oembed( $data, $url, $args ) {

	if ( $args['hero'] ) {
		return str_replace( '?feature=oembed', '?feature=oembed&autoplay=1&mute=1&autohide=1&disablekb=1&controls=0&showinfo=0&modestbranding=1&loop=1&fs=0&autohide=0&rel=0&playsinline=1&enablejsapi=1', $data );
	}

	return $data;
}

/**
 * Prevent saving empty metadata fields, fieldmanager likes doing this.
 *
 * @since 1.0.0
 * @param int    $mid        The meta ID after successful update.
 * @param int    $object_id  Object ID.
 * @param string $meta_key   Meta key.
 * @param mixed  $meta_value Meta value.
 * @return void
 */
function dont_save_empty_metadata( $mid, $object_id, $meta_key, $meta_value ) {

	if ( ! is_array( $meta_value ) && '' === trim( $meta_value ) ) {
		delete_post_meta( $object_id, $meta_key );
	}
}

/**
 * Use our own breadcrumb trail class.
 *
 * @param null  $null Empty.
 * @param array $args Arguments to pass to Breadcrumb_Trail.
 * @return object Breadcrumb trail class.
 */
function filter_breadcrumb_trail_object( $null, $args ) {
	require_once CPS_INC . 'classes/breadcrumb-trail-walker.php';
	return new \CPS_Breadcrumb_Trail( $args );
}
