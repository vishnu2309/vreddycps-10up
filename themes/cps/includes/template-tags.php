<?php
/**
 * Custom template tags for this theme.
 *
 * This file is for custom template tags only and it should not contain
 * functions that will be used for filtering or adding an action.
 *
 * All functions should be prefixed with cps_ in order to prevent
 * pollution of the global namespace and potential conflicts with functions
 * from plugins.
 * Example: `cps_function()`
 *
 * @package CPS
 */

/**
 * Get all faculty members from given args.
 *
 * @param array $args Arguments.
 *
 * @return array|bool|mixed
 */
function cps_get_faculty_members( $args = [] ) {

	$cache_key           = 'cps_faculty_members';
	$all_faculty_members = wp_cache_get( $cache_key );

	if ( ! empty( $all_faculty_members ) ) {
		return $all_faculty_members;
	} else {
		$all_faculty_members = [];
	}

	$offset         = 0;
	$posts_per_page = 100;

	$default_args = array(
		'no_found_rows'          => true,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
		'offset'                 => $offset,
		'posts_per_page'         => $posts_per_page,
		'post_type'              => 'cps-person',
		'fields'                 => 'ids',
		'meta_key'               => 'nu_fim_faculty_lname',
		'orderby'                => 'meta_value title',
		'order'                  => 'ASC',
	);

	$args = wp_parse_args( $args, $default_args );

	do {
		$faculty_members = new \WP_Query( $args );
		$post_count      = count( $faculty_members->posts );

		// Break, if no posts found.
		if ( 0 === $post_count ) {
			break;
		}

		foreach ( $faculty_members->posts as $faculty_member_id ) {

			$first_name = get_post_meta( $faculty_member_id, 'nu_fim_faculty_fname', true );
			$last_name  = get_post_meta( $faculty_member_id, 'nu_fim_faculty_lname', true );
			$role       = get_post_meta( $faculty_member_id, 'nu_fim_academic_title', true );

			// TODO: Need to check if any faculty member does not have last name set.
			if ( empty( $last_name ) ) {
				continue;
			} else {
				$key = strtolower( substr( $last_name, 0, 1 ) );
			}

			$all_faculty_members[ $key ][] = [
				'title'      => get_the_title( $faculty_member_id ),
				'first_name' => $first_name,
				'last_name'  => $last_name,
				'role'       => $role,
				'link'       => get_the_permalink( $faculty_member_id ),
			];
		}

		$offset        += $post_count;
		$args['offset'] = $offset;

	} while ( $post_count === $posts_per_page );

	ksort( $all_faculty_members );

	if ( ! empty( $all_faculty_members ) ) {
		wp_cache_set( $cache_key, $all_faculty_members, HOUR_IN_SECONDS );
	}

	return $all_faculty_members;
}

/**
 * An svg template helper.
 *
 * @param string  $name The SVG Name.
 * @param boolean $echo Either return or echo the SVG Icon string.
 *
 * @return html
 */
function cps_svg_icon( $name, $echo = false ) {

	$file_path = CPS_PATH . '/dist/svg/' . $name . '.svg';

	if ( ! file_exists( $file_path ) ) {
		return;
	}

	$svg_icon = file_get_contents( $file_path ); // phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents

	if ( $echo ) {
		echo wp_kses(
			$svg_icon,
			array(
				'svg'            => array(
					'version'           => true,
					'class'             => true,
					'fill'              => true,
					'height'            => true,
					'xml:space'         => true,
					'xmlns'             => true,
					'xmlns:xlink'       => true,
					'viewbox'           => true,
					'enable-background' => true,
					'width'             => true,
					'x'                 => true,
					'y'                 => true,
				),
				'path'           => array(
					'clip-rule'    => true,
					'd'            => true,
					'fill'         => true,
					'fill-rule'    => true,
					'stroke'       => true,
					'stroke-width' => true,
				),
				'g'              => array(
					'clip-rule'    => true,
					'd'            => true,
					'transform'    => true,
					'fill'         => true,
					'fill-rule'    => true,
					'stroke'       => true,
					'stroke-width' => true,
				),
				'polygon'        => array(
					'clip-rule'    => true,
					'd'            => true,
					'fill'         => true,
					'fill-rule'    => true,
					'stroke'       => true,
					'stroke-width' => true,
					'points'       => true,
				),
				'circle'         => array(
					'clip-rule'    => true,
					'd'            => true,
					'fill'         => true,
					'fill-rule'    => true,
					'stroke'       => true,
					'stroke-width' => true,
					'cx'           => true,
					'cy'           => true,
					'r'            => true,
				),
				'lineargradient' => array(
					'id'                => true,
					'gradientunits'     => true,
					'x'                 => true,
					'y'                 => true,
					'x2'                => true,
					'y2'                => true,
					'gradienttransform' => true,
				),
				'stop'           => array(
					'offset' => true,
					'style'  => true,
				),
			)
		);

		return;
	}

	return $svg_icon;
}

/**
 * Get all programs.
 *
 * @param array $args Additional arguments array.
 *
 * @return array|bool|mixed
 */
function cps_get_programs( $args = [] ) {

	$cache_key = 'cps_programs';

	// Default args.
	$query_args = [
		'no_found_rows'          => true,
		'update_post_meta_cache' => false,
		'update_post_term_cache' => false,
		'post_type'              => 'cps-program',
		'fields'                 => 'ids',
		'tax_query'              => [],
	];

	// Check if program-level is set.
	if ( ! empty( $args['programLevel'] ) ) {

		// It'll be easier to clear out the cache on post save.
		sort( $args['programLevel'] );

		$query_args['tax_query'][] = array(
			'taxonomy' => 'nu_pim_degree_type',
			'field'    => 'term_id',
			'terms'    => $args['programLevel'],
		);

		// Make dynamic cache key.
		$cache_key .= '_' . join( '', $args['programLevel'] );
	}

	$all_programs = wp_cache_get( $cache_key );

	if ( ! empty( $all_programs ) ) {
		return $all_programs;
	} else {
		$all_programs = [];
	}

	$offset         = 0;
	$posts_per_page = 100;

	$query_args['offset']         = $offset;
	$query_args['posts_per_page'] = $posts_per_page;

	do {
		$programs   = new \WP_Query( $query_args );
		$post_count = count( $programs->posts );

		// Break, if no posts found.
		if ( 0 === $post_count ) {
			break;
		}

		foreach ( $programs->posts as $program_id ) {

			$program_data = cps_get_single_program( $program_id );

			if ( empty( $program_data ) ) {
				continue;
			}

			$all_programs[] = $program_data;
		}

		$offset              += $post_count;
		$query_args['offset'] = $offset;

	} while ( $post_count === $posts_per_page );

	if ( ! empty( $all_programs ) ) {
		wp_cache_set( $cache_key, $all_programs, HOUR_IN_SECONDS ); // Todo: Need to check cache expiration time.
	}

	return $all_programs;
}

/**
 * Returns single program data using program ID.
 *
 * @param int $program_id Program ID.
 *
 * @return array
 */
function cps_get_single_program( $program_id ) {

	$program      = get_post( $program_id );
	$program_data = [];

	if ( ! is_a( $program, 'WP_Post' ) ) {
		return $program_data;
	}

	$formatted_title = get_post_meta( $program->ID, 'nu_pim_formatted_title', true );

	// TODO: Need to check if any program does not have formatted_title set.
	if ( empty( $formatted_title ) ) {
		return $program_data;
	}

	$body = \CPS\Program\Helpers\get_program_short_description( $program->ID );

	$location  = '';
	$locations = cps_get_program_location_names( $program->ID );

	if ( ! empty( $locations ) ) {
		$location = $locations[0];
	}

	// Get the initial from formatted title.
	$initial = cps_get_program_initials( $program->ID, $formatted_title );

	// If not able to format the initial, move to next one.
	if ( empty( $initial ) ) {
		return $program_data;
	}

	$level          = '';
	$program_levels = get_the_terms( $program->ID, 'nu_pim_degree_type' );

	if ( ! is_wp_error( $program_levels ) && ! empty( $program_levels ) ) {
		$level = $program_levels[0]->name;
	}

	return [
		'title'    => $formatted_title,
		'initials' => $initial,
		'body'     => wp_strip_all_tags( $body ),
		'link'     => get_the_permalink( $program->ID ),
		'location' => $location,
		'level'    => $level,
	];
}

/**
 * Get initials of the formatted title.
 *
 * @param int    $post_id       Post ID.
 * @param string $program_title Program's formatted title.
 *
 * @return string
 */
function cps_get_program_initials( $post_id, $program_title ) {

	$post_meta = get_post_meta( $post_id, 'cps_program_initials', true );

	if ( ! empty( $post_meta ) ) {
		return $post_meta;
	}

	// Replace any special characters with space.
	$program_title = preg_replace( '/[^A-Za-z0-9]/', ' ', $program_title );

	// Explode the string with space.
	$program_title_parts = explode( ' ', $program_title );

	$program_initial = '';

	foreach ( $program_title_parts as $word ) {

		// Remove any extra spaces.
		$word = trim( $word );

		// If empty, move to next one.
		if ( empty( $word ) ) {
			continue;
		}

		// If first character of the string is in UPPERCASE or DIGIT, include it in creating the initial.
		if ( ctype_upper( $word[0] ) || ctype_digit( $word[0] ) ) {
			$program_initial .= $word[0];
		} elseif ( in_array( $word, [ 'eLearning' ], true ) ) { // Some words are starts with lowercase, but we definitely want them.
			$program_initial .= substr( $word, 0, 2 );
		}
	}

	return $program_initial;
}

/**
 * Get a list of program location names.
 *
 * Note: At the time of writing this, a program is expected to have only one location
 *       but for future compatability we'll just fetch multiple if there are any.
 *
 * @param int|\WP_Post $post Post ID or WP_Post object.
 *
 * @return array A list of program level names.
 * @since 1.0.0
 */
function cps_get_program_location_names( $post ) {

	$locations = [];
	$post      = get_post( $post );

	if ( ! is_a( $post, '\WP_Post' ) ) {
		return [];
	}

	$location_terms = wp_get_post_terms( $post->ID, 'nu_pim_location' );

	if ( is_wp_error( $location_terms ) ) {
		return [];
	}

	$term_name = wp_list_pluck( $location_terms, 'name' );

	return $term_name;
}

/**
 * Gets an array of social links, indexed by type => URL.
 *
 * @return array
 */
function cps_get_social_links() {
	if ( ! class_exists( 'WPSEO_Options' ) ) {
		return [];
	}

	$links       = [];
	$seo_options = \WPSEO_Options::get_option( 'wpseo_social' );
	foreach ( $seo_options as $key => $val ) {
		if ( ! is_string( $val ) || ! $val ) {
			continue;
		}
		$site     = false;
		$site_pos = strpos( $key, '_site' );
		$url_pos  = strpos( $key, '_url' );
		if ( false !== $site_pos ) {
			$site = substr( $key, 0, $site_pos );
		} elseif ( false !== $url_pos ) {
			$site = substr( $key, 0, $url_pos );
		}

		if ( $site ) {
			// Twitter needs to have the site prepended.
			if ( 'twitter' === $site ) {
				$val = "https://twitter.com/{$val}";
			}
			$links[ $site ] = $val;
		}
	}

	return $links;
}

/**
 * Set searchable content types and its related fields.
 *
 * @return array[]
 */
function cps_get_searchable_content_types() {

	return array(
		'all'     => array(
			'post-types' => array(
				'cps-event',
				'cps-person',
				'post',
				'cps-program',
			),
			'label'      => __( 'All', 'cps' ),
		),
		'event'   => array(
			'post-types' => array(
				'cps-event',
			),
			'label'      => __( 'Event', 'cps' ),
		),
		'person'  => array(
			'post-types' => array(
				'cps-person',
			),
			'terms'      => array(
				'person_cat' => array(
					'faculty',
				),
			),
			'meta'       => array(
				'nu_fim_academic_title',
				'nu_fim_faculty_bio',
				'nu_fim_faculty_edu_degree',
				'nu_fim_faculty_prof_exp',
				'nu_fim_faculty_research_articles',
			),
			'label'      => __( 'Faculty', 'cps' ),
		),
		'news'    => array(
			'post-types' => array(
				'post',
			),
			'label'      => __( 'News', 'cps' ),
		),
		'program' => array(
			'post-types' => array(
				'cps-program',
			),
			'label'      => __( 'Program', 'cps' ),
		),
	);
}

/**
 * Get single content type and its fields.
 *
 * @param string $type Content type.
 *
 * @return array|false
 */
function cps_get_searched_content_type( $type ) {

	$content_types = cps_get_searchable_content_types();
	$type          = empty( $type ) ? 'all' : $type;

	return empty( $content_types[ $type ] ) ? false : $content_types[ $type ];
}

/**
 * Get content type label from the post-type.
 *
 * @param string $post_type Post type.
 *
 * @return mixed|string
 */
function cps_get_content_type_label( $post_type ) {

	$label         = '';
	$content_types = cps_get_searchable_content_types();

	unset( $content_types['all'] );

	foreach ( $content_types as $type => $data ) {

		if ( $post_type === $data['post-types'][0] ) {

			$label = $data['label'];

			break;
		}
	}

	return $label;
}

/**
 * Maybe render a default hero if the post does not contain one in its content.
 *
 * @return string
 */
function cps_the_default_hero() {

	$post           = get_post();
	$has_hero_block = \CPS\Helpers\has_hero_block();

	if ( $has_hero_block ) {
		return;
	}

	$attributes['headline']        = get_the_title();
	$attributes['backgroundColor'] = 'black';
	$attributes['navigationColor'] = 'white';

	ob_start();

	include locate_template( 'partials/block-hero.php' );

	$hero = ob_get_clean();

	echo $hero; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Get news or events
 *
 * @return array|bool|mixed
 */
function cps_get_news_events() {
	$topic            = get_query_var( 'topic', '' );
	$month_query      = get_query_var( 'month', '' );
	$posts_per_page   = get_option( 'posts_per_page' );
	$news_events_args = array();

	if ( 'events' === $topic ) {
		if ( ! empty( $month_query ) && preg_match( '/^[0-9]{4}-[0-9]{2}\z/', $month_query ) ) {
			$month_array = explode( '-', $month_query );
			$start_month = gmdate( 'Y-m-01 0:0:0', strtotime( $month_query ) );
			$end_month   = gmdate( 'Y-m-t 23:59:59', strtotime( $month_query ) );

			$news_events_args = array(
				'post_type'              => 'cps-event',
				'posts_per_page'         => 100,
				'update_post_term_cache' => false,
				'meta_key'               => 'cps_event_start',
				'order'                  => 'ASC',
				'meta_query'             => array(
					'relation' => 'AND',
					array(
						'key'     => 'cps_event_start',
						'value'   => $start_month,
						'compare' => '>=',
						'type'    => 'DATE',
					),
					array(
						'key'     => 'cps_event_start',
						'value'   => $end_month,
						'compare' => '<=',
						'type'    => 'DATE',
					),
				),
			);
		} else {
			$news_events_args = array(
				'post_type'              => 'cps-event',
				'posts_per_page'         => 100,
				'update_post_term_cache' => false,
				'meta_key'               => 'cps_event_end',
				'order'                  => 'ASC',
				'meta_query'             => array(
					array(
						'key'     => 'cps_event_end',
						'value'   => gmdate( 'Y-m-d H:i:s' ),
						'compare' => '>=',
						'type'    => 'DATE',
					),
				),
			);
		}
	} else {
		$news_events_args = array(
			'post_type'              => 'post',
			'posts_per_page'         => intval( $posts_per_page ),
			'update_post_term_cache' => false,
			'update_post_meta_cache' => false,
			'order'                  => 'DESC',
		);
	}

	$news_events_query = new \WP_Query( $news_events_args );

	return $news_events_query;
}

/**
 * Get events months
 *
 * @param WP_Query $posts_query WP_Query object.
 *
 * @return array|bool Array of months or false.
 */
function cps_get_events_months( $posts_query ) {

	if ( ! $posts_query->have_posts() || ! $posts_query instanceof \WP_Query ) {
		return false;
	}

	$months = array();

	while ( $posts_query->have_posts() ) {

		$posts_query->the_post();

		$event_start = get_post_meta( get_the_ID(), 'cps_event_start', true );
		$event_end   = get_post_meta( get_the_ID(), 'cps_event_end', true );

		$event_start_time       = strtotime( $event_start );
		$event_start_date       = gmdate( 'Y-m', $event_start_time );
		$event_start_date_label = gmdate( 'Y F', $event_start_time );

		if ( empty( $months[ $event_start_date ] ) ) {
			$months[ $event_start_date ] = $event_start_date_label;
		}

		$event_end_time       = strtotime( $event_end );
		$event_end_date       = gmdate( 'Y-m', $event_end_time );
		$event_end_date_label = gmdate( 'Y F', $event_end_time );

		if ( empty( $months[ $event_end_date ] ) ) {
			$months[ $event_end_date ] = $event_end_date_label;
		}
	}

	wp_reset_postdata();

	if ( ! empty( $months ) ) {
		ksort( $months );
		return $months;
	}

	return false;
}
