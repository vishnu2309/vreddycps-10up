<?php
/**
 * Template Name: [hardcoded] Program
 *
 * @package CPS
 */

?>

<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<?php get_template_part( 'partials/program/hero' ); ?>

			<div class="single-program-content">

				<nav class="single-program-content__nav">
					<button class="js-button-toggle is-style-h3 single-program-content__dropdown-toggle" aria-controls="single-program-content__dropdown" data-mq="max-width: 79.938em">
						<span id="scrollspy-current-section">
							<?php esc_html_e( 'Toggle menu', 'cps' ); ?>
						</span>
					</button>
					<div class="single-program-content__dropdown" id="single-program-content__dropdown">
						<?php get_template_part( 'partials/floating-cta' ); ?>
						<ul class="single-program-content__menu js-scrollspy" data-scrollspy-area="single-program-content__blocks">

							<!-- NOTE: this is generated via FE JS -->

						</ul>
					</div>
				</nav>

				<div class="single-program-content__blocks" id="single-program-content__blocks">

					<?php
					get_template_part( 'partials/program/intro' );
					get_template_part( 'partials/program/overview' );
					get_template_part( 'partials/program/career-prospects' );
					get_template_part( 'partials/program/experiential-coop' );
					get_template_part( 'partials/program/requirements' );
					get_template_part( 'partials/program/faculty' );
					get_template_part( 'partials/program/quote' );

					/*
					 * The content allows an editor to add any block to the program detail
					 * page, it serves as the filling in the program sandwich.
					 */
					the_content();

					get_template_part( 'partials/program/admissions' );
					get_template_part( 'partials/program/cost-and-tuition' );
					get_template_part( 'partials/program/application-deadlines' );
					get_template_part( 'partials/program/concentrations' );
					get_template_part( 'partials/program/student-profile' );
					get_template_part( 'partials/program/research' );
					get_template_part( 'partials/program/study-abroad' );
					get_template_part( 'partials/program/career-opportunities-and-services' );
					get_template_part( 'partials/program/faq' );
					get_template_part( 'partials/program/related-degrees' );
					get_template_part( 'partials/footer-contact' );
					?>
				</div>

			</div>

		<?php endwhile; ?>
	<?php endif; ?>

<?php
get_footer();
