<?php
/**
 * The default single event post template file
 *
 * @package CPS
 */

$event_id             = get_the_ID();
$event_start_datetime = get_post_meta( $event_id, 'cps_event_start', true );
$event_start_date     = new DateTime( $event_start_datetime );
$event_end_datetime   = get_post_meta( $event_id, 'cps_event_end', true );
$event_start_end      = new DateTime( $event_end_datetime );
$event_timezone       = get_post_meta( $event_id, 'cps_event_timezone', true );
$event_campus         = get_post_meta( $event_id, 'cps_event_campus', true );
$event_location       = get_post_meta( $event_id, 'cps_event_location', true );
$event_public         = get_post_meta( $event_id, 'cps_event_public', true );
$event_link           = get_post_meta( $event_id, 'cps_event_link', true );
?>
<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<?php cps_the_default_hero(); ?>

			<p class="block-image-text__date"><?php cps_svg_icon( 'calendar', true ); ?><strong><?php \CPS\Event\Helpers\cps_the_event_date(); ?></strong></p>

			<?php if ( ! empty( $event_start_datetime ) && ! empty( $event_end_datetime ) ) : ?>
				<div class="download wp-block-button">
					<a href="<?php echo esc_url( trailingslashit( get_permalink() ) . 'ics' ); ?>" class="wp-block-button__link">
						<?php esc_html_e( 'Add to your calendar', 'cps' ); ?>
					</a>
				</div>
			<?php endif; ?>

			<div class="single-cps-event-date">
				<p><?php esc_html_e( 'Start Date:', 'cps' ); ?> <?php echo esc_html( $event_start_date->format( 'Y-m-d H:i' ) ); ?></p>
				<p><?php esc_html_e( 'End Date:', 'cps' ); ?> <?php echo esc_html( $event_start_end->format( 'Y-m-d H:i' ) ); ?></p>
				<p><?php esc_html_e( 'Timezone:', 'cps' ); ?> <?php echo esc_html( $event_timezone ); ?></p>
			</div>

			<?php if ( ! empty( $event_campus ) ) : ?>
				<p class="single-cps-event-campus">
					<?php esc_html_e( 'Campus:', 'cps' ); ?> <?php echo esc_html( $event_campus ); ?>
				</p>
			<?php endif; ?>

			<?php if ( ! empty( $event_location ) ) : ?>
			<p class="single-cps-event-location">
				<?php esc_html_e( 'Location:', 'cps' ); ?><br>
				<?php echo wpautop( wp_kses_post( $event_location ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
			</p>
			<?php endif; ?>

			<p class="single-cps-event-public">
				<?php esc_html_e( 'Public event:', 'cps' ); ?>
				<?php
				if ( $event_public ) {
					esc_html_e( 'Yes', 'cps' );
				} else {
					esc_html_e( 'No', 'cps' );
				}
				?>
			</p>

			<?php if ( ! empty( $event_link ) ) : ?>
				<p class="single-cps-event-link">
					<?php esc_html_e( 'Registration link:', 'cps' ); ?>
					<a href="<?php echo esc_url( $event_link ); ?>" target="_blank" rel="nofollow">
						<?php echo esc_html( $event_link ); ?>
					</a>
				</p>
			<?php endif; ?>

			<?php the_content(); // Note: the title is added via the hero block. ?>
		<?php endwhile; ?>
	<?php endif; ?>

	<div class="single-navigation">
		<?php previous_post_link( '%link', 'Previous <span>Event</span>' ); ?>
		<?php next_post_link( '%link', 'Next <span>Event</span>' ); ?>
	</div>

<?php
get_footer();
