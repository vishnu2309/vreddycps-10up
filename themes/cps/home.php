<?php
/**
 * The template file for blog listing page.
 *
 * @package CPS
 */

get_header();

if ( have_posts() ) :

	$index = 0;

	while ( have_posts() ) :

		the_post();

		$media_position = ( 0 === $index % 2 ) ? 'left' : 'right';

		$attributes = array(
			'mediaPosition' => $media_position,
			'headline'      => get_the_title(),
			'copy'          => get_the_excerpt(),
			'ctaLabelOne'   => __( 'Learn More', 'cps' ),
			'ctaLinkOne'    => get_the_permalink(),
		);

		if ( has_post_thumbnail() ) {
			$attributes['image']['id'] = get_post_thumbnail_id();
		}

		include locate_template( 'partials/block-image-text.php' );

		$index++;

	endwhile;

	the_posts_pagination();

endif;

get_footer();
