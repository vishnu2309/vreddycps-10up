<?php
/**
 * The template for displaying the header.
 *
 * @package CPS
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php get_template_part( 'partials/favicons' ); ?>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<div id="top"></div>

		<?php wp_body_open(); ?>

		<a class="is-focusable screen-reader-text" href="#content">
			<?php esc_html_e( 'Skip to Content', 'cps' ); ?>
		</a>

		<?php if ( function_exists( 'NUML_globalheader' ) ) : ?>
			<?php NUML_globalheader(); ?>
		<?php endif; ?>

		<header id="site-header" class="site-header" role="banner">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-header__logo" itemscope itemtype="https://schema.org/Organization">
					<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
						<?php cps_svg_icon( 'logo', true ); ?>
					</a>
					<meta itemprop="logo" src="<?php echo esc_url( get_template_directory_uri() . '/dist/svg/logo.svg' ); ?>">
				</h1>
			<?php else : ?>
				<div class="site-header__logo" itemscope itemtype="https://schema.org/Organization">
					<a itemprop="url" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
						<?php cps_svg_icon( 'logo', true ); ?>
					</a>
					<meta itemprop="logo" src="<?php echo esc_url( get_template_directory_uri() . '/dist/svg/logo.svg' ); ?>">
				</div>
			<?php endif; ?>
			<div class="site-header__navigation" id="site-header-navigation">
				<button type="button" id="site-header-menu-button-close" class="site-header__button site-header__button--menu site-header__button--menu-close">
					<?php cps_svg_icon( 'icon-clear', true ); ?>
					<span class="visually-hidden">
						<?php esc_attr_e( 'Close Menu', 'cps' ); ?>
					</span>
				</button>
				<?php
				wp_nav_menu(
					[
						'theme_location'  => 'navigation-main',
						'container'       => 'nav',
						'container_class' => 'main-nav',
						'container_id'    => 'header-menu',
						'menu_id'         => 'menu-main-navigation',
						'depth'           => 2,
						'walker'          => new Navigation_Main_Walker(),
					]
				);
				?>
			</div>
			<div class="site-header__quick-links">
				<a href="https://my.northeastern.edu/">
					<?php esc_html_e( 'MyNortheastern', 'cps' ); ?>
				</a>
			</div>
			<button type="button" class="site-header__button site-header__button--search">
				<span class="icon-search-open"><?php cps_svg_icon( 'icon-search', true ); ?></span>
				<span class="icon-search-close"><?php cps_svg_icon( 'icon-clear', true ); ?></span>
				<span class="visually-hidden">
					<?php esc_html_e( 'Search', 'cps' ); ?>
				</span>
			</button>
			<div class="site-header__search" id="site-header-search">
				<?php get_search_form(); ?>
			</div>
			<button type="button" id="site-header-menu-button-open" class="site-header__button site-header__button--menu site-header__button--menu-open">
				<?php cps_svg_icon( 'icon-menu', true ); ?>
				<span class="visually-hidden">
					<?php esc_attr_e( 'Open Menu', 'cps' ); ?>
				</span>
			</button>
		</header>

		<main class="global-container container" id="content" role="main">
