<?php
/**
 * The default single post template file
 *
 * @package CPS
 */

?>

<?php get_header(); ?>

	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php cps_the_default_hero(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	<?php endif; ?>

	<div class="single-navigation">
		<?php previous_post_link( '%link', 'Previous <span>Article</span>' ); ?>
		<?php next_post_link( '%link', 'Next <span>Article</span>' ); ?>
	</div>

<?php
get_footer();
